﻿namespace TEA.Contratos
{
    using TEA.Entidades.Negocio.Respuestas;

    public interface IServicioRespuesta
    {
        GuardarRespuestaResponse GuardarRespuesta(GuardarRespuestaRequest request);

        ObtenerRespuestaResponse ObtenerRespuestaPorId(ObtenerRespuestaRequest request);

        GuardarRespuestaResponse GuardarRespuestaCorrecta(GuardarRespuestaRequest request);
        
        GuardarRespuestaResponse GuardarFondo(GuardarRespuestaRequest request);

        GuardarRespuestaResponse GuardarFondoYRespuestaCorrecta(GuardarRespuestaRequest request);

        ObtenerRespuestasResponse ObtenerRespuestasPorEtapa(ObtenerRespuestasRequest request);
    }
}
