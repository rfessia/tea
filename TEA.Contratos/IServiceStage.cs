﻿namespace TEA.Contratos
{
    using TEA.Entidades.Negocio.Stage;

    public interface IServiceStage
    {

        /// <summary>
        /// Obtiene las etapas filtradas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetStagesResponse GetStagesFiltered(GetStagesRequest request);

        /// <summary>
        /// Obtiene las etapas compartidas para asiganar
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetStagesResponse GetSharedStagesForAssigned(GetSharedStagesForAssignedRequest request);

        /// <summary>
        /// Servicio que recupera el arbol de todas las etapas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetTreeStagesResponse GetTreeStagesJson(GetTreeStagesRequest request);

        /// <summary>
        /// Crea una etapa compartida
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        CreateStageResponse CreateSharedStage(CreateSharedStageRequest request);

        /// <summary>
        /// Crea una etapa para un usuario en particular
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        CreateStageResponse CreateStageForUser(CreateStageForUserRequest request);

        /// <summary>
        /// Actualiza una etapa compartida
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        UpdateStageResponse UpdateSharedStage(UpdateSharedStageRequest request);

        /// <summary>
        /// Actualiza una etapa de un usuario en particular
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        UpdateStageResponse UpdateStageForUser(UpdateStageForUserRequest request);

        /// <summary>
        /// Reasignar etapas de un usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        UpdateAssignStageForUserResponse UpdateAssignStageForUser(UpdateAssignStageForUserRequest request);

        /// <summary>
        /// Guarda una etapa como correcta
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SaveCorrectStageResponse SaveCorrectStage(SaveCorrectStageRequest request);

        /// <summary>
        /// Recupera una etapa por el id
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetStageResponse GetStageById(GetStageRequest request);

        /// <summary>
        /// Obtiene las Etapas de un Nivel para un Usuario determinado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetStagesByUserAndLevelResponse GetStagesByUserAndLevel(GetStagesByUserAndLevelRequest request);
        
        /// <summary>
        /// Recupera el último orden de todas las estapas de un Usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetStageResponse GetLastOrderOfStages(GetStageRequest request);

        /// <summary>
        /// Recupera un lote de etapas a partir de una determinada estapa
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetNextStagesWithAnswersResponse GetNextStagesWithAnswers(GetNextStagesWithAnswersRequest request);

        /// <summary>
        /// Elimina una etapa
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        DeleteStageResponse DeleteStage(DeleteStageRequest request);
    }
}
