﻿namespace TEA.Contratos
{
    using TEA.Entidades.Negocio.Respuestas;

    public interface IServiceAnswer
    {
        /// <summary>
        /// Guarda una respuesta
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SaveAnswerResponse SaveAnswer(SaveAnswerRequest request);

        /// <summary>
        /// Obtiene una respuesta por Id
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetAnswerResponse GetAnswerById(GetAnswerRequest request);

        /// <summary>
        /// Guarda una respuesta correcta
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SaveAnswerResponse SaveCorrectAnswer(SaveAnswerRequest request);
        
        /// <summary>
        /// Guarda el fondo de una respuesta
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SaveAnswerResponse SaveBackground(SaveAnswerRequest request);

        /// <summary>
        /// Guardar fondo y respuesta correcta de una respuesta
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        SaveAnswerResponse SaveBackgroundAndCorrectAnswer(SaveAnswerRequest request);

        /// <summary>
        /// Obtiene respuestas por etapa
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetAnswersResponse GetAnswersByStage(GetAnswersRequest request);

        /// <summary>
        /// Elimina una respuesta
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        DeleteAnswerResponse DeleteAnswer(DeleteAnswerRequest request);

        #region TempData

        /// <summary>
        /// Crea una nueva respuesta temporal
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        CreateTmpAnswerResponse CreateTmpAnswer(CreateTmpAnswerRequest request);

        /// <summary>
        /// Crea una respuesta temporal que se va a eliminar
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        CreateTmpAnswerToDeleteResponse CreateTmpAnswerToDelete(CreateTmpAnswerToDeleteRequest request);

        /// <summary>
        /// Obtiene respuestas almacenadas y temporales
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetAnswersAndTmpAnswersResponse GetAnswersAndTmpAnswersByStage(GetAnswersAndTmpAnswersRequest request);

        /// <summary>
        /// Recupera las nuevas respuestas temporales que fueron creadas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetTmpAnswersResponse GetNewsTmpAnswersByStage(GetTmpAnswersRequest request);

        /// <summary>
        /// Recupera las nuevas respuestas temporales que fueron eliminadas
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetTmpAnswersResponse GetNewsTmpAnswersDeletedByStage(GetTmpAnswersRequest request);

        /// <summary>
        /// Ejecuta el sp UpdateAnswers
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        UpdateAnswerWithTempAnswerResponse UpdateAnswerWithTempAnswer(UpdateAnswerWithTempAnswerRequest request);

        /// <summary>
        /// Elimina un archivo  temporal
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        DeleteTmpAnswerResponse DeleteTmpAnswer(DeleteTmpAnswerRequest request);

        /// <summary>
        /// Elimina registros de respuestas temporales
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        DeleteTmpAnswersResponse DeleteTmpAnswersByStage(DeleteTmpAnswersRequest request);

        #endregion
    }
}
