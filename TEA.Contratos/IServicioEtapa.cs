﻿namespace TEA.Contratos
{
    using TEA.Entidades.Negocio.Etapa;

    public interface IServicioEtapa
    {
        ObtenerEtapasResponse ObtenerEtapas(ObtenerEtapasRequest request);

        GuardarEtapaResponse GuardarEtapa(GuardarEtapaRequest request);

        ObtenerEtapaResponse ObtenerEtapaPorId(ObtenerEtapaRequest request);
        
        /// <summary>
        /// Obtiene las Etapas de un Nivel para un Usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ObtenerEtapasResponse ObtenerEtapasPorNivelYUsuario(ObtenerEtapasRequest request);
        
        /// <summary>
        /// Recupera el último orden de todas las estapas de un Usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ObtenerEtapaResponse ObtenerUltimoOrden(ObtenerEtapaRequest request);

        /// <summary>
        /// Obtiene la etapa actual para un usuario o la primera etapa que le corresponda.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ObtenerEtapaResponse ObtenerEtapaActual(ObtenerEtapaRequest request);

        EliminarEtapaResponse EliminarEtapa(EliminarEtapaRequest request);
    }
}
