﻿namespace TEA.Contratos
{
    using TEA.Entidades.Negocio.Level;

    public interface IServiceLevel
    {
        /// <summary>
        /// Búsqueda de Niveles por varios filtros, por defecto se ordena por nombre
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetLevelsResponse GetLevels(GetLevelsRequest request);

        /// <summary>
        /// Recupera todos los niveles de un usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetLevelsByUserResponse GetLevelsByUser(GetLevelsByUserRequest request);

        /// <summary>
        /// Obiene los Niveles de un determinado Diagnóstico
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetLevelsResponse GetLevelsByDiagnostic(GetLevelsRequest request);

        /// <summary>
        /// Crea un Nivel
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        CreateLevelResponse CreateLevel(CreateLevelRequest request);

        /// <summary>
        /// Actualiza un nivel
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        UpdateLevelResponse UpdateLevel(UpdateLevelRequest request);

        /// <summary>
        /// Obtiene un Nivel por su Id
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetLevelResponse GetLevelById(GetLevelRequest request);

        /// <summary>
        /// Elimina un Nivel
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        DeleteLevelResponse DeleteLevel(DeleteLevelRequest request);
    }
}
