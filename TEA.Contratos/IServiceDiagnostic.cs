﻿namespace TEA.Contratos
{
    using TEA.Entidades.Negocio.Diagnostic;
    public interface IServiceDiagnostic
    {
        /// <summary>
        /// Recupera los diagnosticos filtrados, ordenados y paginados
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetDiagnosticsResponse GetDiagnostics(GetDiagnosticsRequest request);
        
        /// <summary>
        /// Recupera un diagnostico por ID
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetDiagnosticResponse GetDiagnosticById(GetDiagnosticRequest request);

        /// <summary>
        /// Crea un diagnostico
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        CreateDiagnosticResponse CreateDiagnostic(CreateDiagnosticRequest request);

        /// <summary>
        /// Actualiza un diagnostico
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        UpdateDiagnosticResponse UpdateDiagnostic(UpdateDiagnosticRequest request);

        /// <summary>
        /// Elimina un diagnostico
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        DeleteDiagnosticResponse DeleteDiagnostic(DeleteDiagnosticRequest request);

        /// <summary>
        /// Recupera un diagnostico por usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>

        GetDiagnosticsResponse GetDiagnosticsByUser(GetDiagnosticsRequest request);
    }
}
