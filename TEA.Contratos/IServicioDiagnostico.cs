﻿namespace TEA.Contratos
{
    using TEA.Entidades.Negocio.Diagnostico;
    public interface IServicioDiagnostico
    {
        ObtenerDiagnosticosResponse ObtenerDiagnosticos(ObtenerDiagnosticosRequest request);
        
        ObtenerDiagnosticoResponse ObtenerDiagnosticoPorId(ObtenerDiagnosticoRequest request);

        GuardarDiagnosticoResponse GuardarDiagnostico(GuardarDiagnosticoRequest request);
        
        GuardarDiagnosticoResponse GuardarUsuarioDiagnostico(GuardarDiagnosticoRequest request);

        EliminarDiagnosticoResponse EliminarDiagnostico(EliminarDiagnosticoRequest request);

        ObtenerDiagnosticosResponse ObtenerDiagnosticosPorUsuario(ObtenerDiagnosticosRequest request);
    }
}
