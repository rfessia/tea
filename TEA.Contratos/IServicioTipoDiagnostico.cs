﻿namespace TEA.Contratos
{
    using TEA.Entidades.Negocio.TipoDiagnostico;

    public interface IServicioTipoDiagnostico
    {
        ObtenerTipoDiagnosticosResponse ObtenerTipoDiagnosticos(ObtenerTipoDiagnosticosRequest request);
    }
}
