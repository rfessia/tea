﻿namespace TEA.Contratos
{
    using TEA.Entidades.Negocio.Usuario;

    public interface IServicioUsuario
    {

        ObtenerUsuarioResponse ObtenerUsuarioPorUserName(ObtenerUsuarioRequest request);

        ObtenerUsuarioResponse ObtenerUsuarioPorId(ObtenerUsuarioRequest request);

        GuardarUsuarioResponse GuardarUsuario(GuardarUsuarioRequest request);

        ObtenerUsuariosResponse ObtenerUsuarios(ObtenerUsuariosRequest request);
        
        ObtenerUsuariosResponse ObtenerIdUsuarios(ObtenerUsuariosRequest request);

        ObtenerUsuariosResponse ObtenerIdUsuariosPorDiagnostico(ObtenerUsuariosRequest request);
    }
}
