﻿namespace TEA.Contratos
{
    using Entidades.Negocio.Gender;
    public interface IServiceGender
    {
        /// <summary>
        /// Recupera todos los géneros
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetGenderResponse GetAllGender(GetGenderRequest request);
    }
}
