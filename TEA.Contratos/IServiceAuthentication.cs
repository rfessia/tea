﻿namespace TEA.Contratos
{
    using TEA.Entidades.Negocio.User;

    public interface IServiceAuthentication
    {
        /// <summary>
        /// En la implementación autentifica la entrada del usuario al sistema.
        /// </summary>
        /// <param name="userName">Nombre de usuario.</param>
        /// <param name="password">Contraseña del usuario.</param>
        /// <returns>Verdadero si el usuario es autenticado.</returns>
        bool ValidarUsuario(string userName, string password);

        /// <summary>
        /// En la implementación inicia una sesión para el usuario.
        /// </summary>
        /// <param name="userName">Nombre de usuario.</param>
        /// <param name="recordarUsuario">Recordar sesión del usuario.</param>
        void Login(string userName, bool recordarUsuario);

        /// <summary>
        /// Genere un tickect encriptado que será utilizado por la cookie se sesión.
        /// </summary>
        /// <returns></returns>
        AutenticacionUsuarioResponse GetEncrypteTicket(AutenticacionUsuarioRequest request);

        /// <summary>
        /// En la implementación finaliza la sesión del usuario.
        /// </summary>
        void FinalizarSesión();
    }
}
