﻿namespace TEA.Contratos
{
    using TEA.Entidades.Negocio.User;

    public interface IServiceUser
    {
        /// <summary>
        /// Recupera un usuario por nombre de usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetUserResponse GetUserByUserName(GetUserRequest request);

        /// <summary>
        /// Recupera un usuario distinto por mismo nombre de usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetUserResponse GetUserByUserNameAndDifferentId(GetUserRequest request);

        /// <summary>
        /// Valida login de un usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetLoginUserResponse GetLoginUser(GetLoginUserRequest request);

        /// <summary>
        /// Recupera un usario por Id
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetUserByIdResponse GetUserById(GetUserByIdRequest request);

        /// <summary>
        /// Crea un usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        CreateUserResponse CreateUser(CreateUserRequest request);

        /// <summary>
        /// Registra un nuevo usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        RegisterUserResponse RegisterUser(RegisterUserRequest request);

        /// <summary>
        /// Activa un usuario registrado
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ActivationUserResponse ActivationUser(ActivationUserRequest request);

        /// <summary>
        /// Actualiza un usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        UpdateUserResponse UpdateDataUser(UpdateDataUserRequest request);

        /// <summary>
        /// Resetea la contraseña de un usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ResetPasswordResponse ResetPasswordUser(ResetPasswordUserRequest request);

        /// <summary>
        /// Recupera usuarios por distintos filtros
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetUsersResponse GetUsers(GetUsersRequest request);

        /// <summary>
        /// Recupera los usuarios de un diagnositco
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetUsersByDiagnosticResponse GetUsersByDiagnostic(GetUsersByDiagnosticRequest request);

        /// <summary>
        /// Elimina un usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        DeleteUserResponse DeleteUser(DeleteUserRequest request);
    }
}
