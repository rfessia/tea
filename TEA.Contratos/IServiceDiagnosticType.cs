﻿namespace TEA.Contratos
{
    using TEA.Entidades.Negocio.DiagnosticType;

    public interface IServiceDiagnosticType
    {
        GetDiagnosticTypesResponse GetDiagnosticTypes(GetDiagnosticTypesRequest request);
    }
}
