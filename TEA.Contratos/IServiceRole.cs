﻿namespace TEA.Contratos
{
    using TEA.Entidades.Negocio.Role;

    public interface IServiceRole
    {
        /// <summary>
        /// Recupera roles
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetRolesResponse GetRoles(GetRolesRequest request);
    }
}
