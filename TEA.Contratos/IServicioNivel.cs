﻿namespace TEA.Contratos
{
    using TEA.Entidades.Negocio.Nivel;

    public interface IServicioNivel
    {
        /// <summary>
        /// Búsqueda de Niveles por varios filtros
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ObtenerNivelesResponse ObtenerNiveles(ObtenerNivelesRequest request);

        /// <summary>
        /// Obiene los Niveles de un determinado Diagnóstico
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ObtenerNivelesResponse ObtenerNivelesPorDiagnostico(ObtenerNivelesRequest request);

        /// <summary>
        /// Obtiene todos los Niveles de un Diagnóstico para un Usuario determinado 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ObtenerNivelesResponse ObtenerNivelesPorDiagnosticoYUsuario(ObtenerNivelesRequest request);

        /// <summary>
        /// Actualiza los Niveles
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GuardarNivelResponse GuardarNivel(GuardarNivelRequest request);

        /// <summary>
        /// Obtiene un Nivel por su Id
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ObtenerNivelResponse ObtenerNivelPorId(ObtenerNivelRequest request);

        /// <summary>
        /// Elimina un Nivel
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        EliminarNivelResponse EliminarNivel(EliminarNivelRequest request);
    }
}
