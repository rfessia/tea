﻿namespace TEA.Contratos
{
    using TEA.Entidades.Negocio.Report;

    public interface IServiceReport
    {
        /// <summary>
        /// Obtiene el histórico de etapas/usuarios filtrados
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetUserStageHistoryResponse GetUserStageHistoryFiltered(GetUserStageHistoryRequest request);

        /// <summary>
        /// Obtiene un registro historico
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetUserStageHistoryByIdResponse GetUserStageHistoryById(GetUserStageHistoryByIdRequest request);
    }
}
