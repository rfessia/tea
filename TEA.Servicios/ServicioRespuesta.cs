﻿namespace TEA.Servicios
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using TEA.Contratos;
    using TEA.Datos;
    using TEA.Entidades.Negocio;
    using TEA.Entidades.Negocio.Respuestas;
    using AutoMapper;

    public class ServiceAnswer : IServiceAnswer
    {
        public SaveAnswerResponse SaveAnswer(SaveAnswerRequest request)
        {
            using (var db = new TEAEntities())
            {
                var response = new SaveAnswerResponse();
                ///Si es un create
                if (request.Answer.AnswerId == 0)
                {
                    var answer = new Answers()
                    {
                        AnswerId = request.Answer.AnswerId,
                        TextCorrect = request.Answer.TextCorrect,
                        ImageName = request.Answer.ImageName,
                        Correct = request.Answer.Correct,
                        Background = request.Answer.Background,
                        StageId = request.Answer.StageId,
                    };
                    db.Answers.Add(answer);
                    response.AnswerId = answer.AnswerId;
                }
                else///Si es una modificación
                {
                    var answer = db.Answers
                        .Where(x => x.CancellationDate == null
                            && x.AnswerId == request.Answer.AnswerId)
                        .FirstOrDefault();
                    answer.AnswerId = request.Answer.AnswerId;
                    answer.TextCorrect = request.Answer.TextCorrect;
                    answer.ImageName = request.Answer.ImageName;
                    answer.Correct = request.Answer.Correct;
                    answer.Background = request.Answer.Background;
                    answer.StageId = request.Answer.StageId;
                    db.Entry(answer).State = EntityState.Modified;
                    response.AnswerId = answer.AnswerId;
                };

                db.SaveChanges();
                return response;
            }
        }

        public GetAnswerResponse GetAnswerById(GetAnswerRequest request)
        {
            using (var db = new TEAEntities())
            {
                var response = new GetAnswerResponse();
                var respuesta = db.Answers
                    .Where(x => x.CancellationDate == null
                        && x.AnswerId == request.AnswerId)
                    .Select(x => new Answer()
                    {
                        AnswerId = x.AnswerId,
                        ImageName = x.ImageName,
                        Correct = x.Correct,
                        Background = x.Background,
                        TextCorrect = x.TextCorrect,
                        StageId = x.StageId

                    }).FirstOrDefault();
                response.Answer = respuesta;
                return response;
            }
        }

        public SaveAnswerResponse SaveCorrectAnswer(SaveAnswerRequest request)
        {
            using (var db = new TEAEntities())
            {
                var validarResponse = new ValidateResponse();
                var answer = db.Answers
                    .Where(x => x.CancellationDate == null
                        && x.AnswerId == request.AnswerCorrectId)
                    .FirstOrDefault();
                if (answer != null)
                {
                    answer.Correct = true;
                    db.Entry(answer).State = EntityState.Modified;
                    db.SaveChanges();
                    validarResponse.IsValid = true;
                }
                else
                {
                    var errores = new Dictionary<string, string>();
                    errores.Add("", "No se ha podido guardar la respuesta, por favor intente nuevamente.");
                    validarResponse.Errors = errores;
                    validarResponse.IsValid = false;
                }

                var response = new SaveAnswerResponse()
                {
                    ValidarResponse = validarResponse,
                };
                return response;
            }
        }

        public SaveAnswerResponse SaveBackground(SaveAnswerRequest request)
        {
            using (var db = new TEAEntities())
            {
                var validarResponse = new ValidateResponse();
                var answer = db.Answers
                    .Where(x => x.CancellationDate == null
                        && x.AnswerId == request.BackgroundId)
                    .FirstOrDefault();
                if (answer != null)
                {
                    answer.Background = true;
                    db.Entry(answer).State = EntityState.Modified;
                    db.SaveChanges();
                    validarResponse.IsValid = true;
                }
                else
                {
                    var errores = new Dictionary<string, string>();
                    errores.Add("", "No se ha podido guardar la respuesta, por favor intente nuevamente.");
                    validarResponse.Errors = errores;
                    validarResponse.IsValid = false;
                };

                var response = new SaveAnswerResponse()
                {
                    ValidarResponse = validarResponse,
                };
                return response;
            }
        }

        public SaveAnswerResponse SaveBackgroundAndCorrectAnswer(SaveAnswerRequest request)
        {
            using (var db = new TEAEntities())
            {
                var validarResponse = new ValidateResponse();
                var answersByStage = db.Answers.Where(x => x.CancellationDate == null && x.StageId == request.StageId).AsQueryable();
                foreach (var answers in answersByStage)
                {
                    if (answers.AnswerId == request.AnswerCorrectId)
                    {
                        answers.Correct = true;
                        answers.Background = false;
                    }
                    else if (answers.AnswerId == request.BackgroundId)
                    {
                        answers.Correct = false;
                        answers.Background = true;
                    }
                    else
                    {
                        answers.Correct = false;
                        answers.Background = false;
                    };

                    db.Entry(answers).State = EntityState.Modified;
                };

                db.SaveChanges();
                validarResponse.IsValid = true;
                var response = new SaveAnswerResponse()
                {
                    ValidarResponse = validarResponse,
                };
                return response;
            }
        }

        public GetAnswersResponse GetAnswersByStage(GetAnswersRequest request)
        {
            using (var db = new TEAEntities())
            {
                var response = new GetAnswersResponse();
                var respuestas = db.Answers
                    .Where(x => x.CancellationDate == null
                        && x.StageId == request.StageId)
                    .Select(x => new Answer()
                    {
                        AnswerId = x.AnswerId,
                        ImageName = x.ImageName,
                        Correct = x.Correct,
                        Background = x.Background,
                        TextCorrect = x.TextCorrect,
                        StageId = x.StageId
                    }).ToList();
                response.Answers = respuestas;
                return response;
            }
        }

        public DeleteAnswerResponse DeleteAnswer(DeleteAnswerRequest request)
        {
            using (var db = new TEAEntities())
            {
                var response = new DeleteAnswerResponse();
                var validateResponse = new ValidateResponse();
                var answer = db.Answers.Where(x => x.CancellationDate == null && x.AnswerId == request.AnswerId).FirstOrDefault();
                if (answer != null)
                {
                    answer.CancellationDate = DateTime.Now;
                    db.Entry(answer).State = EntityState.Modified;
                    db.SaveChanges();
                    validateResponse.IsValid = true;
                    response.StageId = answer.StageId;
                    response.ImageName = answer.ImageName;
                }
                else
                {
                    var errores = new Dictionary<string, string>();
                    errores.Add("", "No se ha podido eliminar la respuesta, por favor intente nuevamente.");
                    validateResponse.Errors = errores;
                    validateResponse.IsValid = false;
                };

                response.ValidateResponse = validateResponse;
                return response;
            };
        }

        #region TempData

        public CreateTmpAnswerResponse CreateTmpAnswer(CreateTmpAnswerRequest request)
        {
            var response = new CreateTmpAnswerResponse();
            using (var db = new TEAEntities())
            {
                var duplicateAnswer = db.Answers.Where(x => x.StageId == request.TmpAnswer.StageId && x.CancellationDate == null && x.ImageOriginalName.Trim().Equals(request.TmpAnswer.ImageOriginalName.Trim())).FirstOrDefault();
                var duplicateTmpAnswer = db.Tmp_Answers.Where(x => x.StageId == request.TmpAnswer.StageId && x.CancellationDate == null && x.ImageOriginalName.Trim().Equals(request.TmpAnswer.ImageOriginalName.Trim())).FirstOrDefault();
                if (duplicateAnswer != null || duplicateTmpAnswer != null)
                {
                    response.ErrorMessage = $"No se ha podido cargar el archivo, ya que existe un archivo con el mismo nombre: {request.TmpAnswer.ImageOriginalName}";
                    response.IsValid = false;
                }
                else
                {
                    var tmpAnswer = Mapper.Map<Tmp_Answers>(request.TmpAnswer);
                    db.Tmp_Answers.Add(tmpAnswer);
                    db.SaveChanges();
                    response.TmpAnswerId = tmpAnswer.TmpAnswerId;
                    response.IsValid = true;
                };
            };

            return response;
        }

        public CreateTmpAnswerToDeleteResponse CreateTmpAnswerToDelete(CreateTmpAnswerToDeleteRequest request)
        {
            var response = new CreateTmpAnswerToDeleteResponse();
            var validateResponse = new ValidateResponse();
            using (var db = new TEAEntities())
            {
                var tmpAnswerDb = db.Tmp_Answers.Where(x => x.AnswerId == request.AnswerId).FirstOrDefault();
                var answerDb = db.Answers.Where(x => x.AnswerId == request.AnswerId).FirstOrDefault();
                if (answerDb == null && tmpAnswerDb == null)
                {
                    var errores = new Dictionary<string, string>();
                    errores.Add("", "No se ha podido eliminar la respuesta, ya que no existe dicha respuesta.");
                    validateResponse.Errors = errores;
                    validateResponse.IsValid = false;
                }
                else
                {
                    if (tmpAnswerDb != null)
                    {
                        tmpAnswerDb.CancellationDate = DateTime.Now;
                        db.Entry(tmpAnswerDb).State = EntityState.Modified;
                        db.SaveChanges();
                        response.TmpAnswerId = tmpAnswerDb.TmpAnswerId;
                    }
                    else if (answerDb != null)
                    {
                        var tmpAnswer = Mapper.Map<Tmp_Answers>(answerDb);
                        tmpAnswer.CancellationDate = DateTime.Now;
                        db.Tmp_Answers.Add(tmpAnswer);
                        db.SaveChanges();
                        response.TmpAnswerId = tmpAnswer.TmpAnswerId;
                    };

                    validateResponse.IsValid = true;
                };
            };

            response.ValidateResponse = validateResponse;
            return response;
        }

        public GetAnswersAndTmpAnswersResponse GetAnswersAndTmpAnswersByStage(GetAnswersAndTmpAnswersRequest request)
        {
            using (var db = new TEAEntities())
            {
                var response = new GetAnswersAndTmpAnswersResponse();
                var tmpAnswers = new List<AnswerGeneric>();
                //Respuestas Activas
                var answerDomain = db.Answers.Where(x => x.CancellationDate == null && x.StageId == request.StageId).ToList();
                //Respuestas temporales Activas o Eliminadas pero que ya existían en respuestas
                var tmpAnswersDomain = db.Tmp_Answers.Where(x => x.StageId == request.StageId && 
                    (x.CancellationDate == null || (x.CancellationDate != null && x.AnswerId != null))).ToList();

                var tmpAnswersIdDeleted = tmpAnswersDomain.Where(x => x.AnswerId != null && x.CancellationDate != null).Select(x => x.AnswerId);
                //TODO: REVISAR
                //Descarto las repuestas que estan activas pero fueron dadas de bajas en temporales
                answerDomain = answerDomain.Where(x => !(tmpAnswersIdDeleted.Contains(x.AnswerId))).ToList();
                tmpAnswers.AddRange(Mapper.Map<List<AnswerGeneric>>(answerDomain));
                tmpAnswers.AddRange(Mapper.Map<List<AnswerGeneric>>(tmpAnswersDomain));
                response.AnswersGeneric = tmpAnswers;
                return response;
            }
        }

        public GetTmpAnswersResponse GetNewsTmpAnswersByStage(GetTmpAnswersRequest request)
        {
            using (var db = new TEAEntities())
            {
                var response = new GetTmpAnswersResponse();
                var tmpAnswersDomain = db.Tmp_Answers.Where(x => x.AnswerId == null && x.StageId == request.StageId).ToList();
                var tmpAnswers = Mapper.Map<List<TmpAnswer>>(tmpAnswersDomain);
                response.TmpAnswers = tmpAnswers;
                return response;
            }
        }

        public GetTmpAnswersResponse GetNewsTmpAnswersDeletedByStage(GetTmpAnswersRequest request)
        {
            using (var db = new TEAEntities())
            {
                var response = new GetTmpAnswersResponse();
                var tmpAnswersDomain = db.Tmp_Answers.Where(x => x.AnswerId == null && x.CancellationDate != null && x.StageId == request.StageId).ToList();
                var tmpAnswers = Mapper.Map<List<TmpAnswer>>(tmpAnswersDomain);
                response.TmpAnswers = tmpAnswers;
                return response;
            }
        }

        public UpdateAnswerWithTempAnswerResponse UpdateAnswerWithTempAnswer(UpdateAnswerWithTempAnswerRequest request)
        {
            using (var db = new TEAEntities())
            {
                var answersByStage = db.Answers.Where(x => x.CancellationDate == null && x.StageId == request.StageId).AsQueryable();
                foreach (var answers in answersByStage)
                {
                    if (request.AnswerCorrectIdAnswer.HasValue && (answers.AnswerId == request.AnswerCorrectIdAnswer))
                    {
                        answers.Correct = true;
                        answers.Background = false;
                    }
                    else if (request.BackgroundIdAnswer.HasValue && (answers.AnswerId == request.BackgroundIdAnswer))
                    {
                        answers.Correct = false;
                        answers.Background = true;
                    }
                    else
                    {
                        answers.Correct = false;
                        answers.Background = false;
                    };

                    db.Entry(answers).State = EntityState.Modified;
                };

                var tmpAnswersByStage = db.Tmp_Answers.Where(x => x.CancellationDate == null && x.StageId == request.StageId).AsQueryable();
                foreach (var tmpAnswers in tmpAnswersByStage)
                {
                    if (request.AnswerCorrectIdTmpAnswer.HasValue && (tmpAnswers.TmpAnswerId == request.AnswerCorrectIdTmpAnswer))
                    {
                        tmpAnswers.Correct = true;
                        tmpAnswers.Background = false;
                    }
                    else if (request.BackgroundIdTmpAnswer.HasValue && (tmpAnswers.TmpAnswerId == request.BackgroundIdTmpAnswer))
                    {
                        tmpAnswers.Correct = false;
                        tmpAnswers.Background = true;
                    }
                    else
                    {
                        tmpAnswers.Correct = false;
                        tmpAnswers.Background = false;
                    };

                    db.Entry(tmpAnswers).State = EntityState.Modified;
                };

                db.SaveChanges();
                db.UpdateAnswers(request.StageId);
                db.SaveChanges();
                return new UpdateAnswerWithTempAnswerResponse();
            };
        }

        public DeleteTmpAnswerResponse DeleteTmpAnswer(DeleteTmpAnswerRequest request)
        {
            using (var db = new TEAEntities())
            {
                var response = new DeleteTmpAnswerResponse();
                var validateResponse = new ValidateResponse();
                var tmpAnswer = db.Tmp_Answers
                    .Where(x => x.TmpAnswerId == request.TmpAnswerId)
                    .FirstOrDefault();
                if (tmpAnswer != null)
                {
                    tmpAnswer.CancellationDate = DateTime.Now;
                    db.Entry(tmpAnswer).State = EntityState.Modified;
                    db.SaveChanges();
                    validateResponse.IsValid = true;
                    response.ImageName = tmpAnswer.ImageName;
                }
                else
                {
                    var errores = new Dictionary<string, string>();
                    errores.Add("", "No se ha podido eliminar la respuesta, por favor intente nuevamente.");
                    validateResponse.Errors = errores;
                    validateResponse.IsValid = false;
                };

                response.ValidateResponse = validateResponse;
                return response;
            }
        }

        public DeleteTmpAnswersResponse DeleteTmpAnswersByStage(DeleteTmpAnswersRequest request)
        {
            using (var db = new TEAEntities())
            {
                var response = new DeleteTmpAnswersResponse();
                var tmpAnswers = db.Tmp_Answers.Where(x => x.StageId == request.StageId).ToList();
                foreach (var tmpAnswer in tmpAnswers)
                {
                    db.Entry(tmpAnswer).State = EntityState.Deleted;
                };

                db.SaveChanges();
                return response;
            }
        }

        #endregion
    }
}
