﻿namespace TEA.Servicios
{
    using AutoMapper;
    using Castle.Core.Internal;
    using Effortless.Net.Encryption;
    using Entidades.Negocio;
    using Entidades.Negocio.User;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using TEA.Contratos;
    using static Entidades.Base.Enumerations;
    using data = Datos;

    public class ServiceUser : IServiceUser
    {
        public GetUserResponse GetUserByUserName(GetUserRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new GetUserResponse();
                var user = db.Users
                    .Where(x => x.CancellationDate == null &&
                        x.UserName == request.UserName)
                    .FirstOrDefault();

                response.User = Mapper.Map<User>(user);
                return response;
            }
        }

        public GetUserResponse GetUserByUserNameAndDifferentId(GetUserRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new GetUserResponse();
                var user = db.Users
                    .Where(x => x.CancellationDate == null
                        && x.UserId != request.UserId
                        && x.UserName == request.UserName)
                    .FirstOrDefault();

                response.User = Mapper.Map<User>(user);
                return response;
            }
        }

        public GetLoginUserResponse GetLoginUser(GetLoginUserRequest request)
        {
            var response = new GetLoginUserResponse();
            var password = Hash.Create(HashType.SHA1, request.Password, string.Empty, false);

            using (var db = new data.TEAEntities())
            {
                var user = db.Users.SingleOrDefault(x => x.CancellationDate == null
                    && x.UserName == request.UserName
                    && x.Password == password);
                if (user != null)
                {
                    response.User = Mapper.Map<User>(user);
                };
            }

            return response;
        }

        public GetUserByIdResponse GetUserById(GetUserByIdRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new GetUserByIdResponse();
                var userQueryable = db.Users.Where(x => x.UserId == request.UserId && x.CancellationDate == null).AsQueryable();
                if (request.UserProfessionalId > 0)
                {
                    userQueryable = userQueryable.Where(x => x.TopUserId == request.UserProfessionalId.Value);
                };

                var userList = userQueryable.FirstOrDefault();
                response.User = Mapper.Map<User>(userList);
                return response;
            }
        }

        public CreateUserResponse CreateUser(CreateUserRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                request.Password = Hash.Create(HashType.SHA1, request.Password, string.Empty, false);
                var user = Mapper.Map<data.Users>(request);
                //Agregar roles nuevos
                user.Genders = db.Genders.Where(x => x.GenderId == request.GenderId).FirstOrDefault();
                user.Users2 = db.Users.Where(x => x.UserId == request.TopUserId).FirstOrDefault();
                var roleNew = db.Roles.Where(x => x.RoleId == (int)RoleEnum.USUARIO).FirstOrDefault();
                if (roleNew != null)
                {
                    user.Roles.Add(roleNew);
                };

                db.Users.Add(user);
                db.SaveChanges();
            };

            return null;
        }

        public RegisterUserResponse RegisterUser(RegisterUserRequest request)
        {
            var response = new RegisterUserResponse();
            var validarResponse = new ValidateResponse();
            using (var db = new data.TEAEntities())
            {
                try
                {
                    request.Password = Hash.Create(HashType.SHA1, request.Password, string.Empty, false);
                    var user = Mapper.Map<data.Users>(request);
                    //Agregar roles nuevos
                    user.Genders = db.Genders.Where(x => x.GenderId == request.GenderId).FirstOrDefault();
                    var roleNew = db.Roles.Where(x => x.RoleId == (int)RoleEnum.ADMINISTRADOR).FirstOrDefault();
                    if (roleNew != null)
                    {
                        user.Roles.Add(roleNew);
                    };

                    db.Users.Add(user);
                    db.SaveChanges();
                    var userAvtivation = new data.UserActivations()
                    {
                        UserId = user.UserId,
                        Token = request.Token,
                    };

                    db.UserActivations.Add(userAvtivation);
                    db.SaveChanges();
                    response.UserId = user.UserId;
                    validarResponse.IsValid = true;
                }
                catch (System.Exception)
                {

                    var errores = new Dictionary<string, string>();
                    errores.Add("MessageError", "No se ha podido registrar el usuario, por favor intente nuevamente.");
                    validarResponse.Errors = errores;
                    validarResponse.IsValid = false;
                };
            };

            response.ValidarResponse = validarResponse;
            return response;
        }

        public ActivationUserResponse ActivationUser(ActivationUserRequest request)
        {
            var response = new ActivationUserResponse();
            var validarResponse = new ValidateResponse();
            using (var db = new data.TEAEntities())
            {
                try
                {
                    var activation = db.UserActivations.Where(x => x.Token == request.Token && x.UserId == request.UserId).FirstOrDefault();
                    if (activation != null)
                    {
                        var user = db.Users.Where(x => x.UserId == request.UserId).FirstOrDefault();
                        if (user != null)
                        {
                            user.CancellationDate = null;
                            db.UserActivations.Remove(activation);
                            db.SaveChanges();
                            validarResponse.IsValid = true;
                        }
                        else
                        {
                            var errores = new Dictionary<string, string>();
                            errores.Add("", "No existe el usuario a activar.");
                            validarResponse.Errors = errores;
                            validarResponse.IsValid = false;
                        };
                    }
                    else
                    {
                        var errores = new Dictionary<string, string>();
                        errores.Add("", "No se ha podido activar el usuario solicitado.");
                        validarResponse.Errors = errores;
                        validarResponse.IsValid = false;
                    };
                }
                catch (Exception)
                {
                    var errores = new Dictionary<string, string>();
                    errores.Add("", "No se ha podido activar el usuario. Intente mas tarde...");
                    validarResponse.Errors = errores;
                    validarResponse.IsValid = false;
                };
            };

            response.ValidarResponse = validarResponse;
            return response;
        }

        public UpdateUserResponse UpdateDataUser(UpdateDataUserRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var user = db.Users.Where(x => x.CancellationDate == null && x.UserId == request.Id).FirstOrDefault();
                Mapper.Map(request, user);
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
            };

            return null;
        }

        public ResetPasswordResponse ResetPasswordUser(ResetPasswordUserRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var user = db.Users.Where(x => x.CancellationDate == null &&
                    x.TopUserId == request.UserProfessionalId &&
                    x.UserId == request.UserId).FirstOrDefault();

                user.Password = Hash.Create(HashType.SHA1, request.Password, string.Empty, false);
                user.ModificationDate = DateTime.Now;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
            };

            return null;
        }

        public GetUsersResponse GetUsers(GetUsersRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new GetUsersResponse();
                var users = db.Users
                    .Where(x => x.CancellationDate == null
                        && x.TopUserId == request.TopUserId)
                    .AsQueryable();

                if (!string.IsNullOrEmpty(request.FilterUserName))
                {
                    users = users.Where(x => x.UserName.Trim().ToUpper().Contains(request.FilterUserName.Trim().ToUpper()));
                };

                if (!string.IsNullOrEmpty(request.FilterFullName))
                {
                    users = users.Where(x => x.Name.Trim().ToUpper().Contains(request.FilterFullName.Trim().ToUpper()));
                };

                if (!string.IsNullOrEmpty(request.FilterDocumentNumber))
                {
                    users = users.Where(x => x.DocumentNumber.ToString().Trim().ToUpper().Contains(request.FilterDocumentNumber.Trim().ToUpper()));
                };

                response.RecordsTotal = users.Count();
                switch (request.Order)
                {
                    case "FullName_desc":
                        users = users.OrderByDescending(x => x.Name).Skip(request.Skip.Value);
                        break;
                    case "Username_desc":
                        users = users.OrderByDescending(x => x.UserId).Skip(request.Skip.Value);
                        break;
                    case "Username_asc":
                        users = users.OrderBy(x => x.UserName).Skip(request.Skip.Value);
                        break;
                    case "DocumentNumber_desc":
                        users = users.OrderByDescending(x => x.DocumentNumber).Skip(request.Skip.Value);
                        break;
                    case "DocumentNumber_asc":
                        users = users.OrderBy(x => x.DocumentNumber).Skip(request.Skip.Value);
                        break;
                    default:
                        users = users.OrderBy(x => x.Name).Skip(request.Skip.Value);
                        break;
                };

                if (request.PageSize.HasValue)
                {
                    users = users.Take(request.PageSize.Value);
                };

                response.Users = Mapper.Map<List<User>>(users);
                return response;
            };
        }

        public GetUsersByDiagnosticResponse GetUsersByDiagnostic(GetUsersByDiagnosticRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new GetUsersByDiagnosticResponse();
                var user = db.Diagnostics.Where(x => x.CancellationDate == null && x.DiagnosticId == request.DiagnosticId).Select(x => x.FK_Diagnostics_User).SingleOrDefault();
                response.User = Mapper.Map<User>(user);
                return response;
            }
        }

        public DeleteUserResponse DeleteUser(DeleteUserRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new DeleteUserResponse();
                var validarResponse = new ValidateResponse();
                var userStageToDelete = new List<data.UsersStages>();
                var user = db.Users
                    .Where(x => x.CancellationDate == null &&
                        x.TopUserId == request.UserProfessionalId &&
                        x.UserId == request.UserId)
                    .FirstOrDefault();
                if (user != null)
                {
                    user.Diagnostics.ForEach(x => x.CancellationDate = DateTime.Now);
                    user.Diagnostics.ForEach(x => x.Levels.ForEach(y => y.CancellationDate = DateTime.Now));
                    user.UsersStages.Where(x => x.Stages.Shared == false).ForEach(y => y.Stages.CancellationDate = DateTime.Now);
                    user.CancellationDate = DateTime.Now;

                    //Compartidas que se quieren eliminar
                    var stagesToDelete = user.UsersStages.Where(x => x.Stages.Shared).ToList();
                    userStageToDelete.AddRange(stagesToDelete);

                    db.Entry(user).State = EntityState.Modified;
                    db.UsersStages.RemoveRange(userStageToDelete);
                    db.SaveChanges();
                    validarResponse.IsValid = true;
                }
                else
                {
                    var errores = new Dictionary<string, string>();
                    errores.Add("", "No se ha podido eliminar el usuario, por favor intente nuevamente.");
                    validarResponse.Errors = errores;
                    validarResponse.IsValid = false;
                };

                response.ValidarResponse = validarResponse;
                return response;
            }
        }
    }
}
