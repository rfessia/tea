﻿namespace TEA.Servicios
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using TEA.Contratos;
    using TEA.Datos;
    using TEA.Entidades.Negocio;
    using TEA.Entidades.Negocio.Diagnostico;
    using TEA.Entidades.Negocio.Usuario;

    public class ServicioDiagnostico : IServicioDiagnostico
    {
        public ObtenerDiagnosticosResponse ObtenerDiagnosticos(ObtenerDiagnosticosRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                try
                {
                    ObtenerDiagnosticosResponse response = new ObtenerDiagnosticosResponse();

                    var diagnosticos = (from d in db.Diagnosticos
                                        where d.Eliminado == false
                                        select new Diagnostico() 
                                        {
                                            Id = d.Id,
                                            Nombre = d.Nombre,
                                            Descripcion = d.Descripcion,
                                            IdTipoDiagnostico = d.IdTipoDiagnostico,
                                            NombreTipoDiagnostico = d.TipoDiagnosticos.Nombre
                                        }).AsQueryable();

                    if (!String.IsNullOrEmpty(request.FilteredText))
                        diagnosticos = diagnosticos.Where(x => x.Nombre.ToUpper().Contains(request.FilteredText.ToUpper()));

                    if (request.FiltroIdTipoDiagnostico != null)
                    {
                        diagnosticos = diagnosticos.Where(x => x.IdTipoDiagnostico == request.FiltroIdTipoDiagnostico.Value);
                    }
                    if (request.FiltroIdDiagnostico != null)
                    {
                        diagnosticos = diagnosticos.Where(x => x.Id == request.FiltroIdDiagnostico.Value);
                    }

                    if (diagnosticos.Count() > 0)
                    {
                        switch (request.Orden)
                        {
                            case "nombre_desc":
                                diagnosticos = diagnosticos.OrderByDescending(x => x.Nombre);
                                break;
                            default:
                                diagnosticos = diagnosticos.OrderBy(x => x.Nombre);
                                break;
                        }
                    }
                    response.Diagnosticos = diagnosticos.ToList();
                    return response;
                }
                catch (System.Exception)
                {
                    throw;
                }
            }

        }

        public ObtenerDiagnosticoResponse ObtenerDiagnosticoPorId(ObtenerDiagnosticoRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                try
                {
                    ObtenerDiagnosticoResponse response = new ObtenerDiagnosticoResponse();

                    var diagnostico = (from d in db.Diagnosticos
                                       where d.Id == request.IdDiagnostico && !d.Eliminado
                                       select new Diagnostico() 
                                       {
                                           Id = d.Id,
                                           Nombre = d.Nombre,
                                           Descripcion = d.Descripcion,
                                           IdTipoDiagnostico = d.IdTipoDiagnostico,
                                           NombreTipoDiagnostico = d.TipoDiagnosticos.Nombre,
                                           Eliminado = d.Eliminado,
                                           IdUsuarioProfesional = d.IdUsuarioProfesional
                                           //Usuarios = d.UsuariosDiagnosticos
                                           //             .Where(x => x.IdDiagnostico == d.Id && !x.Usuarios.Eliminado)
                                           //             .Select(x => new Usuario() 
                                           //             {  
                                           //                 Id = x.Usuarios.Id,
                                           //                 Nombre = x.Usuarios.Nombre,
                                           //                 UserName = x.Usuarios.UserName,
                                           //                 FechaNacimiento = x.Usuarios.FechaNacimiento,
                                           //                 DNI = x.Usuarios.DNI,
                                           //             }).ToList(),
                                            
                                       }).FirstOrDefault();
                    
                    response.Diagnostico = diagnostico;
                    return response;
                }
                catch (System.Exception)
                {
                    throw;
                }
            }

        }

        public GuardarDiagnosticoResponse GuardarDiagnostico(GuardarDiagnosticoRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                try
                {
                    Diagnosticos diagnostico = new Diagnosticos()
                    {
                        Id = request.Diagnostico.Id,
                        Nombre = request.Diagnostico.Nombre,
                        Descripcion = request.Diagnostico.Descripcion,
                        Eliminado = request.Diagnostico.Eliminado,
                        IdTipoDiagnostico = request.Diagnostico.IdTipoDiagnostico,
                        IdUsuarioProfesional = request.Diagnostico.IdUsuarioProfesional.Value
                    };

                    if (request.Diagnostico.Id == 0)//Crear
                    {
                        diagnostico.Eliminado = false;
                        db.Diagnosticos.Add(diagnostico);
                    }
                    else
                    {
                        db.Entry(diagnostico).State = EntityState.Modified;
                    }

                    db.SaveChanges();

                    GuardarDiagnosticoResponse response = new GuardarDiagnosticoResponse();
                    response.IdDiagnostico = diagnostico.Id;
                    return response;
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return null;
        }


        public GuardarDiagnosticoResponse GuardarUsuarioDiagnostico(GuardarDiagnosticoRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                try
                {
                    var diagnostico = db.Diagnosticos.Where(x => x.Id == request.IdDiagnostico).FirstOrDefault();

                    var usuariosAEliminar = diagnostico.Usuarios.ToList();
                    //Eliminar usuarios viejos
                    foreach (var usuario in usuariosAEliminar)
                    {
                        diagnostico.Usuarios.Remove(usuario);
                    }

                    //Agregar usuarios nuevos
                    foreach (var id in request.IdUsuarios)
                    {
                        var nuevoUsuario = db.Usuarios.Where(x => x.Id == id).FirstOrDefault();
                        if (nuevoUsuario != null)
                        {
                            diagnostico.Usuarios.Add(nuevoUsuario);
                        }
                    }

                    db.SaveChanges();

                    ////Eliminar lo registros existentes
                    //foreach (var idUsuario in request.IdUsuarios)
                    //{
                        
                        

                    //    var registroAEliminar = db.UsuariosDiagnosticos.Where(x => x.IdDiagnostico == request.IdDiagnostico && x.IdUsuario == idUsuario).FirstOrDefault();
                    //    if (registroAEliminar != null)
                    //    {
                    //        db.UsuariosDiagnosticos.Remove(registroAEliminar);
                    //    }
                    //}

                    ////Guardar los registros nuevos
                    //foreach (var idUsuario in request.IdUsuarios)
                    //{
                    //    var usuario = db.Usuarios.Where(x => x.Id == idUsuario && !x.Eliminado).FirstOrDefault();
                    //    if (usuario != null)
                    //    {
                    //        UsuariosDiagnosticos usuariosDiagnostico = new UsuariosDiagnosticos()
                    //        {
                    //            IdUsuario = usuario.Id,
                    //            IdDiagnostico = request.IdDiagnostico,
                    //        };
                            
                    //        db.UsuariosDiagnosticos.Add(usuariosDiagnostico);
                    //    }
                    //}

                    //db.SaveChanges();
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return null;
        }


        public EliminarDiagnosticoResponse EliminarDiagnostico(EliminarDiagnosticoRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                EliminarDiagnosticoResponse response = new EliminarDiagnosticoResponse();
                ValidarResponse validarResponse = new ValidarResponse();

                var diagnostico = (from d in db.Diagnosticos
                                   where d.Id == request.IdDiagnostico
                                   select d).FirstOrDefault();

                if (diagnostico != null)
                {
                    diagnostico.Eliminado = true;
                    db.Entry(diagnostico).State = EntityState.Modified;
                    db.SaveChanges();
                    validarResponse.Valido = true;
                }
                else
                {
                    var errores = new Dictionary<string, string>();
                    errores.Add("", "No se ha podido eliminar el diagnóstico, por favor intente nuevamente.");
                    validarResponse.Errores = errores;
                    validarResponse.Valido = false;
                }
                response.ValidarResponse = validarResponse;
                return response;
            }
        }

        public ObtenerDiagnosticosResponse ObtenerDiagnosticosPorUsuario(ObtenerDiagnosticosRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                try
                {
                    ObtenerDiagnosticosResponse response = new ObtenerDiagnosticosResponse();

                    var diagnosticos = db.Diagnosticos.Where(x => x.Usuarios.Any(z => z.Id == request.IdUsuario
                                                               && !x.Eliminado))
                                                      .Select(x => new Diagnostico 
                                                      {
                                                          Id = x.Id,
                                                          Nombre = x.Nombre,
                                                          Descripcion = x.Descripcion,
                                                          Eliminado = x.Eliminado,
                                                          IdTipoDiagnostico = x.IdTipoDiagnostico,
                                                      })
                                                      .ToList();

                    response.Diagnosticos = diagnosticos;
                    return response;
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        //public GuardarDiagnosticoResponse ActualizarDiagnostico(GuardarDiagnosticoRequest request)
        //{
        //    using (TEAEntities db = new TEAEntities())
        //    {
        //        Diagnosticos diagnostico = new Diagnosticos()
        //        {
        //            Id = request.Diagnostico.Id,
        //            Nombre = request.Diagnostico.Nombre,
        //        };
        //        if (request.Diagnostico.Id == 0)//Crear
        //        {
        //            db.Diagnosticos.Add(diagnostico);

        //        }
        //        else
        //        {
        //            db.Entry(diagnostico).State = EntityState.Modified;
        //        }

        //        db.SaveChanges();
        //    }
        //    return null;
        //}
    }
}