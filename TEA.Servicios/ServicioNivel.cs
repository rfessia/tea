﻿namespace TEA.Servicios
{
    using TEA.Contratos;
    using TEA.Datos;
    using TEA.Entidades.Negocio.Nivel;
    using System.Linq;
    using System.Collections.Generic;
    using System;
    using System.Data.Entity;
    using TEA.Entidades.Negocio;

    public class ServicioNivel : IServicioNivel
    {
        public ObtenerNivelesResponse ObtenerNiveles(ObtenerNivelesRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                try
                {
                    ObtenerNivelesResponse response = new ObtenerNivelesResponse();

                    var niveles = (from n in db.Niveles
                                   where n.Eliminado == false
                                   select new Nivel()
                                   {
                                       Id = n.Id,
                                       Nombre = n.Nombre,
                                       Completo = n.Completo,
                                       Orden = n.Orden,
                                       IdDiagnostico = n.Diagnosticos.Id,
                                       NombreDiagnostico = n.Diagnosticos.Nombre,
                                       IdTipoDiagnostico = n.Diagnosticos.IdTipoDiagnostico,
                                   }).AsQueryable();


                    if (request.FiltroIdTipoDiagnostico != null)
                    {
                        niveles = niveles.Where(x => x.IdTipoDiagnostico == request.FiltroIdTipoDiagnostico.Value);
                    }
                    if (request.FiltroIdDiagnostico != null)
                    {
                        niveles = niveles.Where(x => x.IdDiagnostico == request.FiltroIdDiagnostico.Value);
                    }
                    if (request.FiltroIdNivel != null)
                    {
                        niveles = niveles.Where(x => x.Id == request.FiltroIdNivel.Value);
                    }

                    switch (request.Orden)
                    {
                        case "nombre_desc":
                            niveles = niveles.OrderByDescending(x => x.Nombre);
                            break;
                        default:
                            niveles = niveles.OrderBy(x => x.Nombre);
                            break;
                    }

                    response.Niveles = niveles.ToList();
                    return response;
                }
                catch (System.Exception)
                {

                    throw;
                }
            }
        }

        public ObtenerNivelesResponse ObtenerNivelesPorDiagnostico(ObtenerNivelesRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                ObtenerNivelesResponse response = new ObtenerNivelesResponse();

                var niveles = db.Niveles
                                .Where(x => x.IdDiagnostico == request.IdDiagnostico)
                                .Select(x => new Nivel()
                                {
                                    Id = x.Id,
                                    Nombre = x.Nombre,
                                    Completo = x.Completo,
                                    Orden = x.Orden,
                                    IdDiagnostico = x.IdDiagnostico.Value,
                                    NombreDiagnostico = x.Diagnosticos.Nombre,
                                }).ToList();

                response.Niveles = niveles;
                return response;
            }
        }

        public ObtenerNivelesResponse ObtenerNivelesPorDiagnosticoYUsuario(ObtenerNivelesRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                ObtenerNivelesResponse response = new ObtenerNivelesResponse();

                var niveles = db.Niveles.Where(x => x.Diagnosticos.Id == request.IdDiagnostico
                                             && x.Diagnosticos.Usuarios.Any(z => z.Id == request.IdUsuario))
                                    .Select(x => new Nivel()
                                    {
                                        Id = x.Id,
                                        Nombre = x.Nombre,
                                        Completo = x.Completo,
                                        Actual = x.Actual,
                                        Orden = x.Orden,
                                        IdDiagnostico = x.IdDiagnostico.Value,
                                        NombreDiagnostico = x.Diagnosticos.Nombre,
                                    }).ToList();

                response.Niveles = niveles;
                return response;
            }
        }

        public ObtenerNivelResponse ObtenerNivelPorId(ObtenerNivelRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                ObtenerNivelResponse response = new ObtenerNivelResponse();

                var nivel = db.Niveles
                              .Where(x => x.Id == request.IdNivel && x.Eliminado == false)
                              .Select(x => new Nivel()
                              {
                                  Id = x.Id,
                                  Nombre = x.Nombre,
                                  Completo = x.Completo,
                                  Orden = x.Orden,
                                  IdDiagnostico = x.IdDiagnostico.Value,
                                  NombreDiagnostico = x.Diagnosticos.Nombre,
                              }).FirstOrDefault();
                response.Nivel = nivel;
                return response;
            }
        }

        public GuardarNivelResponse GuardarNivel(GuardarNivelRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                Niveles nivel = new Niveles()
                {
                    Id = request.Nivel.Id,
                    Nombre = request.Nivel.Nombre,
                    IdDiagnostico = request.Nivel.IdDiagnostico,
                    Eliminado = request.Nivel.Eliminado,
                    Completo = request.Nivel.Completo,
                    Orden = request.Nivel.Orden,
                };
                if (nivel.Id == 0)//Crear
                {
                    nivel.Eliminado = false;
                    nivel.Completo = false;
                    db.Niveles.Add(nivel);                    
                }
                else
                {
                    db.Entry(nivel).State = EntityState.Modified;

                }
                db.SaveChanges();
            }
            return null;
        }

        public EliminarNivelResponse EliminarNivel(EliminarNivelRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                EliminarNivelResponse response = new EliminarNivelResponse();
                ValidarResponse validarResponse = new ValidarResponse();

                var nivel = (from d in db.Niveles
                                   where d.Id == request.IdNivel
                                   select d).FirstOrDefault();

                if (nivel != null)
                {
                    nivel.Eliminado = true;
                    db.Entry(nivel).State = EntityState.Modified;
                    db.SaveChanges();
                    validarResponse.Valido = true;
                }
                else
                {
                    var errores = new Dictionary<string, string>();
                    errores.Add("", "No se ha podido eliminar el nivel, por favor intente nuevamente.");
                    validarResponse.Errores = errores;
                    validarResponse.Valido = false;
                }
                response.ValidarResponse = validarResponse;
                return response;
            }
        }
    }
}
