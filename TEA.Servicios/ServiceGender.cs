﻿namespace TEA.Servicios
{
    using AutoMapper;
    using Entidades.Negocio.Gender;
    using System.Collections.Generic;
    using System.Linq;
    using TEA.Contratos;
    using data = Datos;

    public class ServiceGender : IServiceGender
    {

        public GetGenderResponse GetAllGender(GetGenderRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new GetGenderResponse();
                var genders = db.Genders.ToList();
                response.Genders = Mapper.Map<IList<Gender>>(genders);
                return response;
            }
        }
    }
}
