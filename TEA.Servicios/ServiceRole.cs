﻿namespace TEA.Servicios
{
    using AutoMapper;
    using System.Collections.Generic;
    using System.Linq;
    using TEA.Contratos;
    using data = Datos;
    using entity = Entidades.Negocio;

    public class ServiceRole : IServiceRole
    {

        public entity.Role.GetRolesResponse GetRoles(entity.Role.GetRolesRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var roles = db.Roles.ToList();
                var response = new entity.Role.GetRolesResponse();
                response.Roles = Mapper.Map<IList<entity.Role.Role>>(roles);
                return response;
            }
        }
    }
}
