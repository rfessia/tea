﻿namespace TEA.Servicios.Infrastucture.Mapping
{
    using AutoMapper;
    using System;
    using entity = Entidades.Negocio;
    using model = Datos;

    public class EntityToModel : Profile
    {
        public override string ProfileName
        {
            get
            {
                return "EntityToModel";
            }
        }

        protected override void Configure()
        {
            //User to Users
            base.CreateMap<entity.User.User, model.Users>();

            //CreateUserRequest to Users
            base.CreateMap<entity.User.CreateUserRequest, model.Users>()
                .ForMember(dest => dest.UserId, opt => opt.Ignore())
                .ForMember(dest => dest.InputDate, opt => opt.UseValue(DateTime.Now));

            //RegisterUserRequest to Users
            base.CreateMap<entity.User.RegisterUserRequest, model.Users>()
                .ForMember(dest => dest.TopUserId, opt => opt.Ignore())
                .ForMember(dest => dest.UserId, opt => opt.Ignore());

            //UpdateDataUserRequest to Roles
            base.CreateMap<entity.User.UpdateDataUserRequest, model.Users>()
                .ForMember(dest => dest.UserId, opt => opt.Ignore())
                .ForMember(dest => dest.ModificationDate, opt => opt.UseValue(DateTime.Now));

            //Role to Roles
            base.CreateMap<entity.Role.Role, model.Roles>();

            //CreateDiagnosticRequest to Diagnostics
            base.CreateMap<entity.Diagnostic.CreateDiagnosticRequest, model.Diagnostics>()
                .ForMember(dest => dest.CancellationDate, opt => opt.Ignore())
                .ForMember(dest => dest.DiagnosticTypes, opt => opt.Ignore())
                .ForMember(dest => dest.Levels, opt => opt.Ignore())
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.UserProfessionalId, opt => opt.MapFrom(src => src.UserProfessionalId));

            //UpdateDiagnosticRequest to Diagnostics
            base.CreateMap<entity.Diagnostic.UpdateDiagnosticRequest, model.Diagnostics>()
                .ForMember(dest => dest.DiagnosticId, opt => opt.Ignore())
                .ForMember(dest => dest.CancellationDate, opt => opt.Ignore())
                .ForMember(dest => dest.DiagnosticTypes, opt => opt.Ignore())
                .ForMember(dest => dest.Levels, opt => opt.Ignore())
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.UserProfessionalId, opt => opt.MapFrom(src => src.UserProfessionalId));

            //CreateLevelRequest to Levels
            base.CreateMap<entity.Level.CreateLevelRequest, model.Levels>();

            //UpdateLevelRequest to Levels
            base.CreateMap<entity.Level.UpdateLevelRequest, model.Levels>()
                .ForMember(dest => dest.LevelId, opt => opt.Ignore())
                .ForMember(dest => dest.FinishDate, opt => opt.Ignore())
                .ForMember(dest => dest.CancellationDate, opt => opt.Ignore())
                .ForMember(dest => dest.Diagnostics, opt => opt.Ignore())
                .ForMember(dest => dest.UsersStages, opt => opt.Ignore());

            //CreateSharedStageRequest to Stages
            base.CreateMap<entity.Stage.CreateSharedStageRequest, model.Stages>()
                .ForMember(dest => dest.Assigned, opt => opt.UseValue(false))
                .ForMember(dest => dest.InputDate, opt => opt.UseValue(DateTime.Now))
                .ForMember(dest => dest.Shared, opt => opt.UseValue(true))
                .ForMember(dest => dest.CancellationDate, opt => opt.Ignore())
                .ForMember(dest => dest.Answers, opt => opt.Ignore())
                .ForMember(dest => dest.UsersStages, opt => opt.Ignore());

            //CreateStageForUserRequest to Stages
            base.CreateMap<entity.Stage.CreateStageForUserRequest, model.Stages>()
                .ForMember(dest => dest.Assigned, opt => opt.UseValue(true))
                .ForMember(dest => dest.InputDate, opt => opt.UseValue(DateTime.Now))
                .ForMember(dest => dest.Shared, opt => opt.UseValue(false))
                .ForMember(dest => dest.CancellationDate, opt => opt.Ignore())
                .ForMember(dest => dest.Answers, opt => opt.Ignore())
                .ForMember(dest => dest.UsersStages, opt => opt.Ignore());

            //CreateStageForUserRequest to UsersStages
            base.CreateMap<entity.Stage.CreateStageForUserRequest, model.UsersStages>()
                .ForMember(dest => dest.StageId, opt => opt.Ignore())
                .ForMember(dest => dest.Levels, opt => opt.Ignore())
                .ForMember(dest => dest.Stages, opt => opt.Ignore())
                .ForMember(dest => dest.Users, opt => opt.Ignore());

            //UpdateStageRequest to Stages
            base.CreateMap<entity.Stage.UpdateStageRequest, model.Stages>()
                .ForMember(dest => dest.StageId, opt => opt.Ignore())
                .ForMember(dest => dest.CancellationDate, opt => opt.Ignore())
                .ForMember(dest => dest.Answers, opt => opt.Ignore())
                .ForMember(dest => dest.UsersStages, opt => opt.Ignore());

            //UpdateSharedStageRequest to Stages
            base.CreateMap<entity.Stage.UpdateSharedStageRequest, model.Stages>()
                .ForMember(dest => dest.CancellationDate, opt => opt.Ignore())
                .ForMember(dest => dest.Answers, opt => opt.Ignore())
                .ForMember(dest => dest.UsersStages, opt => opt.Ignore());

            //UpdateStageForUserRequest to Stages
            base.CreateMap<entity.Stage.UpdateStageForUserRequest, model.Stages>()
                .ForMember(dest => dest.CancellationDate, opt => opt.Ignore())
                .ForMember(dest => dest.Answers, opt => opt.Ignore())
                .ForMember(dest => dest.UsersStages, opt => opt.Ignore());

            //UpdateStageForUserRequest to UsersStages
            base.CreateMap<entity.Stage.UpdateStageForUserRequest, model.UsersStages>()
                .ForMember(dest => dest.StageId, opt => opt.Ignore())
                .ForMember(dest => dest.Levels, opt => opt.Ignore())
                .ForMember(dest => dest.Stages, opt => opt.Ignore())
                .ForMember(dest => dest.Users, opt => opt.Ignore());

            //SaveCorrectStageRequest to Stages
            base.CreateMap<entity.Stage.SaveCorrectStageRequest, model.UsersStagesHistory>()
                .ForMember(dest => dest.Id, opt => opt.Ignore());

            //Answer to Answers
            base.CreateMap<entity.Respuestas.Answer, model.Answers>()
                .ForMember(dest => dest.Stages, opt => opt.Ignore())
                .ForMember(dest => dest.CancellationDate, opt => opt.Ignore());

            //Answer to Answers
            base.CreateMap<entity.Respuestas.TmpAnswer, model.Tmp_Answers>()
                .ForMember(dest => dest.AnswerId, opt => opt.Ignore());
        }
    }
}
