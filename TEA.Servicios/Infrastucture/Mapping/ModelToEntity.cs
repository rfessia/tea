﻿namespace TEA.Servicios.Infrastucture.Mapping
{
    using AutoMapper;
    using entity = Entidades.Negocio;
    using model = Datos;
    using System.Linq;

    public class ModelToEntity : Profile
    {
        public override string ProfileName
        {
            get
            {
                return "ModelToEntity";
            }
        }

        protected override void Configure()
        {
            #region Model To Entity

            //Users to User
            base.CreateMap<model.Users, entity.User.User>()
                .ForMember(dest => dest.Gender, opt => opt.MapFrom(src => src.Genders));

            //Roles to Role
            base.CreateMap<model.Roles, entity.Role.Role>();

            //Genders to Gender
            base.CreateMap<model.Genders, entity.Gender.Gender>();

            //Diagnostics to Diagnostic
            base.CreateMap<model.Diagnostics, entity.Diagnostic.Diagnostic>()
                .ForMember(dest => dest.DiagnosticTypeName, opt => opt.MapFrom(src => src.DiagnosticTypes != null ? src.DiagnosticTypes.Name : null))
                .ForMember(dest => dest.User, opt => opt.MapFrom(src => src.FK_Diagnostics_User));
                //.ForMember(dest => dest.User, opt => opt.Ignore());

            //Levels to Level
            base.CreateMap<model.Levels, entity.Level.Level>()
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Diagnostics.FK_Diagnostics_User.Name != null ? src.Diagnostics.FK_Diagnostics_User.Name : null))
                .ForMember(dest => dest.DiagnosticName, opt => opt.MapFrom(src => src.Diagnostics != null ? src.Diagnostics.Name : null));

            //Stages to Stage
            base.CreateMap<model.Stages, entity.Stage.Stage>()
                .ForMember(dest => dest.DiagnosticTypeId, opt => opt.MapFrom(src => src.UsersStages.Count() == 1 ? src.UsersStages.Select(x => x.Levels.Diagnostics.DiagnosticTypeId).First() : (int?)null))
                .ForMember(dest => dest.DiagnosticId, opt => opt.MapFrom(src => src.UsersStages.Count() == 1 ? src.UsersStages.Select(x => x.Levels.DiagnosticId).First() : (int?)null))
                .ForMember(dest => dest.DiagnosticName, opt => opt.MapFrom(src => src.UsersStages.Count() == 1 ? src.UsersStages.Select(x => x.Levels.Diagnostics.Name).First() : null))
                .ForMember(dest => dest.LevelName, opt => opt.MapFrom(src => src.UsersStages.Count() == 1 ? src.UsersStages.Select(x => x.Levels.Name).First() : null))
                .ForMember(dest => dest.LevelOrder, opt => opt.MapFrom(src => src.UsersStages.Count() == 1 ? src.UsersStages.Select(x => x.Levels.Order).First() : (int?)null))
                .ForMember(dest => dest.LevelId, opt => opt.MapFrom(src => src.UsersStages.Count() == 1 ? src.UsersStages.Select(x => x.LevelId).First() : (int?)null))
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UsersStages.Count() == 1 ? src.UsersStages.Select(x => x.Users.UserId).First() : (int?)null))
                .ForMember(dest => dest.FullNameUser, opt => opt.MapFrom(src => src.UsersStages.Count() == 1 ? src.UsersStages.Select(x => x.Users.Name).First() : null))
                .ForMember(dest => dest.Order, opt => opt.MapFrom(src => src.UsersStages.Count() == 1 ? src.UsersStages.Select(x => x.Order).First() : (int?)null))
                .ForMember(dest => dest.Last, opt => opt.Ignore())
                .ForMember(dest => dest.LevelLast, opt => opt.Ignore())
                .ForMember(dest => dest.UrlImage, opt => opt.Ignore());

            //UsersStages to Stage
            base.CreateMap<model.UsersStages, entity.Stage.Stage>()
                .ForMember(dest => dest.DiagnosticTypeId, opt => opt.MapFrom(src => src.Levels.Diagnostics.DiagnosticTypeId))
                .ForMember(dest => dest.DiagnosticId, opt => opt.MapFrom(src => src.Levels.DiagnosticId))
                .ForMember(dest => dest.DiagnosticName, opt => opt.MapFrom(src => src.Levels.Diagnostics.Name))
                .ForMember(dest => dest.LevelName, opt => opt.MapFrom(src => src.Levels.Name))
                .ForMember(dest => dest.LevelOrder, opt => opt.MapFrom(src => src.Levels.Order))
                .ForMember(dest => dest.Last, opt => opt.Ignore())
                .ForMember(dest => dest.LevelLast, opt => opt.Ignore())
                .ForMember(dest => dest.UrlImage, opt => opt.Ignore());

            //Answers to Answer
            base.CreateMap<model.Answers, entity.Respuestas.Answer>();

            //Tmp_Answers to AnswerGeneric
            base.CreateMap<model.Tmp_Answers, entity.Respuestas.AnswerGeneric>()
                .ForMember(dest => dest.AnswerId, opt => opt.MapFrom(src => src.TmpAnswerId))
                .ForMember(dest => dest.IsTmpAnswer, opt => opt.UseValue(true));

            //Answers to AnswerGeneric
            base.CreateMap<model.Answers, entity.Respuestas.AnswerGeneric>()
                .ForMember(dest => dest.IsTmpAnswer, opt => opt.UseValue(false));

            //Tmp_Answers to Tmp_Answers
            base.CreateMap<model.Tmp_Answers, entity.Respuestas.TmpAnswer>();

            //UsersStagesHistory to Tmp_Answers
            base.CreateMap<model.UsersStagesHistory, entity.Report.UserStageHistory>()
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Users.Name))
                .ForMember(dest => dest.DiagnosticName, opt => opt.MapFrom(src => src.Levels.Diagnostics.Name))
                .ForMember(dest => dest.LevelName, opt => opt.MapFrom(src => src.Levels.Name))
                .ForMember(dest => dest.StageName, opt => opt.MapFrom(src => src.Stages.Name))
                .ForMember(dest => dest.StageShared, opt => opt.MapFrom(src => src.Stages.Shared))
                .ForMember(dest => dest.UserProfessionalName, opt => opt.Ignore());


            #endregion

            #region Model To Model

            //Answers to Tmp_Answers
            base.CreateMap<model.Answers, model.Tmp_Answers>()
                .ForMember(dest => dest.TmpAnswerId, opt => opt.Ignore());

            #endregion
        }
    }
}
