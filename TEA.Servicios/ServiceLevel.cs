﻿namespace TEA.Servicios
{
    using AutoMapper;
    using Castle.Core.Internal;
    using Entidades.Base;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using TEA.Contratos;
    using TEA.Entidades.Negocio;
    using TEA.Entidades.Negocio.Level;
    using data = Datos;

    public class ServiceLevel : IServiceLevel
    {
        public GetLevelsResponse GetLevels(GetLevelsRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new GetLevelsResponse();
                var levels = db.Levels.Where(x => x.CancellationDate == null).AsQueryable();
                if (request.UserProfessionalId > 0)
                {
                    levels = levels.Where(x => x.Diagnostics.UserProfessionalId == request.UserProfessionalId);
                };

                if (request.UserId > 0)
                {
                    levels = levels.Where(x => x.Diagnostics.UserId == request.UserId);
                };

                if (!string.IsNullOrEmpty(request.FilterLevelName))
                {
                    levels = levels.Where(x => x.Name.Trim().ToUpper().Contains(request.FilterLevelName.Trim().ToUpper()));
                };

                if (request.FilterEnabledLevel.HasValue)
                {
                    levels = levels.Where(x => x.Enabled == request.FilterEnabledLevel.Value);
                };

                if (!string.IsNullOrEmpty(request.FilterDiagnosticName))
                {
                    levels = levels.Where(x => x.Diagnostics.Name.Trim().ToUpper().Contains(request.FilterDiagnosticName.Trim().ToUpper()));
                };

                response.RecordsTotal = levels.Count();
                switch (request.Order)
                {

                    case "LevelName_desc":
                        levels = levels.OrderByDescending(x => x.Name).Skip(request.Skip.Value);
                        break;
                    case "DiagnosticName_asc":
                        levels = levels.OrderBy(x => x.Diagnostics.Name).Skip(request.Skip.Value);
                        break;
                    case "DiagnosticName_desc":
                        levels = levels.OrderByDescending(x => x.Diagnostics.Name).Skip(request.Skip.Value);
                        break;
                    case "Enabled_asc":
                        levels = levels.OrderBy(x => x.Enabled).Skip(request.Skip.Value);
                        break;
                    case "Enabled_desc":
                        levels = levels.OrderByDescending(x => x.Enabled).Skip(request.Skip.Value);
                        break;
                    case "Order_asc":
                        levels = levels.OrderBy(x => x.Order).Skip(request.Skip.Value);
                        break;
                    case "Order_desc":
                        levels = levels.OrderByDescending(x => x.Order).Skip(request.Skip.Value);
                        break;
                    default:
                        levels = levels.OrderBy(x => x.Name).Skip(request.Skip.Value);
                        break;
                };

                if (request.PageSize.HasValue)
                {
                    levels = levels.Take(request.PageSize.Value);
                };

                response.Levels = Mapper.Map<List<Level>>(levels);
                return response;
            }
        }

        public GetLevelsByUserResponse GetLevelsByUser(GetLevelsByUserRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new GetLevelsByUserResponse();
                var levels = db.Levels.Where(x => x.CancellationDate == null && x.Diagnostics.UserId == request.UserId).AsQueryable();
                if (request.FromApi)
                {
                    levels = levels.Where(x => x.Enabled == true);
                };

                var levelsList = levels
                    .Select(x => new GetLevelByUser()
                    {
                        Order = x.Order,
                        LevelId = x.LevelId,
                        State = x.FinishDate.HasValue ? (int)Enumerations.State.FINISHED : (int)Enumerations.State.NOTFINISHED,
                        CountStages = x.UsersStages.Count(),
                        LevelName = x.Name,
                    })
                    .OrderBy(x => x.Order)
                    .ToList();

                //var currentLevel = levelsList.Where(x => x.State == (int)Enumerations.State.NOTFINISHED).FirstOrDefault();
                //if (currentLevel != null)
                //{
                //    foreach (var item in levelsList)
                //    {
                //        if (item.LevelId == currentLevel.LevelId)
                //        {
                //            item.CurrentLevel = true;
                //            break;
                //        }
                //    }
                //};

                response.Levels = levelsList;
                return response;
            }
        }

        public GetLevelsResponse GetLevelsByDiagnostic(GetLevelsRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new GetLevelsResponse();
                var niveles = db.Levels.Where(x => x.CancellationDate == null &&
                        x.Diagnostics.UserProfessionalId == request.UserProfessionalId &&
                        x.DiagnosticId == request.DiagnosticId)
                    .Select(x => new Level()
                    {
                        LevelId = x.LevelId,
                        Name = x.Name,
                        FinishDate = x.FinishDate,
                        Order = x.Order,
                        DiagnosticId = x.DiagnosticId.Value,
                        DiagnosticName = x.Diagnostics.Name,
                        Enabled = x.Enabled
                    }).ToList();
                response.Levels = niveles;
                return response;
            }
        }

        public GetLevelResponse GetLevelById(GetLevelRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new GetLevelResponse();
                var nivel = db.Levels.Where(x => x.CancellationDate == null &&
                    x.Diagnostics.UserProfessionalId == request.UserProfessionalId &&
                    x.LevelId == request.LevelId)
                    .Select(x => new Level()
                    {
                        LevelId = x.LevelId,
                        Name = x.Name,
                        FinishDate = x.FinishDate,
                        Order = x.Order,
                        DiagnosticId = x.DiagnosticId.Value,
                        DiagnosticName = x.Diagnostics.Name,
                        UserName = x.Diagnostics.FK_Diagnostics_User.UserName,
                        Enabled = x.Enabled
                    }).FirstOrDefault();
                response.Level = nivel;
                return response;
            }
        }

        public CreateLevelResponse CreateLevel(CreateLevelRequest request)
        {
            var createLevelResponse = new CreateLevelResponse();
            var validateResponse = new ValidateResponse();
            using (var db = new data.TEAEntities())
            {
                ///Recupero una con mismo número de orden
                var orderDuplicate = db.Levels.Where(x => x.CancellationDate == null &&
                    x.DiagnosticId == request.DiagnosticId &&
                    x.Order == request.Order).FirstOrDefault();

                if (orderDuplicate != null)
                {
                    var errores = new Dictionary<string, string>();
                    errores.Add("Order", $"Ya existe un mismo número de orden para la actividad llamada: '{orderDuplicate.Name}'.");
                    validateResponse.Errors = errores;
                    validateResponse.IsValid = false;
                    createLevelResponse.ValidateResponse = validateResponse;
                }
                else
                {
                    var level = Mapper.Map<data.Levels>(request);
                    db.Levels.Add(level);
                    db.SaveChanges();
                    validateResponse.IsValid = true;
                    createLevelResponse.ValidateResponse = validateResponse;
                };
            };

            return createLevelResponse;
        }

        public UpdateLevelResponse UpdateLevel(UpdateLevelRequest request)
        {
            var updateLevelResponse = new UpdateLevelResponse();
            var validateResponse = new ValidateResponse();
            using (var db = new data.TEAEntities())
            {
                ///Recupero una con mismo número de orden
                var orderDuplicate = db.Levels.Where(x => x.CancellationDate == null &&
                    x.DiagnosticId == request.DiagnosticId &&
                    x.LevelId != request.LevelId &&
                    x.Order == request.Order).FirstOrDefault();

                if (orderDuplicate != null)
                {
                    var errores = new Dictionary<string, string>();
                    errores.Add("Order", $"Ya existe un mismo número de orden para la actividad llamada: '{orderDuplicate.Name}'.");
                    validateResponse.Errors = errores;
                    validateResponse.IsValid = false;
                    updateLevelResponse.ValidateResponse = validateResponse;
                }
                else
                {
                    var level = db.Levels.Where(x => x.CancellationDate == null && x.LevelId == request.LevelId).FirstOrDefault();
                    Mapper.Map(request, level);
                    db.Entry(level).State = EntityState.Modified;
                    db.SaveChanges();
                    validateResponse.IsValid = true;
                    updateLevelResponse.ValidateResponse = validateResponse;
                };
            };

            return updateLevelResponse;
        }

        public DeleteLevelResponse DeleteLevel(DeleteLevelRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new DeleteLevelResponse();
                var validarResponse = new ValidateResponse();
                var userStageToDelete = new List<data.UsersStages>();
                var level = db.Levels.Where(x => x.CancellationDate == null &&
                    x.Diagnostics.UserProfessionalId == request.UserProfessionalId &&
                    x.LevelId == request.LevelId)
                    .FirstOrDefault();
                if (level != null)
                {
                    level.CancellationDate = DateTime.Now;
                    level.UsersStages.Where(x => x.Stages.Shared == false).ForEach(y => y.Stages.CancellationDate = DateTime.Now);
                    //Compartidas que se quieren eliminar
                    var stagesToDelete = level.UsersStages.Where(x => x.Stages.Shared).ToList();
                    userStageToDelete.AddRange(stagesToDelete);

                    db.Entry(level).State = EntityState.Modified;
                    db.UsersStages.RemoveRange(userStageToDelete);
                    db.SaveChanges();
                    validarResponse.IsValid = true;
                }
                else
                {
                    var errores = new Dictionary<string, string>();
                    errores.Add("", "No se ha podido eliminar la Actividad, por favor intente nuevamente.");
                    validarResponse.Errors = errores;
                    validarResponse.IsValid = false;
                };

                response.ValidarResponse = validarResponse;
                return response;
            }
        }
    }
}
