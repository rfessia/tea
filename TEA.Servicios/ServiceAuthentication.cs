﻿namespace TEA.Servicios
{
    using System;
    using System.Linq;
    using System.Web.Security;
    using TEA.Contratos;
    using TEA.Datos;
    using TEA.Entidades.Negocio.User;

    public class ServiceAuthentication : IServiceAuthentication
    {

        /// <summary>
        /// Mensaje para las exepciones de campo vacío o nulo.
        /// </summary>
        private const string mensajeAtributoVacioNulo = "El valor no puede estar vacío o nulo.";

        /// <summary>
        /// Autentifica la entrada del usuario al sistema.
        /// </summary>
        /// <param name="userName">Nombre de usuario.</param>
        /// <param name="password">Contraseña del usuario.</param>
        /// <returns>Verdadero si el usuario es autenticado.</returns>
        public bool ValidarUsuario(string user, string pass)
        {
            using (var db = new TEAEntities())
            {
                return db.Users.Where(x => x.UserName == user && x.Password == pass).Any();
            };
        }

        /// <summary>
        /// Inicia una sesión para el usuario.
        /// </summary>
        /// <param name="userName">Nombre de usuario.</param>
        /// <param name="recodarUsuario">Recordar sesión del usuario.</param>
        public void Login(string userName, bool recordarUsuario)
        {
            if (string.IsNullOrEmpty(userName))
            {
                throw new ArgumentException(mensajeAtributoVacioNulo, "userName");
            };

            FormsAuthentication.SetAuthCookie(userName, recordarUsuario);
        }
 
        public AutenticacionUsuarioResponse GetEncrypteTicket(AutenticacionUsuarioRequest request)
        {
            var response = new AutenticacionUsuarioResponse();
            var roles = string.Join(",", request.Usuario.Roles.Select(x => x.Name).ToList());
            // Creo el ticket.
            var ticket = new FormsAuthenticationTicket(
                1,
                request.Usuario.UserName,
                DateTime.Now,
                DateTime.Now.AddMonths(1),
                request.RememberMe,
                string.Format("{0}|{1}|{2}", request.Usuario.UserId, request.Usuario.UserName, roles));

            // Hago la encriptación del ticket.
            response.Encriptado = FormsAuthentication.Encrypt(ticket);
            return response;
        }

        /// <summary>
        /// Finaliza la sesión del usuario.
        /// </summary>
        public void FinalizarSesión()
        {
            FormsAuthentication.SignOut();
        }
    }
}
