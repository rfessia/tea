﻿namespace TEA.Servicios
{
    using AutoMapper;
    using Entidades.Negocio.Report;
    using System.Collections.Generic;
    using System.Linq;
    using TEA.Contratos;
    using data = TEA.Datos;

    public class ServiceReport : IServiceReport
    {
        public GetUserStageHistoryResponse GetUserStageHistoryFiltered(GetUserStageHistoryRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new GetUserStageHistoryResponse();
                var usersId = new List<int>();
                var usersStagesHistories = db.UsersStagesHistory.Where(x => x.Stages.CancellationDate == null &&
                    x.Users.CancellationDate == null &&
                    x.Users.TopUserId == request.UserProfessionalId &&
                    x.Stages.UsersStages.Any(y => y.StageId == x.StageId)
                    ).AsQueryable();

                if (request.FilterUserId > 0)
                {
                    usersStagesHistories = usersStagesHistories.Where(x => x.UserId == request.FilterUserId);
                };

                if (!string.IsNullOrEmpty(request.FilterDiagnosticName))
                {
                    usersStagesHistories = usersStagesHistories.Where(x => x.Levels.Diagnostics.Name.Trim().ToUpper().Contains(request.FilterDiagnosticName.Trim().ToUpper()));
                };

                if (!string.IsNullOrEmpty(request.FilterLevelName))
                {
                    usersStagesHistories = usersStagesHistories.Where(x => x.Levels.Name.Trim().ToUpper().Contains(request.FilterLevelName.Trim().ToUpper()));
                };

                if (!string.IsNullOrEmpty(request.FilterStageName))
                {
                    usersStagesHistories = usersStagesHistories.Where(x => x.Stages.Name.Trim().ToUpper().Contains(request.FilterStageName.Trim().ToUpper()));
                };

                if (usersId.Count() > 0)
                {
                    usersStagesHistories = usersStagesHistories.Where(x => usersId.Distinct().Contains(x.UserId));
                };

                if (request.FilterDateStart.HasValue)
                {
                    usersStagesHistories = usersStagesHistories.Where(x =>
                        (x.FinishDate.Year > request.FilterDateStart.Value.Year) ||
                        (x.FinishDate.Year == request.FilterDateStart.Value.Year && x.FinishDate.Month > request.FilterDateStart.Value.Month) ||
                        (x.FinishDate.Year == request.FilterDateStart.Value.Year && x.FinishDate.Month == request.FilterDateStart.Value.Month && x.FinishDate.Day >= request.FilterDateStart.Value.Day)
                        );
                };

                if (request.FilterDateEnd.HasValue)
                {
                    usersStagesHistories = usersStagesHistories.Where(x =>
                        (x.FinishDate.Year < request.FilterDateEnd.Value.Year) ||
                        (x.FinishDate.Year == request.FilterDateEnd.Value.Year && x.FinishDate.Month < request.FilterDateEnd.Value.Month) ||
                        (x.FinishDate.Year == request.FilterDateEnd.Value.Year && x.FinishDate.Month == request.FilterDateEnd.Value.Month && x.FinishDate.Day <= request.FilterDateEnd.Value.Day)
                        );
                };

                if (request.FilterShared.HasValue)
                {
                    usersStagesHistories = usersStagesHistories.Where(x => x.Stages.Shared == request.FilterShared.Value);
                };

                response.RecordsTotal = usersStagesHistories.Count();
                switch (request.Order)
                {
                    case "UserName_desc":
                        usersStagesHistories = usersStagesHistories.OrderByDescending(x => x.Users.Name).Skip(request.Skip.Value);
                        break;
                    case "StageName_asc":
                        usersStagesHistories = usersStagesHistories.OrderBy(x => x.Stages.Name).Skip(request.Skip.Value);
                        break;
                    case "StageName_desc":
                        usersStagesHistories = usersStagesHistories.OrderByDescending(x => x.Stages.Name).Skip(request.Skip.Value);
                        break;
                    case "LevelName_asc":
                        usersStagesHistories = usersStagesHistories.OrderBy(x => x.Levels.Name).Skip(request.Skip.Value);
                        break;
                    case "LevelName_desc":
                        usersStagesHistories = usersStagesHistories.OrderByDescending(x => x.Levels.Name).Skip(request.Skip.Value);
                        break;
                    case "DiagnosticName_asc":
                        usersStagesHistories = usersStagesHistories.OrderBy(x => x.Levels.Diagnostics.Name).Skip(request.Skip.Value);
                        break;
                    case "DiagnosticName_desc":
                        usersStagesHistories = usersStagesHistories.OrderByDescending(x => x.Levels.Diagnostics.Name).Skip(request.Skip.Value);
                        break;
                    case "StageShared_asc":
                        usersStagesHistories = usersStagesHistories.OrderBy(x => x.Stages.Shared).Skip(request.Skip.Value);
                        break;
                    case "StageShared_desc":
                        usersStagesHistories = usersStagesHistories.OrderByDescending(x => x.Stages.Shared).Skip(request.Skip.Value);
                        break;
                    case "NumberOfAttempts_asc":
                        usersStagesHistories = usersStagesHistories.OrderBy(x => x.NumberOfAttempts).Skip(request.Skip.Value);
                        break;
                    case "NumberOfAttempts_desc":
                        usersStagesHistories = usersStagesHistories.OrderByDescending(x => x.NumberOfAttempts).Skip(request.Skip.Value);
                        break;
                    case "FinishDate_asc":
                        usersStagesHistories = usersStagesHistories.OrderBy(x => x.FinishDate).Skip(request.Skip.Value);
                        break;
                    case "FinishDate_desc":
                        usersStagesHistories = usersStagesHistories.OrderByDescending(x => x.FinishDate).Skip(request.Skip.Value);
                        break;
                    default:
                        usersStagesHistories = usersStagesHistories.OrderBy(x => x.Users.Name).Skip(request.Skip.Value);
                        break;
                };

                if (request.PageSize.HasValue)
                {
                    usersStagesHistories = usersStagesHistories.Take(request.PageSize.Value);
                };

                var usersStagesHistoriesList = usersStagesHistories.ToList();
                response.UsersStagesHistories = Mapper.Map<List<UserStageHistory>>(usersStagesHistoriesList);
                return response;
            }
        }

        public GetUserStageHistoryByIdResponse GetUserStageHistoryById(GetUserStageHistoryByIdRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new GetUserStageHistoryByIdResponse();
                var usersId = new List<int>();
                var usersStagesHistory = db.UsersStagesHistory.Where(x => x.Stages.CancellationDate == null &&
                    x.Users.CancellationDate == null &&
                    x.Id == request.UserStageHistoryId
                    ).FirstOrDefault();

                response.UsersStagesHistory = Mapper.Map<UserStageHistory>(usersStagesHistory);
                return response;
            }
        }
    }
}
