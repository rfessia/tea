﻿namespace TEA.Servicios
{
    using TEA.Contratos;
    using TEA.Datos;
    using TEA.Entidades.Negocio.Usuario;
    using System.Linq;
    using System.Data.Entity;
    using System;
    using System.Collections.Generic;

    public class ServicioUsuario : IServicioUsuario
    {

        public ObtenerUsuarioResponse ObtenerUsuarioPorUserName(ObtenerUsuarioRequest request)
        {

            using (TEAEntities db = new TEAEntities())
            {
                ObtenerUsuarioResponse response = new ObtenerUsuarioResponse();

                var usuario = db.Usuarios
                                .Where(x => x.UserName == request.UserName && !x.Eliminado)
                                .Select(x => new Usuario()
                                {
                                    Id = x.Id,
                                    UserName = x.UserName,
                                    Nombre = x.Nombre,
                                    DNI = x.DNI,
                                    FechaNacimiento = x.FechaNacimiento,
                                    EsAdmin = x.EsAdmin,
                                    EsUsuario = x.EsUsuario,
                                }).FirstOrDefault();
                
                response.Usuario = usuario;
                return response;
            }
        }

        public ObtenerUsuarioResponse ObtenerUsuarioPorId(ObtenerUsuarioRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                ObtenerUsuarioResponse response = new ObtenerUsuarioResponse();

                var usuario = db.Usuarios
                                .Where(x => x.Id == request.IdUsuario && !x.Eliminado)
                                .Select(x => new Usuario()
                                {
                                    Id = x.Id,
                                    UserName = x.UserName,
                                    Nombre = x.Nombre,
                                    DNI = x.DNI,
                                    FechaNacimiento = x.FechaNacimiento,
                                    EsAdmin = x.EsAdmin,
                                    EsUsuario = x.EsUsuario,
                                }).FirstOrDefault();

                response.Usuario = usuario;
                return response;
            }
        }

        public GuardarUsuarioResponse GuardarUsuario(GuardarUsuarioRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                Usuarios usuario = new Usuarios()
                {
                    Id = request.Usuario.Id,
                    UserName = request.Usuario.UserName,
                    Nombre = request.Usuario.Nombre,
                    DNI = request.Usuario.DNI.Value,
                    FechaNacimiento = request.Usuario.FechaNacimiento,
                    EsAdmin = request.Usuario.EsAdmin,
                    EsUsuario = request.Usuario.EsUsuario,
                    Eliminado = request.Usuario.Eliminado,
                    Password = request.Usuario.Password,
                    IdUsuarioPadre = request.Usuario.IdUsuarioPadre
                };
                if (request.Usuario.Id == 0)//Crear
                {
                    usuario.Eliminado = false;
                    db.Usuarios.Add(usuario);

                }
                else
                {
                    db.Entry(usuario).State = EntityState.Modified;
                }

                db.SaveChanges();

            }
            return null;
        }


        public ObtenerUsuariosResponse ObtenerUsuarios(ObtenerUsuariosRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                try
                {
                    ObtenerUsuariosResponse response = new ObtenerUsuariosResponse();

                    var usuarios = (from x in db.Usuarios
                                   where !x.Eliminado
                                   select new Usuario()
                                   {
                                       Id = x.Id,
                                       UserName = x.UserName,
                                       Nombre = x.Nombre,
                                       DNI = x.DNI,
                                       FechaNacimiento = x.FechaNacimiento,
                                       EsAdmin = x.EsAdmin,
                                       EsUsuario = x.EsUsuario,
                                       IdUsuarioPadre = x.IdUsuarioPadre,
                                   }).ToList();

                    if (request.IdUsuarioPadre != null)
                    {
                        usuarios = usuarios.Where(x => x.IdUsuarioPadre == request.IdUsuarioPadre.Value).ToList();
                    }

                    switch (request.Orden)
                    {
                        case "nombre_desc":
                            usuarios = usuarios.OrderByDescending(x => x.Nombre).ToList();
                            break;
                        default:
                            usuarios = usuarios.OrderBy(x => x.Nombre).ToList();
                            break;
                    }

                    response.Usuarios = usuarios.ToList();
                    return response;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public ObtenerUsuariosResponse ObtenerIdUsuarios(ObtenerUsuariosRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                try
                {
                    ObtenerUsuariosResponse response = new ObtenerUsuariosResponse();

                    var idUsuarios = (from x in db.Usuarios
                                      where !x.Eliminado && x.IdUsuarioPadre == request.IdUsuarioPadre.Value
                                      orderby x.Nombre
                                      select x.Id).ToList();

                    response.IdUsuarios = idUsuarios;
                    return response;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public ObtenerUsuariosResponse ObtenerIdUsuariosPorDiagnostico(ObtenerUsuariosRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                List<int> idUsuarios = db.Diagnosticos.Where(x => x.Id == request.IdDiagnostico).FirstOrDefault().Usuarios.Select(x => x.Id).ToList();
                
                //var idUsuarios = (from d in db.Diagnosticos
                //                  where d.Id == request.IdDiagnostico
                //                  select d.Usuarios.Select(x => x.Id).ToList()).FirstOrDefault();

                ObtenerUsuariosResponse response = new ObtenerUsuariosResponse();
                response.IdUsuarios = idUsuarios;

                return response;
            }
        }
    }
}
