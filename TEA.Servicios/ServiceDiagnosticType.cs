﻿namespace TEA.Servicios
{
    using System.Linq;
    using TEA.Contratos;
    using TEA.Datos;
    using TEA.Entidades.Negocio.DiagnosticType;

    public class ServiceDiagnosticType : IServiceDiagnosticType
    {
        public GetDiagnosticTypesResponse GetDiagnosticTypes(GetDiagnosticTypesRequest request)
        {
            using (var db = new TEAEntities())
            {
                var response = new GetDiagnosticTypesResponse();
                var diagnosticTypes = db.DiagnosticTypes
                    .Where(x => x.CancellationDate == null)
                    .Select(x => new DiagnosticType()
                    {
                        DiagnosticTypeId = x.DiagnosticTypeId,
                        Name = x.Name,
                    })
                    .AsQueryable();
                switch (request.Order)
                {
                    case "nombre_desc":
                        diagnosticTypes = diagnosticTypes.OrderByDescending(x => x.Name);
                        break;
                    default:
                        diagnosticTypes = diagnosticTypes.OrderBy(x => x.Name);
                        break;
                };

                response.DiagnosticTypes = diagnosticTypes.ToList();
                return response;
            }
        }
    }
}
