﻿namespace TEA.Servicios
{
    using AutoMapper;
    using Entidades.Negocio.Respuestas;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using TEA.Contratos;
    using TEA.Entidades.Negocio;
    using TEA.Entidades.Negocio.Stage;
    using data = TEA.Datos;

    public class ServiceStage : IServiceStage
    {
        public GetStagesResponse GetStagesFiltered(GetStagesRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new GetStagesResponse();
                var stages = db.Stages.Where(x => x.CancellationDate == null &&
                    x.UserProfessionalId == request.UserProfessionalId
                    ).AsQueryable();
                if (request.FilterShared.HasValue)
                {
                    stages = stages.Where(x => x.Shared == request.FilterShared.Value);
                    if (request.FilterShared.Value == true)
                    {
                        if (request.FilterUserId > 0)
                        {
                            //Si es COMPARTIDA y tengo que buscar por Usuario -> dejo el UserStage del usuario
                            //Filtro por UserId
                            stages = stages.Where(x => x.UsersStages.Any(y => y.UserId == request.FilterUserId.Value));
                            //Si es compartida, dejo solo las del usuario
                            if (request.FilterShared.HasValue && request.FilterShared.Value == true)
                            {
                                foreach (var stage in stages)
                                {
                                    var usersStagesToDelete = stage.UsersStages.Where(x => x.UserId != request.FilterUserId.Value).ToList();
                                    foreach (var userStageToDelete in usersStagesToDelete)
                                    {
                                        stage.UsersStages.Remove(userStageToDelete);
                                    };
                                };
                            };

                        }
                        else
                        {
                            //Si es COMPARTIDA y NO tengo que buscar por usuario -> Elimino TODAS del UserStage
                            foreach (var stage in stages)
                            {
                                var usersStagesToDelete = stage.UsersStages.ToList();
                                foreach (var userStageToDelete in usersStagesToDelete)
                                {
                                    stage.UsersStages.Remove(userStageToDelete);
                                };
                            };
                        };
                    }
                    else if (request.FilterShared.Value == false)
                    {
                        //No es COMPARTIDA, dejo todo el UserStages
                        if (request.FilterUserId > 0)
                        {
                            //Filtro por UserId
                            stages = stages.Where(x => x.UsersStages.Any(y => y.UserId == request.FilterUserId.Value));
                        };
                    };
                }
                else
                {
                    if (request.FilterUserId > 0)
                    {
                        //Si COMPARTIDA es NULL y tengo que buscar por Usuario -> dejo el UserStage del usuario
                        //Filtro por UserId
                        stages = stages.Where(x => x.UsersStages.Any(y => y.UserId == request.FilterUserId.Value));
                        //Si es compartida, dejo solo las del usuario
                        foreach (var stage in stages)
                        {
                            var usersStagesToDelete = stage.UsersStages.Where(x => x.UserId != request.FilterUserId.Value).ToList();
                            foreach (var userStageToDelete in usersStagesToDelete)
                            {
                                stage.UsersStages.Remove(userStageToDelete);
                            };
                        };
                    }
                    else
                    {
                        //Si COMPARTIDA es NULL y NO tengo que buscar por Usuario -> elimino los UserStages de las que son COMPARTIDAS, dejo solo las NO COMPARTIDAS
                        foreach (var stage in stages)
                        {
                            if (stage.Shared)
                            {
                                var usersStagesToDelete = stage.UsersStages.ToList();
                                foreach (var userStageToDelete in usersStagesToDelete)
                                {
                                    stage.UsersStages.Remove(userStageToDelete);
                                };
                            };
                        };
                    };

                };

                if (!string.IsNullOrEmpty(request.FilterStageName))
                {
                    stages = stages.Where(x => x.Name.Trim().ToUpper().Contains(request.FilterStageName.Trim().ToUpper()));
                };

                if (!string.IsNullOrEmpty(request.FilterLevelName))
                {
                    stages = stages.Where(x => x.UsersStages.Any(y => y.Levels.Name.Trim().ToUpper().Contains(request.FilterLevelName.Trim().ToUpper())));
                };

                if (!string.IsNullOrEmpty(request.FilterDiagnosticName))
                {
                    stages = stages.Where(x => x.UsersStages.Any(y => y.Levels.Diagnostics.Name.Trim().ToUpper().Contains(request.FilterDiagnosticName.Trim().ToUpper())));
                };

                if (!string.IsNullOrEmpty(request.FilterUserName))
                {
                    stages = stages.Where(x => x.UsersStages.Any(y => y.Users.Users2.UserName.Trim().ToUpper().Contains(request.FilterUserName.Trim().ToUpper())));
                };

                response.RecordsTotal = stages.Count();
                switch (request.Order)
                {
                    case "StageName_desc":
                        stages = stages.OrderByDescending(x => x.Name).Skip(request.Skip.Value);
                        break;
                    case "LevelName_asc":
                        stages = stages.OrderBy(x => x.UsersStages.OrderBy(y => y.Levels.Name)).Skip(request.Skip.Value);
                        break;
                    case "LevelName_desc":
                        stages = stages.OrderByDescending(x => x.UsersStages.OrderByDescending(y => y.Levels.Name)).Skip(request.Skip.Value);
                        break;
                    case "DiagnosticName_asc":
                        stages = stages.OrderBy(x => x.UsersStages.OrderBy(y => y.Levels.Diagnostics.Name)).Skip(request.Skip.Value);
                        break;
                    case "DiagnosticName_desc":
                        stages = stages.OrderByDescending(x => x.UsersStages.OrderByDescending(y => y.Levels.Diagnostics.Name)).Skip(request.Skip.Value);
                        break;
                    case "StageShared_asc":
                        stages = stages.OrderBy(x => x.Shared).Skip(request.Skip.Value);
                        break;
                    case "StageShared_desc":
                        stages = stages.OrderByDescending(x => x.Shared).Skip(request.Skip.Value);
                        break;
                    default:
                        stages = stages.OrderBy(x => x.Name).Skip(request.Skip.Value);
                        break;
                };

                if (request.PageSize.HasValue)
                {
                    stages = stages.Take(request.PageSize.Value);
                };

                var usersStagesList = stages.ToList();
                response.Stages = Mapper.Map<List<Stage>>(usersStagesList);
                return response;
            }
        }

        public GetStagesResponse GetSharedStagesForAssigned(GetSharedStagesForAssignedRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new GetStagesResponse();
                var stages = db.Stages.Where(x => x.CancellationDate == null &&
                    x.UserProfessionalId == request.UserProfessionalId &&
                    x.Shared).AsQueryable();
                var usersStagesList = stages.ToList();
                response.Stages = Mapper.Map<List<Stage>>(usersStagesList);
                return response;
            }
        }

        public GetTreeStagesResponse GetTreeStagesJson(GetTreeStagesRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new GetTreeStagesResponse();
                var diagnostics = db.Diagnostics.Where(x => x.CancellationDate == null &&
                    x.UserId == request.UserId).ToList();
                string treeStageJson = "[";
                if (diagnostics.Count() > 0)
                {
                    var maxLength = 60;
                    foreach (var diagnostic in diagnostics)
                    {
                        treeStageJson += "{\"text\":\"" + diagnostic.Name + "\"";
                        treeStageJson += ", \"showCheck\": false";
                        treeStageJson += ",\"state\": {";
                        treeStageJson += "\"checked\": false,";
                        treeStageJson += "\"disabled\": true,";
                        treeStageJson += "\"expanded\": true,";
                        treeStageJson += "\"selected\": false}";
                        var levels = diagnostic.Levels.Where(x => x.CancellationDate == null).ToList();
                        if (levels.Count() > 0)
                        {
                            treeStageJson += ", \"nodes\":[";
                            foreach (var level in levels)
                            {
                                treeStageJson += "{\"text\":\"" + level.Name + "\"";
                                treeStageJson += ", \"id\":\"" + level.LevelId + "\"";
                                treeStageJson += ", \"showCheck\": true";
                                treeStageJson += ",\"state\": {";
                                treeStageJson += "\"checked\": false,";
                                treeStageJson += "\"disabled\": false,";
                                treeStageJson += "\"expanded\": false,";
                                treeStageJson += "\"selected\": false}";

                                var stages = level.UsersStages.OrderBy(x => x.Order).Where(x => x.Stages.CancellationDate == null).Select(x => x.Stages).ToList();
                                if (stages.Count() > 0)
                                {
                                    treeStageJson += ", \"nodes\":[";
                                    foreach (var stage in stages)
                                    {
                                        var order = stage.UsersStages.Where(x => x.StageId == stage.StageId && x.LevelId == level.LevelId && x.UserId == request.UserId).Select(x => x.Order).FirstOrDefault();
                                        var stageName = stage.Name.Length <= maxLength ? stage.Name : $"{stage.Name.Substring(0, maxLength)}...";
                                        stageName = $"{order.ToString()} - {stageName}";
                                        treeStageJson += "{\"text\":\"" + stageName + "\"";
                                        treeStageJson += ", \"showCheck\": false";
                                        treeStageJson += ",\"state\": {";
                                        treeStageJson += "\"checked\": false,";
                                        treeStageJson += "\"disabled\": true,";
                                        treeStageJson += "\"expanded\": true,";
                                        treeStageJson += "\"selected\": false}";
                                        treeStageJson += "},";
                                    };

                                    treeStageJson += "]";
                                };

                                treeStageJson += "},";
                            };

                            treeStageJson += "]";
                        };

                        treeStageJson += "},";
                    };
                }
                else
                {
                    treeStageJson += "";
                };

                treeStageJson += "]";
                treeStageJson = treeStageJson.Replace("},]", "}]");
                response.TreeStageJson = treeStageJson;
                return response;
            };
        }

        public CreateStageResponse CreateSharedStage(CreateSharedStageRequest request)
        {
            var createStageResponse = new CreateStageResponse();
            var validateResponse = new ValidateResponse();
            using (var db = new data.TEAEntities())
            {
                var stage = Mapper.Map<data.Stages>(request);
                stage = db.Stages.Add(stage);
                db.SaveChanges();
                validateResponse.IsValid = true;
                createStageResponse.ValidateResponse = validateResponse;
                createStageResponse.StageId = stage.StageId;
            };

            return createStageResponse;
        }

        public CreateStageResponse CreateStageForUser(CreateStageForUserRequest request)
        {
            var createStageResponse = new CreateStageResponse();
            var validateResponse = new ValidateResponse();
            using (var db = new data.TEAEntities())
            {
                ///Recupero uno con mismo número de orden
                var orderDuplicate = db.Stages.Where(x => x.CancellationDate == null && x.UsersStages.Any(y => y.LevelId == request.LevelId && y.Order == request.Order)).FirstOrDefault();
                if (orderDuplicate != null)
                {
                    var errores = new Dictionary<string, string>();
                    errores.Add("Order", $"Ya existe un mismo número de orden para la etapa llamada: '{orderDuplicate.Name}'.");
                    validateResponse.Errors = errores;
                    validateResponse.IsValid = false;
                    createStageResponse.ValidateResponse = validateResponse;
                }
                else
                {
                    var stage = Mapper.Map<data.Stages>(request);
                    stage = db.Stages.Add(stage);
                    var usersStage = Mapper.Map<data.UsersStages>(request);
                    usersStage.StageId = stage.StageId;
                    db.UsersStages.Add(usersStage);
                    db.SaveChanges();
                    validateResponse.IsValid = true;
                    createStageResponse.ValidateResponse = validateResponse;
                    createStageResponse.StageId = stage.StageId;
                };
            };

            return createStageResponse;
        }

        public UpdateStageResponse UpdateSharedStage(UpdateSharedStageRequest request)
        {
            var response = new UpdateStageResponse();
            var validateResponse = new ValidateResponse();
            using (var db = new data.TEAEntities())
            {
                ///Recupero el que se quiere actualizar y lo actualizo
                var stage = db.Stages.Where(x => x.CancellationDate == null && x.StageId == request.StageId).FirstOrDefault();
                Mapper.Map(request, stage);
                db.Entry(stage).State = EntityState.Modified;
                db.SaveChanges();
                validateResponse.IsValid = true;
                response.StageId = stage.StageId;
            };

            response.ValidateResponse = validateResponse;
            return response;
        }

        public UpdateStageResponse UpdateStageForUser(UpdateStageForUserRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var validateResponse = new ValidateResponse();
                ///Recupero uno con mismo número de orden pero diferente al que se quiere actualizar
                var orderDuplicate = db.Stages.Where(x => x.CancellationDate == null && x.UsersStages.Any(y => y.LevelId == request.LevelId && y.Order == request.Order && y.StageId != request.StageId)).FirstOrDefault();
                if (orderDuplicate != null)
                {
                    var errores = new Dictionary<string, string>();
                    errores.Add("Order", $"Ya existe un mismo número de orden para la etapa llamada: '{orderDuplicate.Name}'.");
                    validateResponse.Errors = errores;
                    validateResponse.IsValid = false;
                }
                else
                {
                    ///Recupero el que se quiere actualizar y lo actualizo
                    var stage = db.Stages.Where(x => x.CancellationDate == null && x.StageId == request.StageId).FirstOrDefault();
                    Mapper.Map(request, stage);
                    db.Entry(stage).State = EntityState.Modified;

                    var usersStages = db.UsersStages.Where(x => x.StageId == request.StageId && x.UserId == request.UserId).ToList();
                    foreach (var userStage in usersStages)
                    {
                        db.Entry(userStage).State = EntityState.Deleted;
                    };

                    var newUsersStage = Mapper.Map<data.UsersStages>(request);
                    newUsersStage.StageId = stage.StageId;
                    db.UsersStages.Add(newUsersStage);

                    db.SaveChanges();
                    validateResponse.IsValid = true;
                };

                return new UpdateStageResponse() { ValidateResponse = validateResponse };
            };
        }

        public UpdateAssignStageForUserResponse UpdateAssignStageForUser(UpdateAssignStageForUserRequest request)
        {
            var validateResponse = new ValidateResponse();
            var response = new UpdateAssignStageForUserResponse();
            try
            {
                using (var db = new data.TEAEntities())
                {
                    var userStageToAdd = new List<data.UsersStages>();
                    var userStageToDelete = new List<data.UsersStages>();
                    //Obtengo las etapas actuales
                    var currentUsersStages = db.UsersStages.Where(x => x.UserId == request.UserId && x.Stages.CancellationDate == null).ToList();
                    foreach (var stageToAssign in request.StagesToAssign)
                    {
                        var order = 0;
                        var levelId = stageToAssign.LevelId;

                        ///Si las etapas a asignar viene nulla, tengo que fijarme si para ese nivel existe en la base de datos etapas asigandas
                        ///Si existen etapas asiganadas que no son compartidas, retornar error
                        ///Si existen etapas asiganadas que SON compartidas, dejo seguir
                        ///Si no existen etapas asignadas, dejo seguir
                        if (stageToAssign.StagesId.Count() == 0)
                        {
                            var countStageInDb = currentUsersStages.Where(x => x.Stages.Shared == false &&
                                x.LevelId == levelId).Count();
                            if (countStageInDb > 0)
                            {
                                throw new Exception($"No es posible desasignar todas las estapas de un nivel");
                            };
                        };
                        
                        //Compartidos que fueron desasignados
                        var stagesToDelete = currentUsersStages.Where(x => x.Stages.Shared &&
                            x.LevelId == levelId &&
                            !stageToAssign.StagesId.Contains(x.StageId)).ToList();

                        userStageToDelete.AddRange(stagesToDelete);
                        //Agrego las etapas que tienen el mismo nivel pero que no están actualmente en la base de datos
                        var stagesToAdd = stageToAssign.StagesId.Except(currentUsersStages.Where(x => x.LevelId == levelId).Select(x => x.StageId)).ToList();
                        foreach (var stageId in stageToAssign.StagesId)
                        {
                            //Si tengo que agregar
                            if (stagesToAdd.Contains(stageId))
                            {
                                var userStage = new data.UsersStages()
                                {
                                    StageId = stageId,
                                    Order = order,
                                    LevelId = levelId,
                                    UserId = request.UserId
                                };

                                userStageToAdd.Add(userStage);
                            }
                            else
                            {
                                //Tengo que acualizar
                                var stageToUpdate = currentUsersStages.Where(x => x.LevelId == levelId &&
                                x.StageId == stageId).FirstOrDefault();
                                if (stageToUpdate == null)
                                {
                                    throw new Exception("No existe la epata");
                                }
                                else
                                {
                                    stageToUpdate.Order = order;
                                    db.Entry(stageToUpdate).State = EntityState.Modified;
                                };
                            };

                            order++;
                        };
                    };

                    db.UsersStages.RemoveRange(userStageToDelete);
                    db.UsersStages.AddRange(userStageToAdd);
                    db.SaveChanges();
                    validateResponse.IsValid = true;
                };
            }
            catch (Exception e)
            {
                var errores = new Dictionary<string, string>();
                errores.Add("MsjError", e.Message);
                validateResponse.Errors = errores;
                validateResponse.IsValid = false;
            };

            response.ValidateResponse = validateResponse;
            return response;
        }

        public SaveCorrectStageResponse SaveCorrectStage(SaveCorrectStageRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var saveCorrectStageResponse = new SaveCorrectStageResponse();
                var validateResponse = new ValidateResponse();
                try
                {
                    var usersStagesHistory = Mapper.Map<data.UsersStagesHistory>(request);
                    //usersStagesHistory.FinishDate = DateTime.Now.AddHours(4);
                    db.UsersStagesHistory.Add(usersStagesHistory);
                    db.SaveChanges();
                    validateResponse.IsValid = true;
                }
                catch (Exception e)
                {
                    var errores = new Dictionary<string, string>();
                    errores.Add("", "No se ha podido guardar la etapa, por favor intente nuevamente.");
                    errores.Add("", e.Message);
                    validateResponse.Errors = errores;
                    validateResponse.IsValid = false;
                };

                saveCorrectStageResponse.Validate = validateResponse;
                return saveCorrectStageResponse;
            };
        }

        public GetStageResponse GetStageById(GetStageRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new GetStageResponse();
                var stage = db.Stages.Where(x => x.CancellationDate == null &&
                    x.UserProfessionalId == request.UserProfessionalId &&
                    x.StageId == request.StageId).FirstOrDefault();
                response.Stage = Mapper.Map<Stage>(stage);
                return response;
            }
        }

        public GetStagesByUserAndLevelResponse GetStagesByUserAndLevel(GetStagesByUserAndLevelRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new GetStagesByUserAndLevelResponse();
                var stages = db.UsersStages
                    .Where(x => x.Stages.CancellationDate == null
                        && x.UserId == request.UserId
                        && x.LevelId == request.LevelId)
                    .Select(x => new GetStageByUserAndLevel()
                    {
                        LevelId = x.LevelId,
                        Order = x.Order,
                        StageId = x.StageId,
                    })
                    .OrderBy(x => x.Order)
                    .ToList();

                response.Stages = stages;
                return response;
            }
        }

        public GetStageResponse GetLastOrderOfStages(GetStageRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new GetStageResponse();
                int orden = db.UsersStages
                    .Where(x => x.Stages.CancellationDate == null
                        && x.LevelId == request.LevelId)
                    .Select(x => x.Order)
                    .OrderByDescending(x => x)
                    .FirstOrDefault();
                response.OrderStage = orden + 1;
                return response;
            }
        }

        public GetNextStagesWithAnswersResponse GetNextStagesWithAnswers(GetNextStagesWithAnswersRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                ///Recupero orden de la etapa actual
                int? orderStage = db.UsersStages
                    .Where(x => x.Stages.CancellationDate == null
                        && x.UserId == request.UserId
                        && x.LevelId == request.LevelId
                        && x.StageId == request.StageId)
                    .Select(x => x.Order)
                    .FirstOrDefault();
                if (orderStage != null)
                {
                    ///Recupero todas las etapas que tienen orden mayor o igual al que vino en la request
                    var stages = db.UsersStages
                        .Where(x => x.Stages.CancellationDate == null
                            && x.UserId == request.UserId
                            && x.LevelId == request.LevelId
                            && x.Order >= orderStage)
                        .Select(x => new Stage()
                        {
                            StageId = x.StageId,
                            Name = x.Stages.Name,
                            Order = x.Order,
                            LevelId = x.Levels.LevelId,
                            LevelName = x.Levels.Name,
                            LevelOrder = x.Levels.Order,
                            DiagnosticId = x.Levels.Diagnostics.DiagnosticId,
                            DiagnosticName = x.Levels.Diagnostics.Name,
                            Answers = x.Stages.Answers
                                .Where(y => y.CancellationDate == null)
                                .Select(y => new Answer() { AnswerId = y.AnswerId, ImageName = y.ImageName, Correct = y.Correct, Background = y.Background }).ToList(),
                        })
                        .OrderBy(x => x.Order)
                        .ToList();
                    ///Si la cantidad de etapas recuperas, es menor o igual a la cantidad solicitada.
                    ///Quiere decir que este lote recuperado contiene la última etapa de un nivel.
                    ///Por lo tanto, a la última etapa se le asigna la propiedad 'Last' en true
                    if (stages.Count <= request.CountStages)
                    {
                        stages.Last().Last = true;

                        var levelOrder = stages.Last().LevelOrder;
                        ///Si no recupera algún nivel que sea mayor al orden del nivel de cualquier etapa, quiere decir que es el último nivel
                        var UpperLevel = db.Levels
                            .Where(x => x.Order > levelOrder
                                && x.Diagnostics.UserId == request.UserId)
                            .FirstOrDefault();

                        if (UpperLevel == null)
                        {
                            stages.Last().LevelLast = true;
                        };
                    };

                    var response = new GetNextStagesWithAnswersResponse();
                    response.Stages = stages;
                    return response;
                }
                else
                {
                    return null;
                }
            }
        }

        public DeleteStageResponse DeleteStage(DeleteStageRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new DeleteStageResponse();
                var validarResponse = new ValidateResponse();
                var stage = db.Stages
                    .Where(x => x.CancellationDate == null &&
                        x.UserProfessionalId == request.UserProfessionalId &&
                        x.StageId == request.StageId)
                    .FirstOrDefault();
                if (stage != null)
                {
                    stage.CancellationDate = DateTime.Now;
                    db.Entry(stage).State = EntityState.Modified;
                    db.SaveChanges();
                    validarResponse.IsValid = true;
                }
                else
                {
                    var errores = new Dictionary<string, string>();
                    errores.Add("", "No se ha podido eliminar la etapa, por favor intente nuevamente.");
                    validarResponse.Errors = errores;
                    validarResponse.IsValid = false;
                };

                response.ValidarResponse = validarResponse;
                return response;
            }
        }
    }
}
