﻿namespace TEA.Servicios
{
    using AutoMapper;
    using Castle.Core.Internal;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using TEA.Contratos;
    using TEA.Entidades.Negocio;
    using TEA.Entidades.Negocio.Diagnostic;
    using data = Datos;

    public class ServiceDiagnostic : IServiceDiagnostic
    {
        public GetDiagnosticsResponse GetDiagnostics(GetDiagnosticsRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new GetDiagnosticsResponse();
                var diagnostics = db.Diagnostics.Where(x => x.CancellationDate == null).AsQueryable();
                if (!string.IsNullOrEmpty(request.FilterDiagnostic))
                {
                    diagnostics = diagnostics.Where(x => x.Name.Trim().ToUpper().Contains(request.FilterDiagnostic.Trim().ToUpper()));
                };

                if (request.FilterDiagnosticTypeId != null)
                {
                    diagnostics = diagnostics.Where(x => x.DiagnosticTypeId == request.FilterDiagnosticTypeId.Value);
                };

                if (request.FilterDiagnosticId != null)
                {
                    diagnostics = diagnostics.Where(x => x.DiagnosticId == request.FilterDiagnosticId.Value);
                }

                if (request.UserProfessionalId != null && request.UserProfessionalId != 0)
                {
                    diagnostics = diagnostics.Where(x => x.UserProfessionalId == request.UserProfessionalId.Value);
                }

                response.RecordsTotal = diagnostics.Count();
                switch (request.Order)
                {
                    case "Name_desc":
                        diagnostics = diagnostics.OrderByDescending(x => x.Name).Skip(request.Skip.Value);
                        break;
                    case "Id_asc":
                        diagnostics = diagnostics.OrderBy(x => x.DiagnosticId).Skip(request.Skip.Value);
                        break;
                    case "Id_desc":
                        diagnostics = diagnostics.OrderByDescending(x => x.DiagnosticId).Skip(request.Skip.Value);
                        break;
                    case "DiagnosticTypeName_asc":
                        diagnostics = diagnostics.OrderBy(x => x.DiagnosticTypeId).Skip(request.Skip.Value);
                        break;
                    case "DiagnosticTypeName_desc":
                        diagnostics = diagnostics.OrderByDescending(x => x.DiagnosticTypeId).Skip(request.Skip.Value);
                        break;
                    default:
                        diagnostics = diagnostics.OrderBy(x => x.Name).Skip(request.Skip.Value);
                        break;
                };

                if (request.PageSize.HasValue)
                {
                    diagnostics = diagnostics.Take(request.PageSize.Value);
                };

                response.Diagnostics = Mapper.Map<List<Diagnostic>>(diagnostics);
                return response;
            }
        }

        public GetDiagnosticResponse GetDiagnosticById(GetDiagnosticRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new GetDiagnosticResponse();
                var diagnostic = db.Diagnostics
                        .Where(x => x.CancellationDate == null
                            && x.UserProfessionalId == request.UserProfessionalId
                            && x.DiagnosticId == request.DiagnosticId)
                        .FirstOrDefault();
                response.Diagnostic = Mapper.Map<Diagnostic>(diagnostic);
                return response;
            }
        }

        public CreateDiagnosticResponse CreateDiagnostic(CreateDiagnosticRequest request)
        {
            var createDiagnosticResponse = new CreateDiagnosticResponse();
            var validateResponse = new ValidateResponse();
            using (var db = new data.TEAEntities())
            {
                ///Verifico que no exista ninguno
                var diagnosticDuplicate = db.Diagnostics
                        .Where(x => x.CancellationDate == null
                            && x.UserProfessionalId == request.UserProfessionalId
                            && x.UserId == request.UserId)
                        .FirstOrDefault();

                if (diagnosticDuplicate != null)
                {
                    var errores = new Dictionary<string, string>();
                    errores.Add("UserId", $"Ya existe un Diagnóstico con el mismo tipo de diagnóstico llamado: '{diagnosticDuplicate.Name}'.");
                    validateResponse.Errors = errores;
                    validateResponse.IsValid = false;
                    createDiagnosticResponse.ValidateResponse = validateResponse;
                }
                else
                {
                    var diagnostic = Mapper.Map<data.Diagnostics>(request);
                    db.Diagnostics.Add(diagnostic);
                    db.SaveChanges();
                    validateResponse.IsValid = true;
                    createDiagnosticResponse.ValidateResponse = validateResponse;
                };

            };

            return createDiagnosticResponse;
        }

        public UpdateDiagnosticResponse UpdateDiagnostic(UpdateDiagnosticRequest request)
        {
            var UpdateDiagnostic = new UpdateDiagnosticResponse();
            var validateResponse = new ValidateResponse();
            using (var db = new data.TEAEntities())
            {
                ///Verifico que no exista ninguno
                var diagnosticDuplicate = db.Diagnostics
                        .Where(x => x.CancellationDate == null
                            && x.UserProfessionalId == request.UserProfessionalId
                            && x.UserId == request.UserId
                            && x.DiagnosticId != request.DiagnosticId)
                        .FirstOrDefault();

                if (diagnosticDuplicate != null)
                {
                    var errores = new Dictionary<string, string>();
                    errores.Add("UserId", $"Ya existe un Diagnóstico con el mismo tipo de diagnóstico llamado: '{diagnosticDuplicate.Name}'.");
                    validateResponse.Errors = errores;
                    validateResponse.IsValid = false;
                    UpdateDiagnostic.ValidateResponse = validateResponse;
                }
                else
                {
                    var diagnostic = db.Diagnostics.Where(x => x.CancellationDate == null &&
                   x.UserProfessionalId == request.UserProfessionalId &&
                   x.DiagnosticId == request.DiagnosticId).FirstOrDefault();
                    Mapper.Map(request, diagnostic);
                    db.Entry(diagnostic).State = EntityState.Modified;
                    db.SaveChanges();
                    validateResponse.IsValid = true;
                    UpdateDiagnostic.ValidateResponse = validateResponse;
                };
               
            };

            return UpdateDiagnostic;
        }

        public DeleteDiagnosticResponse DeleteDiagnostic(DeleteDiagnosticRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new DeleteDiagnosticResponse();
                var validarResponse = new ValidateResponse();
                var userStageToDelete = new List<data.UsersStages>();
                var diagnostic = db.Diagnostics.Where(x => x.CancellationDate == null && 
                    x.UserProfessionalId == request.UserProfessionalId && 
                    x.DiagnosticId == request.DiagnosticId).FirstOrDefault();
                if (diagnostic != null)
                {
                    foreach (var level in diagnostic.Levels)
                    {
                        level.CancellationDate = DateTime.Now;
                        level.UsersStages.Where(x => x.Stages.Shared == false).ForEach(y => y.Stages.CancellationDate = DateTime.Now);
                        //Compartidas que se quieren eliminar
                        var stagesToDelete = level.UsersStages.Where(x => x.Stages.Shared).ToList();
                        userStageToDelete.AddRange(stagesToDelete);

                        db.Entry(level).State = EntityState.Modified;
                        db.UsersStages.RemoveRange(userStageToDelete);
                    };

                    diagnostic.CancellationDate = DateTime.Now;
                    db.Entry(diagnostic).State = EntityState.Modified;
                    db.SaveChanges();
                    validarResponse.IsValid = true;
                }
                else
                {
                    var errores = new Dictionary<string, string>();
                    errores.Add("", "No se ha podido eliminar el diagnóstico, por favor intente nuevamente.");
                    validarResponse.Errors = errores;
                    validarResponse.IsValid = false;
                };

                response.ValidarResponse = validarResponse;
                return response;
            }
        }

        public GetDiagnosticsResponse GetDiagnosticsByUser(GetDiagnosticsRequest request)
        {
            using (var db = new data.TEAEntities())
            {
                var response = new GetDiagnosticsResponse();
                var diagnostics = db.Diagnostics.Where(x => x.CancellationDate == null &&
                    x.UserId == request.UserId).AsQueryable();
                response.Diagnostics = Mapper.Map<List<Diagnostic>>(diagnostics);
                return response;
            }
        }
    }
}