﻿namespace TEA.Servicios
{
    using TEA.Contratos;
    using TEA.Datos;
    using TEA.Entidades.Negocio.TipoDiagnostico;
    using System.Linq;
    using System;

    public class ServicioTipoDiagnostico : IServicioTipoDiagnostico
    {
        public ObtenerTipoDiagnosticosResponse ObtenerTipoDiagnosticos(ObtenerTipoDiagnosticosRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                try
                {
                    ObtenerTipoDiagnosticosResponse response = new ObtenerTipoDiagnosticosResponse();

                    var tipoDiagnosticos = (from td in db.TipoDiagnosticos
                                            select new TipoDiagnostico()
                                            {
                                                Id = td.Id,
                                                Nombre = td.Nombre,
                                            }).AsQueryable();

                    switch (request.Orden)
                    {
                        case "nombre_desc":
                            tipoDiagnosticos = tipoDiagnosticos.OrderByDescending(x => x.Nombre);
                            break;
                        default:
                            tipoDiagnosticos = tipoDiagnosticos.OrderBy(x => x.Nombre);
                            break;
                    }

                    response.TipoDiagnostico = tipoDiagnosticos.ToList();
                    return response;
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }
        
    }
}
