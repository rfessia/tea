﻿namespace TEA.Servicios
{
    using TEA.Contratos;
    using TEA.Datos;
    using TEA.Entidades.Negocio.Etapa;
    using System.Linq;
    using System.Data.Entity;
    using TEA.Entidades.Negocio;
    using System.Collections.Generic;
    using System;

    public class ServicioEtapa : IServicioEtapa
    {
        public ObtenerEtapasResponse ObtenerEtapas(ObtenerEtapasRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                try
                {
                    ObtenerEtapasResponse response = new ObtenerEtapasResponse();

                    var etapas = (from e in db.Etapas
                                   select new Etapa()
                                   {
                                       Id = e.Id,
                                       Nombre = e.Nombre,
                                       Titulo = e.Titulo,
                                       Completo = e.Completo,
                                       Orden = e.Orden,
                                       IdNivel = e.Niveles.Id,
                                       NombreNivel = e.Niveles.Nombre,
                                       IdDiagnostico = e.Niveles.Diagnosticos.Id,
                                       NombreDiagnostico = e.Niveles.Diagnosticos.Nombre,
                                       IdTipoDiagnostico = e.Niveles.Diagnosticos.IdTipoDiagnostico,
                                   }).AsQueryable();

                    if (!String.IsNullOrEmpty(request.FiltroTexto))
                        etapas = etapas.Where(x => x.Nombre.ToUpper().Contains(request.FiltroTexto.ToUpper().Trim()));

                    if (request.FiltroIdTipoDiagnostico != null)
                    {
                        etapas = etapas.Where(x => x.IdTipoDiagnostico == request.FiltroIdTipoDiagnostico.Value);
                    }
                    if (request.FiltroIdDiagnostico != null)
                    {
                        etapas = etapas.Where(x => x.IdDiagnostico == request.FiltroIdDiagnostico.Value);
                    }
                    if (request.FiltroIdNivel != null)
                    {
                        etapas = etapas.Where(x => x.IdNivel == request.FiltroIdNivel.Value);
                    }

                    switch (request.Orden)
                    {
                        case "orden":
                            etapas = etapas.OrderBy(x => x.Orden);
                            break;
                        case "orden_desc":
                            etapas = etapas.OrderByDescending(x => x.Orden);
                            break;
                        case "nombre_desc":
                            etapas = etapas.OrderByDescending(x => x.Nombre);
                            break;
                        default:
                            etapas = etapas.OrderBy(x => x.Nombre);
                            break;
                    }

                    response.Etapas = etapas.ToList();
                    return response;
                }
                catch (System.Exception)
                {

                    throw;
                }
            }
        }

        public GuardarEtapaResponse GuardarEtapa(GuardarEtapaRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                Etapas etapa = new Etapas()
                {
                    Id = request.Etapa.Id,
                    Nombre = request.Etapa.Nombre,
                    Titulo = request.Etapa.Titulo,
                    IdNivel = request.Etapa.IdNivel,
                    Completo = request.Etapa.Completo,
                    Orden = request.Etapa.Orden,
                };
                if (etapa.Id == 0)//Crear
                {
                    etapa.Completo = false;
                    db.Etapas.Add(etapa);
                }
                else
                {
                    db.Entry(etapa).State = EntityState.Modified;

                }
                db.SaveChanges();
            }
            return null;
        }

        public ObtenerEtapaResponse ObtenerEtapaPorId(ObtenerEtapaRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                ObtenerEtapaResponse response = new ObtenerEtapaResponse();

                var etapa = db.Etapas
                              .Where(x => x.Id == request.IdEtapa)
                              .Select(x => new Etapa()
                              {
                                  Id = x.Id,
                                  Nombre = x.Nombre,
                                  Titulo = x.Titulo,
                                  Completo = x.Completo,
                                  Orden = x.Orden,
                                  IdNivel = x.Niveles.Id,
                                  NombreNivel = x.Niveles.Nombre,
                                  IdDiagnostico = x.Niveles.Diagnosticos.Id,
                                  NombreDiagnostico = x.Niveles.Diagnosticos.Nombre,
                              }).FirstOrDefault();
                response.Etapa = etapa;
                return response;
            }
        }

        public ObtenerEtapasResponse ObtenerEtapasPorNivelYUsuario(ObtenerEtapasRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                ObtenerEtapasResponse response = new ObtenerEtapasResponse();

                var etapas = db.Etapas.Where(x => x.Niveles.Id == request.IdNivel
                                             && x.Niveles.Diagnosticos.Usuarios.Any(z => z.Id == request.IdUsuario))
                                    .Select(x => new Etapa()
                                    {
                                        Id = x.Id,
                                        Nombre = x.Nombre,
                                        Titulo = x.Titulo,
                                        Completo = x.Completo,
                                        Orden = x.Orden,
                                        IdNivel = x.Niveles.Id,
                                        NombreNivel = x.Niveles.Nombre,
                                        IdDiagnostico = x.Niveles.Diagnosticos.Id,
                                        NombreDiagnostico = x.Niveles.Diagnosticos.Nombre,
                                    }).ToList();
                response.Etapas = etapas;
                return response;
            }
        }

        public ObtenerEtapaResponse ObtenerUltimoOrden(ObtenerEtapaRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                ObtenerEtapaResponse response = new ObtenerEtapaResponse();

                int orden = db.Etapas.Where(x => x.Niveles.Id == request.IdNivel)
                                     .Select(x => x.Orden)
                                     .OrderByDescending(x => x)
                                     .FirstOrDefault();
                response.OrdenEtapa = orden + 1;
                return response;
            }
        }

        public ObtenerEtapaResponse ObtenerEtapaActual(ObtenerEtapaRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                ObtenerEtapaResponse response = new ObtenerEtapaResponse();

                var etapa = db.Etapas.Where(x => x.Niveles.Id == request.IdNivel
                                             && x.Actual
                                             && !x.Completo
                                             && x.Niveles.Diagnosticos.Usuarios.Any(z => z.Id == request.IdUsuario))
                                    .Select(x => new Etapa()
                                    {
                                        Id = x.Id,
                                        Nombre = x.Nombre,
                                        Titulo = x.Titulo,
                                        Completo = x.Completo,
                                        Orden = x.Orden,
                                        IdNivel = x.Niveles.Id,
                                        NombreNivel = x.Niveles.Nombre,
                                        IdDiagnostico = x.Niveles.Diagnosticos.Id,
                                        NombreDiagnostico = x.Niveles.Diagnosticos.Nombre,
                                    }).FirstOrDefault();

                //Si no hay ninguno que sea actual, se toma el primero según el orden que se guardó
                if (etapa == null)
                {
                    etapa = db.Etapas.Where(x => x.Niveles.Id == request.IdNivel
                                              && x.Niveles.Diagnosticos.Usuarios.Any(z => z.Id == request.IdUsuario))
                                     .Select(x => new Etapa()
                                    {
                                        Id = x.Id,
                                        Nombre = x.Nombre,
                                        Titulo = x.Titulo,
                                        Completo = x.Completo,
                                        Orden = x.Orden,
                                        IdNivel = x.Niveles.Id,
                                        NombreNivel = x.Niveles.Nombre,
                                        IdDiagnostico = x.Niveles.Diagnosticos.Id,
                                        NombreDiagnostico = x.Niveles.Diagnosticos.Nombre,
                                    })
                                    .OrderBy(x => x.Orden)
                                    .FirstOrDefault();
                }
                response.Etapa = etapa;
                return response;
            }
        }

        public EliminarEtapaResponse EliminarEtapa(EliminarEtapaRequest request)
        {
            using (TEAEntities db = new TEAEntities())
            {
                EliminarEtapaResponse response = new EliminarEtapaResponse();
                ValidarResponse validarResponse = new ValidarResponse();

                var etapa = (from e in db.Etapas
                                   where e.Id == request.IdEtapa
                                   select e).FirstOrDefault();

                if (etapa != null)
                {
                    db.Entry(etapa).State = EntityState.Deleted;
                    db.SaveChanges();
                    validarResponse.Valido = true;
                }
                else
                {
                    var errores = new Dictionary<string, string>();
                    errores.Add("", "No se ha podido eliminar la etapa, por favor intente nuevamente.");
                    validarResponse.Errores = errores;
                    validarResponse.Valido = false;
                }
                response.ValidarResponse = validarResponse;
                return response;
            }
        }
    }
}
