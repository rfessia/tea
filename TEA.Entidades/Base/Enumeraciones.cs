﻿namespace TEA.Entidades.Base
{
    public class Enumeraciones
    {
        public enum TiposDeDiagnosticos
        {
            RECONOCIMIENTO_DE_EMOCIONES = 1,
            MEMORIA_A_CORTO_PLAZO = 2
        }
    }
}
