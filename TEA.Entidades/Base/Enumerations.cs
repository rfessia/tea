﻿namespace TEA.Entidades.Base
{
    public class Enumerations
    {
        public enum TypesDiagnostics
        {
            RECOGNITION_OF_EMOTIONS = 1,
            SHORT_TERM_MEMORY = 2,
        }

        public enum State
        {
            NOTFINISHED = 0,
            FINISHED = 1,
        }

        public enum RoleEnum
        {
            ADMINISTRADOR = 1,
            USUARIO = 2,
        }
    }
}
