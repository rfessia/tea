﻿namespace TEA.Entidades.Negocio.Etapa
{
    using System.Collections.Generic;
    public class ObtenerEtapasResponse
    {
        public List<Etapa> Etapas { get; set; }
    }
}
