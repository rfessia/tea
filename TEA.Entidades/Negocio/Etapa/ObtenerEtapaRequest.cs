﻿namespace TEA.Entidades.Negocio.Etapa
{
    public class ObtenerEtapaRequest
    {
        public int IdEtapa { get; set; }

        public int IdNivel { get; set; }

        public int IdUsuario { get; set; }
    }
}
