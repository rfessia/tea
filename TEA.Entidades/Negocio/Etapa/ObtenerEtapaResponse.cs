﻿namespace TEA.Entidades.Negocio.Etapa
{
    public class ObtenerEtapaResponse
    {
        public Etapa Etapa { get; set; }

        public int OrdenEtapa { get; set; }
    }
}
