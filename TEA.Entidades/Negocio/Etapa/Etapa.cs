﻿namespace TEA.Entidades.Negocio.Etapa
{
    using System.ComponentModel.DataAnnotations;
    using TEA.Entidades.Negocio.Nivel;

    public class Etapa
    {
        [Required]
        public int Id { get; set; }

        [Display(Name = "Nombre")]
        [Required]
        public string Nombre { get; set; }
        
        [Display(Name = "Titulo")]
        [Required]
        public string Titulo { get; set; }

        [Display(Name = "Imagen")]
        public string UrlImagen { get; set; }

        [Display(Name = "Diagnóstico")]
        public int IdDiagnostico { get; set; }

        public string NombreDiagnostico { get; set; }

        [Display(Name = "Nivel")]
        public int IdNivel { get; set; }
        
        public string NombreNivel { get; set; }

        public bool Completo { get; set; }

        public bool Actual { get; set; }
        
        [Display(Name = "Orden")]
        [Required]
        public int Orden { get; set; }

        public int IdTipoDiagnostico { get; set; }
    }
}
