﻿namespace TEA.Entidades.Negocio.Etapa
{
    public class ObtenerEtapasRequest
    {
        public string Orden { get; set; }

        public string FiltroTexto { get; set; }

        public int IdUsuario { get; set; }

        public int IdNivel { get; set; }

        public int? FiltroIdTipoDiagnostico { get; set; }
        public int? FiltroIdDiagnostico { get; set; }
        public int? FiltroIdNivel { get; set; }
    }
}
