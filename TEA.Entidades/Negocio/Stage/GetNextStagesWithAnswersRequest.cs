﻿namespace TEA.Entidades.Negocio.Stage
{
    public class GetNextStagesWithAnswersRequest
    {
        /// <summary>
        /// Id de la etapa a partir de la cual se recuperan las demás
        /// </summary>
        public int StageId { get; set; }

        /// <summary>
        /// Cantidad de estapas a recuperar
        /// </summary>
        public int CountStages { get; set; } = 10;

        /// <summary>
        /// Id del Usuario
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Id del nivel
        /// </summary>
        public int LevelId { get; set; }
    }
}
