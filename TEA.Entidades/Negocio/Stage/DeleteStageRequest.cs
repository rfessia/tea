﻿namespace TEA.Entidades.Negocio.Stage
{
    public class DeleteStageRequest
    {
        /// <summary>
        /// Id de la etapa
        /// </summary>
        public int StageId { get; set; }

        /// <summary>
        /// Id usuario Profesional
        /// </summary>
        public int UserProfessionalId { get; set; }
    }
}
