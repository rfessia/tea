﻿namespace TEA.Entidades.Negocio.Stage
{
    using System.Collections.Generic;

    public class GetNextStagesWithAnswersResponse
    {
        /// <summary>
        /// Lista de etapas
        /// </summary>
        public List<Stage> Stages { get; set; }
    }
}
