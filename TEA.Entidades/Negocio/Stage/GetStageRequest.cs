﻿namespace TEA.Entidades.Negocio.Stage
{
    public class GetStageRequest
    {
        /// <summary>
        /// Id Etapa
        /// </summary>
        public int StageId { get; set; }

        /// <summary>
        /// Id Nivel
        /// </summary>
        public int LevelId { get; set; }

        /// <summary>
        /// Id Usuario
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Id usuario Profesional
        /// </summary>
        public int UserProfessionalId { get; set; }
    }
}
