﻿namespace TEA.Entidades.Negocio.Stage
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using TEA.Entidades.Negocio.Respuestas;
    using TEA.Entidades.Negocio.User;

    public class Stage
    {
        [Required]
        public int StageId { get; set; }

        [Display(Name = "Nombre")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Imagen")]
        public string UrlImage { get; set; }

        [Display(Name = "Diagnóstico")]
        public int? DiagnosticId { get; set; }

        public string DiagnosticName { get; set; }

        [Display(Name = "Actividad")]
        public int LevelId { get; set; }
        
        public string LevelName { get; set; }

        [Display(Name = "Orden")]
        [Required]
        public int Order { get; set; }

        public bool Shared { get; set; }

        /// <summary>
        /// Id del usuario
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// Nombre del usuario
        /// </summary>
        public string FullNameUser { get; set; }

        /// <summary>
        /// Orden del nivel
        /// </summary>
        public int? LevelOrder { get; set; }

        /// <summary>
        /// Indica si es último nivel
        /// </summary>
        public bool LevelLast { get; set; }

        public int? DiagnosticTypeId { get; set; }

        public List<Answer> Answers { get; set; }

        /// <summary>
        /// Representa la última etapa de un nivel
        /// </summary>
        public bool Last { get; set; }
    }
}
