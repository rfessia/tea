﻿namespace TEA.Entidades.Negocio.Stage
{
    public class GetStagesRequest
    {
        /// <summary>
        /// Orden
        /// </summary>
        public string Order { get; set; }

        /// <summary>
        /// Filtro por Id de Usuario
        /// </summary>
        public int? FilterUserId { get; set; }

        /// <summary>
        /// Usuario profesional
        /// </summary>
        public int UserProfessionalId { get; set; }

        /// <summary>
        /// Filtro si es compartida o no
        /// </summary>
        public bool? FilterShared { get; set; }

        /// <summary>
        /// Filtro nombre de etapa
        /// </summary>
        public string FilterStageName { get; set; }

        /// <summary>
        /// Filtro por nombre de diagnostico
        /// </summary>
        public string FilterDiagnosticName { get; set; }

        /// <summary>
        /// Filtro por nombre de nivel
        /// </summary>
        public string FilterLevelName { get; set; }

        /// <summary>
        /// Filtro por nombre de usuario
        /// </summary>
        public string FilterUserName { get; set; }

        /// <summary>
        /// Salto de pagina
        /// </summary>
        public int? Skip { get; set; } = 0;

        /// <summary>
        /// Tamaño de pagina
        /// </summary>
        public int? PageSize { get; set; }
    }
}
