﻿namespace TEA.Entidades.Negocio.Stage
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Request para guardar una etapa como correcta
    /// </summary>
    public class SaveCorrectStageRequest
    {
        /// <summary>
        /// Id de Usuario
        /// </summary>
        [Required]
        public int UserId { get; set; }

        /// <summary>
        /// Id de etapa
        /// </summary>
        [Required]
        public int StageId { get; set; }

        /// <summary>
        /// Id de Nivel
        /// </summary>
        [Required]
        public int LevelId { get; set; }

        /// <summary>
        /// Número de intentos para llegar al objetivo
        /// </summary>
        [Required]
        public int NumberOfAttempts { get; set; }

        /// <summary>
        /// Fecha actual
        /// </summary>
        public DateTime FinishDate { get; set; }
    }
}
