﻿namespace TEA.Entidades.Negocio.Stage
{
    using System.Collections.Generic;
    public class GetStagesResponse
    {
        /// <summary>
        /// Etapas
        /// </summary>
        public List<Stage> Stages { get; set; }

        /// <summary>
        /// Cantidad de registros totales
        /// </summary>
        public int RecordsTotal { get; set; }
    }
}
