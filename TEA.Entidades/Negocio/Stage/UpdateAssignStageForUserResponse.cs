﻿namespace TEA.Entidades.Negocio.Stage
{
    /// <summary>
    /// Response para reasignar etapas de un usuario
    /// </summary>
    public class UpdateAssignStageForUserResponse
    {
        /// <summary>
        /// Validate
        /// </summary>
        public ValidateResponse ValidateResponse { get; set; }
    }
}
