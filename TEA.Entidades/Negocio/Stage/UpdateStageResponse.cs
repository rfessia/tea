﻿namespace TEA.Entidades.Negocio.Stage
{
    /// <summary>
    /// Response para actualizar una etapa
    /// </summary>
    public class UpdateStageResponse
    {
        /// <summary>
        /// Id de la etapa actualizada
        /// </summary>
        public int StageId { get; set; }

        /// <summary>
        /// Validate
        /// </summary>
        public ValidateResponse ValidateResponse { get; set; }
    }
}
