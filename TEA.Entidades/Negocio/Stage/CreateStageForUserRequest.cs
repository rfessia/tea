﻿namespace TEA.Entidades.Negocio.Stage
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Request para crear una etapa de un usuario
    /// </summary>
    public class CreateStageForUserRequest
    {
        /// <summary>
        /// Id Usuario
        /// </summary>
        [Required]
        public int UserId { get; set; }

        /// <summary>
        /// Nombre de la etapa
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Ordende la etapa
        /// </summary>
        [Required]
        public int Order { get; set; }

        /// <summary>
        /// Id del nivel
        /// </summary>
        [Required]
        public int LevelId { get; set; }

        /// <summary>
        /// Id Usuario profesional
        /// </summary>
        public int UserProfessionalId { get; set; }
    }
}
