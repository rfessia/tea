﻿namespace TEA.Entidades.Negocio.Stage
{
    /// <summary>
    /// Request para recuperar las etapas compartidas a asignar
    /// </summary>
    public class GetSharedStagesForAssignedRequest
    {
        /// <summary>
        /// Id usuario profesional
        /// </summary>
        public int UserProfessionalId { get; set; }
    }
}
