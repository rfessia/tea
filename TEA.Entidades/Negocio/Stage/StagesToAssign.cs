﻿using System.Collections.Generic;

namespace TEA.Entidades.Negocio.Stage
{
    /// <summary>
    /// Entidad para asignar niveles a un usuario
    /// </summary>
    public class StagesToAssign
    {
        /// <summary>
        /// Id del nivel
        /// </summary>
        public int LevelId { get; set; }

        /// <summary>
        /// Nombre del nivel
        /// </summary>
        public string LevelName { get; set; }

        /// <summary>
        /// String que representa un elemento html con la información de todas las etapas seleccionadas
        /// </summary>
        public List<int> StagesId { get; set; }
    }
}
