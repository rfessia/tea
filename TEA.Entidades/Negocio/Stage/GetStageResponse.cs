﻿namespace TEA.Entidades.Negocio.Stage
{
    public class GetStageResponse
    {
        /// <summary>
        /// Etapa
        /// </summary>
        public Stage Stage { get; set; }

        /// <summary>
        /// Orden de la Etapa
        /// </summary>
        public int OrderStage { get; set; }
    }
}
