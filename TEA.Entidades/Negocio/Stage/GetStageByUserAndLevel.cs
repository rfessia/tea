﻿namespace TEA.Entidades.Negocio.Stage
{
    public class GetStageByUserAndLevel
    {
        /// <summary>
        /// Id del nivel
        /// </summary>
        public int LevelId { get; set; }

        /// <summary>
        /// Id de la etapa
        /// </summary>
        public int StageId { get; set; }

        /// <summary>
        /// Orden
        /// </summary>
        public int Order { get; set; }
    }
}
