﻿namespace TEA.Entidades.Negocio.Stage
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Request para actualizar la etapa de un usuario en particular
    /// </summary>
    public class UpdateStageForUserRequest
    {
        /// <summary>
        /// Id Usuario
        /// </summary>
        [Required]
        public int UserId { get; set; }

        /// <summary>
        /// Id de la etapa
        /// </summary>
        [Required]
        public int StageId { get; set; }

        /// <summary>
        /// Nombre de la etapa
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Ordende la etapa
        /// </summary>
        [Required]
        public int Order { get; set; }

        /// <summary>
        /// Id del nivel
        /// </summary>
        [Required]
        public int LevelId { get; set; }

        /// <summary>
        /// Fecha de modificación (en el create se deja nula)
        /// </summary>
        public DateTime? ModificationDate { get; set; }

        /// <summary>
        /// Id usuario profesional
        /// </summary>
        public int UserProfessionalId { get; set; }
    }
}
