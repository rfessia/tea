﻿namespace TEA.Entidades.Negocio.Stage
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Request para crear una etapa compartida
    /// </summary>
    public class CreateSharedStageRequest
    {
        /// <summary>
        /// Nombre de la etapa
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Id usuario Profesional
        /// </summary>
        public int UserProfessionalId { get; set; }
    }
}
