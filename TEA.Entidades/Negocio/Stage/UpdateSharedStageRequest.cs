﻿namespace TEA.Entidades.Negocio.Stage
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Request para actualizar una etapa compartida
    /// </summary>
    public class UpdateSharedStageRequest
    {
        /// <summary>
        /// Id de la etapa
        /// </summary>
        [Required]
        public int StageId { get; set; }

        /// <summary>
        /// Nombre de la etapa
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Fecha de modificación (en el create se deja nula)
        /// </summary>
        public DateTime? ModificationDate { get; set; }

        /// <summary>
        /// Id usuario profesional
        /// </summary>
        public int UserProfessionalId { get; set; }
    }
}
