﻿namespace TEA.Entidades.Negocio.Stage
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Request para actualizar una etapa
    /// </summary>
    public class UpdateStageRequest
    {
        /// <summary>
        /// Id de la etapa
        /// </summary>
        [Required]
        public int StageId { get; set; }

        /// <summary>
        /// Nomre
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Orden
        /// </summary>
        [Required]
        public int Order { get; set; }

        /// <summary>
        /// Id del nivel
        /// </summary>
        [Required]
        public int LevelId { get; set; }
    }
}
