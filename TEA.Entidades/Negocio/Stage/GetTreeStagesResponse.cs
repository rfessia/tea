﻿namespace TEA.Entidades.Negocio.Stage
{
    /// <summary>
    /// Response para obtener el arbol de etapas
    /// </summary>
    public class GetTreeStagesResponse
    {
        /// <summary>
        /// Arbol en formato json
        /// </summary>
        public string TreeStageJson { get; set; }
    }
}
