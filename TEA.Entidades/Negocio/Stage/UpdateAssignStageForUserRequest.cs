﻿namespace TEA.Entidades.Negocio.Stage
{
    using System.Collections.Generic;

    /// <summary>
    /// Request para reasignar etapas de un usuario
    /// </summary>
    public class UpdateAssignStageForUserRequest
    {
        /// <summary>
        /// Id del usuario
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Lista de etapas a asignar
        /// </summary>
        public List<StagesToAssign> StagesToAssign { get; set; }
    }
}
