﻿namespace TEA.Entidades.Negocio.Stage
{
    using System.Collections.Generic;

    public class GetStagesByUserAndLevelResponse
    {
        /// <summary>
        /// Lista de estapas
        /// </summary>
        public List<GetStageByUserAndLevel> Stages { get; set; }
    }
}
