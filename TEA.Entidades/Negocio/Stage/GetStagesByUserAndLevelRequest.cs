﻿namespace TEA.Entidades.Negocio.Stage
{
    public class GetStagesByUserAndLevelRequest
    {
        /// <summary>
        /// Id Usuario
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Id Nivel
        /// </summary>
        public int LevelId { get; set; }
    }
}
