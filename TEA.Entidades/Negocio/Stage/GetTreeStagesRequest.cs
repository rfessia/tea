﻿namespace TEA.Entidades.Negocio.Stage
{
    /// <summary>
    /// Request para obtener el arbol de etapas
    /// </summary>
    public class GetTreeStagesRequest
    {
        /// <summary>
        /// Id del usuario
        /// </summary>
        public int UserId { get; set; }
    }
}
