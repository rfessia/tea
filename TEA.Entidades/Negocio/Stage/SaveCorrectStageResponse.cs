﻿namespace TEA.Entidades.Negocio.Stage
{
    /// <summary>
    /// Response para guardar una etapa como correcta
    /// </summary>
    public class SaveCorrectStageResponse
    {
        public ValidateResponse Validate { get; set; }
    }
}
