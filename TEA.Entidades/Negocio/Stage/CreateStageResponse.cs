﻿namespace TEA.Entidades.Negocio.Stage
{
    /// <summary>
    /// Response para crear una etapa
    /// </summary>
    public class CreateStageResponse
    {
        /// <summary>
        /// Id de la etapa creada
        /// </summary>
        public int? StageId { get; set; }

        /// <summary>
        /// Validación
        /// </summary>
        public ValidateResponse ValidateResponse { get; set; }
    }
}
