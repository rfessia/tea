﻿namespace TEA.Entidades.Negocio.Role
{
    using System.Collections.Generic;

    /// <summary>
    /// Response para recuperar roles
    /// </summary>
    public class GetRolesResponse
    {
        /// <summary>
        /// Lista de roles
        /// </summary>
        public IList<Role> Roles { get; set; }
    }
}
