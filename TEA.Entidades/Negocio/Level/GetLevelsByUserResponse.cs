﻿namespace TEA.Entidades.Negocio.Level
{
    using System.Collections.Generic;

    /// <summary>
    /// Response para obtener los niveles por usuario
    /// </summary>
    public class GetLevelsByUserResponse
    {
        /// <summary>
        /// Lista de todos los niveles
        /// </summary>
        public List<GetLevelByUser> Levels { get; set; }
    }
}
