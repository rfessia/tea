﻿namespace TEA.Entidades.Negocio.Level
{
    public class GetLevelsRequest
    {
        /// <summary>
        /// Orden
        /// </summary>
        public string Order { get; set; }

        /// <summary>
        /// Id diagnostico
        /// </summary>
        public int DiagnosticId { get; set; }

        /// <summary>
        /// Id de Usuario
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// Usuario profesional
        /// </summary>
        public int? UserProfessionalId { get; set; }

        /// <summary>
        /// Filtro tipo diagnostico
        /// </summary>
        public int? FilterDiagnosticTypeId { get; set; }

        /// <summary>
        /// Filtro diagnostico
        /// </summary>
        public int? FilterDiagnosticId { get; set; }

        /// <summary>
        /// Filtro nivel
        /// </summary>
        public int? FilterLevelId { get; set; }

        /// <summary>
        /// Filtro por nombre de nivel
        /// </summary>
        public string FilterLevelName { get; set; }

        /// <summary>
        /// Filtro por nombre de diagnostico
        /// </summary>
        public string FilterDiagnosticName { get; set; }

        /// <summary>
        /// Filtro si está habilitado o no
        /// </summary>
        public bool? FilterEnabledLevel { get; set; }

        /// <summary>
        /// Salto de pagina
        /// </summary>
        public int? Skip { get; set; } = 0;

        /// <summary>
        /// Tamaño de pagina
        /// </summary>
        public int? PageSize { get; set; }
    }
}
