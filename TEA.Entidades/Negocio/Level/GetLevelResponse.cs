﻿namespace TEA.Entidades.Negocio.Level
{
    public class GetLevelResponse
    {
        /// <summary>
        /// Nivel
        /// </summary>
        public Level Level { get; set; }
    }
}
