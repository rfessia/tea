﻿namespace TEA.Entidades.Negocio.Level
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Request para crear un nivel
    /// </summary>
    public class CreateLevelRequest
    {
        /// <summary>
        /// Nombre del nivel
        /// </summary>
        [Display(Name = "Nombre")]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Indica si está habilitado o no
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// Id del diagnóstico
        /// </summary>
        [Display(Name = "Diagnóstico")]
        [Required]
        public int DiagnosticId { get; set; }

        /// <summary>
        /// Orden del nivel
        /// </summary>
        [Display(Name = "Orden")]
        [Required]
        public int Order { get; set; }

        /// <summary>
        /// Eliminar en un futuro
        /// </summary>
        public int UserId { get; set; }
    }
}
