﻿namespace TEA.Entidades.Negocio.Level
{
    /// <summary>
    /// Un nivel para un usuario
    /// </summary>
    public class GetLevelByUser
    {
        /// <summary>
        /// Id del Nivel
        /// </summary>
        public int LevelId { get; set; }

        /// <summary>
        /// Nombre del Nivel
        /// </summary>
        public string LevelName { get; set; }

        /// <summary>
        /// Estado del nivel
        /// 0: Bloqueado
        /// 1: Activo
        /// </summary>
        public int State { get; set; }

        /// <summary>
        /// Nivel Actual
        /// </summary>
        public bool CurrentLevel { get; set; }

        /// <summary>
        /// Orden a mostrar
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Cantidad de etapas que tiene el nivel
        /// </summary>
        public int CountStages { get; set; }
    }
}
