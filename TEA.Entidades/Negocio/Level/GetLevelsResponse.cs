﻿namespace TEA.Entidades.Negocio.Level
{
    using System.Collections.Generic;

    public class GetLevelsResponse
    {
        /// <summary>
        /// Lista de Niveles
        /// </summary>
        public List<Level> Levels { get; set; }

        /// <summary>
        /// Cantidad de registros totales
        /// </summary>
        public int RecordsTotal { get; set; }
    }
}
