﻿namespace TEA.Entidades.Negocio.Level
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Level
    {
        /// <summary>
        /// Id del nivel
        /// </summary>
        public int LevelId { get; set; }

        /// <summary>
        /// Nombre del nivel
        /// </summary>
        [Display(Name = "Nombre")]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Indica si está habilitado o no
        /// </summary>
        [Display(Name = "Habilitado")]
        [Required]
        public bool Enabled { get; set; }

        /// <summary>
        /// Id del diagnóstico
        /// </summary>
        [Display(Name = "Diagnóstico")]
        [Required]
        public int DiagnosticId { get; set; }

        /// <summary>
        /// Nombre del diagnóstico
        /// </summary>
        public string DiagnosticName { get; set; }

        /// <summary>
        /// Fecha de eliminación
        /// </summary>
        public DateTime? CancellationDate { get; set; }

        /// <summary>
        /// Fecha de finalización
        /// </summary>
        public DateTime? FinishDate { get; set; }

        /// <summary>
        /// Orden del nivel
        /// </summary>
        [Display(Name = "Orden")]
        [Required]
        public int Order { get; set; }

        /// <summary>
        /// Id del usuario
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Nombre del usuarios asociado a un nivel
        /// </summary>
        public string UserName { get; set; }
    }
}
