﻿namespace TEA.Entidades.Negocio.Level
{
    /// <summary>
    /// Request para obtener los niveles por usuario
    /// </summary>
    public class GetLevelsByUserRequest
    {
        /// <summary>
        /// Id del Usuario
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Indica que viene desde el web api
        /// </summary>
        public bool FromApi { get; set; }
    }
}
