﻿namespace TEA.Entidades.Negocio.Level
{
    public class GetLevelRequest
    {
        /// <summary>
        /// Id del nivel
        /// </summary>
        public int LevelId { get; set; }

        /// <summary>
        /// Id usuario profesional
        /// </summary>
        public int UserProfessionalId { get; set; }
    }
}
