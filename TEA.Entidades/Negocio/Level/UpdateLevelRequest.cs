﻿namespace TEA.Entidades.Negocio.Level
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Request para actualizar un nivel
    /// </summary>
    public class UpdateLevelRequest
    {
        /// <summary>
        /// Id del nivel
        /// </summary>
        [Required]
        public int LevelId { get; set; }

        /// <summary>
        /// Nombre del nivel
        /// </summary>
        [Display(Name = "Nombre")]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Indica si está habilitado o no
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// Id del diagnóstico
        /// </summary>
        [Display(Name = "Diagnóstico")]
        [Required]
        public int DiagnosticId { get; set; }

        /// <summary>
        /// Orden del nivel
        /// </summary>
        [Display(Name = "Orden")]
        [Required]
        public int Order { get; set; }

        /// <summary>
        /// Fecha de finalización
        /// </summary>
        public DateTime? FinishDate { get; set; }

        /// <summary>
        /// Fecha de eliminación
        /// </summary>
        public DateTime? CancellationDate { get; set; }
    }
}
