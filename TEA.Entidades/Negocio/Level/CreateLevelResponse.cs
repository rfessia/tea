﻿namespace TEA.Entidades.Negocio.Level
{
    /// <summary>
    /// Response para crear un nivel
    /// </summary>
    public class CreateLevelResponse
    {
        /// <summary>
        /// Validación
        /// </summary>
        public ValidateResponse ValidateResponse { get; set; }
    }
}
