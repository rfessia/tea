﻿namespace TEA.Entidades.Negocio.Level
{
    /// <summary>
    /// Response para actualizar un nivel
    /// </summary>
    public class UpdateLevelResponse
    {
        /// <summary>
        /// Validación
        /// </summary>
        public ValidateResponse ValidateResponse { get; set; }
    }
}
