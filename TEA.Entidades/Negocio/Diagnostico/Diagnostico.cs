﻿namespace TEA.Entidades.Negocio.Diagnostico
{
    using System.ComponentModel.DataAnnotations;
    using System.Collections.Generic;
    using TEA.Entidades.Negocio.Usuario;
    public class Diagnostico
    {
        [Required]
        public int Id { get; set; }

        [Display(Name = "Nombre")]
        [Required]
        public string Nombre { get; set; }

        [Display(Name = "Descripción")]
        public string Descripcion { get; set; }

        [Display(Name = "Tipo de Diagnóstico")]
        public int IdTipoDiagnostico { get; set; }

        [Display(Name = "Tipo de Diagnóstico")]
        public string NombreTipoDiagnostico { get; set; }

        public bool Eliminado { get; set; }

        [Required]
        public int? IdUsuarioProfesional { get; set; }

        public List<Usuario> Usuarios { get; set; }
    }
}
