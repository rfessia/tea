﻿namespace TEA.Entidades.Negocio.Diagnostico
{
    public class ObtenerDiagnosticosRequest
    {
        public int IdDiagnostico { get; set; }
        public string Orden { get; set; }
        public int IdNivel { get; set; }
        public string FilteredText { get; set; }
        public int IdUsuario { get; set; }
        public int? FiltroIdTipoDiagnostico { get; set; }
        public int? FiltroIdDiagnostico { get; set; }
    }
}
