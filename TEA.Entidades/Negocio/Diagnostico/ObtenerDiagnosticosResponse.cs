﻿namespace TEA.Entidades.Negocio.Diagnostico
{
    using System.Collections.Generic;
    public class ObtenerDiagnosticosResponse
    {
        public List<Diagnostico> Diagnosticos { get; set; }
        public Diagnostico Diagnostico { get; set; }
    }
}
