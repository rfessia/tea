﻿namespace TEA.Entidades.Negocio.Diagnostico
{
    public class EliminarDiagnosticoRequest
    {
        public int IdDiagnostico { get; set; }
    }
}
