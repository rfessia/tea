﻿namespace TEA.Entidades.Negocio.Diagnostico
{
    public class ObtenerDiagnosticoResponse
    {
        public Diagnostico Diagnostico { get; set; }
    }
}
