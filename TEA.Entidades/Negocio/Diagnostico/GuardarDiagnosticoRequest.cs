﻿namespace TEA.Entidades.Negocio.Diagnostico
{
    using System.Collections.Generic;
    public class GuardarDiagnosticoRequest
    {
        public Diagnostico Diagnostico { get; set; }
        public List<int> IdUsuarios { get; set; }
        public int IdDiagnostico { get; set; }
    }
}
