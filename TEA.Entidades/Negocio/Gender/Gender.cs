﻿namespace TEA.Entidades.Negocio.Gender
{
    using System.ComponentModel.DataAnnotations;

    public class Gender
    {
        [Required]
        public int GenderId { get; set; }

        [Required]
        [MaxLength(150)]
        public string Name { get; set; }
    }
}
