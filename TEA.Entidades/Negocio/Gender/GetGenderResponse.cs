﻿namespace TEA.Entidades.Negocio.Gender
{
    using System.Collections.Generic;

    /// <summary>
    /// Response para recuperar todos los géneros
    /// </summary>
    public class GetGenderResponse
    {
        /// <summary>
        /// Géneros
        /// </summary>
        public IList<Gender> Genders { get; set; }
    }
}
