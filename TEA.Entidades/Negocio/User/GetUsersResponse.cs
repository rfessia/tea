﻿namespace TEA.Entidades.Negocio.User
{
    using System.Collections.Generic;

    /// <summary>
    /// Response para obtener usuarios
    /// </summary>
    public class GetUsersResponse
    {
        /// <summary>
        /// Lista de usuarios
        /// </summary>
        public List<User> Users { get; set; }

        /// <summary>
        /// Id de usuario
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Id de usuarios
        /// </summary>
        public List<int> UsersId { get; set; }

        /// <summary>
        /// Cantidad de registros totales
        /// </summary>
        public int RecordsTotal { get; set; }
    }
}
