﻿namespace TEA.Entidades.Negocio.User
{
    public class GetUserResponse
    {
        /// <summary>
        /// Usuario
        /// </summary>
        public User User { get; set; }
    }
}
