﻿namespace TEA.Entidades.Negocio.User
{
    /// <summary>
    /// Request para obtener los usuarios de un diagnositco
    /// </summary>
    public class GetUsersByDiagnosticRequest
    {
        public int DiagnosticId { get; set; }
    }
}
