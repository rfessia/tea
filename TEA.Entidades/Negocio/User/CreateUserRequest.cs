﻿namespace TEA.Entidades.Negocio.User
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Request para crear una persona
    /// </summary>
    public class CreateUserRequest
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string UserName { get; set; }

        [MinLength(6)]
        [Required]
        public string Password { get; set; }

        [Required]
        public string Name { get; set; }

        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        public DateTime? Birthdate { get; set; }

        [Required]
        public int? DocumentNumber { get; set; }

        [Required]
        public int GenderId { get; set; }

        public int? TopUserId { get; set; }
    }
}
