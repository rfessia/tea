﻿namespace TEA.Entidades.Negocio.User
{

    /// <summary>
    /// Request para eliminar un usuario
    /// </summary>
    public class DeleteUserRequest
    {
        /// <summary>
        /// Id usuario
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Id Usuario profesional
        /// </summary>
        public int UserProfessionalId { get; set; }
    }
}
