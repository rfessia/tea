﻿namespace TEA.Entidades.Negocio.User
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Request para registrar un usuario
    /// </summary>
    public class RegisterUserRequest
    {

        [Required]
        public string UserName { get; set; }

        [MinLength(6)]
        [Required]
        public string Password { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Name { get; set; }

        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        public DateTime? Birthdate { get; set; }

        [Required]
        public int? DocumentNumber { get; set; }

        [Required]
        public int GenderId { get; set; }

        [Required]
        public string Token { get; set; }

        [Required]
        public DateTime InputDate { get; set; }

        [Required]
        public DateTime CancellationDate { get; set; }
    }
}
