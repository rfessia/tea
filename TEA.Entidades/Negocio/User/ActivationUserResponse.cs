﻿namespace TEA.Entidades.Negocio.User
{
    /// <summary>
    /// Response para activar un usuario
    /// </summary>
    public class ActivationUserResponse
    {
        /// <summary>
        /// Validación
        /// </summary>
        public ValidateResponse ValidarResponse { get; set; }
    }
}
