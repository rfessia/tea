﻿namespace TEA.Entidades.Negocio.User
{
    /// <summary>
    /// Response para eliminar un usuario
    /// </summary>
    public class DeleteUserResponse
    {
        /// <summary>
        /// Validación
        /// </summary>
        public ValidateResponse ValidarResponse { get; set; }
    }
}
