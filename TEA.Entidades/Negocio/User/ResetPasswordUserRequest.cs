﻿namespace TEA.Entidades.Negocio.User
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Request para resetea la contraseña
    /// </summary>
    public class ResetPasswordUserRequest
    {
        [Required]
        public int UserId { get; set; }

        [MinLength(6)]
        [Required]
        public string Password { get; set; }

        [Required]
        public int UserProfessionalId { get; set; }
    }
}
