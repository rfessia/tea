﻿namespace TEA.Entidades.Negocio.User
{
    using Role;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using TEA.Entidades.Negocio.Gender;

    public class User
    {
        [Required]
        public int UserId { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string RePassword { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public DateTime Birthdate { get; set; }

        [Required]
        public int DocumentNumber { get; set; }

        [Required]
        public int GenderId { get; set; }

        public DateTime InputDate { get; set; }

        public DateTime? ModificationDate { get; set; }

        public DateTime? CancellationDate { get; set; }

        public int? TopUserId { get; set; }

        public Gender Gender { get; set; }

        public IList<Role> Roles { get; set; }
    }
}
