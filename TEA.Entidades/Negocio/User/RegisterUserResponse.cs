﻿namespace TEA.Entidades.Negocio.User
{
    /// <summary>
    /// Response para registrar un usuario
    /// </summary>
    public class RegisterUserResponse
    {
        /// <summary>
        /// Id Usuario
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Validación
        /// </summary>
        public ValidateResponse ValidarResponse { get; set; }
    }
}
