﻿namespace TEA.Entidades.Negocio.User
{
    /// <summary>
    /// Response para obtener usuario por Id
    /// </summary>
    public class GetUserByIdResponse
    {
        /// <summary>
        /// Usuario
        /// </summary>
        public User User { get; set; }
    }
}
