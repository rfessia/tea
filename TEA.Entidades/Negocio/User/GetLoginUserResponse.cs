﻿namespace TEA.Entidades.Negocio.User
{
    /// <summary>
    /// Response para validar login
    /// </summary>
    public class GetLoginUserResponse
    {
        /// <summary>
        /// Usuario
        /// </summary>
        public User User { get; set; }
    }
}
