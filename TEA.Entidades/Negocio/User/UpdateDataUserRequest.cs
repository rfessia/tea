﻿namespace TEA.Entidades.Negocio.User
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Request para actualizar una persona
    /// </summary>
    public class UpdateDataUserRequest
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public DateTime Birthdate { get; set; }

        [Required]
        public int DocumentNumber { get; set; }

        [Required]
        public int GenderId { get; set; }
    }
}
