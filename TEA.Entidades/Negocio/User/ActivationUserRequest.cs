﻿namespace TEA.Entidades.Negocio.User
{
    /// <summary>
    /// Request para activar un usuario
    /// </summary>
    public class ActivationUserRequest
    {
        /// <summary>
        /// Id del usuario
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Token de activación
        /// </summary>
        public string Token { get; set; }
    }
}
