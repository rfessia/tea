﻿using System.Collections.Generic;

namespace TEA.Entidades.Negocio.User
{
    /// <summary>
    /// Response para obtener los usuarios de un diagnositco
    /// </summary>
    public class GetUsersByDiagnosticResponse
    {
        /// <summary>
        /// Lista de usuarios de un diagnostico
        /// </summary>
        public User User { get; set; }
    }
}
