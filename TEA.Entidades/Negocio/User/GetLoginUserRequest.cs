﻿namespace TEA.Entidades.Negocio.User
{
    /// <summary>
    /// Request para validar login
    /// </summary>
    public class GetLoginUserRequest
    {
        /// <summary>
        /// Nombre de usuario
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Contraseña
        /// </summary>
        public string Password { get; set; }
    }
}
