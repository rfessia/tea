﻿namespace TEA.Entidades.Negocio.User
{
    public class GetUserRequest
    {
        /// <summary>
        /// Nombre de usuario
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Id de Usuario
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Contraseña
        /// </summary>
        public string UserPassword { get; set; }

        /// <summary>
        /// Id usuario profesional
        /// </summary>
        public int UserProfessionalId { get; set; }
    }
}
