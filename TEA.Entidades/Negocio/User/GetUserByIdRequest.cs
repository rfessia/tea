﻿namespace TEA.Entidades.Negocio.User
{
    /// <summary>
    /// Request para obtener usuario por Id
    /// </summary>
    public class GetUserByIdRequest
    {
        /// <summary>
        /// Id usuario
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Id usuario profesional
        /// </summary>
        public int? UserProfessionalId { get; set; }
    }
}
