﻿namespace TEA.Entidades.Negocio.User
{
    /// <summary>
    /// Request para obtener usuario
    /// </summary>
    public class GetUsersRequest
    {
        /// <summary>
        /// Orden
        /// </summary>
        public string Order { get; set; }

        /// <summary>
        /// Id de diagnostico
        /// </summary>
        public int DiagnosticId { get; set; }

        /// <summary>
        /// Id de usuario padre
        /// </summary>
        public int? TopUserId { get; set; }

        /// <summary>
        /// Filtro por nombre de usuario
        /// </summary>
        public string FilterUserName { get; set; }

        /// <summary>
        /// Filtro por Nombre completo
        /// </summary>
        public string FilterFullName { get; set; }

        /// <summary>
        /// Filtro por número de Documento
        /// </summary>
        public string FilterDocumentNumber { get; set; }

        /// <summary>
        /// Salto de pagina
        /// </summary>
        public int? Skip { get; set; } = 0;

        /// <summary>
        /// Tamaño de pagina
        /// </summary>
        public int? PageSize { get; set; }
    }
}
