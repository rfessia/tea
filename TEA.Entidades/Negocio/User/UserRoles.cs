﻿namespace TEA.Entidades.Negocio.User
{
    public static class UserRoles
    {
        public const string Administrador = "Administrador";
        public const string Usuario = "Usuario";
    }
}
