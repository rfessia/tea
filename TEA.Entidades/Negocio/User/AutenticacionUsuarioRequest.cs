﻿namespace TEA.Entidades.Negocio.User
{
    public class AutenticacionUsuarioRequest
    {
        /// <summary>
        /// Usurario
        /// </summary>
        public User Usuario { get; set; }

        /// <summary>
        /// Recuérdame
        /// </summary>
        public bool RememberMe { get; set; }
    }
}
