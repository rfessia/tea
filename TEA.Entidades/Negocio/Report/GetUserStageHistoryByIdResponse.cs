﻿namespace TEA.Entidades.Negocio.Report
{
    /// <summary>
    /// Response para un registro historico
    /// </summary>
    public class GetUserStageHistoryByIdResponse
    {
        /// <summary>
        /// registro histórico
        /// </summary>
        public UserStageHistory UsersStagesHistory { get; set; }
    }
}
