﻿namespace TEA.Entidades.Negocio.Report
{
    using System;

    /// <summary>
    /// Entidad que representa el historial de las etapas/usuarios
    /// </summary>
    public class UserStageHistory
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Id de Usuario
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Nombre del Usuario profesional
        /// </summary>
        public string UserProfessionalName { get; set; }

        /// <summary>
        /// Nombre del diagnóstico
        /// </summary>
        public string DiagnosticName { get; set; }

        /// <summary>
        /// Nombre de la etapa
        /// </summary>
        public string StageName { get; set; }

        /// <summary>
        /// Nombre del nivel
        /// </summary>
        public string LevelName { get; set; }

        /// <summary>
        /// Número de intentos para llegar al objetivo
        /// </summary>
        public int NumberOfAttempts { get; set; }

        /// <summary>
        /// Fecha de finalización
        /// </summary>
        public DateTime FinishDate { get; set; }

        /// <summary>
        /// Indica si es una etapa compartida o no
        /// </summary>
        public bool StageShared { get; set; }
    }
}
