﻿namespace TEA.Entidades.Negocio.Report
{
    using System.Collections.Generic;
    
    /// <summary>
    /// Response para obtener registros históricos
    /// </summary>
    public class GetUserStageHistoryResponse
    {
        /// <summary>
        /// Lista de registros históricos
        /// </summary>
        public List<UserStageHistory> UsersStagesHistories { get; set; }

        /// <summary>
        /// Cantidad de registros totales
        /// </summary>
        public int RecordsTotal { get; set; }
    }
}
