﻿namespace TEA.Entidades.Negocio.Report
{
    /// <summary>
    /// Request para un registro historico
    /// </summary>
    public class GetUserStageHistoryByIdRequest
    {
        /// <summary>
        /// Id para filtrar por historial
        /// </summary>
        public int UserStageHistoryId { get; set; }
    }
}
