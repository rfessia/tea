﻿namespace TEA.Entidades.Negocio.Nivel
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System;
    using System.Linq;

    public class Nivel
    {
        public int Id { get; set; }

        [Display(Name = "Nombre")]
        [Required]
        public string Nombre { get; set; }

        [Display(Name = "Diagnóstico")]
        [Required]
        public int IdDiagnostico { get; set; }

        public string NombreDiagnostico { get; set; }
        
        public bool Eliminado { get; set; }

        public bool Completo { get; set; }

        public bool Actual { get; set; }

        [Display(Name = "Orden")]
        [Required]
        public int Orden { get; set; }

        public int IdTipoDiagnostico { get; set; }
     }
}
