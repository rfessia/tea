﻿namespace TEA.Entidades.Negocio.Nivel
{
    public class ObtenerNivelesRequest
    {
        public string Orden { get; set; }

        public int IdDiagnostico { get; set; }

        public int IdUsuario { get; set; }

        public int? FiltroIdTipoDiagnostico { get; set; }
        public int? FiltroIdDiagnostico { get; set; }
        public int? FiltroIdNivel { get; set; }
    }
}
