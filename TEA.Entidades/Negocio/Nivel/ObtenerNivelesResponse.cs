﻿namespace TEA.Entidades.Negocio.Nivel
{
    using System.Collections.Generic;

    public class ObtenerNivelesResponse
    {
        public List<Nivel> Niveles { get; set; }
    }
}
