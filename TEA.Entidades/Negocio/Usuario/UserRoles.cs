﻿namespace TEA.Entidades.Negocio.Usuario
{
    public static class UserRoles
    {
        public const string Administrador = "Administrador";
        public const string Usuario = "Usuario";
    }
}
