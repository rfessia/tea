﻿namespace TEA.Entidades.Negocio.Usuario
{
    using System.Collections.Generic;
    public class ObtenerUsuariosResponse
    {
        public List<Usuario> Usuarios { get; set; }
        public List<int> IdUsuarios { get; set; }
    }
}
