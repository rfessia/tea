﻿namespace TEA.Entidades.Negocio.Usuario
{
    public class ObtenerUsuarioRequest
    {
        public string UserName { get; set; }
        public int IdUsuario { get; set; }
    }
}
