﻿namespace TEA.Entidades.Negocio.Usuario
{
    public class AutenticacionUsuarioRequest
    {
        public Usuario Usuario { get; set; }
        public bool rememberMe { get; set; }
    }
}
