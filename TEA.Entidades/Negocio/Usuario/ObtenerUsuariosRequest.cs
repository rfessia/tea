﻿namespace TEA.Entidades.Negocio.Usuario
{
    public class ObtenerUsuariosRequest
    {
        public string Orden { get; set; }
        public int IdDiagnostico { get; set; }
        public int? IdUsuarioPadre { get; set; }
    }
}
