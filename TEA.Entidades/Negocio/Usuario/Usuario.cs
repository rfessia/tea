﻿namespace TEA.Entidades.Negocio.Usuario
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Usuario
    {
        [Required]
        public int Id { get; set; }

        [Display(Name = "Nombre de Usuario")]
        [Required]
        public string UserName { get; set; }

        [Display(Name = "Contraseña")]
        [Required]
        public string Password { get; set; }

        [Display(Name = "Repita contraseña")]
        [Required]
        public string RePassword { get; set; }

        [Display(Name = "Nombre")]
        [Required]
        public string Nombre { get; set; }

        [Display(Name = "Fecha de Nacimiento")]
        [Required]
        public DateTime FechaNacimiento { get; set; }

        [Display(Name = "DNI")]
        [Required]
        public int? DNI { get; set; }

        [Display(Name = "Es Administrador")]
        public bool EsAdmin { get; set; }
        
        [Display(Name = "Es Usuario")]
        public bool EsUsuario { get; set; }

        public bool Eliminado { get; set; }

        public int? IdUsuarioPadre { get; set; }
    }
}
