﻿namespace TEA.Entidades.Negocio.Diagnostic
{
    public class GetDiagnosticsRequest
    {
        /// <summary>
        /// Id diagnostico
        /// </summary>
        public int DiagnosticId { get; set; }

        /// <summary>
        /// Orden
        /// </summary>
        public string Order { get; set; }

        /// <summary>
        /// Id del Nivel
        /// </summary>
        public int LevelId { get; set; }

        /// <summary>
        /// Filtro por Diagnostico
        /// </summary>
        public string FilterDiagnostic { get; set; }

        /// <summary>
        /// Filtro por Tipo de Diagnostico
        /// </summary>
        public int? FilterDiagnosticTypeId { get; set; }

        /// <summary>
        /// Filtro por Diagnóstico
        /// </summary>
        public int? FilterDiagnosticId { get; set; }

        /// <summary>
        /// Id del Usuario
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Id del Usuario Profesional
        /// </summary>
        public int? UserProfessionalId { get; set; }

        /// <summary>
        /// Salto de pagina
        /// </summary>
        public int? Skip { get; set; } = 0;

        /// <summary>
        /// Tamaño de pagina
        /// </summary>
        public int? PageSize { get; set; }
    }
}
