﻿namespace TEA.Entidades.Negocio.Diagnostic
{
    public class CreateDiagnosticResponse
    {
        /// <summary>
        /// Id Diagnostico
        /// </summary>
        public int DiagnositcId { get; set; }

        /// <summary>
        /// Validación
        /// </summary>
        public ValidateResponse ValidateResponse { get; set; }
    }
}
