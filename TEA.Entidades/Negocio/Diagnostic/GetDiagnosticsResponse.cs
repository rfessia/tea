﻿namespace TEA.Entidades.Negocio.Diagnostic
{
    using System.Collections.Generic;
    public class GetDiagnosticsResponse
    {
        /// <summary>
        /// Lista de Diagnosticos
        /// </summary>
        public List<Diagnostic> Diagnostics { get; set; }

        /// <summary>
        /// Cantidad de registros totales
        /// </summary>
        public int RecordsTotal { get; set; }
    }
}
