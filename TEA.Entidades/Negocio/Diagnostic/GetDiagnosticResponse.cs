﻿namespace TEA.Entidades.Negocio.Diagnostic
{
    public class GetDiagnosticResponse
    {
        /// <summary>
        /// Diagnostico
        /// </summary>
        public Diagnostic Diagnostic { get; set; }
    }
}
