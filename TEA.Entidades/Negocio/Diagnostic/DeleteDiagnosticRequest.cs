﻿namespace TEA.Entidades.Negocio.Diagnostic
{
    public class DeleteDiagnosticRequest
    {
        /// <summary>
        /// Id diagnostico
        /// </summary>
        public int DiagnosticId { get; set; }

        /// <summary>
        /// Id del usuario profesional
        /// </summary>
        public int UserProfessionalId { get; set; }
    }
}
