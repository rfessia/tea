﻿namespace TEA.Entidades.Negocio.Diagnostic
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using TEA.Entidades.Negocio.User;

    public class Diagnostic
    {
        /// <summary>
        /// Id del Diagnositco
        /// </summary>
        [Required]
        public int DiagnosticId { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Descripción
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// ID Tipo de Diagnostico
        /// </summary>
        public int DiagnosticTypeId { get; set; }

        /// <summary>
        /// Nombre del Tipo de Diagnostico
        /// </summary>
        public string DiagnosticTypeName { get; set; }

        /// <summary>
        /// Id Usuario Profesional
        /// </summary>
        [Required]
        public int UserProfessionalId { get; set; }

        /// <summary>
        /// Usuario
        /// </summary>
        public User User { get; set; }
    }
}
