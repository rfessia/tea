﻿namespace TEA.Entidades.Negocio.Diagnostic
{
    public class UpdateDiagnosticResponse
    {
        /// <summary>
        /// Validación
        /// </summary>
        public ValidateResponse ValidateResponse { get; set; }
    }
}
