﻿namespace TEA.Entidades.Negocio.Diagnostic
{
    using System.ComponentModel.DataAnnotations;

    public class CreateDiagnosticRequest
    {
        /// <summary>
        /// Id del Diagnositco
        /// </summary>
        [Required]
        public int DiagnosticId { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Descripción
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// ID Tipo de Diagnostico
        /// </summary>
        [Required]
        public int DiagnosticTypeId { get; set; }

        /// <summary>
        /// Id Usuario Profesional
        /// </summary>
        [Required]
        public int UserProfessionalId { get; set; }

        /// <summary>
        /// Id Usuario
        /// </summary>
        [Required]
        public int UserId { get; set; }
    }
}
