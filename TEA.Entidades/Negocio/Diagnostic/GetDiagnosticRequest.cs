﻿namespace TEA.Entidades.Negocio.Diagnostic
{
    public class GetDiagnosticRequest
    {
        /// <summary>
        /// Id diagnostico
        /// </summary>
        public int DiagnosticId { get; set; }

        /// <summary>
        /// Id Usuario Profesional
        /// </summary>
        public int UserProfessionalId { get; set; }
    }
}
