﻿namespace TEA.Entidades.Negocio.Respuestas
{
    /// <summary>
    /// Request para ejecutar sp UpdateAnswers
    /// </summary>
    public class UpdateAnswerWithTempAnswerRequest
    {
        /// <summary>
        /// Id de la etapa
        /// </summary>
        public int StageId { get; set; }

        /// <summary>
        /// Id de la respuesta correcta
        /// </summary>
        public int? AnswerCorrectIdAnswer { get; set; }

        /// <summary>
        /// Id de la respuesta de fondo
        /// </summary>
        public int? BackgroundIdAnswer { get; set; }

        /// <summary>
        /// Id de la respuesta temporal correcta
        /// </summary>
        public int? AnswerCorrectIdTmpAnswer { get; set; }

        /// <summary>
        /// Id de la respuesta temporal de fondo
        /// </summary>
        public int? BackgroundIdTmpAnswer { get; set; }
    }
}
