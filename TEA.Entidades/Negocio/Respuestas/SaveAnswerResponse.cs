﻿namespace TEA.Entidades.Negocio.Respuestas
{
    public class SaveAnswerResponse
    {
        public int AnswerId { get; set; }
        public ValidateResponse ValidarResponse { get; set; }
    }
}
