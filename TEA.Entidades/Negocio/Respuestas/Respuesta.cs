﻿namespace TEA.Entidades.Negocio.Respuestas
{
    using System.ComponentModel.DataAnnotations;

    public class Respuesta
    {
        [Required]
        public int Id { get; set; }

        [Display(Name = "Texto de Respuesta Correcta")]
        public string TextoCorrecto { get; set; }

        public string NombreImagen { get; set; }

        public bool Correcto { get; set; }

        public bool Fondo { get; set; }

        [Required]
        public int IdEtapa { get; set; }
    }
}
