﻿namespace TEA.Entidades.Negocio.Respuestas
{
    public class GetAnswerRequest
    {
        /// <summary>
        /// Id de respuesta
        /// </summary>
        public int AnswerId { get; set; }
    }
}
