﻿namespace TEA.Entidades.Negocio.Respuestas
{
    /// <summary>
    /// Request para eliminar registros de respuestas temporales
    /// </summary>
    public class DeleteTmpAnswersRequest
    {
        /// <summary>
        /// Id de Etapa
        /// </summary>
        public int StageId { get; set; }
    }
}
