﻿namespace TEA.Entidades.Negocio.Respuestas
{
    /// <summary>
    /// Response para crear una respuesta termporal
    /// </summary>
    public class CreateTmpAnswerResponse
    {
        /// <summary>
        /// Id de respuesta temporal
        /// </summary>
        public int TmpAnswerId { get; set; }

        /// <summary>
        /// Indice si es valido o no
        /// </summary>
        public bool IsValid { get; set; }

        /// <summary>
        /// Mensaje de error
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
