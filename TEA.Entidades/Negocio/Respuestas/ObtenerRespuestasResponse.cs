﻿namespace TEA.Entidades.Negocio.Respuestas
{
    using System.Collections.Generic;

    public class ObtenerRespuestasResponse
    {
        public List<Respuesta> Respuestas { get; set; }
    }
}
