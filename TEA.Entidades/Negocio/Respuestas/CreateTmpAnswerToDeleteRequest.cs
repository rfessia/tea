﻿namespace TEA.Entidades.Negocio.Respuestas
{
    /// <summary>
    /// Request para crear una respuesta temporal que se va a eliminar
    /// </summary>
    public class CreateTmpAnswerToDeleteRequest
    {
        /// <summary>
        /// Respuesta temporal a crear
        /// </summary>
        public int AnswerId { get; set; }
    }
}
