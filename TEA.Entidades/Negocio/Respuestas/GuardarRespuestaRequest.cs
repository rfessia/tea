﻿namespace TEA.Entidades.Negocio.Respuestas
{
    using System.ComponentModel.DataAnnotations;
    using TEA.Entidades.Negocio.Etapa;

    public class GuardarRespuestaRequest
    {
        public Respuesta Respuesta { get; set; }

        [Display(Name = "Etapa")]
        public string NombreEtapa { get; set; }

        [Display(Name = "Título")]
        public string TituloEtapa { get; set; }

        public int IdDiagnostico { get; set; }
        
        [Display(Name = "Diagnóstico")]
        public string NombreDiagnostico { get; set; }

        public int IdNivel { get; set; }

        [Display(Name = "Nivel")]
        public string NombreNivel { get; set; }

        public int IdFondo { get; set; }

        public int IdRespCorrecta { get; set; }

        public int IdEtapa { get; set; }

    }
}
