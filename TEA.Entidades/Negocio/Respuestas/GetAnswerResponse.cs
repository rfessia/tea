﻿namespace TEA.Entidades.Negocio.Respuestas
{
    public class GetAnswerResponse
    {
        /// <summary>
        /// Respuesta
        /// </summary>
        public Answer Answer { get; set; }
    }
}
