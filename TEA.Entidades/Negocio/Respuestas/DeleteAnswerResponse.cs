﻿namespace TEA.Entidades.Negocio.Respuestas
{
    /// <summary>
    /// Response para eliminar una etapa
    /// </summary>
    public class DeleteAnswerResponse
    {
        /// <summary>
        /// Id de la etapa
        /// </summary>
        public int? StageId { get; set; }

        /// <summary>
        /// Indica si es válido o no
        /// </summary>
        public ValidateResponse ValidateResponse { get; set; }

        /// <summary>
        /// Nombre de la imagen eliminada
        /// </summary>
        public string ImageName { get; set; }
    }
}
