﻿namespace TEA.Entidades.Negocio.Respuestas
{
    /// <summary>
    /// Request para crear una respuesta termporal
    /// </summary>
    public class CreateTmpAnswerRequest
    {
        /// <summary>
        /// Respuesta temporal a crear
        /// </summary>
        public TmpAnswer TmpAnswer { get; set; }
    }
}
