﻿namespace TEA.Entidades.Negocio.Respuestas
{
    using System.Collections.Generic;

    /// <summary>
    /// Response para recuperar las respuestas temporales
    /// </summary>
    public class GetTmpAnswersResponse
    {
        /// <summary>
        /// Respuestas temporales
        /// </summary>
        public List<TmpAnswer> TmpAnswers { get; set; }
    }
}
