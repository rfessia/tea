﻿namespace TEA.Entidades.Negocio.Respuestas
{
    using System.Collections.Generic;

    public class GetAnswersResponse
    {
        /// <summary>
        /// Respuestas
        /// </summary>
        public List<Answer> Answers { get; set; }
    }
}
