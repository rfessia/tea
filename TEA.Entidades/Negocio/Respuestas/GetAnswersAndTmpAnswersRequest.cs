﻿namespace TEA.Entidades.Negocio.Respuestas
{
    /// <summary>
    /// Request para recuperar las respuestas almacenadas y temporales
    /// </summary>
    public class GetAnswersAndTmpAnswersRequest
    {
        /// <summary>
        /// Id de la etapa
        /// </summary>
        public int StageId { get; set; }
    }
}
