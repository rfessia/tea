﻿namespace TEA.Entidades.Negocio.Respuestas
{
    /// <summary>
    /// Response para eliminar un archivo temporal
    /// </summary>
    public class DeleteTmpAnswerResponse
    {
        /// <summary>
        /// Indica si es válido o no
        /// </summary>
        public ValidateResponse ValidateResponse { get; set; }

        /// <summary>
        /// Nombre de la imagen eliminada
        /// </summary>
        public string ImageName { get; set; }
    }
}
