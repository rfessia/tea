﻿namespace TEA.Entidades.Negocio.Respuestas
{
    using System.ComponentModel.DataAnnotations;
    using TEA.Entidades.Negocio.Stage;

    public class SaveAnswerRequest
    {
        public Answer Answer { get; set; }

        [Display(Name = "Etapa")]
        public string StageName { get; set; }

        [Display(Name = "Título")]
        public string StageTitle { get; set; }

        public int DiagnosticId { get; set; }
        
        [Display(Name = "Diagnóstico")]
        public string DiagnosticName { get; set; }

        public int LevelId { get; set; }

        [Display(Name = "Actividad")]
        public string LevelName { get; set; }

        /// <summary>
        /// Id de la respuesta de fondo
        /// </summary>
        public int BackgroundId { get; set; }

        public int AnswerCorrectId { get; set; }

        public int StageId { get; set; }

    }
}
