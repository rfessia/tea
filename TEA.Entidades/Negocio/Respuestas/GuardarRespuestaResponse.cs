﻿namespace TEA.Entidades.Negocio.Respuestas
{
    public class GuardarRespuestaResponse
    {
        public int IdRespuesta { get; set; }
        public ValidarResponse ValidarResponse { get; set; }
    }
}
