﻿namespace TEA.Entidades.Negocio.Respuestas
{
    /// <summary>
    /// Request para eliminar una etapa
    /// </summary>
    public class DeleteAnswerRequest
    {
        /// <summary>
        /// Id de la etapa
        /// </summary>
        public int AnswerId { get; set; }
    }
}
