﻿namespace TEA.Entidades.Negocio.Respuestas
{
    public class GetAnswersRequest
    {
        /// <summary>
        /// Id Etapa
        /// </summary>
        public int StageId { get; set; }
    }
}
