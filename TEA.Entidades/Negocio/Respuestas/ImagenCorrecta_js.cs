﻿using System.Collections.Generic;
namespace TEA.Entidades.Negocio.Respuestas
{
    public class ImagenCorrecta_js
    {
        public string answerId { get; set; }
        public string stageId { get; set; }
        public bool isTmpAnswer { get; set; }
        public string answerIdCorrect { get; set; }
        public string idFondo { get; set; }
        public string imgUrl { get; set; }
        public string name { get; set; }
        public long size { get; set; }
        public string url { get; set; }
        public string thumbnailUrl { get; set; }
        public string deleteUrl { get; set; }
        public string deleteType { get; set; }
    }
}
