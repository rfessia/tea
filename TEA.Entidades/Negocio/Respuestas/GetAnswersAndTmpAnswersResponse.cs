﻿namespace TEA.Entidades.Negocio.Respuestas
{
    using System.Collections.Generic;

    /// <summary>
    /// Response para recuperar las respuestas almacenadas y temporales
    /// </summary>
    public class GetAnswersAndTmpAnswersResponse
    {
        /// <summary>
        /// Respuestas
        /// </summary>
        public List<AnswerGeneric> AnswersGeneric { get; set; }
    }
}
