﻿namespace TEA.Entidades.Negocio.Respuestas
{
    /// <summary>
    /// Response para crear una respuesta temporal que se va a eliminar
    /// </summary>
    public class CreateTmpAnswerToDeleteResponse
    {
        /// <summary>
        /// Indica si es válido o no
        /// </summary>
        public ValidateResponse ValidateResponse { get; set; }

        /// <summary>
        /// Id de respuesta temporal
        /// </summary>
        public int TmpAnswerId { get; set; }
    }
}
