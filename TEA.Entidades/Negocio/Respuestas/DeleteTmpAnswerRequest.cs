﻿namespace TEA.Entidades.Negocio.Respuestas
{
    /// <summary>
    /// Request para eliminar un archivo temporal
    /// </summary>
    public class DeleteTmpAnswerRequest
    {
        /// <summary>
        /// Id del archivo temporal
        /// </summary>
        public int TmpAnswerId { get; set; }
    }
}
