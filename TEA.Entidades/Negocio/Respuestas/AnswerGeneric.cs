﻿namespace TEA.Entidades.Negocio.Respuestas
{
    using System;

    /// <summary>
    /// Representa todas las respuestas, tanto temporales como no
    /// </summary>
    public class AnswerGeneric
    {
        /// <summary>
        /// Id de la respuesta
        /// </summary>
        public int? AnswerId { get; set; }

        /// <summary>
        /// Indica si es o no una respuesta temporal
        /// </summary>
        public bool IsTmpAnswer { get; set; }

        /// <summary>
        /// Id de la etapa
        /// </summary>
        public int StageId { get; set; }

        /// <summary>
        /// Texto correcto
        /// </summary>
        public string TextCorrect { get; set; }

        /// <summary>
        /// Nombre original de la imagen
        /// </summary>
        public string ImageOriginalName { get; set; }

        /// <summary>
        /// Nombre de la imagen
        /// </summary>
        public string ImageName { get; set; }

        /// <summary>
        /// Indica si es la respuesta correcta
        /// </summary>
        public bool Correct { get; set; }

        /// <summary>
        /// Indica si es una imagen de fondo
        /// </summary>
        public bool Background { get; set; }

        /// <summary>
        /// Fecha de baja
        /// </summary>
        public DateTime? CancellationDate { get; set; }
    }
}
