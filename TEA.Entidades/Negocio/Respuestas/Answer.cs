﻿namespace TEA.Entidades.Negocio.Respuestas
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Respuesta
    /// </summary>
    public class Answer
    {
        /// <summary>
        /// Id de la respuesta
        /// </summary>
        [Required]
        public int AnswerId { get; set; }

        /// <summary>
        /// Id de la etapa
        /// </summary>
        [Required]
        public int StageId { get; set; }

        /// <summary>
        /// Texto correcto
        /// </summary>
        [Display(Name = "Texto de Respuesta Correcta")]
        public string TextCorrect { get; set; }

        /// <summary>
        /// Nombre original de la imagen
        /// </summary>
        [Required]
        public string ImageOriginalName { get; set; }

        /// <summary>
        /// Nombre de la imagen
        /// </summary>
        [Required]
        public string ImageName { get; set; }

        /// <summary>
        /// Indica si es la respuesta correcta
        /// </summary>
        public bool Correct { get; set; }

        /// <summary>
        /// Indica si es una imagen de fondo
        /// </summary>
        public bool Background { get; set; }
    }
}
