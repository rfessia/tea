﻿namespace TEA.Entidades.Negocio.Respuestas
{
    /// <summary>
    /// Request para recuperar las respuestas temporales
    /// </summary>
    public class GetTmpAnswersRequest
    {
        /// <summary>
        /// Id de respuesta
        /// </summary>
        public int StageId { get; set; }

        /// <summary>
        /// Indica si fue o no cancelada la operación
        /// </summary>
        public bool IsCancel { get; set; }
    }
}
