﻿namespace TEA.Entidades.Negocio
{
    using System.Collections.Generic;
    public class ValidarResponse
    {
        public bool Valido { get; set; }
        public Dictionary<string, string> Errores { get; set; }
    }
}
