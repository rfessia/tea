﻿namespace TEA.Entidades.Negocio.TipoDiagnostico
{
    using System.Collections.Generic;
    public class ObtenerTipoDiagnosticosResponse
    {
        public List<TipoDiagnostico> TipoDiagnostico { get; set; }
    }
}
