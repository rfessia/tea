﻿namespace TEA.Entidades.Negocio.TipoDiagnostico
{
    using System.ComponentModel.DataAnnotations;
    public class TipoDiagnostico
    {
        public int Id { get; set; }

        [Display(Name = "Nombre")]
        public string Nombre { get; set; }
    }
}
