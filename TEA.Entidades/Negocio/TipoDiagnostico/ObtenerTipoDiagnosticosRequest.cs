﻿namespace TEA.Entidades.Negocio.TipoDiagnostico
{
    public class ObtenerTipoDiagnosticosRequest
    {
        public string Orden { get; set; }
        public int IdUsuario { get; set; }
    }
}
