﻿namespace TEA.Entidades.Negocio.DiagnosticType
{
    public class GetDiagnosticTypesRequest
    {
        /// <summary>
        /// Orden
        /// </summary>
        public string Order { get; set; }
        
        /// <summary>
        /// Id Usuario
        /// </summary>
        public int UserId { get; set; }
    }
}
