﻿namespace TEA.Entidades.Negocio.DiagnosticType
{
    using System.ComponentModel.DataAnnotations;
    public class DiagnosticType
    {
        /// <summary>
        /// Id
        /// </summary>
        public int DiagnosticTypeId { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Display(Name = "Nombre")]
        public string Name { get; set; }
    }
}
