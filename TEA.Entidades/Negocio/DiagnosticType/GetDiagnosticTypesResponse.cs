﻿namespace TEA.Entidades.Negocio.DiagnosticType
{
    using System.Collections.Generic;
    public class GetDiagnosticTypesResponse
    {
        public List<DiagnosticType> DiagnosticTypes { get; set; }
    }
}
