﻿namespace TEA.Comun.Exportacion.ExportacionPdf
{
    /// <summary>
    /// Tipo de fila de una tabla de un documento pdf
    /// </summary>
    public enum PdfTipoFilaTabla
    {
        EncabezadoTabla,
        DetalleTabla,
        PieTabla
    }
}
