﻿namespace TEA.Comun.Exportacion.ExportacionPdf
{
    /// <summary>
    /// Tipo de texto de un documento pdf
    /// </summary>
    public enum PdfTipoTexto
    {
        Titulo,
        Subtitlo,
        Detalle
    }
}
