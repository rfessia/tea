﻿namespace TEA.Comun.Exportacion.ExportacionPdf
{
    /// <summary>
    /// Tamaño de página de un documento pdf
    /// </summary>
    public enum PdfTamanoPagina
    {
        A4,
        Carta,
        Legal
    }
}
