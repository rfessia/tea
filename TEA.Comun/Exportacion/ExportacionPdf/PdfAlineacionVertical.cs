﻿namespace TEA.Comun.Exportacion.ExportacionPdf
{
    /// <summary>
    /// Alineación vertical de un elemento de un docuemnto pdf
    /// </summary>
    public enum PdfAlineacionVertical
    {
        Superior,
        Medio,
        Inferior
    }
}
