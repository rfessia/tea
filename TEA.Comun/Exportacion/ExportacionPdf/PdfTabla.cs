﻿namespace TEA.Comun.Exportacion.ExportacionPdf
{
    using System.Collections.ObjectModel;
    using System.Linq;

    /// <summary>
    /// Representa una tabla de un pdf
    /// </summary>
    public class PdfTabla : PdfElemento
    {
        /// <summary>
        /// Crea una instancia de la clase
        /// </summary>
        /// <param name="cantidadColumnas">Cantidad de columnas que tendrá la tabla</param>
        public PdfTabla(int cantidadColumnas)
            : this(cantidadColumnas, null)
        { }

        /// <summary>
        /// Crea una instancia de la clase
        /// </summary>
        /// <param name="anchosRelativosColumnas"></param>
        public PdfTabla(float[] anchosRelativosColumnas)
            : this(0, anchosRelativosColumnas)
        { }

        /// <summary>
        /// Crea una instancia de la clase
        /// </summary>
        /// <param name="cantidadColumnas">Cantidad de columnas que tendrá la tabla</param>
        /// <param name="anchosRelativosColumnas">Arreglo de medidas con el ancho relativo (en %) que tendrá cada columna</param>
        private PdfTabla(int cantidadColumnas, float[] anchosRelativosColumnas)
            : base(PdfTipoElemento.Tabla)
        {
            this.AnchoRelativo = 100F;
            this.Filas = new Collection<PdfFilaTabla>();

            if (anchosRelativosColumnas == null)
            {
                this.CantidadColumnas = cantidadColumnas;

                float anchoColumna = 100F / (float)cantidadColumnas;
                Collection<float> anchosColumnas = new Collection<float>();
                for (int i = 1; i <= cantidadColumnas; i++)
                {
                    anchosColumnas.Add(anchoColumna);
                    this.AnchosRelativosColumnas = anchosColumnas.ToArray();
                }
            }
            else
            {
                this.CantidadColumnas = anchosRelativosColumnas.Length;
                this.AnchosRelativosColumnas = anchosRelativosColumnas;
            }
        }

        /// <summary>
        /// Arreglo de medidas con el ancho relativo (en %) de cada columna
        /// </summary>
        public float[] AnchosRelativosColumnas { get; private set; }

        /// <summary>
        /// Filas de la tabla
        /// </summary>
        public Collection<PdfFilaTabla> Filas { get; set; }

        /// <summary>
        /// Cantidad de columnas de la tabla
        /// </summary>
        public int CantidadColumnas { get; private set; }

        /// <summary>
        /// Ancho relativo (en %) de la tabla
        /// </summary>
        public float AnchoRelativo { get; set; }
    }
}