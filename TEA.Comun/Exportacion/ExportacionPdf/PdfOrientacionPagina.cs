﻿namespace TEA.Comun.Exportacion.ExportacionPdf
{
    /// <summary>
    /// Orientación de página de un documento pdf
    /// </summary>
    public enum PdfOrientacionPagina
    {
        Vertical,
        Horizontal
    }
}
