﻿namespace TEA.Comun.Exportacion.ExportacionPdf
{
    /// <summary>
    /// Familia de fuente de un documento pdf
    /// </summary>
    public enum PdfFamiliaFuente
    {
        Courier,
        Helvetica,
        TimesNewRoman
    }
}
