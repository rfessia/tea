﻿namespace TEA.Comun.Exportacion.ExportacionPdf
{
    using System.Drawing;

    /// <summary>
    /// Representa la configuración de la fuente que tendrá un texto de un pdf
    /// </summary>
    public class PdfConfiguracionFuente
    {
        /// <summary>
        /// Crea una instancia de la clase
        /// </summary>
        public PdfConfiguracionFuente()
        {
            this.Color = Color.Black;
        }

        /// <summary>
        /// Familia de la fuente
        /// </summary>
        public PdfFamiliaFuente Familia { get; set; }

        /// <summary>
        /// Tamaño de la fuente
        /// </summary>
        public float Tamano { get; set; }

        /// <summary>
        /// Estilo de la fuente : Negrita, Cursiva; etc
        /// </summary>
        public PdfEstiloFuente Estilo { get; set; }

        /// <summary>
        /// Color de la fuente
        /// </summary>
        public Color Color { get; set; }

        /// <summary>
        /// Devuelve la configuración de fuente default de un texto de un pdf según el tipo de texto
        /// </summary>
        /// <param name="pdfTipoTexto">Tipo de texto del que se quiere obtener la configuración de la fuente</param>
        /// <returns>Devuelve la configuración de fuente default de un texto de un pdf según el tipo de texto</returns>
        public static PdfConfiguracionFuente ObtenerConfiguracionFuenteDefault(PdfTipoTexto pdfTipoTexto)
        {
            PdfConfiguracionFuente fuente = new PdfConfiguracionFuente();

            if (pdfTipoTexto == PdfTipoTexto.Titulo)
            {
                fuente.Familia = PdfFamiliaFuente.Helvetica;
                fuente.Color = Color.Gray;
                fuente.Tamano = 16.0F;
                fuente.Estilo = PdfEstiloFuente.Bold;
            }
            else if (pdfTipoTexto == PdfTipoTexto.Subtitlo)
            {
                fuente.Familia = PdfFamiliaFuente.Helvetica;
                fuente.Color = Color.Gray;
                fuente.Tamano = 12.0F;
                fuente.Estilo = PdfEstiloFuente.BoldItalic;
            }
            else if (pdfTipoTexto == PdfTipoTexto.Detalle)
            {
                fuente.Familia = PdfFamiliaFuente.Helvetica;
                fuente.Color = Color.Black;
                fuente.Tamano = 10.0F;
                fuente.Estilo = PdfEstiloFuente.Normal;
            }

            return fuente;
        }

        /// <summary>
        /// Devuelve la configuración de fuente default de un texto de una celda de un pdf según el tipo de fila
        /// </summary>
        /// <param name="tipoFila">Tipo de fila a la cual pertenece la celda de la cual se quiere obtener la configuración de la fuente</param>
        /// <returns>Devuelve la configuración de fuente default de un texto de una celda de un pdf según el tipo de fila</returns>
        public static PdfConfiguracionFuente ObtenerConfiguracionFuenteDefault(PdfTipoFilaTabla tipoFila)
        {
            PdfConfiguracionFuente pdfConfiguracionFuente = new PdfConfiguracionFuente();

            if (tipoFila == PdfTipoFilaTabla.EncabezadoTabla)
            {
                pdfConfiguracionFuente.Familia = PdfFamiliaFuente.Helvetica;
                pdfConfiguracionFuente.Tamano = 10.0F;
                pdfConfiguracionFuente.Estilo = PdfEstiloFuente.Bold;
                pdfConfiguracionFuente.Color = Color.FromArgb(2, 85, 100);
            }
            else if (tipoFila == PdfTipoFilaTabla.DetalleTabla)
            {
                pdfConfiguracionFuente.Familia = PdfFamiliaFuente.Helvetica;
                pdfConfiguracionFuente.Tamano = 9.0F;
                pdfConfiguracionFuente.Estilo = PdfEstiloFuente.Normal;
                pdfConfiguracionFuente.Color = Color.Black;
            }
            else if (tipoFila == PdfTipoFilaTabla.PieTabla)
            {
                pdfConfiguracionFuente.Familia = PdfFamiliaFuente.Helvetica;
                pdfConfiguracionFuente.Tamano = 10.0F;
                pdfConfiguracionFuente.Estilo = PdfEstiloFuente.Bold;
                pdfConfiguracionFuente.Color = Color.FromArgb(2, 85, 100);
            }

            return pdfConfiguracionFuente;
        }

        /// <summary>
        /// Devuelve la configuración de fuente default de un texto de un encabezado o pie de página de un pdf
        /// </summary>
        /// <returns>Devuelve la configuración de fuente default de un texto de un encabezado o pie de página de un pdf</returns>
        public static PdfConfiguracionFuente ObtenerConfiguracionFuenteEncabezadoPieDefault()
        {
            PdfConfiguracionFuente fuente = new PdfConfiguracionFuente()
            {
                Color = System.Drawing.Color.Black,
                Familia = PdfFamiliaFuente.Helvetica,
                Estilo = PdfEstiloFuente.Normal,
                Tamano = 8.0F
            };

            return fuente;
        }
    }
}