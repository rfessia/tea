﻿namespace TEA.Comun.Exportacion.ExportacionPdf
{
    /// <summary>
    /// Tipo de elemento de un docuemnto pdf
    /// </summary>
    public enum PdfTipoElemento
    {
        Tabla,
        TextoDeParrafo
    }
}
