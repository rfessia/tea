﻿namespace TEA.Comun.Exportacion.ExportacionPdf
{
    using System.Collections.ObjectModel;

    /// <summary>
    /// Representa un párrafo de un pdf
    /// </summary>
    public class PdfParrafo
    {
        /// <summary>
        /// Crea una instancia de la clase
        /// </summary>
        public PdfParrafo()
            : this(PdfAlineacionHorizontal.Izquierda)
        { }

        /// <summary>
        /// Crea una instancia de la clase
        /// </summary>
        /// <param name="pdfAlineacion">Alineación que tendrá el párrafo</param>
        public PdfParrafo(PdfAlineacionHorizontal pdfAlineacion)
        {
            this.Elementos = new Collection<PdfElemento>();
            this.EspacioArriba = 0.0F;
            this.EspacioAbajo = 5.0F;
            this.Alineacion = pdfAlineacion;
        }

        /// <summary>
        /// Espacio arriba del párrafo
        /// </summary>
        public float EspacioArriba { get; set; }

        /// <summary>
        /// Espacio abajo del párrafo
        /// </summary>
        public float EspacioAbajo { get; set; }

        /// <summary>
        /// Alineación del párrafo
        /// </summary>
        public PdfAlineacionHorizontal Alineacion { get; set; }

        /// <summary>
        /// Elementos del párrafo
        /// </summary>
        public Collection<PdfElemento> Elementos { get; set; }
    }
}