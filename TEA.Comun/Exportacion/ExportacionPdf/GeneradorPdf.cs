﻿namespace TEA.Comun.Exportacion.ExportacionPdf
{
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using System;
    using System.IO;

    /// <summary>
    /// Generador de un documento pdf
    /// </summary>
    public class GeneradorPdf
    {
        /// <summary>
        /// Objeto que representa el documento pdf que se va a generar
        /// </summary>
        private PdfDocumento pdfDocumento;

        /// <summary>
        /// Tipo de contenido a usar para el response de un archivo pdf
        /// </summary>
        public const string ContentType = "application/pdf";

        /// <summary>
        /// Crea una instancia de la clase
        /// </summary>
        /// <param name="pdfDocumento">Documento pdf que se va a generar</param>
        public GeneradorPdf(PdfDocumento pdfDocumento)
        {
            this.pdfDocumento = pdfDocumento;
        }

        /// <summary>
        /// Crea una instancia de la clase
        /// </summary>
        /// <param name="pdfTabla">Tabla que aparecerá en el documento pdf que se va a generar</param>
        public GeneradorPdf(PdfTabla pdfTabla)
        {
            PdfTexto pdfTexto = new PdfTexto(PdfTipoTexto.Detalle, DateTime.Now.ToString("dd/MM/yyyy HH:mm"));
            pdfTexto.Fuente = PdfConfiguracionFuente.ObtenerConfiguracionFuenteEncabezadoPieDefault();
            PdfParrafo encabezadoPagina = new PdfParrafo(PdfAlineacionHorizontal.Centrado);
            encabezadoPagina.Elementos.Add(pdfTexto);

            PdfParrafo pdfParrafo = new PdfParrafo();
            pdfParrafo.Elementos.Add(pdfTabla);
            PdfDocumento pdfDocumento = new PdfDocumento();
            pdfDocumento.Parrafos.Add(pdfParrafo);

            pdfDocumento.EncabezadoPagina = encabezadoPagina;

            this.pdfDocumento = pdfDocumento;
        }

        /// <summary>
        /// Genera un documento pdf de acuerdo a lo especificado en el constructor de la clase y devuelve los bytes del mismo
        /// </summary>
        /// <returns>Devuelve los bytes del documento pdf generado</returns>
        public byte[] ObtenerPdf()
        {
            Document document = ObtenerDocumento(this.pdfDocumento);

            MemoryStream memoryStream = new MemoryStream();
            PdfWriter pdfWriter = PdfWriter.GetInstance(document, memoryStream);
            ItsEvents ev = new ItsEvents(this.pdfDocumento);
            pdfWriter.PageEvent = ev;

            document.Open();

            foreach (PdfParrafo pdfParrafo in this.pdfDocumento.Parrafos)
            {
                Paragraph paragraph = ObtenerParrafo(pdfParrafo);
                document.Add(paragraph);
            }

            document.Close();
            pdfWriter.Close();

            byte[] archivoPdf = memoryStream.ToArray();
            memoryStream.Dispose();

            return archivoPdf;
        }

        /// <summary>
        /// Genera un objeto Paragaph de iTextSharp a partir de un objeto PdfParrafo
        /// </summary>
        /// <param name="pdfParrafo">Objeto PdfParrafo a partir del cual se generará el objeto Paragraph</param>
        /// <returns>Devuelve el objeto Paragaph de iTextSharp generado a partir de un objeto PdfParrafo</returns>
        internal static Paragraph ObtenerParrafo(PdfParrafo pdfParrafo)
        {
            Paragraph paragraph = new Paragraph();

            paragraph.SpacingBefore = pdfParrafo.EspacioArriba;
            paragraph.SpacingAfter = pdfParrafo.EspacioAbajo;
            paragraph.Alignment = ObtenerAlineacionHorizonal(pdfParrafo.Alineacion);

            foreach (PdfElemento pdfElemento in pdfParrafo.Elementos)
            {
                if (pdfElemento.TipoElemento == PdfTipoElemento.Tabla)
                {
                    PdfTabla tabla = (PdfTabla)pdfElemento;
                    PdfPTable pdfPTable = ObtenerTabla(tabla, pdfParrafo.Alineacion);

                    foreach (PdfFilaTabla fila in tabla.Filas)
                    {
                        foreach (PdfCeldaTabla celda in fila.Celdas)
                        {
                            PdfPCell pdfPCell = ObtenerCelda(fila.TipoFila, celda);
                            pdfPTable.AddCell(pdfPCell);
                        }
                    }

                    paragraph.Add(pdfPTable);
                }
                else if (pdfElemento.TipoElemento == PdfTipoElemento.TextoDeParrafo)
                {
                    PdfTexto pdfTexto = (PdfTexto)pdfElemento;
                    Phrase phrase = ObtenerFrase(pdfTexto);
                    paragraph.Add(phrase);
                }
            }

            return paragraph;
        }

        /// <summary>
        /// Genera un objeto Font de iTextSharp a partir de un objeto PdfConfiguracionFuente
        /// </summary>
        /// <param name="pdfConfiguracionFuente">Objeto PdfConfiguracionFuente a partir del cual se generará el objeto Font</param>
        /// <returns>Devuelve el objeto Font de iTextSharp generado a partir de un objeto PdfConfiguracionFuente</returns>
        internal static Font ObtenerFuente(PdfConfiguracionFuente pdfConfiguracionFuente)
        {
            Font.FontFamily fontFamily;

            if (pdfConfiguracionFuente.Familia == PdfFamiliaFuente.Helvetica)
            {
                fontFamily = Font.FontFamily.HELVETICA;
            }
            else if (pdfConfiguracionFuente.Familia == PdfFamiliaFuente.Courier)
            {
                fontFamily = Font.FontFamily.COURIER;
            }
            else if (pdfConfiguracionFuente.Familia == PdfFamiliaFuente.TimesNewRoman)
            {
                fontFamily = Font.FontFamily.TIMES_ROMAN;
            }
            else
            {
                fontFamily = Font.FontFamily.TIMES_ROMAN;
            }

            int fontStyle;

            if (pdfConfiguracionFuente.Estilo == PdfEstiloFuente.Bold)
            {
                fontStyle = Font.BOLD;
            }
            else if (pdfConfiguracionFuente.Estilo == PdfEstiloFuente.BoldItalic)
            {
                fontStyle = Font.BOLDITALIC;
            }
            else if (pdfConfiguracionFuente.Estilo == PdfEstiloFuente.Italic)
            {
                fontStyle = Font.ITALIC;
            }
            else if (pdfConfiguracionFuente.Estilo == PdfEstiloFuente.Normal)
            {
                fontStyle = Font.NORMAL;
            }
            else if (pdfConfiguracionFuente.Estilo == PdfEstiloFuente.Underline)
            {
                fontStyle = Font.UNDERLINE;
            }
            else
            {
                fontStyle = Font.NORMAL;
            }

            BaseColor fontColor = new BaseColor(pdfConfiguracionFuente.Color);

            Font font = new Font(fontFamily, pdfConfiguracionFuente.Tamano, fontStyle, fontColor);

            return font;
        }

        /// <summary>
        /// Genera un objeto Document de iTextSharp a partir de un objeto PdfDocumento
        /// </summary>
        /// <param name="pdfDocumento">Objeto PdfDocumento a partir del cual se generará el objeto Document</param>
        /// <returns>Devuelve el objeto Document de iTextSharp generado a partir de un objeto PdfDocumento</returns>
        private static Document ObtenerDocumento(PdfDocumento pdfDocumento)
        {
            Rectangle pageSize;

            if (pdfDocumento.TamanoPagina == PdfTamanoPagina.A4)
            {
                pageSize = PageSize.A4;
            }
            else if (pdfDocumento.TamanoPagina == PdfTamanoPagina.Legal)
            {
                pageSize = PageSize.LEGAL;
            }
            else if (pdfDocumento.TamanoPagina == PdfTamanoPagina.Carta)
            {
                pageSize = PageSize.LETTER;
            }
            else
            {
                pageSize = PageSize.A4;
            }

            if (pdfDocumento.OrientacionPagina == PdfOrientacionPagina.Horizontal)
            {
                pageSize = pageSize.Rotate();
            }

            Document document = new Document(pageSize, pdfDocumento.MargenIzquierdo, pdfDocumento.MargenDerecho, pdfDocumento.MargenSuperior, pdfDocumento.MargenInferior);

            document.AddAuthor(pdfDocumento.Autor);

            document.AddCreator(pdfDocumento.Creador);

            document.AddCreationDate();

            document.AddTitle(pdfDocumento.TituloDocumento);

            return document;
        }

        /// <summary>
        /// Genera un objeto PdfPTable de iTextSharp a partir de un objeto PdfTabla
        /// </summary>
        /// <param name="pdfTabla">Objeto PdfTabla a partir del cual se generará el objeto PdfPTable</param>
        /// <param name="pdfAlineacionHorizontalTabla">Alineación que tendrá la tabla dentro del párrafo</param>
        /// <returns>Devuelve el objeto PdfPTable de iTextSharp generado a partir de un objeto PdfTabla</returns>
        private static PdfPTable ObtenerTabla(PdfTabla pdfTabla, PdfAlineacionHorizontal pdfAlineacionHorizontalTabla)
        {
            PdfPTable pdfPTable = new PdfPTable(pdfTabla.CantidadColumnas);

            pdfPTable.SetWidths(pdfTabla.AnchosRelativosColumnas);
            pdfPTable.WidthPercentage = pdfTabla.AnchoRelativo;
            pdfPTable.HorizontalAlignment = ObtenerAlineacionHorizonal(pdfAlineacionHorizontalTabla);

            return pdfPTable;
        }

        /// <summary>
        /// Genera un objeto Phrase de iTextSharp a partir de un objeto PdfTexto
        /// </summary>
        /// <param name="pdfTexto">Objeto PdfTexto a partir del cual se generará el objeto Phrase</param>
        /// <returns>Devuelve el objeto Phrase de iTextSharp generado a partir de un objeto PdfTexto</returns>
        private static Phrase ObtenerFrase(PdfTexto pdfTexto)
        {
            Phrase phrase = new Phrase();
            Font font = ObtenerFuente(pdfTexto.Fuente);
            phrase = new Phrase(pdfTexto.Texto, font);
            return phrase;
        }

        /// <summary>
        /// Genera un objeto PdfPCell de iTextSharp a partir de un objeto pdfCeldaTabla según el tipo PdfTipoFilaTabla
        /// </summary>
        /// <param name="tipoFila">Tipo de fila que se tendrá en cuenta al momento de crear la celda</param>
        /// <param name="pdfCeldaTabla">Objeto PdfCeldaTabla a partir del cual se generará el objeto PdfPCell</param>
        /// <returns>Devuelve el objeto PdfPCell de iTextSharp generado a partir de un objeto pdfCeldaTabla según el tipo PdfTipoFilaTabla</returns>
        private static PdfPCell ObtenerCelda(PdfTipoFilaTabla tipoFila, PdfCeldaTabla pdfCeldaTabla)
        {
            PdfPCell pdfPCell = new PdfPCell();

            pdfPCell.BorderColor = new BaseColor(pdfCeldaTabla.ColorBorde);
            pdfPCell.BorderWidth = pdfCeldaTabla.GrosorBorde;

            if (pdfCeldaTabla.Fuente == null)
            {
                pdfCeldaTabla.Fuente = PdfConfiguracionFuente.ObtenerConfiguracionFuenteDefault(tipoFila);
            }

            Font font = ObtenerFuente(pdfCeldaTabla.Fuente);
            Phrase phrase = new Phrase(pdfCeldaTabla.Texto, font);

            Paragraph paragraph = new Paragraph(phrase);
            paragraph.Alignment = ObtenerAlineacionHorizonal(pdfCeldaTabla.AlineacionHorizontal);
            pdfPCell.VerticalAlignment = ObtenerAlineacionVertical(pdfCeldaTabla.AlineacionVertical);

            pdfPCell.AddElement(paragraph);

            return pdfPCell;
        }

        /// <summary>
        /// Devuelve el valor que representa una determinada alineación de iTextSharp según el tipo PdfAlineacionHorizontal especificado
        /// </summary>
        /// <param name="pdfAlineacionHorizontal">Alineación horizontal a partir de la cual se quiere obtener su valor equivalente en iTextSharp</param>
        /// <returns>Devuelve el valor que representa una determinada alineación de iTextSharp según el tipo PdfAlineacionHorizontal especificado</returns>
        private static int ObtenerAlineacionHorizonal(PdfAlineacionHorizontal pdfAlineacionHorizontal)
        {
            if (pdfAlineacionHorizontal == PdfAlineacionHorizontal.Izquierda)
            {
                return Element.ALIGN_LEFT;
            }
            else if (pdfAlineacionHorizontal == PdfAlineacionHorizontal.Centrado)
            {
                return Element.ALIGN_CENTER;
            }
            else if (pdfAlineacionHorizontal == PdfAlineacionHorizontal.Derecha)
            {
                return Element.ALIGN_RIGHT;
            }
            else if (pdfAlineacionHorizontal == PdfAlineacionHorizontal.Justificado)
            {
                return Element.ALIGN_JUSTIFIED;
            }
            else
            {
                return Element.ALIGN_LEFT;
            }
        }

        /// <summary>
        /// Devuelve el valor que representa una determinada alineación de iTextSharp según el tipo PdfAlineacionVertical especificado
        /// </summary>
        /// <param name="pdfAlineacionVertical">Alineación vertical a partir de la cual se quiere obtener su valor equivalente en iTextSharp</param>
        /// <returns>Devuelve el valor que representa una determinada alineación de iTextSharp según el tipo PdfAlineacionVertical especificado</returns>
        private static int ObtenerAlineacionVertical(PdfAlineacionVertical pdfAlineacionVertical)
        {
            if (pdfAlineacionVertical == PdfAlineacionVertical.Superior)
            {
                return Element.ALIGN_TOP;
            }
            else if (pdfAlineacionVertical == PdfAlineacionVertical.Inferior)
            {
                return Element.ALIGN_BOTTOM;
            }
            else if (pdfAlineacionVertical == PdfAlineacionVertical.Medio)
            {
                return Element.ALIGN_MIDDLE;
            }
            else
            {
                return Element.ALIGN_MIDDLE;
            }
        }
    }

    /// <summary>
    /// Controlador de eventos del generador de pdf
    /// </summary>
    public class ItsEvents : PdfPageEventHelper
    {
        /// <summary>
        /// Documento pdf del cual se tiene en cuenta la info al momento en que se produzcan los eventos
        /// </summary>
        private PdfDocumento pdfDocumento;

        /// <summary>
        /// Crea una instancia de la clase
        /// </summary>
        /// <param name="pdfDocumento">Documento pdf del cual se tendrá en cuenta la info al momento en que se produzcan los eventos</param>
        public ItsEvents(PdfDocumento pdfDocumento)
        {
            this.pdfDocumento = pdfDocumento;
        }

        /// <summary>
        /// Evento que se produce cuando comienza una página en un objeto Document de iTextSharp
        /// </summary>
        /// <param name="writer">Objeto de iTextSharp que va escribiendo en documento pdf</param>
        /// <param name="document">Objeto de iTextSharp que representa el documento pdf</param>
        public override void OnStartPage(PdfWriter writer, Document document)
        {
            PdfPTable pdfTable = ObtenerTablaEncabezadoPie(document);
            PdfPCell pdfCell = ObtenerCeldaEncabezadoPie(document);

            if (this.pdfDocumento.EncabezadoPagina != null)
            {
                Paragraph paragraph = GeneradorPdf.ObtenerParrafo(this.pdfDocumento.EncabezadoPagina);

                pdfCell.AddElement(paragraph);
                pdfTable.AddCell(pdfCell);
                pdfTable.WriteSelectedRows(0, -1, 0, document.PageSize.Height - document.TopMargin + 15 + this.pdfDocumento.EncabezadoPaginaAlto, writer.DirectContent);
            }
        }

        /// <summary>
        /// Evento que se produce cuando termina una página en un objeto Document de iTextSharp
        /// </summary>
        /// <param name="writer">Objeto de iTextSharp que va escribiendo en documento pdf</param>
        /// <param name="document">Objeto de iTextSharp que representa el documento pdf</param>
        public override void OnEndPage(PdfWriter writer, Document document)
        {
            PdfPTable pdfTable = ObtenerTablaEncabezadoPie(document);
            PdfPCell pdfCell = ObtenerCeldaEncabezadoPie(document);

            if (this.pdfDocumento.PiePagina != null)
            {
                Paragraph paragraph = GeneradorPdf.ObtenerParrafo(this.pdfDocumento.PiePagina);
                pdfCell.AddElement(paragraph);
            }

            if (this.pdfDocumento.MostrarNumeroPagina)
            {
                Paragraph paragraph = new Paragraph();

                PdfConfiguracionFuente fuente = PdfConfiguracionFuente.ObtenerConfiguracionFuenteEncabezadoPieDefault();
                Font font = GeneradorPdf.ObtenerFuente(fuente);

                Phrase phrase = new Phrase(string.Concat("Pág. ", writer.PageNumber), font);
                paragraph.Add(phrase);
                paragraph.Alignment = Element.ALIGN_RIGHT;

                pdfCell.AddElement(paragraph);
            }

            if (this.pdfDocumento.PiePagina != null || this.pdfDocumento.MostrarNumeroPagina)
            {
                pdfTable.AddCell(pdfCell);
                pdfTable.WriteSelectedRows(0, -1, 0, (document.BottomMargin + 10), writer.DirectContent);
            }
        }

        /// <summary>
        /// Genera una tabla PdfPTable de iTextSharp para usar en el encabezado o pie de una página
        /// </summary>
        /// <param name="document">Objeto Document de iTextSharp en donde se quiere colocar el encabezado o pie</param>
        /// <returns>Devuelve una tabla PdfPTable de iTextSharp para usar en el encabezado o pie de una página</returns>
        private static PdfPTable ObtenerTablaEncabezadoPie(Document document)
        {
            PdfPTable footerTbl = new PdfPTable(1);
            footerTbl.TotalWidth = document.PageSize.Width;

            return footerTbl;
        }

        /// <summary>
        /// Genera una celda PdfPCell de iTextSharp para usar en el encabezado o pie de una página
        /// </summary>
        /// <param name="document">Objeto Document de iTextSharp en donde se quiere colocar el encabezado o pie</param>
        /// <returns>Devuelve una celda PdfPCell de iTextSharp para usar en el encabezado o pie de una página</returns>
        private static PdfPCell ObtenerCeldaEncabezadoPie(Document document)
        {
            PdfPCell cell = new PdfPCell();
            cell.Border = 0;
            cell.PaddingRight = document.RightMargin;
            cell.PaddingLeft = document.LeftMargin;

            return cell;
        }
    }
}
