﻿namespace TEA.Comun.Exportacion.ExportacionPdf
{
    using System.Drawing;

    /// <summary>
    /// Representa una celda de una tabla de un pdf
    /// </summary>
    public class PdfCeldaTabla
    {
        /// <summary>
        /// Crea una instancia de la clase
        /// </summary>
        public PdfCeldaTabla()
            : this(string.Empty, PdfAlineacionHorizontal.Izquierda)
        { }

        /// <summary>
        /// Crea una instancia de la clase
        /// </summary>
        /// <param name="pdfAlineacionHorizontal">Alineación horizontal que tendrá la celda</param>
        public PdfCeldaTabla(PdfAlineacionHorizontal pdfAlineacionHorizontal)
            : this(string.Empty, pdfAlineacionHorizontal)
        { }

        /// <summary>
        /// Crea una instancia de la clase
        /// </summary>
        /// <param name="texto">Texto a incluir en la celda</param>
        public PdfCeldaTabla(string texto)
            : this(texto, PdfAlineacionHorizontal.Izquierda)
        { }

        /// <summary>
        /// Crea una instancia de la clase
        /// </summary>
        /// <param name="texto">Texto a incluir en la celda</param>
        public PdfCeldaTabla(string texto, PdfAlineacionHorizontal pdfAlineacionHorizontal)
        {
            this.Texto = texto;
            this.ColorBorde = Color.Gray;
            this.GrosorBorde = 0.5f;
            this.AlineacionHorizontal = pdfAlineacionHorizontal;
            this.AlineacionVertical = PdfAlineacionVertical.Medio;
        }

        /// <summary>
        /// Texto dentro de la celda
        /// </summary>
        public string Texto { get; set; }

        /// <summary>
        /// Fuente del texto de la celda
        /// </summary>
        public PdfConfiguracionFuente Fuente { get; set; }

        /// <summary>
        /// Color del borde de la celda
        /// </summary>
        public Color ColorBorde { get; set; }

        /// <summary>
        /// Grosor de la lína del borde de la celda
        /// </summary>
        public float GrosorBorde { get; set; }

        /// <summary>
        /// Alineación horizontal de la celda
        /// </summary>
        public PdfAlineacionHorizontal AlineacionHorizontal { get; set; }

        /// <summary>
        /// Alineación vertical de la celda
        /// </summary>
        public PdfAlineacionVertical AlineacionVertical { get; set; }
    }
}