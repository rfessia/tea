﻿namespace TEA.Comun.Exportacion.ExportacionPdf
{
    /// <summary>
    /// Representa un elemento que se puede incluir en un párrafo de un pdf
    /// </summary>
    public abstract class PdfElemento
    {
        /// <summary>
        /// Crea una instacia de la clase
        /// </summary>
        /// <param name="tipoElemento">Tipo de elemento que se creará</param>
        public PdfElemento(PdfTipoElemento tipoElemento)
        {
            this.TipoElemento = tipoElemento;
        }

        /// <summary>
        /// Tipo de elemento del pdf
        /// </summary>
        public PdfTipoElemento TipoElemento { get; private set; }
    }
}
