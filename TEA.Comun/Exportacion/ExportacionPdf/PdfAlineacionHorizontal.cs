﻿namespace TEA.Comun.Exportacion.ExportacionPdf
{
    /// <summary>
    /// Alineación horizontal de un elemento de un documento pdf
    /// </summary>
    public enum PdfAlineacionHorizontal
    {
        Izquierda,
        Derecha,
        Centrado,
        Justificado
    }
}
