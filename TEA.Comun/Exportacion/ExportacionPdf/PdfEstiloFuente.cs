﻿namespace TEA.Comun.Exportacion.ExportacionPdf
{
    /// <summary>
    /// Estilo de fuente de un documento pdf
    /// </summary>
    public enum PdfEstiloFuente
    {
        Normal,
        Bold,
        Italic,
        BoldItalic,
        Underline
    }
}
