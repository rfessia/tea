﻿namespace TEA.Comun.Exportacion.ExportacionPdf
{
    public class PdfTexto : PdfElemento
    {
        /// <summary>
        /// Crea una instancia de la clase
        /// </summary>
        /// <param name="pdfTipoTexto">Tipo de </param>
        public PdfTexto(PdfTipoTexto pdfTipoTexto)
            : this(pdfTipoTexto, string.Empty)
        { }

        /// <summary>
        /// Crea una instancia de la clase
        /// </summary>
        /// <param name="pdfTipoTexto">Tipo de texto que se creará</param>
        /// <param name="texto">Texto que contendrá</param>
        public PdfTexto(PdfTipoTexto pdfTipoTexto, string texto)
            : base(PdfTipoElemento.TextoDeParrafo)
        {
            this.TipoTexto = pdfTipoTexto;
            this.Texto = texto;
            this.Fuente = PdfConfiguracionFuente.ObtenerConfiguracionFuenteDefault(pdfTipoTexto);
        }

        /// <summary>
        /// Tipo de texto
        /// </summary>
        public PdfTipoTexto TipoTexto { get; private set; }

        /// <summary>
        /// Contenido del texto
        /// </summary>
        public string Texto { get; set; }

        /// <summary>
        /// Configuración de la fuente
        /// </summary>
        public PdfConfiguracionFuente Fuente { get; set; }
    }
}