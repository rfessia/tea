﻿namespace TEA.Comun.Exportacion.ExportacionPdf
{
    using System.Collections.ObjectModel;

    /// <summary>
    /// Representa una fila de una tabla de un pdf
    /// </summary>
    public class PdfFilaTabla
    {
        /// <summary>
        /// Crea una instancia de la clase
        /// </summary>
        /// <param name="tipoFila">Tipo de fila que se creará</param>
        public PdfFilaTabla(PdfTipoFilaTabla tipoFila)
        {
            this.TipoFila = tipoFila;
            this.Celdas = new Collection<PdfCeldaTabla>();
        }

        /// <summary>
        /// Tipo de fila
        /// </summary>
        public PdfTipoFilaTabla TipoFila { get; private set; }

        /// <summary>
        /// Celdas de la fila
        /// </summary>
        public Collection<PdfCeldaTabla> Celdas { get; set; }
    }
}
