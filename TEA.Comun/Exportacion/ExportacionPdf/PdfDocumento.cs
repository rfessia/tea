﻿namespace TEA.Comun.Exportacion.ExportacionPdf
{
    using System.Collections.ObjectModel;

    /// <summary>
    /// Representa un documento pdf
    /// </summary>
    public class PdfDocumento
    {
        /// <summary>
        /// Crea una instancia de la clase
        /// </summary>
        public PdfDocumento()
            : this(PdfTamanoPagina.A4, string.Empty, string.Empty, string.Empty)
        { }

        /// <summary>
        /// Crea una instancia de la clase
        /// </summary>
        /// <param name="pdfPageSize">Tamaño de página que tendrá el documento pdf</param>
        /// <param name="tituloDocumento">Título que se incluirá en las propiedades de documento pdf</param>
        /// <param name="creador">Creador del documento que se incluirá en las propiedades de documento pdf</param>
        /// <param name="autor">Autor del documento que se incluirá en las propiedades de documento pdf</param>
        public PdfDocumento(PdfTamanoPagina pdfTamanoPagina, string tituloDocumento, string creador, string autor)
        {
            this.Parrafos = new Collection<PdfParrafo>();
            this.TamanoPagina = pdfTamanoPagina;
            this.OrientacionPagina = PdfOrientacionPagina.Vertical;
            this.Creador = creador;
            this.Autor = autor;
            this.TituloDocumento = tituloDocumento;
            this.MargenIzquierdo = 25;
            this.MargenDerecho = 25;
            this.MargenSuperior = 30;
            this.MargenInferior = 25;
            this.MostrarNumeroPagina = true;
            this.EncabezadoPaginaAlto = 10;
        }

        /// <summary>
        /// Tamaño de página del documento pdf
        /// </summary>
        public PdfTamanoPagina TamanoPagina { get; set; }

        /// <summary>
        /// Orientación de la página del documento pdf
        /// </summary>
        public PdfOrientacionPagina OrientacionPagina { get; set; }

        /// <summary>
        /// Título que figura en las propiedades del docuymento pdf
        /// </summary>
        public string TituloDocumento { get; set; }

        /// <summary>
        /// Creador que figura en las propiedades de documento pdf
        /// </summary>
        public string Creador { get; set; }

        /// <summary>
        /// Autor que figura en las propiedades de documento pdf
        /// </summary>
        public string Autor { get; set; }

        /// <summary>
        /// Párrafos del documento pdf
        /// </summary>
        public Collection<PdfParrafo> Parrafos { get; set; }

        /// <summary>
        /// Encabezado del documento pdf
        /// </summary>
        public PdfParrafo EncabezadoPagina { get; set; }

        /// <summary>
        /// Alto del encabezado de la página
        /// </summary>
        public float EncabezadoPaginaAlto { get; set; }

        /// <summary>
        /// Pie de página del documento pdf
        /// </summary>
        public PdfParrafo PiePagina { get; set; }

        /// <summary>
        /// Margen superior del documento pdf
        /// </summary>
        public float MargenSuperior { get; set; }

        /// <summary>
        /// Margen inferior del documento pdf
        /// </summary>
        public float MargenInferior { get; set; }

        /// <summary>
        /// Margen derecho del documento pdf
        /// </summary>
        public float MargenDerecho { get; set; }

        /// <summary>
        /// Margen izquierod del documento pdf
        /// </summary>
        public float MargenIzquierdo { get; set; }

        /// <summary>
        /// Indica si se muestra en número de página
        /// </summary>
        public bool MostrarNumeroPagina { get; set; }
    }
}