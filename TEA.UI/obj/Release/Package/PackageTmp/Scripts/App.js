﻿var tokenKey = 'accessTokenTEAPP';
var userNameTeapp = 'userNameTEAPP';
var refreshTokenTeapp = 'refreshTokenTEAPP';
//var urlApi = "http://localhost:44305";
var urlApi = "http://rfessia-001-site2.htempurl.com/";

$(function () {
    $(".imgBotonBienvenidos").click(function () {
        $("#UserName").val(null);
        $("#Password").val(null);
        $("#UnauthorizedMessage").hide();
        loggedIn();
    });
    
    showHideLogOut();
});

function removeCurrentItems() {
    var tokenKeyExist = sessionStorage.getItem(tokenKey);
    if (tokenKeyExist) {
        sessionStorage.removeItem(tokenKey);
    };

    var userExist = sessionStorage.getItem(userNameTeapp);
    if (userExist) {
        sessionStorage.removeItem(userNameTeapp);
    };

    var refreshTokenExist = sessionStorage.getItem(refreshTokenTeapp);
    if (refreshTokenExist) {
        sessionStorage.removeItem(refreshTokenTeapp);
    };
};

function updateCurrentItems(tokenValue, userValue, refreshTokenValue) {
    removeCurrentItems();
    sessionStorage.setItem(tokenKey, tokenValue);
    sessionStorage.setItem(userNameTeapp, userValue);
    sessionStorage.setItem(refreshTokenTeapp, refreshTokenValue);
};

function login(event) {
    blockUIGeneric();
    $("#UnauthorizedMessage").hide();
    event.preventDefault();
    var loginData = {
        grant_type: 'password',
        username: $("#UserName").val(),
        password: $("#Password").val(),
    };

    $.ajax({
        type: 'POST',
        url: urlApi + '/Token',
        data: loginData
    }).done(function (data) {
        var isAdmin = false;
        if (data.error && data.error == "IsAdmin") {
            isAdmin = true
        };

        if (isAdmin) {
            $('#loginModal').modal('hide');
            getMyusers();
        }
        else {
            var user = JSON.parse(data.user);
            updateCurrentItems(data.access_token, user.UserName, data.refresh_token);
            window.location.href = window.location.origin + '/EmotionRecognition';
        };

    }).fail(function (showError) {
        unblockUI();
        var a = showError;
        if (showError.status === 401) {
            $("#UnauthorizedMessage").children('span').text(showError.responseJSON.error_description)
            $("#UnauthorizedMessage").show();
        }
    });
};

function getMyusers() {
    var loginData = {
        username: $("#UserName").val(),
        password: $("#Password").val()
    };
    $.ajax({
        type: 'GET',
        url: urlApi + '/Api/User/GetMyUsers',
        data: loginData
    }).done(function (data) {
        unblockUI();
        if (data.Validate.IsValid) {
            $(".select-user").select2({
                placeholder: {
                    id: '-1',
                    text: 'Seleccione un Usuario'
                },
                allowClear: true,
                data: data.Users,
                dropdownParent: $("#selectUserModal")
            });
        }
        else {
            alert("Ha ocurrido un error inesperado");
        };

        $('.select-user').select2('val', '-1');
        $("#UserIdMessage").hide();
        $('#selectUserModal').modal('show');

    }).fail(function (showError) {
        unblockUI();
        alert("Ha ocurrido un error inesperado");
    });
};

function selectMyUser() {
    blockUIGeneric();
    var userId = $(".select-user").val();
    if (userId > 0) {
        var loginData = {
            grant_type: 'password',
            username: $("#UserName").val(),
            password: $("#Password").val(),
            userId: userId,
        };
        $.ajax({
            type: 'POST',
            url: urlApi + '/Token',
            data: loginData
        }).done(function (data) {
            var user = JSON.parse(data.user)
            if (user.IsAuthenticated) {
                updateCurrentItems(data.access_token, user.UserName, data.refresh_token);
                window.location.href = window.location.origin + '/EmotionRecognition';
            }
            else {
                unblockUI();
                alert("Ha ocurrido  un error inesperado");
            };

        }).fail(function (showError) {
            unblockUI();
            alert("Ha ocurrido  un error inesperado");
        });
    }
    else {
        unblockUI();
        $("#UserIdMessage").show();
    };
}

function getMyHeaders() {
    var token = sessionStorage.getItem(tokenKey);
    var headers = {
        Authorization: null,
    };
    if (token) {
        headers.Authorization = 'Bearer ' + token;
        return headers;
    }
    else {
        return null;
    };
};

function LogOut(event) {
    event.preventDefault();
    var headers = getMyHeaders();
    $.ajax({
        type: 'POST',
        url: urlApi + '/Api/User/LogOut',
        headers: headers,
    }).done(function (data) {
    }).fail();

    removeCurrentItems();
    showHideLogOut();
};

function loggedIn() {
    var token = sessionStorage.getItem(tokenKey);
    if (token) {
        blockUIGeneric();
        var headers = getMyHeaders();
        $.ajax({
            type: 'GET',
            url: urlApi + '/Api/User/IsLoggedIn',
            headers: headers,
        }).done(function (data) {
            if (data.IsValid) {
                window.location.href = window.location.origin + '/EmotionRecognition';
            } else {
                $('#loginModal').modal('show');
                removeCurrentItems();
                showHideLogOut();
                unblockUI();
            };
            
        }).fail(function (data) {
            $('#loginModal').modal('show');
            removeCurrentItems();
            showHideLogOut();
            unblockUI();
        });
    }
    else {
        showHideLogOut();
        $('#loginModal').modal('show');
    };
};

function showHideLogOut() {
    var token = sessionStorage.getItem(tokenKey);
    if (token) {
        $("li#LogOut").show();
    }
    else {
        $("li#LogOut").hide();
    };
};

function blockUIGeneric() {
    $.blockUI({
        message: $('#imageBlockUI'),
        css: {
            top: ($(window).height() - 50) / 2 + 'px',
            left: ($(window).width() - 50) / 2 + 'px',
            width: '50px',
            backgroundColor: 'none',
            border: 'none',
        }
    });
};

function unblockUI() {
    $.unblockUI();
};