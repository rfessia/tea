﻿namespace TEA.UI.Controllers
{
    using System.Web.Mvc;

    public class EmotionRecognitionController : Controller
    {
        // GET: EmotionRecognition
        public ActionResult Index()
        {
            return View("~/Views/EmotionRecognition/Index.cshtml", "_LayoutEmotionRecognition");
        }
    }
}