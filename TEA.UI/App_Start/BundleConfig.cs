﻿namespace TEA.UI
{
    using System.Web.Optimization;

    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Clear();
            bundles.ResetAll();
            BundleTable.EnableOptimizations = true;
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/blockui").Include(
                        "~/Scripts/jquery.blockUI.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/isotope").Include(
                      "~/Scripts/Plugins/isotope.js"));

            bundles.Add(new ScriptBundle("~/bundles/imagesloaded").Include(
                      "~/Scripts/Plugins/imagesloaded.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/wow").Include(
                      "~/Scripts/Plugins/wow.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/smoothscroll").Include(
                      "~/Scripts/Plugins/smoothscroll.js"));

            bundles.Add(new ScriptBundle("~/bundles/flexslider").Include(
                      "~/Scripts/Plugins/jquery.flexslider.js"));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                      "~/Scripts/Plugins/custom.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/App").Include(
                      "~/Scripts/App.js"));

            bundles.Add(new ScriptBundle("~/bundles/EmotionRecognition").Include(
                      "~/Scripts/webfontloader.js",
                      "~/Scripts/Types/phaser.js",
                      "~/Scripts/Games/EmotionRecognition/Lib/Mixins.js",
                      "~/Scripts/Plugins/phase-slide.js",
                      "~/Scripts/Plugins/modal.js",
                      "~/Scripts/Games/EmotionRecognition/States/Main.js",
                      "~/Scripts/Games/EmotionRecognition/States/Preload.js",
                      "~/Scripts/Games/EmotionRecognition/States/MainMenu.js",
                      "~/Scripts/Games/EmotionRecognition/States/Level.js",
                      "~/Scripts/Games/EmotionRecognition/States/Stage.js",
                      "~/Scripts/Games/EmotionRecognition/States/Game.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/Site.css"));
        }
    }
}
