﻿/// <reference path="../states/mainmenu.ts" />
var tokenKey = 'accessTokenTEAPP';
var userNameTeapp = 'userNameTEAPP';
var refreshTokenTeapp = 'refreshTokenTEAPP';
var mixins = {
    addMenuOption: function (text, callback, className) {

        // use the className argument, or fallback to menuConfig, but
        // if menuConfig isn't set, just use "default"
        className || (className = this.menuConfig.className || 'default');

        // set the x coordinate to game.world.center if we use "center"
        // otherwise set it to menuConfig.startX
        var x = this.menuConfig.startX === "center" ?
            game.world.centerX :
            this.menuConfig.startX;

        // set Y coordinate based on menuconfig
        var y = this.menuConfig.startY;

        // create
        var txt = game.add.text(
            x,
            (this.optionCount * 80) + y,
            text,
            style.navitem[className]
        );

        // use the anchor method to center if startX set to center.
        txt.anchor.setTo(this.menuConfig.startX === "center" ? 0.5 : 0.0);

        txt.inputEnabled = true;

        txt.events.onInputUp.add(callback);
        txt.events.onInputOver.add(function (target) {
            target.setStyle(style.navitem.hover);
        });
        txt.events.onInputOut.add(function (target) {
            target.setStyle(style.navitem[className]);
        });

        this.optionCount++;
    },
    getHeaders: function () {

        var token = sessionStorage.getItem(tokenKey);
        var headers = {
            Authorization: null,
        };
        if (token) {
            headers.Authorization = 'Bearer ' + token;
            return headers;
        }
        else {
            return null;
        }
    },

    getInfoSessionStorage: function () {
        var token = sessionStorage.getItem(tokenKey);
        var refreshToken = sessionStorage.getItem(refreshTokenTeapp);
        var user = sessionStorage.getItem(userNameTeapp);
        if (token && refreshToken && user) {
            var loginData = {
                grant_type: 'refresh_token',
                username: user,
                refresh_token: refreshToken,
            };
            return loginData;
        }
        else {
            return null;
        };
    },

    refreshToken: function () {
        var result = false;
        var infoSessionStorage = mixins.getInfoSessionStorage();
        if (infoSessionStorage) {
            $.ajax({
                type: 'POST',
                async: false,
                url: urlApi + '/Token',
                data: infoSessionStorage
            }).done(function (data) {
                var user = JSON.parse(data.user);
                mixins.updateCurrentItems(data.access_token, user.UserName, data.refresh_token);
                result = true;
            }).fail(function (showError) {
                mixins.removeCurrentItems();
                window.location.href = window.location.origin;
            });
        }
        else {
            window.location.href = window.location.origin;
        };
    },


    removeCurrentItems: function () {
        var tokenKeyExist = sessionStorage.getItem(tokenKey);
        if (tokenKeyExist) {
            sessionStorage.removeItem(tokenKey);
        };

        var userExist = sessionStorage.getItem(userNameTeapp);
        if (userExist) {
            sessionStorage.removeItem(userNameTeapp);
        };

        var refreshTokenExist = sessionStorage.getItem(refreshTokenTeapp);
        if (refreshTokenExist) {
            sessionStorage.removeItem(refreshTokenTeapp);
        };
    },

    updateCurrentItems: function (tokenValue, userValue, refreshTokenValue) {
        mixins.removeCurrentItems();
        sessionStorage.setItem(tokenKey, tokenValue);
        sessionStorage.setItem(userNameTeapp, userValue);
        sessionStorage.setItem(refreshTokenTeapp, refreshTokenValue);
    },


    addBackgroundPrincipal: function (context) {
        var this_ = context;
        this_.background = this_.add.sprite(0, 0, "BackgroundAlone");
        this_.background.height = $(window).height();
        this_.background.width = $(window).width();
        this_.widthPersonajes = $(window).width() / 12;
        this_.heightPersonajes = $(window).height() / 12;

        this_.personaje5 = game.add.sprite(0, 0, 'Personaje5');
        this_.personaje4 = game.add.sprite(0, 0, 'Personaje4');
        this_.personaje3 = game.add.sprite(0, 0, 'Personaje3');
        this_.personaje2 = game.add.sprite(0, 0, 'Personaje2');
        this_.personaje1 = game.add.sprite(0, 0, 'Personaje1');

        this_.personaje5.visible = false;
        this_.personaje4.visible = false;
        this_.personaje3.visible = false;
        this_.personaje2.visible = false;
        this_.personaje1.visible = false;
    },

    resizePrincipal: function (context) {
        var this_ = context;
        this_.personaje5.visible = true;
        this_.personaje4.visible = true;
        this_.personaje3.visible = true;
        this_.personaje2.visible = true;
        this_.personaje1.visible = true;

        this_.widthPersonajes = $(window).width() / 12;
        this_.heightPersonajes = $(window).height() / 12;
        game.scale.setGameSize($(window).width(), $(window).height());

        this_.background.height = $(window).height();
        this_.background.width = $(window).width();

        if (this_.btnJugar) {
            this_.btnJugar.scale.setTo(1, 1);
            this_.btnJugar.x = game.world.centerX - (this_.btnJugar.width / 2);
            this_.btnJugar.y = (this_.heightPersonajes * 4) - this_.btnJugar.height;
        };

        if ($(window).width() > 1200) {
            this_.personaje5.x = this_.widthPersonajes * 1;
            this_.personaje4.x = this_.widthPersonajes * 3.5;
            this_.personaje3.x = this_.widthPersonajes * 5.5;
            this_.personaje2.x = this_.widthPersonajes * 8;
            this_.personaje1.x = this_.widthPersonajes * 10.5;

            this_.personaje5.scale.setTo(1, 1);
            this_.personaje4.scale.setTo(1, 1);
            this_.personaje3.scale.setTo(1, 1);
            this_.personaje2.scale.setTo(1, 1);
            this_.personaje1.scale.setTo(1, 1);

            this_.personaje5.y = (this_.heightPersonajes * 12) - this_.personaje5.height;
            this_.personaje4.y = (this_.heightPersonajes * 11) - this_.personaje4.height;
            this_.personaje3.y = (this_.heightPersonajes * 10) - this_.personaje3.height;
            this_.personaje2.y = (this_.heightPersonajes * 11) - this_.personaje2.height;
            this_.personaje1.y = (this_.heightPersonajes * 12) - this_.personaje1.height;
        }
        else if ($(window).width() > 992) {
            this_.personaje5.x = this_.widthPersonajes * 1;
            this_.personaje4.x = this_.widthPersonajes * 3.5;
            this_.personaje3.x = this_.widthPersonajes * 5.5;
            this_.personaje2.x = this_.widthPersonajes * 8;
            this_.personaje1.x = this_.widthPersonajes * 10.5;

            this_.personaje5.scale.setTo(0.8, 0.8);
            this_.personaje4.scale.setTo(0.75, 0.75);
            this_.personaje3.scale.setTo(0.7, 0.7);
            this_.personaje2.scale.setTo(0.75, 0.75);
            this_.personaje1.scale.setTo(0.8, 0.8);

            this_.personaje5.y = (this_.heightPersonajes * 12) - this_.personaje5.height;
            this_.personaje4.y = (this_.heightPersonajes * 11) - this_.personaje4.height;
            this_.personaje3.y = (this_.heightPersonajes * 10) - this_.personaje3.height;
            this_.personaje2.y = (this_.heightPersonajes * 11) - this_.personaje2.height;
            this_.personaje1.y = (this_.heightPersonajes * 12) - this_.personaje1.height;
        }
        else if ($(window).width() > 768) {
            this_.personaje5.x = this_.widthPersonajes * 0.5;
            this_.personaje4.x = this_.widthPersonajes * 3;
            this_.personaje3.x = this_.widthPersonajes * 5.5;
            this_.personaje2.x = this_.widthPersonajes * 7.5;
            this_.personaje1.x = this_.widthPersonajes * 10;

            this_.personaje5.scale.setTo(0.7, 0.7);
            this_.personaje4.scale.setTo(0.65, 0.65);
            this_.personaje3.scale.setTo(0.6, 0.6);
            this_.personaje2.scale.setTo(0.65, 0.65);
            this_.personaje1.scale.setTo(0.65, 0.65);

            this_.personaje5.y = (this_.heightPersonajes * 12) - this_.personaje5.height;
            this_.personaje4.y = (this_.heightPersonajes * 11) - this_.personaje4.height;
            this_.personaje3.y = (this_.heightPersonajes * 10) - this_.personaje3.height;
            this_.personaje2.y = (this_.heightPersonajes * 11) - this_.personaje2.height;
            this_.personaje1.y = (this_.heightPersonajes * 12) - this_.personaje1.height;
            if (this_.btnJugar) {
                this_.btnJugar.scale.setTo(0.8, 0.8);
                this_.btnJugar.x = game.world.centerX - (this_.btnJugar.width / 2);
                this_.btnJugar.y = (this_.heightPersonajes * 4) - this_.btnJugar.height;
            };
        }
        else if ($(window).width() > 576) {
            this_.personaje5.x = this_.widthPersonajes * 0.5;
            this_.personaje4.x = this_.widthPersonajes * 3;
            this_.personaje3.x = this_.widthPersonajes * 5.5;
            this_.personaje2.x = this_.widthPersonajes * 7.5;
            this_.personaje1.x = this_.widthPersonajes * 10;

            this_.personaje5.scale.setTo(0.55, 0.55);
            this_.personaje4.scale.setTo(0.5, 0.5);
            this_.personaje3.scale.setTo(0.45, 0.45);
            this_.personaje2.scale.setTo(0.5, 0.5);
            this_.personaje1.scale.setTo(0.55, 0.55);

            this_.personaje5.y = (this_.heightPersonajes * 12) - this_.personaje5.height;
            this_.personaje4.y = (this_.heightPersonajes * 11) - this_.personaje4.height;
            this_.personaje3.y = (this_.heightPersonajes * 10) - this_.personaje3.height;
            this_.personaje2.y = (this_.heightPersonajes * 11) - this_.personaje2.height;
            this_.personaje1.y = (this_.heightPersonajes * 12) - this_.personaje1.height;
            if (this_.btnJugar) {
                this_.btnJugar.scale.setTo(0.7, 0.7);
                this_.btnJugar.x = game.world.centerX - (this_.btnJugar.width / 2);
                this_.btnJugar.y = (this_.heightPersonajes * 4) - this_.btnJugar.height;
            };
        }
        else if ($(window).width() > 279) {

            this_.personaje4.visible = false;
            this_.personaje2.visible = false;
            this_.personaje1.visible = false;

            this_.personaje5.x = this_.widthPersonajes * 2;
            this_.personaje3.x = this_.widthPersonajes * 7;

            this_.personaje5.scale.setTo(0.55, 0.55);
            this_.personaje3.scale.setTo(0.5, 0.5);

            this_.personaje5.y = (this_.heightPersonajes * 11) - this_.personaje5.height;
            this_.personaje3.y = (this_.heightPersonajes * 10) - this_.personaje3.height;

            if (this_.btnJugar) {
                this_.btnJugar.scale.setTo(0.7, 0.7);
                this_.btnJugar.x = game.world.centerX - (this_.btnJugar.width / 2);
                this_.btnJugar.y = (this_.heightPersonajes * 4) - this_.btnJugar.height;
            };
        }
        else {
            this_.personaje5.visible = false;
            this_.personaje4.visible = false;
            this_.personaje2.visible = false;
            this_.personaje1.visible = false;

            this_.personaje3.scale.setTo(0.4, 0.4);
            this_.personaje3.x = $(window).width() / 2 - this_.personaje3.width / 2;
            this_.personaje3.y = (this_.heightPersonajes * 10) - this_.personaje3.height;

            if (this_.btnJugar) {
                this_.btnJugar.scale.setTo(0.3, 0.3);
                this_.btnJugar.x = game.world.centerX - (this_.btnJugar.width / 2);
                this_.btnJugar.y = (this_.heightPersonajes * 4) - this_.btnJugar.height;
            };
        }

        var min = 2000;
        var max = 4000;
        var time = Math.round(Math.random() * (max - min) + min);
        var scale = 0.3;
        game.tweens.removeFrom(this_.btnJugar.scale);
        game.tweens.removeFrom(this_.btnJugar.position);
        game.tweens.removeFrom(this_.personaje5.scale);
        game.tweens.removeFrom(this_.personaje5.position);
        game.tweens.removeFrom(this_.personaje4.scale);
        game.tweens.removeFrom(this_.personaje4.position);
        game.tweens.removeFrom(this_.personaje3.scale);
        game.tweens.removeFrom(this_.personaje3.position);
        game.tweens.removeFrom(this_.personaje2.scale);
        game.tweens.removeFrom(this_.personaje2.position);
        game.tweens.removeFrom(this_.personaje1.scale);
        game.tweens.removeFrom(this_.personaje1.position);

        var scaleBeforeX = this_.personaje5.scale.x + scale;
        var scaleBeforeY = this_.personaje5.scale.x + scale;
        var positionX = this_.personaje5.x - (this_.personaje5.width + (this_.personaje5.width * scale) - this_.personaje5.width) / 2;
        var positionY = this_.personaje5.y - (this_.personaje5.height + (this_.personaje5.height * scale) - this_.personaje5.height) / 2;
        game.add.tween(this_.personaje5.scale)
            .to({ x: scaleBeforeX, y: scaleBeforeY }, time, Phaser.Easing.Elastic.Out, true).loop().yoyo(true, time);
        game.add.tween(this_.personaje5.position)
            .to({ x: positionX, y: positionY }, time, Phaser.Easing.Elastic.Out, true).loop().yoyo(true, time);

        time = Math.round(Math.random() * (max - min) + min)
        scaleBeforeX = this_.personaje4.scale.x - scale;
        scaleBeforeY = this_.personaje4.scale.x - scale;
        positionX = this_.personaje4.x - (this_.personaje4.width - (this_.personaje4.width * scale) - this_.personaje4.width) / 2;
        positionY = this_.personaje4.y - (this_.personaje4.height - (this_.personaje4.height * scale) - this_.personaje4.height) / 2;
        game.add.tween(this_.personaje4.scale)
            .to({ x: scaleBeforeX, y: scaleBeforeY }, time, Phaser.Easing.Circular.Out, true).loop().yoyo(true, time);
        game.add.tween(this_.personaje4.position)
            .to({ x: positionX, y: positionY }, time, Phaser.Easing.Circular.Out, true).loop().yoyo(true, time);


        time = Math.round(Math.random() * (max - min) + min)
        scaleBeforeX = this_.personaje3.scale.x + scale;
        scaleBeforeY = this_.personaje3.scale.x + scale;
        positionX = this_.personaje3.x - (this_.personaje3.width + (this_.personaje3.width * scale) - this_.personaje3.width) / 2;
        positionY = this_.personaje3.y - (this_.personaje3.height + (this_.personaje3.height * scale) - this_.personaje3.height) / 2;
        game.add.tween(this_.personaje3.scale)
            .to({ x: scaleBeforeX, y: scaleBeforeY }, time, Phaser.Easing.Bounce.Out, true).loop().yoyo(true, time);
        game.add.tween(this_.personaje3.position)
            .to({ x: positionX, y: positionY }, time, Phaser.Easing.Bounce.Out, true).loop().yoyo(true, time);


        time = Math.round(Math.random() * (max - min) + min)
        scaleBeforeX = this_.personaje2.scale.x - scale;
        scaleBeforeY = this_.personaje2.scale.x - scale;
        positionX = this_.personaje2.x - (this_.personaje2.width - (this_.personaje2.width * scale) - this_.personaje2.width) / 2;
        positionY = this_.personaje2.y - (this_.personaje2.height - (this_.personaje2.height * scale) - this_.personaje2.height) / 2;
        game.add.tween(this_.personaje2.scale)
            .to({ x: scaleBeforeX, y: scaleBeforeY }, time, Phaser.Easing.Linear.None, true).loop().yoyo(true, time);
        game.add.tween(this_.personaje2.position)
            .to({ x: positionX, y: positionY }, time, Phaser.Easing.Linear.None, true).loop().yoyo(true, time);


        time = Math.round(Math.random() * (max - min) + min)
        scaleBeforeX = this_.personaje1.scale.x + scale;
        scaleBeforeY = this_.personaje1.scale.x + scale;
        positionX = this_.personaje1.x - (this_.personaje1.width + (this_.personaje1.width * scale) - this_.personaje1.width) / 2;
        positionY = this_.personaje1.y - (this_.personaje1.height + (this_.personaje1.height * scale) - this_.personaje1.height) / 2;
        game.add.tween(this_.personaje1.scale)
            .to({ x: scaleBeforeX, y: scaleBeforeY }, time, Phaser.Easing.Exponential.Out, true).loop().yoyo(true, time);
        game.add.tween(this_.personaje1.position)
            .to({ x: positionX, y: positionY }, time, Phaser.Easing.Exponential.Out, true).loop().yoyo(true, time);


        time = 2000;
        scaleBeforeX = this_.btnJugar.scale.x + 0.08;
        scaleBeforeY = this_.btnJugar.scale.x + 0.08;
        positionX = this_.btnJugar.x - (this_.btnJugar.width + (this_.btnJugar.width * 0.08) - this_.btnJugar.width) / 2;
        positionY = this_.btnJugar.y - (this_.btnJugar.height + (this_.btnJugar.height * 0.08) - this_.btnJugar.height) / 2;
        game.add.tween(this_.btnJugar.scale)
            .to({ x: scaleBeforeX, y: scaleBeforeY }, time, Phaser.Easing.Bounce.Out, true).loop().yoyo(true, time);
        game.add.tween(this_.btnJugar.position)
            .to({ x: positionX, y: positionY }, time, Phaser.Easing.Bounce.Out, true).loop().yoyo(true, time);

        this.animationGame(context);
    },

    animationPrincipal: function (context) {
        var this_ = context;
        var scaleOriginal = this_.personaje5.scale.x;
        var scaleBefore = this_.personaje5.scale.x - 0.1;
        var tween = game.add.tween(this_.personaje5.scale)
            .to({ x: scaleBefore, y: scaleBefore }, 100, Phaser.Easing.Linear.None)
            .to({ x: scaleOriginal, y: scaleOriginal }, 100, Phaser.Easing.Linear.None)
            .start();
    },

    wiggle: function (aProgress: number, aPeriod1: number, aPeriod2: number): number {
        var current1: number = aProgress * Math.PI * 2 * aPeriod1;
        var current2: number = aProgress * (Math.PI * 2 * aPeriod2 + Math.PI / 2);

        return Math.sin(current1) * Math.cos(current2);
    },

    animationGame: function (context) {

        var slider = game['phaseSlider'];
        if (slider) {
            var min = 1000;
            var max = 3000;
            var time = Math.round(Math.random() * (max - min) + min);
            var scale = 0.07;
            //var count = 1;
            slider.sliderMainGroup.children.forEach(function (groupItem) {
                groupItem.children.forEach(function (childrenItem) {
                    //if (count == 1) {
                    //    console.log(childrenItem.scale.x);
                    //};
                    game.tweens.removeFrom(childrenItem.scale);
                    game.tweens.removeFrom(childrenItem.position);
                    time = Math.round(Math.random() * (max - min) + min);
                    scale = Math.random() * (0.05 - 0.02) + 0.02;
                    var delay = Math.round(Math.random() * (6000 - 3000) + 2000);

                    var scaleBeforeX = childrenItem.scale.x + scale;
                    var scaleBeforeY = childrenItem.scale.x + scale;
                    var positionX = childrenItem.x - (childrenItem.width + (childrenItem.width * scale) - childrenItem.width) / 2;
                    var positionY = childrenItem.y - (childrenItem.height + (childrenItem.height * scale) - childrenItem.height) / 2;
                    //game.add.tween(childrenItem.scale)
                    //    .to({ x: scaleBeforeX, y: scaleBeforeY }, time, Phaser.Easing.Elastic.Out, true, delay).loop().yoyo(true, time);
                    //game.add.tween(childrenItem.position)
                    //    .to({ x: positionX, y: positionY }, time, Phaser.Easing.Elastic.Out, true, delay).loop().yoyo(true, time);

                    var aRadius = 10;
                    var aXPeriod1 = 10;
                    var aXPeriod2 = -10;

                    game.add.tween(childrenItem.position)
                        //.to({ x: childrenItem.position.x + aRadius }, 200, function (k) {
                        //    return mixins.wiggle(k, aXPeriod1, aXPeriod2);
                        //}, true, delay)
                        .to({ x: childrenItem.position.x + aRadius }, 200, function (k) {
                            return mixins.wiggle(k, aXPeriod1, aXPeriod2);
                        }, true, delay).loop().yoyo(true, time * 2);
                    ////count++;

                });
            });
        }

        //game.phaseSlider.sliderMainGroup.children.forEach(function (groupItem) {
        //    console.log(groupItem);
        //});
        //var this_ = context;
        //console.log(this_.groupFinally);

        //game.tweens.removeFrom(this_.btnJugar.scale);
        //game.tweens.removeFrom(this_.btnJugar.position);



        //var scaleBeforeX = this_.personaje5.scale.x + scale;
        //var scaleBeforeY = this_.personaje5.scale.x + scale;
        //var positionX = this_.personaje5.x - (this_.personaje5.width + (this_.personaje5.width * scale) - this_.personaje5.width) / 2;
        //var positionY = this_.personaje5.y - (this_.personaje5.height + (this_.personaje5.height * scale) - this_.personaje5.height) / 2;
        //game.add.tween(this_.personaje5.scale)
        //    .to({ x: scaleBeforeX, y: scaleBeforeY }, time, Phaser.Easing.Elastic.Out, true).loop().yoyo(true, time);
        //game.add.tween(this_.personaje5.position)
        //    .to({ x: positionX, y: positionY }, time, Phaser.Easing.Elastic.Out, true).loop().yoyo(true, time);

    },

    resizeMainMenu: function (context) {
        var this_ = context;
        var widthColumn = $(window).width() / 12;
        var heightRow = $(window).height() / 12;
        this_.btnBackPagePrincipal.scale.setTo(0.3, 0.3);
        this_.btnBackPagePrincipal.x = 10;
        this_.btnBackPagePrincipal.scale.setTo(0.3, 0.3);
        this_.btnSound.scale.setTo(0.3, 0.3);
        if ($(window).width() > 1200) {
            if ($(window).height() > 600) {
            }
            else if ($(window).height() > 480) {
            }
            else if ($(window).height() > 400) {
            }
            else if ($(window).height() > 360) {
            }
            else if ($(window).height() > 270) {
            }
            else if ($(window).height() > 210) {
            }
            else {
            }
        }
        else if ($(window).width() > 992) {
            if ($(window).height() > 600) {
            }
            else if ($(window).height() > 480) {
            }
            else if ($(window).height() > 400) {
            }
            else if ($(window).height() > 360) {
            }
            else if ($(window).height() > 270) {
            }
            else if ($(window).height() > 210) {
                this_.btnBackPagePrincipal.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
            }
            else {
                this_.btnBackPagePrincipal.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
            }
        }
        else if ($(window).width() > 768) {
            if ($(window).height() > 600) {
            }
            else if ($(window).height() > 480) {
            }
            else if ($(window).height() > 400) {
            }
            else if ($(window).height() > 360) {
                this_.btnBackPagePrincipal.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
            }
            else if ($(window).height() > 270) {
                this_.btnBackPagePrincipal.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
            }
            else if ($(window).height() > 210) {
                this_.btnBackPagePrincipal.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
            }
            else {
                this_.btnBackPagePrincipal.scale.setTo(0.2, 0.2);
                this_.btnSound.scale.setTo(0.2, 0.2);
            }
        }
        else if ($(window).width() > 576) {
            if ($(window).height() > 600) {
            }
            else if ($(window).height() > 480) {
            }
            else if ($(window).height() > 400) {
            }
            else if ($(window).height() > 360) {
                this_.btnBackPagePrincipal.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
            }
            else if ($(window).height() > 270) {
                this_.btnBackPagePrincipal.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
            }
            else if ($(window).height() > 210) {
                this_.btnBackPagePrincipal.scale.setTo(0.2, 0.2);
                this_.btnSound.scale.setTo(0.2, 0.2);
            }
            else {
                this_.btnBackPagePrincipal.scale.setTo(0.2, 0.2);
                this_.btnSound.scale.setTo(0.2, 0.2);
            }
        }
        else if ($(window).width() > 279) {
            this_.btnBackPagePrincipal.scale.setTo(0.25, 0.25);
            this_.btnSound.scale.setTo(0.25, 0.25);
            if ($(window).height() > 600) {
            }
            else if ($(window).height() > 480) {
            }
            else if ($(window).height() > 400) {
                this_.btnBackPagePrincipal.scale.setTo(0.2, 0.2);
                this_.btnSound.scale.setTo(0.2, 0.2);
            }
            else if ($(window).height() > 360) {
                this_.btnBackPagePrincipal.scale.setTo(0.2, 0.2);
                this_.btnSound.scale.setTo(0.2, 0.2);
            }
            else if ($(window).height() > 270) {
                this_.btnBackPagePrincipal.scale.setTo(0.15, 0.15);
                this_.btnSound.scale.setTo(0.15, 0.15);
            }
            else if ($(window).height() > 210) {
                this_.btnBackPagePrincipal.scale.setTo(0.2, 0.2);
                this_.btnSound.scale.setTo(0.2, 0.2);
            }
            else {
                this_.btnBackPagePrincipal.scale.setTo(0.15, 0.15);
                this_.btnSound.scale.setTo(0.15, 0.15);
            }
        }
        else {
            this_.btnBackPagePrincipal.scale.setTo(0.2, 0.2);
            this_.btnSound.scale.setTo(0.2, 0.2);
            if ($(window).height() > 600) {
                this_.btnBackPagePrincipal.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
            }
            else if ($(window).height() > 480) {
            }
            else if ($(window).height() > 400) {
            }
            else if ($(window).height() > 360) {
            }
            else if ($(window).height() > 270) {
                this_.btnBackPagePrincipal.scale.setTo(0.15, 0.15);
                this_.btnSound.scale.setTo(0.15, 0.15);
            }
            else if ($(window).height() > 210) {
                this_.btnBackPagePrincipal.scale.setTo(0.15, 0.15);
                this_.btnSound.scale.setTo(0.15, 0.15);
            }
            else {
                this_.btnBackPagePrincipal.scale.setTo(0.15, 0.15);
                this_.btnSound.scale.setTo(0.15, 0.15);
            }
        }
 
        this_.btnBackPagePrincipal.y = game.world.height - this_.btnBackPagePrincipal.height;
        this_.btnSound.y = game.world.height - this_.btnSound.height;
        this_.btnSound.x = 10 + this_.btnBackPagePrincipal.width;
    },

    resizeLevel: function (context) {
        var this_ = context;
        var widthColumn = $(window).width() / 12;
        var heightRow = $(window).height() / 12;
        this_.btnHome.scale.setTo(0.3, 0.3);
        this_.btnHome.x = 10;
        this_.btnSound.scale.setTo(0.3, 0.3);
        this_.leftArrow.scale.setTo(1, 1);
        this_.rightArrow.scale.setTo(1, 1);
        this_.leftArrow.y = game.world.centerY - (this_.leftArrow.height / 2);
        this_.rightArrow.y = game.world.centerY - (this_.leftArrow.height / 2);
        var countChildren = this_.scrollingMap.children.length;
        if ($(window).width() > 1200) {

            if ($(window).height() > 600) {
                this_.columns = 4;
                this_.rows = 3;
                this_.scaleIcon = 0.6;
                this_.cartelActividadesEscrito.scale.setTo(0.75, 0.75);
            }
            else if ($(window).height() > 480) {
                this_.columns = 5;
                this_.rows = 2;
                this_.scaleIcon = 0.6;
                this_.cartelActividadesEscrito.scale.setTo(0.8, 0.8);
            }
            else if ($(window).height() > 400) {
                this_.columns = 4;
                this_.rows = 2;
                this_.scaleIcon = 0.6;
                this_.cartelActividadesEscrito.scale.setTo(0.7, 0.7);
            }
            else if ($(window).height() > 360) {
                this_.columns = 4;
                this_.rows = 2;
                this_.scaleIcon = 0.5;
                this_.cartelActividadesEscrito.scale.setTo(0.6, 0.6);
            }
            else if ($(window).height() > 270) {
                this_.columns = 6;
                this_.rows = 1;
                this_.scaleIcon = 0.6;
                this_.cartelActividadesEscrito.scale.setTo(0.6, 0.6);
            }
            else if ($(window).height() > 210) {
                this_.columns = 6;
                this_.rows = 1;
                this_.scaleIcon = 0.5;
                this_.cartelActividadesEscrito.scale.setTo(0.6, 0.6);
            }
            else {
                this_.columns = 6;
                this_.rows = 1;
                this_.scaleIcon = 0.4;
                this_.cartelActividadesEscrito.scale.setTo(0.4, 0.4);
            }
        }
        else if ($(window).width() > 992) {
            if ($(window).height() > 600) {
                this_.columns = 4;
                this_.rows = 3;
                this_.scaleIcon = 0.6;
                this_.cartelActividadesEscrito.scale.setTo(0.75, 0.75);
            }
            else if ($(window).height() > 480) {
                this_.columns = 5;
                this_.rows = 2;
                this_.scaleIcon = 0.6;
                this_.cartelActividadesEscrito.scale.setTo(0.8, 0.8);
            }
            else if ($(window).height() > 400) {
                this_.columns = 4;
                this_.rows = 2;
                this_.scaleIcon = 0.6;
                this_.cartelActividadesEscrito.scale.setTo(0.7, 0.7);
            }
            else if ($(window).height() > 360) {
                this_.columns = 4;
                this_.rows = 2;
                this_.scaleIcon = 0.5;
                this_.cartelActividadesEscrito.scale.setTo(0.6, 0.6);
            }
            else if ($(window).height() > 270) {
                this_.columns = 6;
                this_.rows = 1;
                this_.scaleIcon = 0.6;
                this_.cartelActividadesEscrito.scale.setTo(0.6, 0.6);
            }
            else if ($(window).height() > 210) {
                this_.columns = 6;
                this_.rows = 1;
                this_.scaleIcon = 0.5;
                this_.cartelActividadesEscrito.scale.setTo(0.6, 0.6);
                this_.btnHome.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
            }
            else {
                this_.columns = 6;
                this_.rows = 1;
                this_.scaleIcon = 0.4;
                this_.cartelActividadesEscrito.scale.setTo(0.4, 0.4);
                this_.btnHome.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
            }
        }
        else if ($(window).width() > 768) {
            if ($(window).height() > 600) {
                this_.columns = 4;
                this_.rows = 3;
                this_.scaleIcon = 0.6;
                this_.cartelActividadesEscrito.scale.setTo(0.75, 0.75);
            }
            else if ($(window).height() > 480) {
                this_.columns = 4;
                this_.rows = 2;
                this_.scaleIcon = 0.6;
                this_.cartelActividadesEscrito.scale.setTo(0.75, 0.75);
            }
            else if ($(window).height() > 400) {
                this_.columns = 4;
                this_.rows = 2;
                this_.scaleIcon = 0.6;
                this_.cartelActividadesEscrito.scale.setTo(0.7, 0.7);
            }
            else if ($(window).height() > 360) {
                this_.columns = 4;
                this_.rows = 2;
                this_.scaleIcon = 0.5;
                this_.cartelActividadesEscrito.scale.setTo(0.6, 0.6);
                this_.btnHome.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
            }
            else if ($(window).height() > 270) {
                this_.columns = 5;
                this_.rows = 1;
                this_.scaleIcon = 0.5;
                this_.cartelActividadesEscrito.scale.setTo(0.6, 0.6);
                this_.btnHome.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
            }
            else if ($(window).height() > 210) {
                this_.columns = 5;
                this_.rows = 1;
                this_.scaleIcon = 0.5;
                this_.cartelActividadesEscrito.scale.setTo(0.6, 0.6);
                this_.btnHome.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
            }
            else {
                this_.columns = 5;
                this_.rows = 1;
                this_.scaleIcon = 0.4;
                this_.cartelActividadesEscrito.scale.setTo(0.4, 0.4);
                this_.btnHome.scale.setTo(0.2, 0.2);
                this_.btnSound.scale.setTo(0.2, 0.2);
            }
        }
        else if ($(window).width() > 576) {
            if ($(window).height() > 600) {
                this_.columns = 3;
                this_.rows = 3;
                this_.scaleIcon = 0.6;
                this_.cartelActividadesEscrito.scale.setTo(0.65, 0.65);
            }
            else if ($(window).height() > 480) {
                this_.columns = 3;
                this_.rows = 2;
                this_.scaleIcon = 0.6;
                this_.cartelActividadesEscrito.scale.setTo(0.6, 0.6);
            }
            else if ($(window).height() > 400) {
                this_.columns = 3;
                this_.rows = 2;
                this_.scaleIcon = 0.6;
                this_.cartelActividadesEscrito.scale.setTo(0.6, 0.6);
            }
            else if ($(window).height() > 360) {
                this_.columns = 4;
                this_.rows = 2;
                this_.scaleIcon = 0.5;
                this_.cartelActividadesEscrito.scale.setTo(0.6, 0.6);
                this_.btnHome.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);

                this_.leftArrow.scale.setTo(0.8, 0.8);
                this_.rightArrow.scale.setTo(0.8, 0.8);
                this_.leftArrow.y = ($(window).height() * 60 / 100) - (this_.leftArrow.height / 2);
                this_.rightArrow.y = ($(window).height() * 60 / 100) - (this_.leftArrow.height / 2);
            }
            else if ($(window).height() > 270) {
                this_.columns = 4;
                this_.rows = 1;
                this_.scaleIcon = 0.5;
                this_.cartelActividadesEscrito.scale.setTo(0.5, 0.5);
                this_.btnHome.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
                this_.leftArrow.scale.setTo(0.8, 0.8);
                this_.rightArrow.scale.setTo(0.8, 0.8);
                this_.leftArrow.y = ($(window).height() * 60 / 100) - (this_.leftArrow.height / 2);
                this_.rightArrow.y = ($(window).height() * 60 / 100) - (this_.leftArrow.height / 2);
            }
            else if ($(window).height() > 210) {
                this_.columns = 4;
                this_.rows = 1;
                this_.scaleIcon = 0.45;
                this_.cartelActividadesEscrito.scale.setTo(0.5, 0.5);
                this_.btnHome.scale.setTo(0.2, 0.2);
                this_.btnSound.scale.setTo(0.2, 0.2);
                this_.leftArrow.scale.setTo(0.8, 0.8);
                this_.rightArrow.scale.setTo(0.8, 0.8);
                this_.leftArrow.y = ($(window).height() * 60 / 100) - (this_.leftArrow.height / 2);
                this_.rightArrow.y = ($(window).height() * 60 / 100) - (this_.leftArrow.height / 2);
            }
            else {
                this_.columns = 4;
                this_.rows = 1;
                this_.scaleIcon = 0.4;
                this_.cartelActividadesEscrito.scale.setTo(0.4, 0.4);
                this_.btnHome.scale.setTo(0.2, 0.2);
                this_.btnSound.scale.setTo(0.2, 0.2);
                this_.leftArrow.scale.setTo(0.8, 0.8);
                this_.rightArrow.scale.setTo(0.8, 0.8);
                this_.leftArrow.y = ($(window).height() * 60 / 100) - (this_.leftArrow.height / 2);
                this_.rightArrow.y = ($(window).height() * 60 / 100) - (this_.leftArrow.height / 2);
            }
        }
        else if ($(window).width() > 279) {
            this_.btnHome.scale.setTo(0.25, 0.25);
            this_.btnSound.scale.setTo(0.25, 0.25);
            this_.leftArrow.scale.setTo(0.7, 0.7);
            this_.rightArrow.scale.setTo(0.7, 0.7);
            if ($(window).height() > 600) {
                this_.columns = 2;
                this_.rows = 4;
                this_.scaleIcon = 0.5;
                this_.cartelActividadesEscrito.scale.setTo(0.35, 0.35);
            }
            else if ($(window).height() > 480) {
                this_.columns = 2;
                this_.rows = 3;
                this_.scaleIcon = 0.5;
                this_.cartelActividadesEscrito.scale.setTo(0.35, 0.35);
            }
            else if ($(window).height() > 400) {
                this_.columns = 2;
                this_.rows = 3;
                this_.scaleIcon = 0.5;
                this_.cartelActividadesEscrito.scale.setTo(0.35, 0.35);
                this_.btnHome.scale.setTo(0.2, 0.2);
                this_.btnSound.scale.setTo(0.2, 0.2);
            }
            else if ($(window).height() > 360) {
                this_.columns = 2;
                this_.rows = 3;
                this_.scaleIcon = 0.4;
                this_.cartelActividadesEscrito.scale.setTo(0.35, 0.35);
                this_.btnHome.scale.setTo(0.2, 0.2);
                this_.btnSound.scale.setTo(0.2, 0.2);
            }
            else if ($(window).height() > 270) {
                this_.columns = 2;
                this_.rows = 3;
                this_.scaleIcon = 0.3;
                this_.cartelActividadesEscrito.scale.setTo(0.35, 0.35);
                this_.btnHome.scale.setTo(0.15, 0.15);
                this_.btnSound.scale.setTo(0.15, 0.15);
                this_.leftArrow.scale.setTo(0.6, 0.6);
                this_.rightArrow.scale.setTo(0.6, 0.6);
            }
            else if ($(window).height() > 210) {
                this_.columns = 2;
                this_.rows = 1;
                this_.scaleIcon = 0.45;
                this_.cartelActividadesEscrito.scale.setTo(0.35, 0.35);
                this_.btnHome.scale.setTo(0.2, 0.2);
                this_.btnSound.scale.setTo(0.2, 0.2);
                this_.leftArrow.scale.setTo(0.6, 0.6);
                this_.rightArrow.scale.setTo(0.6, 0.6);
                this_.leftArrow.y = ($(window).height() * 65 / 100) - (this_.leftArrow.height / 2);
                this_.rightArrow.y = ($(window).height() * 65 / 100) - (this_.leftArrow.height / 2);
            }
            else {
                this_.columns = 2;
                this_.rows = 1;
                this_.scaleIcon = 0.3;
                this_.cartelActividadesEscrito.scale.setTo(0.35, 0.35);
                this_.btnHome.scale.setTo(0.15, 0.15);
                this_.btnSound.scale.setTo(0.15, 0.15);
                this_.leftArrow.scale.setTo(0.5, 0.5);
                this_.rightArrow.scale.setTo(0.5, 0.5);
                this_.leftArrow.y = ($(window).height() * 70 / 100) - (this_.leftArrow.height / 2);
                this_.rightArrow.y = ($(window).height() * 70 / 100) - (this_.leftArrow.height / 2);
            }
        }
        else {
            this_.btnHome.scale.setTo(0.2, 0.2);
            this_.btnSound.scale.setTo(0.2, 0.2);
            this_.leftArrow.scale.setTo(0.6, 0.6);
            this_.rightArrow.scale.setTo(0.6, 0.6);
            if ($(window).height() > 600) {
                this_.columns = 1;
                this_.rows = 5;
                this_.scaleIcon = 0.4;
                this_.cartelActividadesEscrito.scale.setTo(0.2, 0.2);
                this_.btnHome.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
            }
            else if ($(window).height() > 480) {
                this_.columns = 1;
                this_.rows = 4;
                this_.scaleIcon = 0.4;
                this_.cartelActividadesEscrito.scale.setTo(0.2, 0.2);
            }
            else if ($(window).height() > 400) {
                this_.columns = 1;
                this_.rows = 4;
                this_.scaleIcon = 0.35;
                this_.cartelActividadesEscrito.scale.setTo(0.2, 0.2);
            }
            else if ($(window).height() > 360) {
                this_.columns = 1;
                this_.rows = 3;
                this_.scaleIcon = 0.35;
                this_.cartelActividadesEscrito.scale.setTo(0.2, 0.2);
            }
            else if ($(window).height() > 270) {
                this_.columns = 1;
                this_.rows = 3;
                this_.scaleIcon = 0.3;
                this_.cartelActividadesEscrito.scale.setTo(0.2, 0.2);
                this_.btnHome.scale.setTo(0.15, 0.15);
                this_.btnSound.scale.setTo(0.15, 0.15);
            }
            else if ($(window).height() > 210) {
                this_.columns = 1;
                this_.rows = 2;
                this_.scaleIcon = 0.3;
                this_.cartelActividadesEscrito.scale.setTo(0.2, 0.2);
                this_.btnHome.scale.setTo(0.15, 0.15);
                this_.btnSound.scale.setTo(0.15, 0.15);
            }
            else {
                this_.columns = 1;
                this_.rows = 1;
                this_.scaleIcon = 0.3;
                this_.cartelActividadesEscrito.scale.setTo(0.2, 0.2);
                this_.btnHome.scale.setTo(0.15, 0.15);
                this_.btnSound.scale.setTo(0.15, 0.15);
                this_.leftArrow.y = ($(window).height() * 65 / 100) - (this_.leftArrow.height / 2);
                this_.rightArrow.y = ($(window).height() * 65 / 100) - (this_.leftArrow.height / 2);
            }
        }

        this_.pages = Math.ceil(countChildren / (this_.rows * this_.columns));
        this_.thumbWidth = 140 * this_.scaleIcon;
        this_.thumbHeight = 174 * this_.scaleIcon;
        this_.spacing = this_.thumbWidth * 40 / 100;

        this_.margin = this_.cartelActividadesEscrito.height * 40 / 100;
        var columHeight = this_.thumbHeight * this_.rows + this_.spacing * (this_.rows - 1) + this_.cartelActividadesEscrito.height + 30;

        this_.cartelActividadesEscrito.y = game.world.centerY - (columHeight / 2);
        this_.cartelActividadesEscrito.x = game.world.centerX - (this_.cartelActividadesEscrito.width / 2);

        this_.scrollingMap.width = this_.pages * $(window).width();
        this_.scrollingMap.height = $(window).height();
        this_.scrollingMap.input.boundsRect.x = $(window).width() - this_.pages * $(window).width();
        this_.scrollingMap.input.boundsRect.y = $(window).height() - $(window).height();
        this_.scrollingMap.input.boundsRect.width = (this_.pages * $(window).width() * 2) - $(window).width();
        this_.scrollingMap.input.boundsRect.height = $(window).height() * 2 - $(window).height();

        var rowLength = this_.thumbWidth * this_.columns + this_.spacing * (this_.columns - 1);
        this_.leftMargin = ($(window).width() - rowLength) / 2;
        this_.leftArrow.x = this_.leftMargin / 2;
        this_.rightArrow.x = game.world.width - (this_.leftMargin / 2);
        this_.btnHome.y = game.world.height - this_.btnHome.height;
        this_.btnSound.y = game.world.height - this_.btnSound.height;
        this_.btnSound.x = 10 + this_.btnHome.width;

        var levelnr = 0;
        for (var k = 0; k < this_.pages; k++) {
            for (var y = 0; y < this_.rows; y++) {
                for (var x = 0; x < this_.columns; x++) {
                    var xpos = k * $(window).width() + this_.leftMargin + x * (this_.thumbWidth + this_.spacing);
                    var ypos = (game.world.centerY - (columHeight / 2)) + this_.cartelActividadesEscrito.height + this_.margin + (y * (this_.thumbHeight + this_.spacing));
                    var IconGroup = this_.scrollingMap.children[levelnr];
                    if (IconGroup) {
                        IconGroup.width = this_.thumbWidth;
                        IconGroup.height = this_.thumbHeight;
                        IconGroup.x = xpos;
                        IconGroup.y = ypos;
                        IconGroup.xOrg = xpos;
                        IconGroup.yOrg = ypos;
                        levelnr = levelnr + 1;
                    };
                };
            };
        };

        this_.currentPage = 0;
        this_.changePage(0);
    },

    resizeStage: function (context) {
        var this_ = context;
        var widthColumn = $(window).width() / 12;
        var heightRow = $(window).height() / 12;
        this_.btnHome.scale.setTo(0.3, 0.3);
        this_.btnHome.x = 10;
        this_.btnSound.scale.setTo(0.3, 0.3);
        this_.leftArrow.scale.setTo(1, 1);
        this_.rightArrow.scale.setTo(1, 1);
        this_.leftArrow.y = game.world.centerY - (this_.leftArrow.height / 2);
        this_.rightArrow.y = game.world.centerY - (this_.leftArrow.height / 2);
        var countChildren = this_.scrollingMap.children.length;
        if ($(window).width() > 1200) {

            if ($(window).height() > 600) {
                this_.columns = 4;
                this_.rows = 3;
                this_.scaleIcon = 0.6;
                this_.cartelEtapaEscrito.scale.setTo(0.75, 0.75);
            }
            else if ($(window).height() > 480) {
                this_.columns = 5;
                this_.rows = 2;
                this_.scaleIcon = 0.6;
                this_.cartelEtapaEscrito.scale.setTo(0.8, 0.8);
            }
            else if ($(window).height() > 400) {
                this_.columns = 4;
                this_.rows = 2;
                this_.scaleIcon = 0.6;
                this_.cartelEtapaEscrito.scale.setTo(0.7, 0.7);
            }
            else if ($(window).height() > 360) {
                this_.columns = 4;
                this_.rows = 2;
                this_.scaleIcon = 0.5;
                this_.cartelEtapaEscrito.scale.setTo(0.6, 0.6);
            }
            else if ($(window).height() > 270) {
                this_.columns = 6;
                this_.rows = 1;
                this_.scaleIcon = 0.6;
                this_.cartelEtapaEscrito.scale.setTo(0.6, 0.6);
            }
            else if ($(window).height() > 210) {
                this_.columns = 6;
                this_.rows = 1;
                this_.scaleIcon = 0.5;
                this_.cartelEtapaEscrito.scale.setTo(0.6, 0.6);
            }
            else {
                this_.columns = 6;
                this_.rows = 1;
                this_.scaleIcon = 0.4;
                this_.cartelEtapaEscrito.scale.setTo(0.4, 0.4);
            }
        }
        else if ($(window).width() > 992) {
            if ($(window).height() > 600) {
                this_.columns = 4;
                this_.rows = 3;
                this_.scaleIcon = 0.6;
                this_.cartelEtapaEscrito.scale.setTo(0.75, 0.75);
            }
            else if ($(window).height() > 480) {
                this_.columns = 5;
                this_.rows = 2;
                this_.scaleIcon = 0.6;
                this_.cartelEtapaEscrito.scale.setTo(0.8, 0.8);
            }
            else if ($(window).height() > 400) {
                this_.columns = 4;
                this_.rows = 2;
                this_.scaleIcon = 0.6;
                this_.cartelEtapaEscrito.scale.setTo(0.7, 0.7);
            }
            else if ($(window).height() > 360) {
                this_.columns = 4;
                this_.rows = 2;
                this_.scaleIcon = 0.5;
                this_.cartelEtapaEscrito.scale.setTo(0.6, 0.6);
            }
            else if ($(window).height() > 270) {
                this_.columns = 6;
                this_.rows = 1;
                this_.scaleIcon = 0.6;
                this_.cartelEtapaEscrito.scale.setTo(0.6, 0.6);
            }
            else if ($(window).height() > 210) {
                this_.columns = 6;
                this_.rows = 1;
                this_.scaleIcon = 0.5;
                this_.cartelEtapaEscrito.scale.setTo(0.6, 0.6);
                this_.btnHome.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
            }
            else {
                this_.columns = 6;
                this_.rows = 1;
                this_.scaleIcon = 0.4;
                this_.cartelEtapaEscrito.scale.setTo(0.4, 0.4);
                this_.btnHome.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
            }
        }
        else if ($(window).width() > 768) {
            if ($(window).height() > 600) {
                this_.columns = 4;
                this_.rows = 3;
                this_.scaleIcon = 0.6;
                this_.cartelEtapaEscrito.scale.setTo(0.75, 0.75);
            }
            else if ($(window).height() > 480) {
                this_.columns = 4;
                this_.rows = 2;
                this_.scaleIcon = 0.6;
                this_.cartelEtapaEscrito.scale.setTo(0.75, 0.75);
            }
            else if ($(window).height() > 400) {
                this_.columns = 4;
                this_.rows = 2;
                this_.scaleIcon = 0.6;
                this_.cartelEtapaEscrito.scale.setTo(0.7, 0.7);
            }
            else if ($(window).height() > 360) {
                this_.columns = 4;
                this_.rows = 2;
                this_.scaleIcon = 0.5;
                this_.cartelEtapaEscrito.scale.setTo(0.6, 0.6);
                this_.btnHome.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
            }
            else if ($(window).height() > 270) {
                this_.columns = 5;
                this_.rows = 1;
                this_.scaleIcon = 0.5;
                this_.cartelEtapaEscrito.scale.setTo(0.6, 0.6);
                this_.btnHome.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
            }
            else if ($(window).height() > 210) {
                this_.columns = 5;
                this_.rows = 1;
                this_.scaleIcon = 0.5;
                this_.cartelEtapaEscrito.scale.setTo(0.6, 0.6);
                this_.btnHome.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
            }
            else {
                this_.columns = 5;
                this_.rows = 1;
                this_.scaleIcon = 0.4;
                this_.cartelEtapaEscrito.scale.setTo(0.4, 0.4);
                this_.btnHome.scale.setTo(0.2, 0.2);
                this_.btnSound.scale.setTo(0.2, 0.2);
            }
        }
        else if ($(window).width() > 576) {
            if ($(window).height() > 600) {
                this_.columns = 3;
                this_.rows = 3;
                this_.scaleIcon = 0.6;
                this_.cartelEtapaEscrito.scale.setTo(0.65, 0.65);
            }
            else if ($(window).height() > 480) {
                this_.columns = 3;
                this_.rows = 2;
                this_.scaleIcon = 0.6;
                this_.cartelEtapaEscrito.scale.setTo(0.6, 0.6);
            }
            else if ($(window).height() > 400) {
                this_.columns = 3;
                this_.rows = 2;
                this_.scaleIcon = 0.6;
                this_.cartelEtapaEscrito.scale.setTo(0.6, 0.6);
            }
            else if ($(window).height() > 360) {
                this_.columns = 4;
                this_.rows = 2;
                this_.scaleIcon = 0.5;
                this_.cartelEtapaEscrito.scale.setTo(0.6, 0.6);
                this_.btnHome.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
                this_.leftArrow.scale.setTo(0.8, 0.8);
                this_.rightArrow.scale.setTo(0.8, 0.8);
                this_.leftArrow.y = ($(window).height() * 60 / 100) - (this_.leftArrow.height / 2);
                this_.rightArrow.y = ($(window).height() * 60 / 100) - (this_.leftArrow.height / 2);
            }
            else if ($(window).height() > 270) {
                this_.columns = 4;
                this_.rows = 1;
                this_.scaleIcon = 0.5;
                this_.cartelEtapaEscrito.scale.setTo(0.5, 0.5);
                this_.btnHome.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
                this_.leftArrow.scale.setTo(0.8, 0.8);
                this_.rightArrow.scale.setTo(0.8, 0.8);
                this_.leftArrow.y = ($(window).height() * 60 / 100) - (this_.leftArrow.height / 2);
                this_.rightArrow.y = ($(window).height() * 60 / 100) - (this_.leftArrow.height / 2);
            }
            else if ($(window).height() > 210) {
                this_.columns = 4;
                this_.rows = 1;
                this_.scaleIcon = 0.45;
                this_.cartelEtapaEscrito.scale.setTo(0.5, 0.5);
                this_.btnHome.scale.setTo(0.2, 0.2);
                this_.btnSound.scale.setTo(0.2, 0.2);
                this_.leftArrow.scale.setTo(0.8, 0.8);
                this_.rightArrow.scale.setTo(0.8, 0.8);
                this_.leftArrow.y = ($(window).height() * 60 / 100) - (this_.leftArrow.height / 2);
                this_.rightArrow.y = ($(window).height() * 60 / 100) - (this_.leftArrow.height / 2);
            }
            else {
                this_.columns = 4;
                this_.rows = 1;
                this_.scaleIcon = 0.4;
                this_.cartelEtapaEscrito.scale.setTo(0.4, 0.4);
                this_.btnHome.scale.setTo(0.2, 0.2);
                this_.btnSound.scale.setTo(0.2, 0.2);
                this_.leftArrow.scale.setTo(0.8, 0.8);
                this_.rightArrow.scale.setTo(0.8, 0.8);
                this_.leftArrow.y = ($(window).height() * 60 / 100) - (this_.leftArrow.height / 2);
                this_.rightArrow.y = ($(window).height() * 60 / 100) - (this_.leftArrow.height / 2);
            }
        }
        else if ($(window).width() > 279) {
            this_.btnHome.scale.setTo(0.25, 0.25);
            this_.btnSound.scale.setTo(0.25, 0.25);
            this_.leftArrow.scale.setTo(0.7, 0.7);
            this_.rightArrow.scale.setTo(0.7, 0.7);
            if ($(window).height() > 600) {
                this_.columns = 2;
                this_.rows = 4;
                this_.scaleIcon = 0.5;
                this_.cartelEtapaEscrito.scale.setTo(0.35, 0.35);
            }
            else if ($(window).height() > 480) {
                this_.columns = 2;
                this_.rows = 3;
                this_.scaleIcon = 0.5;
                this_.cartelEtapaEscrito.scale.setTo(0.35, 0.35);
            }
            else if ($(window).height() > 400) {
                this_.columns = 2;
                this_.rows = 3;
                this_.scaleIcon = 0.5;
                this_.cartelEtapaEscrito.scale.setTo(0.35, 0.35);
                this_.btnHome.scale.setTo(0.2, 0.2);
                this_.btnSound.scale.setTo(0.2, 0.2);
            }
            else if ($(window).height() > 360) {
                this_.columns = 2;
                this_.rows = 3;
                this_.scaleIcon = 0.4;
                this_.cartelEtapaEscrito.scale.setTo(0.35, 0.35);
                this_.btnHome.scale.setTo(0.2, 0.2);
                this_.btnSound.scale.setTo(0.2, 0.2);
            }
            else if ($(window).height() > 270) {
                this_.columns = 2;
                this_.rows = 3;
                this_.scaleIcon = 0.3;
                this_.cartelEtapaEscrito.scale.setTo(0.35, 0.35);
                this_.btnHome.scale.setTo(0.15, 0.15);
                this_.btnSound.scale.setTo(0.15, 0.15);
                this_.leftArrow.scale.setTo(0.6, 0.6);
                this_.rightArrow.scale.setTo(0.6, 0.6);
            }
            else if ($(window).height() > 210) {
                this_.columns = 2;
                this_.rows = 1;
                this_.scaleIcon = 0.45;
                this_.cartelEtapaEscrito.scale.setTo(0.35, 0.35);
                this_.btnHome.scale.setTo(0.2, 0.2);
                this_.btnSound.scale.setTo(0.2, 0.2);
                this_.leftArrow.scale.setTo(0.6, 0.6);
                this_.rightArrow.scale.setTo(0.6, 0.6);
                this_.leftArrow.y = ($(window).height() * 65 / 100) - (this_.leftArrow.height / 2);
                this_.rightArrow.y = ($(window).height() * 65 / 100) - (this_.leftArrow.height / 2);
            }
            else {
                this_.columns = 2;
                this_.rows = 1;
                this_.scaleIcon = 0.3;
                this_.cartelEtapaEscrito.scale.setTo(0.35, 0.35);
                this_.btnHome.scale.setTo(0.15, 0.15);
                this_.btnSound.scale.setTo(0.15, 0.15);
                this_.leftArrow.scale.setTo(0.5, 0.5);
                this_.rightArrow.scale.setTo(0.5, 0.5);
                this_.leftArrow.y = ($(window).height() * 70 / 100) - (this_.leftArrow.height / 2);
                this_.rightArrow.y = ($(window).height() * 70 / 100) - (this_.leftArrow.height / 2);
            }
        }
        else {
            this_.btnHome.scale.setTo(0.2, 0.2);
            this_.btnSound.scale.setTo(0.2, 0.2);
            this_.leftArrow.scale.setTo(0.6, 0.6);
            this_.rightArrow.scale.setTo(0.6, 0.6);
            if ($(window).height() > 600) {
                this_.columns = 1;
                this_.rows = 5;
                this_.scaleIcon = 0.4;
                this_.cartelEtapaEscrito.scale.setTo(0.2, 0.2);
                this_.btnHome.scale.setTo(0.25, 0.25);
                this_.btnSound.scale.setTo(0.25, 0.25);
            }
            else if ($(window).height() > 480) {
                this_.columns = 1;
                this_.rows = 4;
                this_.scaleIcon = 0.4;
                this_.cartelEtapaEscrito.scale.setTo(0.2, 0.2);
            }
            else if ($(window).height() > 400) {
                this_.columns = 1;
                this_.rows = 4;
                this_.scaleIcon = 0.35;
                this_.cartelEtapaEscrito.scale.setTo(0.2, 0.2);
            }
            else if ($(window).height() > 360) {
                this_.columns = 1;
                this_.rows = 3;
                this_.scaleIcon = 0.35;
                this_.cartelEtapaEscrito.scale.setTo(0.2, 0.2);
            }
            else if ($(window).height() > 270) {
                this_.columns = 1;
                this_.rows = 3;
                this_.scaleIcon = 0.3;
                this_.cartelEtapaEscrito.scale.setTo(0.2, 0.2);
                this_.btnHome.scale.setTo(0.15, 0.15);
                this_.btnSound.scale.setTo(0.15, 0.15);
            }
            else if ($(window).height() > 210) {
                this_.columns = 1;
                this_.rows = 2;
                this_.scaleIcon = 0.3;
                this_.cartelEtapaEscrito.scale.setTo(0.2, 0.2);
                this_.btnHome.scale.setTo(0.15, 0.15);
                this_.btnSound.scale.setTo(0.15, 0.15);
            }
            else {
                this_.columns = 1;
                this_.rows = 1;
                this_.scaleIcon = 0.3;
                this_.cartelEtapaEscrito.scale.setTo(0.2, 0.2);
                this_.btnHome.scale.setTo(0.15, 0.15);
                this_.btnSound.scale.setTo(0.15, 0.15);
                this_.leftArrow.y = ($(window).height() * 65 / 100) - (this_.leftArrow.height / 2);
                this_.rightArrow.y = ($(window).height() * 65 / 100) - (this_.leftArrow.height / 2);
            }
        }

        this_.pages = Math.ceil(countChildren / (this_.rows * this_.columns));
        this_.thumbWidth = 140 * this_.scaleIcon;
        this_.thumbHeight = 174 * this_.scaleIcon;
        this_.spacing = this_.thumbWidth * 40 / 100;

        this_.margin = this_.cartelEtapaEscrito.height * 40 / 100;
        var columHeight = this_.thumbHeight * this_.rows + this_.spacing * (this_.rows - 1) + this_.cartelEtapaEscrito.height + 30;

        this_.cartelEtapaEscrito.y = game.world.centerY - (columHeight / 2);
        this_.cartelEtapaEscrito.x = game.world.centerX - (this_.cartelEtapaEscrito.width / 2);

        this_.scrollingMap.width = this_.pages * $(window).width();
        this_.scrollingMap.height = $(window).height();
        this_.scrollingMap.input.boundsRect.x = $(window).width() - this_.pages * $(window).width();
        this_.scrollingMap.input.boundsRect.y = $(window).height() - $(window).height();
        this_.scrollingMap.input.boundsRect.width = (this_.pages * $(window).width() * 2) - $(window).width();
        this_.scrollingMap.input.boundsRect.height = $(window).height() * 2 - $(window).height();

        var rowLength = this_.thumbWidth * this_.columns + this_.spacing * (this_.columns - 1);
        this_.leftMargin = ($(window).width() - rowLength) / 2;
        this_.leftArrow.x = this_.leftMargin / 2;
        this_.rightArrow.x = game.world.width - (this_.leftMargin / 2);
        this_.btnHome.y = game.world.height - this_.btnHome.height;
        this_.btnSound.y = game.world.height - this_.btnSound.height;
        this_.btnSound.x = 10 + this_.btnSound.width;

        var stagenr = 0;
        for (var k = 0; k < this_.pages; k++) {
            for (var y = 0; y < this_.rows; y++) {
                for (var x = 0; x < this_.columns; x++) {
                    var xpos = k * $(window).width() + this_.leftMargin + x * (this_.thumbWidth + this_.spacing);
                    var ypos = (game.world.centerY - (columHeight / 2)) + this_.cartelEtapaEscrito.height + this_.margin + (y * (this_.thumbHeight + this_.spacing));
                    var IconGroup = this_.scrollingMap.children[stagenr];
                    if (IconGroup) {
                        IconGroup.width = this_.thumbWidth;
                        IconGroup.height = this_.thumbHeight;
                        IconGroup.x = xpos;
                        IconGroup.y = ypos;
                        IconGroup.xOrg = xpos;
                        IconGroup.yOrg = ypos;
                        stagenr = stagenr + 1;
                    };
                };
            };
        };

        this_.currentPage = 0;
        this_.changePage(0);
    },

    resizeGame: function (context) {
        var this_ = context;
        var widthColumn = $(window).width() / 12;
        var ratio = 1;
        ratio = ($(window).height() - ($(window).height() * 22 / 100)) / 600;
        var widthPhoto = 960 * ratio;
        var heightPhoto = 600 * ratio;
        var heightBackgroundSlider = $(window).height() * 22 / 100;
        var ySliderBackground = $(window).height() - ($(window).height() * 22 / 100);
        var changeBtnHomeY = false;

        ///Si el ancho de la foto + el 20% es mayor al ancho de la pantalla, -> quiere decir que es vertical
        //Tomo el ratio del ancho
        this_.blockBackgroundPhoto.y = 0;
        if ((widthPhoto + (widthPhoto * 20 / 100)) > $(window).width()) {
            var oldHeightPhoto = heightPhoto;
            ratio = ($(window).width() - ($(window).width() * 22 / 100)) / 960;
            widthPhoto = 960 * ratio;
            heightPhoto = 600 * ratio;
            var ratioBackground = heightBackgroundSlider / oldHeightPhoto;
            heightBackgroundSlider = heightPhoto * ratioBackground;
            this_.blockBackgroundPhoto.y = game.world.centerY - ((heightPhoto + heightBackgroundSlider) / 2);
            ySliderBackground = game.world.centerY + (heightPhoto / 2) - (heightBackgroundSlider / 2);
            changeBtnHomeY = true;
        };

        this_.blockBackgroundPhoto.x = game.world.centerX - (widthPhoto / 2);
        this_.blockBackgroundPhoto.width = widthPhoto;
        this_.blockBackgroundPhoto.height = heightPhoto;

        var widthBlockBackground = widthPhoto * 0.97;
        var heightBlockBackground = heightPhoto * 0.97;
        this_.blockBackground.width = widthBlockBackground;
        this_.blockBackground.height = heightBlockBackground;
        this_.blockBackground.x = game.world.centerX - (widthBlockBackground / 2);
        this_.blockBackground.y = (this_.blockBackgroundPhoto.centerY) - (heightBlockBackground / 2)


        var widthDestinationBlock = widthPhoto / 4;
        var heightDestinationBlock = heightPhoto / 4;
        this_.blockDestination.width = heightDestinationBlock;
        this_.blockDestination.height = heightDestinationBlock;
        this_.blockDestination.x = game.world.centerX + (widthPhoto / 2) - heightDestinationBlock;
        this_.blockDestination.y = this_.blockBackgroundPhoto.y + heightPhoto - heightDestinationBlock;
        this_.circleDestination.width = heightDestinationBlock;
        this_.circleDestination.height = heightDestinationBlock;
        this_.circleDestination.x = game.world.centerX + (widthPhoto / 2) - (heightDestinationBlock / 2);
        this_.circleDestination.y = this_.blockBackgroundPhoto.y + heightPhoto - (heightDestinationBlock / 2);
        this_.circleDestination.anchor.setTo(0.5);

        var scale = 2;
        var scaleBeforeX = this_.circleDestination.scale.x + (this_.circleDestination.scale.x * 50 / 100);
        var scaleBeforeY = this_.circleDestination.scale.y + (this_.circleDestination.scale.y * 50 / 100);;
        game.tweens.removeFrom(this_.circleDestination.scale);
        this_.tweenCircleDestination = game.add.tween(this_.circleDestination.scale)
            .to({ x: scaleBeforeX, y: scaleBeforeY }, 4000, Phaser.Easing.Elastic.Out, true, 2000).loop().yoyo(true, 0);

        if ($(window).width() > 2200) {
            this_.btnHome.scale.setTo(0.7, 0.7);
            this_.btnSound.scale.setTo(0.7, 0.7);
        }
        else if ($(window).width() > 992) {
            this_.btnHome.scale.setTo(0.35, 0.35);
            this_.btnSound.scale.setTo(0.35, 0.35);
        }
        else if ($(window).width() > 768) {
            this_.btnHome.scale.setTo(0.3, 0.3);
            this_.btnSound.scale.setTo(0.3, 0.3);
        }
        else if ($(window).width() > 576) {
            this_.btnHome.scale.setTo(0.2, 0.2);
            this_.btnSound.scale.setTo(0.2, 0.2);
        }
        else {
            this_.btnHome.scale.setTo(0.12, 0.12);
            this_.btnSound.scale.setTo(0.12, 0.12);
        };

        this_.bgRect.width = widthPhoto;
        this_.bgRect.height = heightBackgroundSlider;
        this_.bgRect.x = game.world.centerX - (widthPhoto / 2);
        this_.bgRect.y = ySliderBackground;

        var countItemsSlider = game.state.states.Game.slider.getCountItems();
        var widthItemSlider = heightBackgroundSlider;
        var widthBackgroundSlider = widthPhoto - (widthPhoto * 20 / 100);
        var marginItemsSlider = (widthBackgroundSlider - (countItemsSlider * widthItemSlider)) / (countItemsSlider - 1);
        var xInitialSlider = game.world.centerX - (widthBackgroundSlider / 2);
        game.state.states.Game.slider.resizeAllItems(widthBackgroundSlider, widthItemSlider, marginItemsSlider, xInitialSlider, this_.bgRect.y, widthPhoto);


        //this_.btnBackPagePrincipal.y = game.world.height - this_.btnBackPagePrincipal.height;
        //this_.btnSound.y = game.world.height - this_.btnSound.height;
        //this_.btnSound.x = 10 + this_.btnBackPagePrincipal.width;

        //this_.btnHome.x = $(window).width() - this_.btnHome.width;
        //this_.btnSound.x = this_.btnHome.x;

        this_.btnHome.x = 2;
        this_.btnSound.x = 2;
        if (changeBtnHomeY) {
            //this_.btnHome.y = this_.blockBackgroundPhoto.y;
            this_.btnHome.y = ySliderBackground + (heightBackgroundSlider / 2) - (this_.btnHome.height / 2);
        }
        else {
            //this_.btnHome.y = 0;
            this_.btnHome.y = $(window).height() - this_.btnHome.height;
        };

        this_.btnSound.y = this_.btnHome.y - this_.btnSound.height;

        game.scale.setGameSize($(window).width(), $(window).height());
        this_.background.height = $(window).height();
        this_.background.width = $(window).width();

        if (this_.emitterRain) {
            var lifespan = $(window).height() * 3000 / 640;
            this_.emitterRain.x = game.world.centerX;
            this_.emitterRain.y = 0;
            this_.emitterRain.width = $(window).width();
            this_.emitterRain.lifespan = lifespan;
        };

        if (game["modals"]["modalStage"]) {
            this.resizeModal(game["modals"]["modalStage"]);
        };

        this.animationGame(context);
    },

    resizeModal: function (modal) {
        var ratioModal = 1;
        var widthModal = modal.children[2]._bounds.width;
        var heightModal = modal.children[2]._bounds.height;
        ratioModal = ($(window).width() - ($(window).width() * 5 / 100)) / widthModal;
        var widthFinal = widthModal * ratioModal;
        var heightFinal = heightModal * ratioModal;

        if ((heightFinal + (heightFinal * 5 / 100)) > $(window).height()) {
            ratioModal = ($(window).height() - ($(window).height() * 5 / 100)) / heightModal;
            widthFinal = widthModal * ratioModal;
            heightFinal = heightModal * ratioModal;
        };

        ratioModal = widthFinal / widthModal;
        modal.children[2].scale.setTo(ratioModal, ratioModal);
        modal.children[2].x = game.world.centerX - (modal.children[2].width / 2);
        modal.children[2].y = game.world.centerY - (modal.children[2].height / 2);

        //Fondo
        modal.children[1].width = $(window).width();
        modal.children[1].height = $(window).height();
    },

    btnSoundClicked: function (btnSound) {
        var this_ = game.state.states.Main;
        if (this_.audioBackground.isPlaying) {
            this_.audioBackground.stop();
            btnSound.frame == 2;
            btnSound._onOutFrame = 2
            btnSound._onOverFrame = 3
        }
        else {
            this_.audioBackground.play();
            btnSound.frame == 0;
            btnSound._onOutFrame = 0
            btnSound._onOverFrame = 1
        };
    },

    playWinStage: function () {
        var this_ = game.state.states.Main;
        this_.audioWinStage.loop = false;
        this_.audioWinStage.play();
    },

    playWinActivity: function () {
        var this_ = game.state.states.Main;
        this_.audioWinActivity.loop = false;
        this_.audioWinActivity.play();
    },

    playClik: function () {
        var this_ = game.state.states.Main;
        this_.audioClick.loop = false;
        this_.audioClick.play();
    },

    playErrorShort: function () {
        var this_ = game.state.states.Main;
        this_.audioErrorShort.loop = false;
        this_.audioErrorShort.play();
    },


    loadScriptStates: function () {
        //game.load.script('MainMenu', '/Scripts/Games/EmotionRecognition/States/MainMenu.js');
        //game.load.script('Level', '/Scripts/Games/EmotionRecognition/States/Level.js');
        //game.load.script('Stage', '/Scripts/Games/EmotionRecognition/States/Stage.js');
        //game.load.script('Game', '/Scripts/Games/EmotionRecognition/States/Game.js');

        //var versionProJs = 3;
        //game.load.script('MainMenu', '/Scripts/Games/EmotionRecognition/States/MainMenu' + versionProJs + '.js');
        //game.load.script('Level', '/Scripts/Games/EmotionRecognition/States/Level' + versionProJs + '.js');
        //game.load.script('Stage', '/Scripts/Games/EmotionRecognition/States/Stage' + versionProJs + '.js');
        //game.load.script('Game', '/Scripts/Games/EmotionRecognition/States/Game' + versionProJs + '.js');
    },

    loadFontStates: function () {
        game.load.bitmapFont('font72', '/Scripts/Games/EmotionRecognition/Assets/Images/font72.png', '/Scripts/Games/EmotionRecognition/Assets/Fonts/font72.xml'); // created with http://kvazars.com/littera/
    },

    addAllStates: function (context) {
        game.state.add('MainMenu', MainMenu);
        game.state.add('Level', Level);
        game.state.add('Stage', Stage);
        game.state.add('Game', Game);
    },

    addAudio: function () {
        var this_ = game.state.states.Main;
        this_.audioBackground = game.add.audio('backgroundMusic');
        this_.audioBackground.loop = true;
        this_.audioBackground.play();
        this_.audioBackground.isPlaying = true;
        this_.audioBackground.volume = 0.9;

        this_.audioWinStage = game.add.audio('winStage');
        this_.audioWinActivity = game.add.audio('winActivity');
        this_.audioWinActivity.volume = 0.6;
        this_.audioClick = game.add.audio('click');
        this_.audioErrorShort = game.add.audio('errorShort');
    },

    loadSpritesheets: function () {
        game.load.spritesheet('Personaje1', '/Scripts/Games/EmotionRecognition/Assets/Images/Principal/personaje1.png', 500, 500);
        game.load.spritesheet('Personaje2', '/Scripts/Games/EmotionRecognition/Assets/Images/Principal/personaje2.png', 500, 500);
        game.load.spritesheet('Personaje3', '/Scripts/Games/EmotionRecognition/Assets/Images/Principal/personaje3.png', 500, 500);
        game.load.spritesheet('Personaje4', '/Scripts/Games/EmotionRecognition/Assets/Images/Principal/personaje4.png', 500, 500);
        game.load.spritesheet('Personaje5', '/Scripts/Games/EmotionRecognition/Assets/Images/Principal/personaje5.png', 500, 500);
        game.load.spritesheet('btnJugar', '/Scripts/Games/EmotionRecognition/Assets/Images/Buttons/btn_jugar.png', 325, 94);
        game.load.spritesheet('levelselecticons', '/Scripts/Games/EmotionRecognition/Assets/Images/Nivel/sprite_icon_level.png', 140, 174);
        //arrows image
        game.load.spritesheet("level_arrows", "/Scripts/Games/EmotionRecognition/Assets/Images/Nivel/level_arrows.png", 48, 48);
        game.load.spritesheet("arrowSliderLeft", "/Scripts/Games/EmotionRecognition/Assets/Images/Juego/arrow-slider-left.png", 60, 71);
        game.load.spritesheet("arrowSliderRight", "/Scripts/Games/EmotionRecognition/Assets/Images/Juego/arrow-slider-right.png", 60, 71);
        game.load.spritesheet("posterStage", "/Scripts/Games/EmotionRecognition/Assets/Images/Etapa/poster_stage.png", 835, 578);
        game.load.spritesheet("posterFinalSatage", "/Scripts/Games/EmotionRecognition/Assets/Images/Etapa/poster_final_stage.png", 835, 578);
        game.load.spritesheet('btnHome', '/Scripts/Games/EmotionRecognition/Assets/Images/Buttons/sprite_btn_home.png', 220, 220);
        game.load.spritesheet('btnMenu', '/Scripts/Games/EmotionRecognition/Assets/Images/Buttons/sprite_btn_menu.png', 220, 220);
        game.load.spritesheet('btnRepeat', '/Scripts/Games/EmotionRecognition/Assets/Images/Buttons/sprite_btn_repeat.png', 220, 220);
        game.load.spritesheet('btnNext', '/Scripts/Games/EmotionRecognition/Assets/Images/Buttons/sprite_btn_next.png', 220, 220);
        game.load.spritesheet('btnArrowLeft', '/Scripts/Games/EmotionRecognition/Assets/Images/Buttons/sprite_btn_arrow_left.png', 220, 220);
        game.load.spritesheet('btnSound', '/Scripts/Games/EmotionRecognition/Assets/Images/Buttons/sprite_btn_sound.png', 220, 220);
    },

    loadImagesGame: function () {
        game.load.image("BackgroundPhoto", "/Scripts/Games/EmotionRecognition/Assets/Images/Juego/bg-foto.png");
        game.load.image("BackgroundSlider", "/Scripts/Games/EmotionRecognition/Assets/Images/Juego/bg-slider.png");
        game.load.image("BlockDestination", "/Scripts/Games/EmotionRecognition/Assets/Images/Juego/destino-fondo.png");
        game.load.image("CircleDestination", "/Scripts/Games/EmotionRecognition/Assets/Images/Juego/colocar-circulo.png");
        game.load.image("CartelActividadesEscrito", "/Scripts/Games/EmotionRecognition/Assets/Images/Nivel/cartelActividadesEscrito.png");
        game.load.image("CartelEtapaEscrito", "/Scripts/Games/EmotionRecognition/Assets/Images/Nivel/cartelEtapaEscrito.png");
        // transparent background used to scroll
        game.load.image("titleLevelFinal", "/Scripts/Games/EmotionRecognition/Assets/Images/Etapa/title_level_final.png");
        game.load.image("titleStageFinal", "/Scripts/Games/EmotionRecognition/Assets/Images/Etapa/title_stage_final.png");
        game.load.image("messajeCongratulation", "/Scripts/Games/EmotionRecognition/Assets/Images/Etapa/mess_congratulation.png");
        game.load.image("messajecongratulationFinalLevel", "/Scripts/Games/EmotionRecognition/Assets/Images/Etapa/mess_congratulation_final_level.png");
        game.load.image("starEmpty", "/Scripts/Games/EmotionRecognition/Assets/Images/Etapa/star_empty.png");
        game.load.image("starFull", "/Scripts/Games/EmotionRecognition/Assets/Images/Etapa/star_full.png");
        game.load.image("transp", "/Scripts/Games/EmotionRecognition/Assets/Images/Nivel/transp.png");
        game.load.image("starParticle", "/Scripts/Games/EmotionRecognition/Assets/Images/Etapa/star_particle.png");
    },

    loadAudio: function () {
        game.load.audio('backgroundMusic', '/Scripts/Games/EmotionRecognition/Assets/Audio/Background.mp3');
        game.load.audio('winStage', '/Scripts/Games/EmotionRecognition/Assets/Audio/WinStage.mp3');
        game.load.audio('winActivity', '/Scripts/Games/EmotionRecognition/Assets/Audio/WinActivity.mp3');
        game.load.audio('click', '/Scripts/Games/EmotionRecognition/Assets/Audio/Click.mp3');
        game.load.audio('errorShort', '/Scripts/Games/EmotionRecognition/Assets/Audio/ErrorShort.mp3');
    },

    loadImagesAnswersGame: function (context) {
        //var urlBackend = "http://localhost:44305";
        var urlBackend = "http://rfessia-001-site1.htempurl.com/";
        var this_ = game.state.states.Game;
        if (this_.stages) {
            for (var i = 0; i < this_.stages.length; i++) {
                for (var y = 0; y < this_.stages[i].Answers.length; y++) {
                    if (this_.stages[i].Answers[y].Background) {
                        game.load.image("background_" + this_.stages[i].Answers[y].AnswerId, urlBackend + "/Stages/" + this_.stages[i].StageId + "/Images/" + this_.stages[i].Answers[y].ImageName);
                    }
                    else {
                        game.load.image("block_" + this_.stages[i].Answers[y].AnswerId, urlBackend + "/Stages/" + this_.stages[i].StageId + "/Images/" + this_.stages[i].Answers[y].ImageName);
                    }

                    //if (this_.stages[i].Answers[y].Background) {
                    //    game.load.image("background_" + this_.stages[i].Answers[y].AnswerId, "../assets/" + this_.stages[i].StageId + "/Images/" + this_.stages[i].Answers[y].ImageName);
                    //}
                    //else {
                    //    game.load.image("block_" + this_.stages[i].Answers[y].AnswerId, "../assets/" + this_.stages[i].StageId + "/Images/" + this_.stages[i].Answers[y].ImageName);
                    //}
                };
            };
        };
    },

    padLeft: function (myData) {
        debugger;
        var data = myData.toString();
        var len = data.length;
        return len == 1 ? '0' + data : data;
    },

    getMyDateTimeString: function () {
        var d = new Date,
            dformat = [(mixins.padLeft(d.getMonth() + 1)),
                mixins.padLeft(d.getDate()),
            d.getFullYear()].join('/') + ' ' +
                [mixins.padLeft(d.getHours()),
                mixins.padLeft(d.getMinutes()),
                mixins.padLeft(d.getSeconds())].join(':');
        return dformat;
    }
};