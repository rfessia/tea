/// <reference path="../../../Plugins/phase-slide.ts" />
/// <reference path="../../../Plugins/modal.ts" />
var Game = function (game) {
    this.game = game;
    this.background = null;
    this.backState = null;
    this.slider = null;
    this.currentStage = null;
    this.reg = {};
    this.spritesToDelete = [];
    this.timeCheck = null;
    this.numberOfAttempts = null;
    this.stages = null;
};
Game.prototype = {
    init: function (dataStage) {
        this.numberOfAttempts = 0;
    },
    preload: function () {
        mixins.addBackgroundPrincipal(this);
        this.slider = new phaseSlider(game);
    },
    create: function () {
        var _this = this;
        ///Si currentStage es null, es la primera vez que entra por lo tanto recupero el primero.
        ///Si no es porque se vuelve a repetir el ciclo y tengo que recuperar la siguiente etapa
        for (var i = 0, len = this.stages.length; i < len; i++) {
            if (this.currentStage == null) {
                this.currentStage = this.stages[0];
                break;
            }
            else if (this.stages[i].StageId === this.currentStage.StageId) {
                this.currentStage = this.stages[i + 1];
                break;
            }
        }
        ;
        ///Modal
        this.reg.modal = new gameModal(game);
        game.physics.startSystem(Phaser.Physics.ARCADE);
        var group0 = game.add.group();
        this.groupFinally = game.add.group();
        var arrayGroupTemp = [];
        var widthPosition = 0;
        var ratio = 1;
        //if (widthPhoto > $(window).width()) {
        ratio = ($(window).height() - ($(window).height() * 22 / 100)) / 600;
        //};
        var widthPhoto = (960 * ratio);
        var heightPhoto = 600 * ratio;
        //Tamaño de imagenes
        //dependiendo si del alto y del ancho, se toma el mas pequeño y se adapta
        var maxSizePhoto = $(window).width() - ($(window).width() * 22 / 100);
        if ($(window).height() < $(window).width()) {
            maxSizePhoto = $(window).height() - ($(window).height() * 22 / 100);
        }
        ;
        var widthDestinationBlock = widthPhoto / 4;
        var heightDestinationBlock = heightPhoto / 4;
        //Bloque destino
        this.blockDestination = game.add.sprite(50, 50, "BlockDestination");
        this.blockDestination.width = heightDestinationBlock;
        this.blockDestination.height = heightDestinationBlock;
        this.blockDestination.x = game.world.centerX + (widthPhoto / 2) - heightDestinationBlock;
        this.blockDestination.y = heightPhoto - heightDestinationBlock;
        this.blockDestination.inputEnabled = true;
        game.physics.arcade.enable(this.blockDestination);
        this.spritesToDelete.push(this.blockDestination);
        this.circleDestination = game.add.sprite(400, 300, 'CircleDestination');
        this.circleDestination.width = heightDestinationBlock;
        this.circleDestination.height = heightDestinationBlock;
        this.circleDestination.x = game.world.centerX + (widthPhoto / 2) - (heightDestinationBlock / 2);
        this.circleDestination.y = heightPhoto - (heightDestinationBlock / 2);
        this.circleDestination.anchor.setTo(0.5);
        this.spritesToDelete.push(this.circleDestination);
        var scale = 2;
        var scaleBeforeX = this.circleDestination.scale.x - scale;
        var scaleBeforeY = this.circleDestination.scale.x - scale;
        this.tweenCircleDestination = game.add.tween(this.circleDestination.scale)
            .to({ x: scaleBeforeX, y: scaleBeforeY }, 4000, Phaser.Easing.Elastic.Out, true, 2000).loop().yoyo(true, 0);
        //Ancho del fondo transaparente del slider. Se agrega el 20% del fondo rojo de la foto principal
        var widthBackgroundSlider = widthPhoto - (widthPhoto * 20 / 100);
        //Alto del fondo transparente del slider, se toma el 22% de la parte inferior de la pantalla
        var heightBackgroundSlider = $(window).height() * 22 / 100;
        //Alto de cada figura del slider, se toma 20% de la parte inferior de la pantalla
        var heightSlider = (20 * $(window).height() / 100);
        //Ancho de cada slider, tiene que ser un cuadrado, por eso se toma igual al alto
        var widthSlider = heightSlider;
        //Cantidad de items a mostrar en el slider
        var countItemsSlider = Math.floor(widthBackgroundSlider / widthSlider);
        //Margen entre cada items del sliders
        var marginItemsSlider = (widthBackgroundSlider - (countItemsSlider * widthSlider)) / (countItemsSlider - 1);
        //X para posicionar el slider: se centra donde comienza la el fondo rojo de la imagen principal
        var xInitialSlider = game.world.centerX - (widthBackgroundSlider / 2);
        //Y para posicionar el slider: se coloca debajo, a 21% del alto de la pantalla porque se deja el 22% en total, mas 1% en margen top y 1% en margen bottom
        var yInitialSlider = $(window).height() - ($(window).height() * 21 / 100);
        var heightBlocks = heightSlider;
        var countGroup = 0;
        for (var i = 0; i < this.currentStage.Answers.length; i++) {
            if (this.currentStage.Answers[i].Background) {
                //Fondo rojo de la imagen background
                this.blockBackgroundPhoto = game.add.sprite(game.world.centerX - (widthPhoto / 2), 0, "BackgroundPhoto");
                this.blockBackgroundPhoto.width = widthPhoto;
                this.blockBackgroundPhoto.height = heightPhoto;
                group0.add(this.blockBackgroundPhoto);
                //Imagen background
                var widthBlockBackground = widthPhoto * 0.97;
                var heightBlockBackground = heightPhoto * 0.97;
                this.blockBackground = game.add.sprite(game.world.centerX - (widthBlockBackground / 2), (this.blockBackgroundPhoto.centerY) - (heightBlockBackground / 2), "background_" + this.currentStage.Answers[i].AnswerId);
                this.blockBackground.width = widthBlockBackground;
                this.blockBackground.height = heightBlockBackground;
                group0.add(this.blockBackground);
                this.spritesToDelete.push(this.blockBackgroundPhoto);
                this.spritesToDelete.push(this.blockBackground);
            }
            else {
                this.block = game.add.sprite(widthSlider * widthPosition, 0, "block_" + this.currentStage.Answers[i].AnswerId);
                game.physics.arcade.enable(this.block);
                this.block.ImageName = this.currentStage.Answers[i].ImageName;
                this.block.Correct = this.currentStage.Answers[i].Correct;
                this.block.width = widthSlider;
                this.block.height = heightSlider;
                this.block.inputEnabled = true;
                this.block.input.enableDrag();
                this.block.input.useHandCursor = true;
                this.block.originalPosition = this.block.position.clone();
                this.block.events.onDragStart.add(function (currentSprite) {
                    game.tweens.removeFrom(currentSprite.scale);
                    game.tweens.removeFrom(currentSprite.position);
                    currentSprite.parent.bringToTop(currentSprite);
                }, this);
                this.block.events.onDragStop.add(function (currentSprite) {
                    this.numberOfAttempts++;
                    if (!game.physics.arcade.overlap(currentSprite, _this.blockDestination, function () {
                        if (currentSprite.Correct) {
                            _this.createModalStage(_this);
                            currentSprite.input.draggable = false;
                            group0.add(currentSprite);
                            currentSprite.parent.parent.remove(currentSprite);
                            currentSprite.position.copyFrom(_this.blockDestination.position);
                            _this.saveCorrectStage(_this);
                            ///Realizar llamada ajax para guardar la etapa correcta
                            _this.reg.modal.showModal("modalStage");
                            //_this.modalStage = true;
                            _this.emitterRain = game.add.emitter(game.world.centerX, 0, 500);
                            _this.emitterRain.width = game.world.width;
                            //_this.emitterRain.angle = 30; // uncomment to set an angle for the rain.
                            _this.emitterRain.makeParticles('starParticle');
                            _this.emitterRain.minParticleScale = 0.1;
                            _this.emitterRain.maxParticleScale = 0.5;
                            _this.emitterRain.setYSpeed(100, 200);
                            _this.emitterRain.setXSpeed(-100, 100);
                            _this.emitterRain.minRotation = 100;
                            _this.emitterRain.maxRotation = 400;
                            var lifespan = $(window).height() * 3000 / 640;
                            _this.emitterRain.start(false, lifespan, 5, 0);
                        }
                        else {
                            mixins.playErrorShort();
                            currentSprite.position.copyFrom(currentSprite.originalPosition);
                            mixins.animationGame(_this);
                        }
                    })) {
                        mixins.playErrorShort();
                        currentSprite.position.copyFrom(currentSprite.originalPosition);
                        mixins.animationGame(_this);
                    }
                }, this);
                arrayGroupTemp.push(this.block);
                this.spritesToDelete.push(this.block);
                if (arrayGroupTemp.length >= countItemsSlider || i >= this.currentStage.Answers.length - 1) {
                    var gruopTemp = this.createGroup("group_" + countGroup.toString(), arrayGroupTemp);
                    this.groupFinally.add(gruopTemp);
                    arrayGroupTemp = [];
                    widthPosition = 0;
                    countGroup++;
                }
                else {
                    widthPosition++;
                }
                ;
            }
        }
        ;
        if (arrayGroupTemp.length > 0) {
            var gruopTemp = this.createGroup("group_" + countGroup.toString(), arrayGroupTemp);
            this.groupFinally.add(gruopTemp);
            arrayGroupTemp = [];
            countGroup++;
        }
        group0.add(this.blockDestination);
        this.spritesToDelete.push(this.blockDestination);
        var colorSliderBG = 0xFFFFFF;
        var sliderBGAlpha = 0.4;
        this.bgRect = game.add.graphics(widthSlider, heightSlider);
        this.bgRect.beginFill(colorSliderBG, sliderBGAlpha);
        //X está corrido 10% de cada lado, porque en el GAME se agrega 20% de cada lado
        var xSliderBackground = game.world.centerX - (widthBackgroundSlider / 2);
        //Y está corrido 1% de alto y bajo, porque en el GAME se agrega 2% de cada uno 
        var ySliderBackground = $(window).height() - ($(window).height() * 22 / 100);
        this.bgRect.x = game.world.centerX - (widthPhoto / 2);
        this.bgRect.y = ySliderBackground;
        this.bgRect.drawRect(0, 0, widthPhoto, heightBackgroundSlider);
        this.spritesToDelete.push(this.bgRect);
        this.btnHome = game.add.button(0, 0, 'btnHome', this.homeClick, this, 1, 0);
        this.btnHome.frame = 0;
        this.slider.createSlider({
            countItems: countItemsSlider,
            marginItemsSlider: marginItemsSlider,
            customSliderBG: false,
            mode: "horizontal",
            sliderBGAlpha: sliderBGAlpha,
            width: widthSlider,
            height: heightSlider,
            x: xInitialSlider,
            y: yInitialSlider,
            widthSlider: widthBackgroundSlider,
            heightSlider: heightBackgroundSlider,
            objects: [this.groupFinally],
            onNextCallback: function () {
            },
            onPrevCallback: function () {
            }
        });
        var frame = 0;
        var overFrame = 1;
        if (game.state.states.Main.audioBackground.isPlaying == false) {
            frame = 2;
            overFrame = 3;
        }
        ;
        this.btnSound = game.add.button(0, 0, 'btnSound', mixins.btnSoundClicked, this, overFrame, frame);
        mixins.resizeGame(this);
    },
    update: function () {
        var _this = this;
        _this.circleDestination.angle += 1;
    },
    delay: function () {
        var _this = this;
        _this.timeCheck = game.time.now;
    },
    createGroup: function (groupName, arrayGroupTemp) {
        this[groupName] = game.add.group();
        for (var y = 0; y < arrayGroupTemp.length; y++) {
            this[groupName].add(arrayGroupTemp[y]);
        }
        return this[groupName];
    },
    stopDrag: function (currentSprite, endSprite) {
        if (!game.physics.arcade.overlap(currentSprite, endSprite, function () {
            this.group0.add(currentSprite);
            this.group1.remove(currentSprite);
            currentSprite.position.copyFrom(endSprite.position);
        })) {
            currentSprite.position.copyFrom(currentSprite.originalPosition);
        }
    },
    createModalStage: function (_this) {
        var contentPosterStage = null;
        var contentTitlePoster = null;
        var contentMessajeCongratulation = null;
        if (_this.currentStage.Last) {
            mixins.playWinActivity();
            contentTitlePoster = "titleLevelFinal";
            contentMessajeCongratulation = "messajecongratulationFinalLevel";
            contentPosterStage = "posterFinalSatage";
        }
        else {
            mixins.playWinStage();
            contentTitlePoster = "titleStageFinal";
            contentMessajeCongratulation = "messajeCongratulation";
            contentPosterStage = "posterStage";
        }
        ;
        var scaleValue = 0.5;
        var spritePosterStage = game.add.sprite(0, 0, contentPosterStage);
        var columnPosterStage = spritePosterStage.width / 12;
        var titlePoster = game.add.image(0, 0, contentTitlePoster);
        titlePoster.x = (spritePosterStage.width / 2) - (titlePoster.width / 2);
        titlePoster.y = (spritePosterStage.height * 5 / 100);
        var messajeCongratulation = game.add.image(0, 0, contentMessajeCongratulation);
        messajeCongratulation.x = (spritePosterStage.width / 2) - (messajeCongratulation.width / 2);
        messajeCongratulation.y = (spritePosterStage.height * 25 / 100);
        if (_this.currentStage.Last == false || _this.currentStage.Last == null || _this.currentStage.Last == undefined) {
            var contentStarLeft = null;
            var contentCenter = null;
            var contentStarRight = null;
            if (_this.numberOfAttempts == 1) {
                contentStarLeft = "starFull";
                contentCenter = "starFull";
                contentStarRight = "starFull";
            }
            else if (_this.numberOfAttempts <= 3) {
                contentStarLeft = "starFull";
                contentCenter = "starFull";
                contentStarRight = "starEmpty";
            }
            else if (_this.numberOfAttempts <= 5) {
                contentStarLeft = "starFull";
                contentCenter = "starEmpty";
                contentStarRight = "starEmpty";
            }
            else {
                contentStarLeft = "starEmpty";
                contentCenter = "starEmpty";
                contentStarRight = "starEmpty";
            }
            ;
            var starLeft = game.add.image(0, 0, contentStarLeft);
            starLeft.x = (columnPosterStage * 3) - (starLeft.width / 2);
            starLeft.y = (spritePosterStage.height * 43 / 100);
            var starCenter = game.add.image(0, 0, contentCenter);
            starCenter.x = (columnPosterStage * 6) - (starCenter.width / 2);
            starCenter.y = (spritePosterStage.height * 38 / 100);
            var starRight = game.add.image(0, 0, contentStarRight);
            starRight.x = (columnPosterStage * 9) - (starRight.width / 2);
            starRight.y = (spritePosterStage.height * 43 / 100);
        }
        ;
        var spriteHome = game.add.button(0, 0, "btnHome", _this.homeClick, this, 1, 0);
        spriteHome.scale.setTo(scaleValue, scaleValue);
        spriteHome.x = (columnPosterStage * 4.5) - (spriteHome.width / 2);
        spriteHome.y = spritePosterStage.height - (spritePosterStage.height * 25 / 100);
        spriteHome.inputEnabled = true;
        spriteHome.input.useHandCursor = true;
        var spriteRepeat = game.add.button(0, 0, "btnRepeat", _this.repeatClick, this, 1, 0);
        spriteRepeat.scale.setTo(scaleValue, scaleValue);
        spriteRepeat.x = (columnPosterStage * 6) - (spriteRepeat.width / 2);
        spriteRepeat.y = spritePosterStage.height - (spritePosterStage.height * 25 / 100);
        spriteRepeat.inputEnabled = true;
        spriteRepeat.input.useHandCursor = true;
        var spriteNext = game.add.button(0, 0, "btnNext", _this.nextClick, this, 1, 0);
        spriteNext.scale.setTo(scaleValue, scaleValue);
        spriteNext.x = (columnPosterStage * 7.5) - (spriteNext.width / 2);
        spriteNext.y = spritePosterStage.height - (spritePosterStage.height * 25 / 100);
        spriteNext.inputEnabled = true;
        spriteNext.input.useHandCursor = true;
        var groupModalFinalStage = game.add.group();
        groupModalFinalStage.add(spritePosterStage);
        groupModalFinalStage.add(titlePoster);
        groupModalFinalStage.add(messajeCongratulation);
        if (_this.currentStage.Last == false || _this.currentStage.Last == null || _this.currentStage.Last == undefined) {
            groupModalFinalStage.add(starLeft);
            groupModalFinalStage.add(starCenter);
            groupModalFinalStage.add(starRight);
        }
        ;
        groupModalFinalStage.add(spriteHome);
        groupModalFinalStage.add(spriteRepeat);
        groupModalFinalStage.add(spriteNext);
        ///////// modal 3 //////////
        this.reg.modal.createModal({
            type: "modalStage",
            includeBackground: true,
            modalCloseOnInput: false,
            itemsArr: [
                {
                    type: "group",
                    content: groupModalFinalStage,
                    atlasParent: "posterStage",
                    offsetY: -110,
                }
            ]
        });
    },
    showModalStageFinal: function () {
        this.reg.modal.showModal("modalStageFinal");
    },
    destroySprites: function (_this) {
        for (var i = 0; i < _this.spritesToDelete.length; i++) {
            _this.spritesToDelete[i].destroy();
        }
        ;
        _this.spritesToDelete = [];
        _this.btnHome.destroy();
        _this.btnSound.destroy();
    },
    destroyEmitter: function (_this) {
        if (_this.emitterRain && _this.emitterRain.on && _this.emitterRain.exists) {
            _this.emitterRain.destroy();
        }
        ;
    },
    destroySliderControlsGroup: function (phaserSlider) {
        var length = phaserSlider.sliderControlsGroup.children.length;
        for (var i = 0; i < length; i++) {
            phaserSlider.sliderControlsGroup.children[0].destroy();
        }
        ;
    },
    nextClick: function () {
        mixins.playClik();
        //Llamar a la función asíncrona para guardar a db el finishDate
        ///Preguntar si es la ultima
        ///-> Si es la ultima, mostrar una pantalla de nivel finalizado y volver a la pantalla de seleccionar otro nivel.
        ///-> Si no es la ultima
        ///--> Buscar el siguiente a currentStage y reemplazar currentStage con los datos recuperados
        ///---> Si hay otro stage, ir a la pantalla de la siguiente etapa (repetir el ciclo)
        ///---> Si no hay otro stage, mostrar la página de preloado, volver a realizar llamada ajax para recuperar las demas etapas y eliminar las imagenes de las etapas cargadas anteriormente, vaciar currentStage
        ///Para recuperar cada sprite del mundo, tengo que acceder a game.world.children -> iterar cada uno
        ///-> por cada children, iterar children nuevamente y fijarse si tienen la propiedad "key" -> esos se tienen que eliminar
        var _this = game.state.states.Game;
        _this.reg.modal.destroyModal('modalStage');
        //_this.modalStage = false;
        _this.destroySprites(_this);
        _this.destroyEmitter(_this);
        _this.destroySliderControlsGroup(game['phaseSlider']);
        var last = _this.stages.length - 1;
        if (_this.currentStage.Last) {
            //alert("ULTIMA ETAPA");
            _this.game.world.children.forEach(function (item) {
                if (item.name == "group") {
                    item.destroy();
                }
                ;
            });
            _this.currentStage = null;
            _this.getAllLevels(_this);
            game.state.start('Preload', false, false, false, null, null, null, true);
        }
        else if (_this.stages[last].StageId == _this.currentStage.StageId) {
            _this.getStageById(_this.currentStage.StageId, _this.currentStage.LevelId, _this);
        }
        else {
            _this.create(game);
        }
        ;
    },
    repeatClick: function () {
        mixins.playClik();
        var _this = game.state.states.Game;
        if (_this.stages[0].StageId === _this.currentStage.StageId) {
            _this.currentStage = null;
        }
        else {
            for (var i = 0, len = _this.stages.length; i < len; i++) {
                if (_this.stages[i].StageId === _this.currentStage.StageId) {
                    _this.currentStage = _this.stages[i - 1];
                    break;
                }
            }
            ;
        }
        _this.destroySprites(_this);
        _this.destroySliderControlsGroup(game['phaseSlider']);
        _this.destroyEmitter(_this);
        //_this.modalStage = false;
        _this.reg.modal.destroyModal('modalStage');
        _this.create(game);
    },
    homeClick: function () {
        mixins.playClik();
        var _this = game.state.states.Game;
        _this.destroySprites(_this);
        _this.destroyEmitter(_this);
        //_this.modalStage = false;
        _this.reg.modal.destroyModal('modalStage');
        _this.destroySliderControlsGroup(game['phaseSlider']);
        _this.currentStage = null;
        for (var i = 0; i < _this.stages.length; i++) {
            for (var y = 0; y < _this.stages[i].Answers.length; y++) {
                if (_this.stages[i].Answers[y].Background) {
                    _this.cache.removeImage("background_" + _this.stages[i].Answers[y].AnswerId);
                }
                else {
                    _this.cache.removeImage("block_" + _this.stages[i].Answers[y].AnswerId);
                }
                ;
            }
            ;
        }
        ;
        game.state.start("MainMenu", true, false);
    },
    getStageById: function (stageId, levelId, _this) {
        var headers = mixins.getHeaders();
        $.ajax({
            type: 'GET',
            url: urlApi + '/Api/Stage/GetStagesWithAnswers',
            headers: headers,
            data: { stageId: stageId, levelId: levelId },
        }).done(function (data) {
            isReady = true;
            game.state.start("Game", true, false, data);
        }).fail(function (data) {
            if (data.status === 401) {
                var refresh = mixins.refreshToken();
                if (refresh) {
                    _this.getStageById(_this);
                }
                ;
            }
            ;
        });
    },
    saveCorrectStage: function (_this) {
        var currentStage = _this.currentStage;
        var headers = mixins.getHeaders();
        var myDate = mixins.getMyDateTimeString();
        $.ajax({
            type: 'GET',
            url: urlApi + '/Api/Stage/SaveCorrectStage',
            headers: headers,
            data: { stageId: currentStage.StageId, levelId: currentStage.LevelId, numberOfAttempts: _this.numberOfAttempts, myDate: myDate },
        }).done(function (data) {
            _this.numberOfAttempts = 0;
            if (!data.IsValid) {
                alert("Fallo al guardar la etapa " + currentStage.StageId);
            }
            ;
        }).fail(function (data) {
            if (data.status === 401) {
                var refresh = mixins.refreshToken();
                if (refresh) {
                    _this.saveCorrectStage(_this);
                }
                ;
            }
            else {
                alert("Fallo al guardar la etapa " + currentStage.StageId);
                _this.numberOfAttempts = 0;
            }
            ;
        });
    },
    getAllLevels: function (_this) {
        var headers = mixins.getHeaders();
        $.ajax({
            type: 'GET',
            url: urlApi + '/Api/Level/GetAll',
            headers: headers,
        }).done(function (data) {
            isReady = true;
            game.state.start("Level", true, false, data);
        }).fail(function (data) {
            if (data.status === 401) {
                var refresh = mixins.refreshToken();
                if (refresh) {
                    _this.getAllLevels(_this);
                }
                ;
            }
            ;
        });
    },
};
Phaser.Utils.mixinPrototype(Game.prototype, mixins);
window.addEventListener('resize', function () {
    if (game.state.current == 'Game') {
        mixins.resizeGame(game.state.states.Game);
    }
    ;
});
//# sourceMappingURL=Game.js.map