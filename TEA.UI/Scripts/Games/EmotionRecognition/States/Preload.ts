﻿var Preload = function (game) {
    this.background = null;
    this.backState = null;
};

Preload.prototype = {
    ///firstTime: Bool -> Indica si es la primera vez que se inicia el Preload
    ///dataLoad: ArrayString -> Nombre de la/s funcion/es para Cargar al Game
    ///dataAdd: ArrayString ->  Nombre de la/s funcion/es para Agregar datos al Game
    ///nextState: String -> Siguiente estado para ir
    ///wait: Bool -> Indica si debe esperar o ya está listo
    init: function (firsTime, dataLoad, dataAdd, nextState, wait) {
        this.firsTime = firsTime;
        this.dataLoad = dataLoad;
        this.dataAdd = dataAdd;
        this.nextState = nextState;
        this.wait = wait;
    },

    preload: function () {
        this.load.onLoadStart.add(this.loadStart, this);
        this.load.onFileComplete.add(this.fileComplete, this);
        this.load.onLoadComplete.add(this.loadComplete, this);

        if (this.firsTime) {
            game.add.sprite(0, 0, 'BackgroundAlone');
        }
        else {
            mixins.addBackgroundPrincipal(this);
        };
        
        this.loading = this.add.sprite(0, 0, 'Loading');
        var ratio = 1;
        ratio = ($(window).width() / (this.loading.width + $(window).width() * 10 / 100));
        if (ratio > 2) {
            ratio = 1.5;
        };

        this.loading.width = this.loading.width * ratio;
        this.loading.height = this.loading.height * ratio;
        this.loading.x = game.world.centerX - this.loading.width / 2;
        this.loading.y = game.world.centerY - this.loading.height / 2;

        this.logo = game.make.sprite(0, 0, 'Logo');
        this.logo.width = this.logo.width * ratio;
        this.logo.height = this.logo.height * ratio;
        this.logo.x = this.loading.x;
        this.logo.y = $(window).height() * 5 / 100;
        this.text = game.add.text(0, 0, 'Cargando...', { fill: '#000000' });
        this.text.x = game.world.centerX - this.text.width / 2;
        this.text.y = game.world.centerY + this.text.height;
        game.load.setPreloadSprite(this.loading);
        game.add.existing(this.logo).scale.setTo(0.5);
        if (this.dataLoad) {
            this.dataLoad.forEach(function (element) {
                window["mixins"][element](game);
            });
        };
    },

    create: function () {
        if (this.dataAdd) {
            this.dataAdd.forEach(function (element) {
                window["mixins"][element](game);
            });
        };

        if (!this.wait) {
            isReady = true;
        };
    },

    loadStart: function () {
        this.text.setText("Cargando...");
        this.text.x = game.world.centerX - this.text.width / 2;
        this.text.y = game.world.centerY + this.text.height;
    },

    fileComplete: function (progress, cacheKey, success, totalLoaded, totalFiles) {
        this.text.setText("Cargando: " + progress + "%");
        this.text.x = game.world.centerX - this.text.width / 2;
        this.text.y = game.world.centerY + this.text.height;
    },

    loadComplete: function () {
        this.text.setText("Carga completa");
        this.text.x = game.world.centerX - this.text.width / 2;
        this.text.y = game.world.centerY + this.text.height;
    },

    update: function () {
        if (isReady === true) {
            if (this.nextState) {
                this.state.start(this.nextState);
            };
        };
    },
};

Phaser.Utils.mixinPrototype(Preload.prototype, mixins);