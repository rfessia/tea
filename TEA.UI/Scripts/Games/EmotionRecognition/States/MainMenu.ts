﻿/// <reference path="../../../typings/globals/jquery/index.d.ts" />
var MainMenu = function () { };

MainMenu.prototype = {

    menuConfig: {
        startY: 260,
        startX: 30
    },

    init: function () {
        this.optionCount = 1;
        mixins.addBackgroundPrincipal(this);
    },

    create: function () {
        var scaleValue = 0.8;
        this.btnJugar = game.add.button(0, 0, 'btnJugar', this.clickJugar, this, 1, 0);
        this.btnJugar.scale.setTo(scaleValue, scaleValue);
        this.btnJugar.x = game.world.centerX - (this.btnJugar.width / 2);
        this.btnJugar.y = game.world.centerY - (this.btnJugar.height / 2);
        this.btnBackPagePrincipal = game.add.button(0, 0, 'btnArrowLeft', this.BackPagePrincipalClicked, this, 1, 0);
        this.btnBackPagePrincipal.frame = 0;
        var frame = 0;
        var overFrame = 1;
        if (game.state.states.Main.audioBackground.isPlaying == false) {
            frame = 2;
            overFrame = 3;
        };

        this.btnSound = game.add.button(0, 0, 'btnSound', mixins.btnSoundClicked, this, overFrame, frame);
        mixins.resizePrincipal(this);
        mixins.resizeMainMenu(this);
    },

    clickJugar: function () {
        mixins.playClik();
        this.getAllLevels();
        game.state.start('Preload', false, false, false, null, null, null, true);
    },

    BackPagePrincipalClicked: function () {
        window.location.href = window.location.origin;
    },

    getAllLevels: function () {
        var headers = mixins.getHeaders();
        $.ajax({
            type: 'GET',
            url: urlApi + '/Api/Level/GetAll',
            headers: headers,
        }).done(function (data) {
            isReady = true;
            game.state.start("Level", true, false, data);
        }).fail(function (data) {
            if (data.status === 401) {
                var refresh = mixins.refreshToken();
                if (refresh) {
                    game.state.states.MainMenu.getAllLevels();
                };
            };
        });
    },
};

Phaser.Utils.mixinPrototype(MainMenu.prototype, mixins);

window.addEventListener('resize', function () {
    if (game.state.current == 'MainMenu') {
        mixins.resizeMainMenu(game.state.states.MainMenu);
    };
});