﻿var PLAYER_DATA = null;
var Stage = function (game) {
    this.game = game;
};

Stage.prototype = {

    init: function (dataStage) {
        this.dataStage = dataStage;
    },

    preload: function () {
        mixins.addBackgroundPrincipal(game.state.states.MainMenu);
        mixins.resizePrincipal(game.state.states.MainMenu);

        if (this.dataStage.length > 0) {
            PLAYER_DATA = this.dataStage;
        };

        this.columns = 3;
        this.rows = 4;
        //this.pages = Math.ceil(35 / (this.rows * this.columns));
        this.pages = Math.ceil(PLAYER_DATA.length / (this.rows * this.columns));
        this.thumbWidth = 60;
        this.thumbHeight = 60;
        this.spacing = 60;
        this.leftArrow;
        this.rightArrow;
        this.currentPage = 0;
    },

    create: function () {
        var widthCartelEtapaEscrito = $(window).width() * 50 / 100;
        this.cartelEtapaEscrito = this.game.add.sprite(game.world.centerX - (widthCartelEtapaEscrito / 2), 20, 'CartelEtapaEscrito');
        this.cartelEtapaEscrito.width = widthCartelEtapaEscrito;
        this.canChangePage = true;
        this.scrollingMap = game.add.tileSprite(0, 0, this.pages * $(window).width(), $(window).height(), "transp");
        this.scrollingMap.inputEnabled = true;
        this.scrollingMap.input.enableDrag(false);
        this.scrollingMap.input.allowVerticalDrag = false;
        this.scrollingMap.input.boundsRect = new Phaser.Rectangle(0, 0, 0, 0);
        this.currentPage = 0;

        this.createStageIcons();

        this.scrollingMap.events.onDragStart.add(function (sprite, pointer) {
            this.scrollingMap.startPointerPosition = new Phaser.Point(pointer.x, pointer.y);
            this.scrollingMap.startPosition = this.scrollingMap.x;
        }, this);
        this.scrollingMap.events.onDragStop.add(function (sprite, pointer) {
            if (this.scrollingMap.startPosition == this.scrollingMap.x && this.scrollingMap.startPointerPosition.x == pointer.x && this.scrollingMap.startPointerPosition.y == pointer.y) {
                for (var i = 0; i < this.scrollingMap.children.length; i++) {
                    var bounds = this.scrollingMap.children[i].getBounds();
                    if (bounds.contains(pointer.x, pointer.y)) {
                        if (this.scrollingMap.children[i].frame > 0) {
                            //stage = this.scrollingMap.children[i].stageNumber;
                            //game.state.start("PlayStage");
                        }
                        // if the stage is locked, then shake the button
                        else {
                            var buttonTween = game.add.tween(this.scrollingMap.children[i])
                            buttonTween.to({
                                x: this.scrollingMap.children[i].x + this.thumbWidth / 15
                            }, 20);
                            buttonTween.to({
                                x: this.scrollingMap.children[i].x - this.thumbWidth / 15
                            }, 20);
                            buttonTween.to({
                                x: this.scrollingMap.children[i].x + this.thumbWidth / 15
                            }, 20);
                            buttonTween.to({
                                x: this.scrollingMap.children[i].x - this.thumbWidth / 15
                            }, 20);
                            buttonTween.to({
                                x: this.scrollingMap.children[i].x
                            }, 20);
                            buttonTween.start();
                        }
                        break;
                    }
                }
            }
            else {
                if (this.scrollingMap.startPosition - this.scrollingMap.x > $(window).width() / 8) {
                    this.changePage(1);
                }
                else {
                    if (this.scrollingMap.startPosition - this.scrollingMap.x < - $(window).width() / 8) {
                        this.changePage(-1);
                    }
                    else {
                        this.changePage(0);
                    }
                }
            }
        }, this);

        this.leftArrow = game.add.button(0, 0, "level_arrows", this.arrowClicked);
        this.leftArrow.anchor.setTo(0.5);
        this.leftArrow.frame = 0;
        this.leftArrow.alpha = 0.3;
        this.rightArrow = game.add.button(0, 0, "level_arrows", this.arrowClicked);
        this.rightArrow.anchor.setTo(0.5);
        this.rightArrow.frame = 1;

        this.btnHome = game.add.button(0, 0, 'btnHome', this.menuClicked, this, 1, 0);
        this.btnHome.frame = 0;

        var frame = 0;
        var overFrame = 1;
        if (game.state.states.Main.audioBackground.isPlaying == false) {
            frame = 2;
            overFrame = 3;
        };

        this.btnSound = game.add.button(0, 0, 'btnSound', mixins.btnSoundClicked, this, overFrame, frame);

        mixins.resizeStage(this);
        this.animateStegeIcons();
    },

    createStageIcons: function () {
        var stagenr = 0;
        var lengthStage = PLAYER_DATA.length;
        //var lengthStage = 35;
        // looping through all stages
        for (var k = 0; k < lengthStage; k++) {
            var groupStageIcon = this.createStageIcon(0, 0, stagenr);
            var backicon = groupStageIcon.getAt(0);
            backicon.health = stagenr;
            backicon.inputEnabled = true;
            backicon.events.onInputDown.add(this.onSpriteDown, this);
            this.scrollingMap.addChild(groupStageIcon);
            stagenr = stagenr + 1;
        };
    },

    // -------------------------------------
    // Add stage icon buttons
    // -------------------------------------
    createStageIcon: function (xpos, ypos, stagenr) {
        // create new group
        var IconGroup = this.game.add.group();
        IconGroup.x = xpos;
        IconGroup.y = ypos;

        // keep original position, for restoring after certain tweens
        IconGroup.xOrg = xpos;
        IconGroup.yOrg = ypos;

        // determine background frame
        var frame = 1;
        // add background
        //var icon1 = this.game.add.sprite(0, 0, 'levelselecticons', frame);
        var icon1 = this.add.button(0, 0, "levelselecticons", null, this, 5, frame);
        IconGroup.add(icon1);

        var txt = this.game.add.bitmapText(0, 0, 'font72', '' + (stagenr + 1), 48);
        txt.x = icon1.width / 2.3 - txt.width / 2;
        txt.y = icon1.height / 3 - txt.height / 2;
        IconGroup.add(txt);

        return IconGroup;
    },

    onSpriteDown: function (sprite, pointer) {
        mixins.playClik();
        var stagenr = sprite.health;
        // simulate button press animation to indicate selection
        var IconGroup = this.scrollingMap.children[stagenr];
        var scaleOriginal = IconGroup.scale.x;
        var scaleBefore = IconGroup.scale.x - 0.1;
        var tween = this.game.add.tween(IconGroup.scale)
            .to({ x: scaleBefore, y: scaleBefore }, 100, Phaser.Easing.Linear.None)
            .to({ x: scaleOriginal, y: scaleOriginal }, 100, Phaser.Easing.Linear.None)
            .start();

        // it's a little tricky to pass selected stagenr to callback function, but this works:
        tween.onComplete.add(function () { this.onStageSelected(sprite.health); }, this);
    },

    animateStegeIcons: function () {
        // slide all icons into screen
        for (var i = 0; i < this.scrollingMap.children.length; i++) {
            // get variables
            var IconGroup = this.scrollingMap.children[i];
            IconGroup.y = IconGroup.y + 600;
            var y = IconGroup.y;
            // tween animation
            this.game.add.tween(IconGroup).to({ y: y - 600 }, 500, Phaser.Easing.Back.Out, true, (i * 40));
        };
    },

    onStageSelected: function (stagenr) {
        this.getStageById(PLAYER_DATA[stagenr].StageId, PLAYER_DATA[stagenr].LevelId);
        game.state.start("Preload", true, false, false, null, null, null, true);
    },

    changePage: function (page) {
        var this_ = game.state.states.Stage;
        if (this_.canChangePage) {
            this_.canChangePage = false;
            this_.currentPage += page;
            var tween = game.add.tween(this_.scrollingMap).to({
                x: this_.currentPage * -$(window).width()
            }, 300, Phaser.Easing.Cubic.Out, true);
            tween.onComplete.add(function () {
                this_.canChangePage = true;
            }, this_);

            this_.rightArrow.alpha = 0.3;
            this_.leftArrow.alpha = 0.3;
            if (this_.pages > 1) {
                if (this_.currentPage == this_.pages - 1) {
                    this_.rightArrow.alpha = 0.3;
                    this_.leftArrow.alpha = 1;
                } else if (this_.currentPage == 0) {
                    this_.leftArrow.alpha = 0.3;
                    this_.rightArrow.alpha = 1;
                }
                else {
                    this_.rightArrow.alpha = 1;
                    this_.leftArrow.alpha = 1;
                };
            };
        };
    },

    arrowClicked: function (button) {
        var this_ = game.state.states.Stage;
        if (button.frame == 1 && this_.currentPage < this_.pages - 1) {
            Stage.prototype.changePage(1);
            mixins.playClik();
        }
        if (button.frame == 0 && this_.currentPage > 0) {
            Stage.prototype.changePage(-1);
            mixins.playClik();
        }
    },

    menuClicked: function (button) {
        mixins.playClik();
        game.state.start("MainMenu", true, false);
    },

    getStageById: function (stageId, levelId) {
        var headers = mixins.getHeaders();
        $.ajax({
            type: 'GET',
            url: urlApi + '/Api/Stage/GetStagesWithAnswers',
            headers: headers,
            data: { stageId: stageId, levelId: levelId },
        }).done(function (data) {
            isReady = true;
            game.state.states.Game.stages = data;
            game.state.start('Preload', false, false, false, ['loadImagesAnswersGame'], null, 'Game', false);
        }).fail(function (data) {
            if (data.status === 401) {
                var refresh = mixins.refreshToken();
                if (refresh) {
                    game.state.states.Stage.getStageById(stageId, levelId);
                };
            };
        });
    },
};

Phaser.Utils.mixinPrototype(Stage.prototype, mixins);

window.addEventListener('resize', function () {
    if (game.state.current == 'Stage') {
        mixins.resizeStage(game.state.states.Stage);
    };
});