//var urlApi = "http://localhost:44305";
var urlApi = "http://rfessia-001-site2.htempurl.com/";
//var game = new Phaser.Game($(window).width(), $(window).height(), Phaser.AUTO, 'game'),
//var game = new Phaser.Game($(window).width() * window.devicePixelRatio, $(window).height() * window.devicePixelRatio, Phaser.CANVAS, 'game'),
var game = new Phaser.Game($(window).width(), $(window).height(), Phaser.CANVAS, 'game'), Main = function () { }, audioBackground = null, isReady = false;
Main.prototype = {
    preload: function () {
        //Voy a cargar la imagen de fondo sola con el logo y llamo al estado preload
        game.load.image('BackgroundAlone', '/Scripts/Games/EmotionRecognition/Assets/Images/Principal/BG_solo.png');
        game.load.image('Logo', '/Scripts/Games/EmotionRecognition/Assets/Images/Principal/logo.png');
        game.load.image('Loading', '/Scripts/Games/EmotionRecognition/Assets/Images/loading.png');
    },
    create: function () {
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.scale.setGameSize($(window).width(), $(window).height());
        game.state.add('Preload', Preload);
        game.state.start('Preload', false, false, true, ['loadFontStates', 'loadSpritesheets', 'loadImagesGame', 'loadAudio'], ['addAllStates', 'addAudio'], 'MainMenu', false);
    },
};
game.state.add('Main', Main);
game.state.start('Main');
window.addEventListener('resize', function () {
    mixins.resizePrincipal(game.state.states.MainMenu);
});
//# sourceMappingURL=Main.js.map