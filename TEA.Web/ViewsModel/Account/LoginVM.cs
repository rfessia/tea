﻿namespace TEA.Web.ViewsModel.Account
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// View Model para loguearse
    /// </summary>
    public class LoginVM
    {
        [Required]
        [Display(Name = "Nombre de Usuario")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [Display(Name = "Recuérdame")]
        public bool RememberMe { get; set; }

        public int NumberTrials { get; set; }

        public string MessageError { get; set; }
    }
}