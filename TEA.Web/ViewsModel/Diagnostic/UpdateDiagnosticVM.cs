﻿namespace TEA.Web.ViewsModel.Diagnostic
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public class UpdateDiagnosticVM
    {
        /// <summary>
        /// Id del Diagnositco
        /// </summary>
        [Required]
        public int DiagnosticId { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Required]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        /// <summary>
        /// Descripción
        /// </summary>
        [Display(Name = "Descripción")]
        public string Description { get; set; }

        /// <summary>
        /// ID Tipo de Diagnostico
        /// </summary>
        [Display(Name = "Tipo de Diagnóstico")]
        public int DiagnosticTypeId { get; set; }

        /// <summary>
        /// Id Usuario
        /// </summary>
        [Display(Name = "Usuario")]
        [Required]
        public int? UserId { get; set; }

        /// <summary>
        /// Lista de usuarios
        /// </summary>
        public IList<SelectListItem> UsersItems { get; set; }

        /// <summary>
        /// Lista de diagnosticos
        /// </summary>
        public IList<SelectListItem> DiagnosticTypes { get; set; }
    }
}