﻿namespace TEA.Web.ViewsModel.Diagnostic
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    /// <summary>
    /// View model para index de diagnosticos
    /// </summary>
    public class IndexDiagnosticVM
    {
        /// <summary>
        /// Id del Diagnositco
        /// </summary>
        public int DiagnosticId { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Descripción
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// ID Tipo de Diagnostico
        /// </summary>
        [Display(Name = "Tipo de Diagnóstico")]
        public int DiagnosticTypeId { get; set; }

        /// <summary>
        /// Nombre del Tipo de Diagnostico
        /// </summary>
        public string DiagnosticTypeName { get; set; }

        /// <summary>
        /// Acciones
        /// </summary>
        [Display(Name = "Acciones")]
        public string Actions { get; set; }

        /// <summary>
        /// Lista de diagnosticos
        /// </summary>
        public IList<SelectListItem> DiagnosticTypes { get; set; }

        /// <summary>
        /// Filtro para buscar por nombre de diagnostico
        /// </summary>
        [Display(Name = "Nombre del diagnóstico")]
        public string FilterDiagnosticName { get; set; }
    }
}