﻿namespace TEA.Web.ViewsModel.Diagnostic
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Viewmodel para detalla de diagnostico
    /// </summary>
    public class DetailDiagnosticVM
    {
        [Display(Name = "Tipo de Diagnóstico")]
        public string DiagnosticTypeName { get; set; }

        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Display(Name = "Descripción")]
        public string Description { get; set; }

        [Display(Name = "Usuario")]
        public string UserName { get; set; }
    }
}