﻿namespace TEA.Web.ViewsModel.User
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// View model para detalle de usuario
    /// </summary>
    public class DetailUserVM
    {
        [Display(Name = "Nombre de Usuario Único")]
        public string UserName { get; set; }

        [Display(Name = "Nombre Completo")]
        public string Name { get; set; }

        [Display(Name = "Fecha de Nacimiento")]
        public string Birthdate { get; set; }

        [Display(Name = "DNI")]
        public string DocumentNumber { get; set; }

        [Display(Name = "Género")]
        public string Gender { get; set; }

        [Display(Name = "Roles")]
        public string Roles { get; set; }
    }
}