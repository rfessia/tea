﻿namespace TEA.Web.ViewsModel.User
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    /// <summary>
    /// View model para create usuario
    /// </summary>
    public class CreateUserVM
    {
        [Required]
        public int Id { get; set; }

        [Display(Name = "Nombre de Usuario Único")]
        [RegularExpression(@"^\S*$", ErrorMessage = "El Nombre de Usuario no debe contener espacios")]
        [Required]
        public string UserName { get; set; }

        [Display(Name = "Contraseña")]
        [MinLength(6)]
        [Required]
        public string Password { get; set; }

        [Display(Name = "Repita contraseña")]
        [MinLength(6)]
        [Required]
        [System.ComponentModel.DataAnnotations.Compare("Password")]
        public string RePassword { get; set; }

        [Display(Name = "Nombre Completo")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Fecha de Nacimiento")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        public DateTime? Birthdate { get; set; }

        [Display(Name = "DNI")]
        [Required]
        public int? DocumentNumber { get; set; }

        [Display(Name = "Género")]
        [Required]
        public int GenderId { get; set; }

        public DateTime InputDate { get; set; }

        public DateTime? ModificationDate { get; set; }

        public DateTime? CancellationDate { get; set; }

        public int? TopUserId { get; set; }

        /// <summary>
        /// Roles seleccionados
        /// </summary>
        public IList<string> SelectedRoles { get; set; }
        
        /// <summary>
        /// Lista de roles
        /// </summary>
        public IList<SelectListItem> Roles { get; set; }

        /// <summary>
        /// Lista de géneros
        /// </summary>
        public IList<SelectListItem> Genders { get; set; }
    }
}