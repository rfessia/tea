﻿namespace TEA.Web.ViewsModel.User
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// VM para lista de usaurios
    /// </summary>
    public class IndexUserVM
    {
        /// <summary>
        /// Id de Usuario
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Nombre completo
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Nombre de Usuario
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Número de Documento
        /// </summary>
        public int DocumentNumber { get; set; }

        /// <summary>
        /// Acciones
        /// </summary>
        [Display(Name = "Acciones")]
        public string Actions { get; set; }

        /// <summary>
        /// Filtro para buscar por nombre de usuario
        /// </summary>
        [Display(Name = "Nombre de Usuario")]
        public string FilterUserName { get; set; }

        /// <summary>
        /// Filtro para buscar por nombre completo
        /// </summary>
        [Display(Name = "Nombre Completo")]
        public string FilterFullName { get; set; }

        /// <summary>
        /// Filtro para buscar por numero de documento
        /// </summary>
        [Display(Name = "Documento")]
        public int? FilterDocumentNumber { get; set; }
    }
}