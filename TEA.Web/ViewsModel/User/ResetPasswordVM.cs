﻿namespace TEA.Web.ViewsModel.User
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// VM para resetear contraseña
    /// </summary>
    public class ResetPasswordVM
    {
        /// <summary>
        /// Id usuario
        /// </summary>
        [Required]
        public int UserId { get; set; }

        /// <summary>
        /// Nombre completo
        /// </summary>
        [Display(Name = "Nombre Completo")]
        public string Name { get; set; }

        /// <summary>
        /// Contraseña
        /// </summary>
        [Display(Name = "Nueva Contraseña")]
        [MinLength(6)]
        [Required]
        public string Password { get; set; }

        /// <summary>
        /// Contraseña repetida
        /// </summary>
        [Display(Name = "Repita contraseña")]
        [MinLength(6)]
        [Required]
        [System.ComponentModel.DataAnnotations.Compare("Password")]
        public string RePassword { get; set; }

        /// <summary>
        /// ID Usuario profesional
        /// </summary>
        public int? TopUserId { get; set; }
    }
}