﻿namespace TEA.Web.ViewsModel.Report
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    /// <summary>
    /// VM para representar los registros históricos
    /// </summary>
    public class IndexUserStageHistoryVM
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Nombre de Usuario
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Nombre del Usuario profesional
        /// </summary>
        public string UserProfessionalName { get; set; }

        /// <summary>
        /// Nombre del diagnóstico
        /// </summary>
        public string DiagnosticName { get; set; }

        /// <summary>
        /// Nombre de la etapa
        /// </summary>
        public string StageName { get; set; }

        /// <summary>
        /// Indica si es una etapa compartida o no
        /// </summary>
        public string StageShared { get; set; }

        /// <summary>
        /// Nombre del nivel
        /// </summary>
        public string LevelName { get; set; }

        /// <summary>
        /// Número de intentos para llegar al objetivo
        /// </summary>
        public string NumberOfAttempts { get; set; }

        /// <summary>
        /// Fecha de finalizació de la etapa
        /// </summary>
        public string FinishDate { get; set; }

        /// <summary>
        /// Acciones
        /// </summary>
        [Display(Name = "Acciones")]
        public string Actions { get; set; }

        /// <summary>
        /// Nombre de la etapa
        /// </summary>
        [Display(Name = "Etapa")]
        public string FilterStageName { get; set; }

        /// <summary>
        /// Filtro Nombre del nivel
        /// </summary>
        [Display(Name = "Actividad")]
        public string FilterLevelName { get; set; }

        /// <summary>
        /// Filtro Nombre del diagnóstico
        /// </summary>
        [Display(Name = "Diagnóstico")]
        public string FilterDiagnosticName { get; set; }

        /// <summary>
        /// Indica si es una etapa compartida o no
        /// </summary>
        [Display(Name = "Tipo de Etapa")]
        public bool? FilterSharedStage { get; set; }

        /// <summary>
        /// Fecha de finalización desde
        /// </summary>
        //[DataType(DataType.Date)]
        [Display(Name = "Fecha Desde")]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string FilterDateStart { get; set; }

        /// <summary>
        /// Fecha de finalización hasta
        /// </summary>
        //[DataType(DataType.Date)]
        [Display(Name = "Fecha Hasta")]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string FilterDateEnd { get; set; }

        /// <summary>
        /// Lista de usuarios
        /// </summary>
        public IList<SelectListItem> UsersItems { get; set; }

        /// <summary>
        /// Filtro para buscar por nombre de usuario
        /// </summary>
        [Display(Name = "Usuario")]
        public int? FilterUserId { get; set; }

        /// <summary>
        /// Orden del reporte
        /// </summary>
        public string FilterSortReport { get; set; }
    }
}