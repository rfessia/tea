﻿namespace TEA.Web.ViewsModel.Level
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// View model para detalle de nivel
    /// </summary>
    public class DetailLevelVM
    {
        [Display(Name = "Actividad")]
        public string LevelName { get; set; }

        [Display(Name = "Diagnostico")]
        public string DiagnosticName { get; set; }

        [Display(Name = "Usuarios")]
        public string UserName { get; set; }

        [Display(Name = "Habilitada")]
        public string Enabled { get; set; }

        [Display(Name = "Orden")]
        public string Order { get; set; }
    }
}