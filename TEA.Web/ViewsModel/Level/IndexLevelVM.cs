﻿namespace TEA.Web.ViewsModel.Level
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    /// <summary>
    /// View model para index de niveles
    /// </summary>
    public class IndexLevelVM
    {
        /// <summary>
        /// Id del Nivel
        /// </summary>
        public int LevelId { get; set; }

        /// <summary>
        /// Nombre del nivel
        /// </summary>
        public string LevelName { get; set; }

        /// <summary>
        /// Indica si está habilitado o no
        /// </summary>
        public string Enabled { get; set; }

        /// <summary>
        /// Orden del nivel
        /// </summary>
        public string Order { get; set; }

        /// <summary>
        /// Nombre del usuario
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Nombre del nivel
        /// </summary>
        public string DiagnosticName { get; set; }

        /// <summary>
        /// Acciones
        /// </summary>
        [Display(Name = "Acciones")]
        public string Actions { get; set; }

        /// <summary>
        /// Filtro para buscar por nombre de diagnostico
        /// </summary>
        [Display(Name = "Actividad")]
        public string FilterLevelName { get; set; }

        /// <summary>
        /// Filtro para buscar por nombre de diagnostico
        /// </summary>
        [Display(Name = "Diagnóstico")]
        public string FilterDiagnosticName { get; set; }

        /// <summary>
        /// Indica si es una actividad está habilitada o no
        /// </summary>
        [Display(Name = "Habilitada")]
        public bool? FilterEnabled { get; set; }

        /// <summary>
        /// Lista de usuarios
        /// </summary>
        public IList<SelectListItem> UsersItems { get; set; }

        /// <summary>
        /// Filtro para buscar por nombre de usuario
        /// </summary>
        [Display(Name = "Usuario")]
        public int? FilterUserId { get; set; }
    }
}