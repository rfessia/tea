﻿namespace TEA.Web.ViewsModel.Level
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    /// <summary>
    /// View model para editar un nivel
    /// </summary>
    public class EditLevelVM
    {
        /// <summary>
        /// Id del nivel
        /// </summary>
        public int LevelId { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Required]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        /// <summary>
        /// Indica si está habilitado o no
        /// </summary>
        [Display(Name = "Habilitada")]
        [Required]
        public bool Enabled { get; set; }

        /// <summary>
        /// Orden
        /// </summary>
        [Required]
        [Display(Name = "Orden")]
        public int? Order { get; set; }

        /// <summary>
        /// Usuarios
        /// </summary>
        [Display(Name = "Usuario")]
        public string UserName { get; set; }

        /// <summary>
        /// ID del Diagnostico
        /// </summary>
        [Display(Name = "Diagnóstico")]
        public int DiagnosticId { get; set; }

        /// <summary>
        /// Lista de diagnosticos
        /// </summary>
        public IList<SelectListItem> Diagnostics { get; set; }
    }
}