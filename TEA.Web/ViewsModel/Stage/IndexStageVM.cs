﻿namespace TEA.Web.ViewsModel.Stage
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    /// <summary>
    /// View model para listado de etapas
    /// </summary>
    public class IndexStageVM
    {
        /// <summary>
        /// Id de la etapa
        /// </summary>
        public int StageId { get; set; }

        /// <summary>
        /// Orden de la etapa
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Nombre del diagnóstico
        /// </summary>
        public string DiagnosticName { get; set; }

        /// <summary>
        /// Nombre del nivel
        /// </summary>
        public string LevelName { get; set; }

        /// <summary>
        /// Nombre de la etapa
        /// </summary>
        public string StageName { get; set; }

        /// <summary>
        /// Indica si es una etapa compartida o no
        /// </summary>
        public string StageShared { get; set; }

        /// <summary>
        /// Acciones
        /// </summary>
        [Display(Name = "Acciones")]
        public string Actions { get; set; }

        /// <summary>
        /// Filtro para buscar por nombre de nivel
        /// </summary>
        [Display(Name = "Etapa")]
        public string FilterStageName { get; set; }

        /// <summary>
        /// Filtro para buscar por nombre de nivel
        /// </summary>
        [Display(Name = "Actividad")]
        public string FilterLevelName { get; set; }

        /// <summary>
        /// Filtro para buscar por nombre de diagnostico
        /// </summary>
        [Display(Name = "Diagnóstico")]
        public string FilterDiagnosticName { get; set; }

        /// <summary>
        /// Indica si es una etapa compartida o no
        /// </summary>
        [Display(Name = "Tipo de Etapa")]
        public bool? FilterSharedStage { get; set; }

        /// <summary>
        /// Lista de usuarios
        /// </summary>
        public IList<SelectListItem> UsersItems { get; set; }

        /// <summary>
        /// Filtro para buscar por nombre de usuario
        /// </summary>
        [Display(Name = "Usuario")]
        public int? FilterUserId { get; set; }
    }
}