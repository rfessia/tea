﻿namespace TEA.Web.ViewsModel.Stage
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// View model para crear respuestas de una etapa de un usuario
    /// </summary>
    public class CreateAnswerForUserVM
    {
        /// <summary>
        /// Id de la etapa
        /// </summary>
        [Required(ErrorMessage = "No existe etapa asociada a la respuesta")]
        public int? StageId { get; set; }

        /// <summary>
        /// Id Usuario
        /// </summary>
        [Required]
        public int? UserId { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Required]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        /// <summary>
        /// Orden
        /// </summary>
        [Required]
        [Display(Name = "Orden")]
        public int? Order { get; set; }

        /// <summary>
        /// Id del nivel
        /// </summary>
        [Display(Name = "Seleccione una Actividad")]
        [Required(ErrorMessage = "Debe seleccionar una Actividad")]
        public int? LevelId { get; set; }

        /// <summary>
        /// Nombre de la respuesta correcta
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe seleccionar una respuesta correcta")]
        public string AnswerCorrectName { get; set; }

        /// <summary>
        /// Nombre de la imagen de fondo
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe seleccionar una imagen de fondo")]
        public string BackgroundName { get; set; }
    }
}