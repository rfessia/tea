﻿namespace TEA.Web.ViewsModel.Stage
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public class SelectTypeStageToCreateVM
    {
        /// <summary>
        /// Indica que la nueva etapa corresponde a un Usuario en particular
        /// </summary>
        public bool? NewStageWhitUser { get; set; }

        /// <summary>
        /// Filtro para buscar por nombre de usuario
        /// </summary>
        [Display(Name = "Usuario")]
        public int? FilterUserId { get; set; }

        /// <summary>
        /// Lista de usuarios
        /// </summary>
        public IList<SelectListItem> UsersItems { get; set; }
    }
}