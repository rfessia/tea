﻿namespace TEA.Web.ViewsModel.Stage
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// View Model para crear etapa comparitda
    /// </summary>
    public class CreateSharedStageVM
    {
        /// <summary>
        /// Id de la etapa
        /// </summary>
        public int? StageId { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Required]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        /// <summary>
        /// Arbol completo de todas las etapas / niveles / diagnosticos
        /// </summary>
        public string TreeStages { get; set; }
    }
}