﻿namespace TEA.Web.ViewsModel.Stage
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Entidad para asignar niveles a un usuario
    /// </summary>
    public class StagesToAssignVM
    {
        /// <summary>
        /// Id del nivel
        /// </summary>
        [Required]
        public int LevelId { get; set; }

        /// <summary>
        /// Nombre del nivel
        /// </summary>
        public string LevelName { get; set; }

        /// <summary>
        /// String que representa un elemento html con la información de todas las etapas seleccionadas
        /// </summary>
        public string StagesSelected { get; set; }
    }
}