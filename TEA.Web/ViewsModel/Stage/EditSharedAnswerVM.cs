﻿namespace TEA.Web.ViewsModel.Stage
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// View model para editar respuestas de una etapa compartida
    /// </summary>
    public class EditSharedAnswerVM
    {
        /// <summary>
        /// Id de la etapa
        /// </summary>
        [Required(ErrorMessage = "No existe etapa asociada a la respuesta")]
        public int? StageId { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Required]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        /// <summary>
        /// Nombre de la respuesta correcta
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe seleccionar una respuesta correcta")]
        public string AnswerCorrectName { get; set; }

        /// <summary>
        /// Nombre de la imagen de fondo
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe seleccionar una imagen de fondo")]
        public string BackgroundName { get; set; }
    }
}