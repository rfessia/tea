﻿namespace TEA.Web.ViewsModel.Stage
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// View Model para editar etapa comparitda
    /// </summary>
    public class EditSharedStageVM
    {
        /// <summary>
        /// Id de la etapa
        /// </summary>
        [Required]
        public int? StageId { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Required]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        /// <summary>
        /// Arbol completo de todas las etapas / niveles / diagnosticos
        /// </summary>
        public string TreeStages { get; set; }
    }
}