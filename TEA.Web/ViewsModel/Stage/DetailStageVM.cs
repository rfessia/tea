﻿namespace TEA.Web.ViewsModel.Stage
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// View model para detalle de una etapa
    /// </summary>
    public class DetailStageVM
    {
        [Display(Name = "Etapa")]
        public string StageName { get; set; }

        [Display(Name = "Actividad")]
        public string LevelName { get; set; }

        [Display(Name = "Diagnostico")]
        public string DiagnosticName { get; set; }

        [Display(Name = "Compartida")]
        public string StageShared { get; set; }
    }
}