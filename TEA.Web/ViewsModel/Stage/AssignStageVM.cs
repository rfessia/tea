﻿namespace TEA.Web.ViewsModel.Stage
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// VM para la asignación de etapas a un usuario
    /// </summary>
    public class AssignStageVM
    {
        /// <summary>
        /// Id del Usuario
        /// </summary>
        [Required]
        public int UserId { get; set; }

        /// <summary>
        /// Nombre de usuario
        /// </summary>
        [Display(Name = "Nombre Usuario")]
        public string FullName { get; set; }

        /// <summary>
        /// Lista de etapas compartidas en formato HTML para elemento sorteable
        /// </summary>
        public string SharedStages { get; set; }

        /// <summary>
        /// Lista de etapas a asignar
        /// </summary>
        [Required]
        public List<StagesToAssignVM> StagesToAssign { get; set; }
    }
}