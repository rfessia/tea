﻿namespace TEA.Web.ViewsModel.Stage
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// View Model utilizado para crear une etapa de un usuario en particular
    /// </summary>
    public class CreateStageForUserVM
    {
        /// <summary>
        /// Id de la etapa
        /// </summary>
        public int? StageId { get; set; }

        /// <summary>
        /// Id Usuario
        /// </summary>
        [Required]
        public int? UserId { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Required]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        /// <summary>
        /// Orden
        /// </summary>
        [Required]
        [Display(Name = "Orden")]
        public int? Order { get; set; }

        /// <summary>
        /// Id del nivel
        /// </summary>
        [Display(Name = "Seleccione una Actividad")]
        [Required(ErrorMessage = "Debe seleccionar una Actividad")]
        public int? LevelId { get; set; }

        /// <summary>
        /// Nombre del usuario
        /// </summary>
        public string FullNameUser { get; set; }

        /// <summary>
        /// Arbol completo de todas las etapas / niveles / diagnosticos
        /// </summary>
        public string TreeStages { get; set; }
    }
}