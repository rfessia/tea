﻿namespace TEA.Web.Filters
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Security.Principal;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using System.Web.Security;
    using TEA.Web.Infrastucture.Security;

    //[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class CustomAuthenticationAttribute : AuthorizeAttribute
    {
        //List<string> rolesAccepted = new List<string>();

        //public CustomAuthenticationAttribute()
        //{
        //    string roles = ConfigurationManager.AppSettings["RolesAceptados"];
        //    string[] acceptedRoles = roles.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        //    foreach (var acceptedRole in acceptedRoles)
        //    {
        //        rolesAccepted.Add(acceptedRole);
        //    }
        //}

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            //var skipAuthorization = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true) ||
            //    filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true);

            //if (skipAuthorization)
            //{
            //    base.OnAuthorization(filterContext);
            //}

            //var cookie = filterContext.HttpContext.Request.Cookies["asd"];
            base.OnAuthorization(filterContext);
            //if (filterContext.Result == null)
            //{
            //    filterContext.Result = new RedirectResult("~/Account/Login");
            //    return;
            //}
            //else
            //{
            //    base.OnAuthorization(filterContext);
            //}
            bool userInRole = false;
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                
                var listaRoles = this.Roles;
                HttpCookie authCookie = filterContext.HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];

                if (authCookie != null)
                {
                    FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                    CustomIdentity identity = new CustomIdentity(authTicket.Name, authTicket.UserData);

                    string[] roles = authTicket.UserData.Split('|')[2].Split(',');

                    foreach (var rol in roles)
                    {
                        if (listaRoles.Equals(rol))
                        {
                            userInRole = true;
                            break;
                        }
                    }
                }
                
            }

            if (!userInRole)
            {
                // get from cached variable from web configuration
                string loginUrl = "";
                if (filterContext.HttpContext.Request != null)
                {
                    loginUrl = "~/Account/login?ReturnUrl=" + filterContext.HttpContext.Request.Url.PathAndQuery;
                }
                else
                {
                    loginUrl = "~/Account/login";
                }

                filterContext.Result = new RedirectResult(loginUrl);
            }
        }

        //protected override bool AuthorizeCore(HttpContextBase httpContext)
        //{
        //    bool userInRole = false;
        //    var listaRoles = this.Roles;
        //    HttpCookie authCookie = httpContext.Request.Cookies[FormsAuthentication.FormsCookieName];

        //    if (authCookie != null)
        //    {
        //        FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
        //        CustomIdentity identity = new CustomIdentity(authTicket.Name, authTicket.UserData);

        //        string[] roles = authTicket.UserData.Split('|')[2].Split(',');

        //        foreach (var rol in roles)
        //        {
        //            if (listaRoles.Contains(rol))
        //            {
        //                userInRole = true;
        //                break;
        //            }
        //        }
        //    }
        //    return userInRole;
        //}

        //protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        //{
        //    filterContext.HttpContext.Response.Redirect("~/View/Shared/Error");
        //}
    }
}