/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */

$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        //forceIframeTransport: true,
        //url: '//localhost:56145/Etapa/GetFiles',
        url: 'UploadFile',
        //add: function (e, data) {
        //    //$("#label1").css("Display", "none");
        //    //data.submit();
        //},
        //done: function (e, data) {
        //    $.each(data.result, function (index, file) {
        //        var fila = "<tr><td><span><img style='max-width:40px;' src='" + file[index].thumbnailUrl + "'></span></td><td><p>" + file[index].name + "</p></td><td><span>" + file[index].size + "</span></td><td><a class='btn btn-danger delete' href='/Etapa/EliminarArchivo'><i class='glyphicon glyphicon-trash'></i>Cancelar</a></td><td><input type='checkbox' class='inFondo' hidden='hidden'><a class='aFondo' style='width:auto;float:left;' href='javascript:void(0)' onclick='clickFondo(this); return false;'><i class='btn btn-default btn-md glyphicon glyphicon-picture'></i></a><span class='sFondo' style='display:none;'><i class='btn btn-primary btn-md glyphicon glyphicon-picture icoFondo' style='pointer-events: none;'></i></span></td><td><input type='checkbox' class='inRespCorrecta' hidden='hidden'><a class='aRespCorrecta' style='width:auto;float:left;' href='javascript:void(0)' onclick='clickRespCorrecta(this); return false;'><i class='btn btn-default btn-md glyphicon glyphicon-ok icoRespCorrecta'></i></a><span class='sRespCorrecta' style='display:none;'><i class='btn btn-success btn-md glyphicon glyphicon-ok icoFondo' style='pointer-events: none;'></i></span></td></tr>";
                           
        //        alert(file[index].deleteUrl)
        //    })
        //}
    });

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect'
        //'//localhost:56145/Etapa/hola'
        //window.location.href.replace(
        //    /\/[^\/]*$/,
        //    '/cors/result.html?%s'
        //)
    );

    if (window.location.hostname === 'blueimp.github.io') {
        // Demo settings:
        $('#fileupload').fileupload('option', {
            url: '//jquery-file-upload.appspot.com/',
            // Enable image resizing, except for Android and Opera,
            // which actually support image resizing, but fail to
            // send Blob objects via XHR requests:
            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            maxFileSize: 999000,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
        });
        // Upload server status check for browsers with CORS support:
        if ($.support.cors) {
            $.ajax({
                url: '//jquery-file-upload.appspot.com/',
                type: 'HEAD'
            }).fail(function () {
                $('<div class="alert alert-danger"/>')
                    .text('Upload server currently unavailable - ' +
                            new Date())
                    .appendTo('#fileupload');
            });
        }
    } else {
        // Load existing files:
        $('#fileupload').addClass('fileupload-processing');
        var stageId = $('#StageId').val();
        if (!stageId) {
            stageId = 0;
        };
        var url = window.location.origin + '/Stage/GetFiles?stageId=' + stageId;
        $.ajax({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            //url: 'Respuestas',
            url: url,
            dataType: 'json',
            context: $('#fileupload')[0]
        }).always(function () {
            $(this).removeClass('fileupload-processing');
        }).done(function (result) {
            $('#AnswerCorrectName').val("");
            $('#BackgroundName').val("");
            $(this).fileupload('option', 'done')
                .call(this, $.Event('done'), { result: result });
            $(result.files).each(function (index, file) {
                if (file.idFondo != null) {
                    $('.inFondo').each(function () {
                        var fondo = $(this);
                        if (fondo.val().split('-')[0] == file.idFondo) {
                            fondo.prop('checked', true);
                            fondo.siblings('.aFondo').click();
                            clickFondo();
                        }
                        else {
                            fondo.prop('checked', false);
                        }
                    });
                }

                if (file.answerIdCorrect != null) {
                    $('.inRespCorrecta').each(function () {
                        var fondo = $(this);
                        if (fondo.val().split('-')[0] == file.answerIdCorrect) {
                            fondo.prop('checked', true);
                            fondo.siblings('.aRespCorrecta').click();
                            clickFondo();
                        }
                        else {
                            fondo.prop('checked', false);
                        }
                    });
                }
            });
        });
    }

});
