﻿$(document).ready(function () {
    $('.slimScroll').slimScroll({
        height: '200px'
    });

    var byId = function (id) { return document.getElementById(id); },
		loadScripts = function (desc, callback) {
		    var deps = [], key, idx = 0;
		    for (key in desc) {
		        deps.push(key);
		    };

		    (function _next() {
		        var pid,
					name = deps[idx],
					script = document.createElement('script');

		        script.type = 'text/javascript';
		        script.src = desc[deps[idx]];

		        pid = setInterval(function () {
		            if (window[name]) {
		                clearTimeout(pid);

		                deps[idx++] = window[name];

		                if (deps[idx]) {
		                    _next();
		                } else {
		                    callback.apply(null, deps);
		                }
		            }
		        }, 30);

		        document.getElementsByTagName('head')[0].appendChild(script);
		    })()
		};

    $('[id^=stage-]').each(function () {
        var id = this.id.split('-').pop();
        var group = {};
        var sort = true;
        if (id == 0) {
            group = {
                name: 'stage',
                pull: true,
                put: false
            };
        }
        else {
            group = {
                name: 'stage',
                pull: false,
                put: true
            };
        }

        Sortable.create(byId('stage-' + id), {
            sort: sort,
            group: group,
            animation: 150,
            filter: '.js-remove',
            onFilter: function (evt) {
                var elementToAdd = '<li id="' + evt.item.id + '" draggable="false" class="" style="">' + evt.item.innerText + '</li>';
                $('ul#stage-0').append(elementToAdd);
                evt.item.parentNode.removeChild(evt.item);
                if ($(evt.to).find('li').length == 1) {
                    $(evt.to).find('li.description').show();
                };
            },
            onAdd: function (evt) {
                if ($(evt.to).find('li#' + evt.clone.id).length > 1) {
                    $($(evt.to).find('li#' + evt.clone.id)).each(function (i) {
                        $('ul#' + evt.to.id + '>li[id="' + this.id + '"]').slice(1).remove();
                    });
                };

                if ($(evt.to).find('li').length == 2) {
                    $(evt.to).find('li.description').hide();
                };

                if ($(evt.item).html().indexOf("js-remove") == -1) {
                    $(evt.item).html($(evt.item).html() + "<i class='js-remove fa fa-times'></i>");
                };
            }
        });

    });

    $("#Save").on("click", function (e) {
        blockUIGeneric();
        var userId = $("#UserId").val();
        var stagesToAssign = [];
        $('[id^=stage-]').each(function () {
            var levelId = this.id.split('-').pop();
            if (levelId != 0) {
                var stagesSelected = [];
                $(this).children('li').each(function () {
                    if ($(this).attr('id')) {
                        stagesSelected.push($(this).attr('id'));
                    };
                });
                var stageToAssign = {
                    LevelId: parseInt(levelId),
                    StagesSelected: stagesSelected.toString(),
                };

                stagesToAssign.push(stageToAssign);
            };
        });

        var model = {
            UserId: userId,
            StagesToAssign: stagesToAssign
        };

        $.ajax({
            url: '/Stage/AssignStage',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(model),
            success: function (data) {
                if (data.isValid) {
                    var url = (window.location.origin + '/User')
                    window.location.href = url;
                } else {
                    unblockUI();
                    writeErrors(data.errorList);
                };
            },
            error: function (e) {
                unblockUI();
                alert("Ha ocurrido un error interno");
            }
        });

        $("#Stages").val(stagesToAssign);
    });
});

function searchMyElements(stageInput, stageUl) {
    var input, filter, ul, li, a, i;
    input = document.getElementById(stageInput);
    filter = input.value.toUpperCase();
    li = $('ul#' + stageUl + '>li')
    for (i = 0; i < li.length; i++) {
        if (li[i].className != "description") {
            if (li[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            };
        };
    }
}

function writeErrors(errorList) {
    for (var key in errorList) {
        // skip loop if the property is from prototype
        if (!errorList.hasOwnProperty(key)) continue;
        var obj = errorList[key];
        for (var prop in obj) {
            // skip loop if the property is from prototype
            if (!obj.hasOwnProperty(prop)) continue;
            var input = $("input[name='" + key + "']");
            if (input.length > 0) {
                input.removeClass("valid");
                input.addClass("input-validation-error");
            };

            var span = $("[data-valmsg-for='" + key + "']");
            if (span.length > 0) {
                span.html("<span for='" + key + "' class>" + obj[prop] + "</span>");
                span.removeClass("field-validation-valid");
                span.addClass("field-validation-error");
            };

            if (key == "MsjError") {
                var p = $("p[name='" + key + "']");
                p.html(obj[prop]);
            };
        };
    };
};