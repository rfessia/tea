﻿$(function () {

    $("#SaveStage").click(function (e) {
        e.preventDefault();
        var respCorrecta = $('input:checked.inRespCorrecta');
        var fondo = $('input:checked.inFondo');
        if (respCorrecta.length == 1 && fondo.length == 1) {
            var answerCorrectId = respCorrecta.val();
            var backgroundId = fondo.val();
            var stageId = $('#StageId').val();

            /*POST*/
            $.ajax({
                url: '/Stage/SaveAnswer',
                type: 'POST',
                data: {
                    AnswerCorrectId: answerCorrectId,
                    BackgroundId: backgroundId,
                    StageId: stageId,
                },
                success: function (data) {
                    if (data.resultado) {
                        alert("Clave: " + data.clave + " Valor: " + data.valor);
                    }
                    else {
                        alert("Clave: " + data.clave + " Valor: " + data.valor);
                    };
                },
                error: function (e) {
                    alert('error');
                }
            });

        }
    });
});

function clickFondo(e) {
    var elemento = $(e);

    //Oculto img respuesta correcta
    elemento.parent().siblings().find('a.aRespCorrecta').css("display", "none");
    //Oculto img Fondo
    elemento.css("display", "none");
    //Cargo img nuevo fondo
    elemento.siblings('span').css("display", "block");
    //Marco hidden
    elemento.siblings('input').prop('checked', true);

    var fila = $(e).parent('td').parent('tr');
    //Oculto inputs
    fila.find(':input').prop("disabled", true);
    //Marco Fila
    fila.css("background-color", "rgb(160, 197, 232);");
    //Oculto img Hermanas nuevo fondo
    fila.siblings().children('td').children('span.sFondo').css("display", "none")

    //Por cada fila hermana que NO tenga checkeado respCorrecta
    fila.siblings().children('td').children('input.inRespCorrecta').each(function () {
        if (!this.checked) {
            //Fondo transparente
            $(this).parent('td').parent('tr').css("background-color", "transparent");
            //Muestro img Hermanas de fondo
            $(this).parent('td').siblings().children('a.aFondo').css("display", "block")
            //Muestro img RespCorrecta hermanas
            $(this).siblings('a.aRespCorrecta').css("display", "block")
            //Muestro inputs de hermanas
            $(this).prop("disabled", false);
            $(this).parent('td').siblings().find(':input').prop("disabled", false);
        }
    });

    //Desmarco hidden de hermanas
    fila.siblings().children('td').children('input.inFondo').prop('checked', false);

}

function clickRespCorrecta(e) {
    var elemento = $(e);
    //Oculto img Fondo
    elemento.parent().siblings().find('a.aFondo').css("display", "none");
    //Oculto img RespCorrecta
    elemento.css("display", "none");
    //Cargo img nueva RespCorrecta
    elemento.siblings('span').css("display", "block");
    //Marco hidden
    elemento.siblings('input').prop('checked', true);

    var fila = $(e).parent('td').parent('tr');
    //Oculto inputs
    fila.find(':input').prop("disabled", true);
    //Marco Fila
    fila.css("background-color", "rgb(160, 225, 160)");
    //Oculto img Hermanas nueva RespCorrecta
    fila.siblings().children('td').children('span.sRespCorrecta').css("display", "none")

    //Por cada fila hermana que NO tenga checkeado Fondo
    fila.siblings().children('td').children('input.inFondo').each(function () {
        if (!this.checked) {
            //Fondo transparente
            $(this).parent('td').parent('tr').css("background-color", "transparent");
            $(this).siblings('a.aFondo').css("display", "block")
            //Muestro img RespCorrecta hermanas
            $(this).parent('td').siblings().children('a.aRespCorrecta').css("display", "block")
            //Muestro inputs de hermanas
            $(this).prop("disabled", false);
            $(this).parent('td').siblings().find(':input').prop("disabled", false);
        }
    });

    //Desmarco hidden de hermanas
    fila.siblings().children('td').children('input.inRespCorrecta').prop('checked', false);
}