﻿$(function () {
    $('#levelTable').DataTable({
        language: {
            url: '/Scripts/plugins/datatables/Spanish.json'
        },
        "processing": true, // for show processing bar
        "serverSide": true, // for process on server side
        "orderMulti": false, // for disable multi column order
        "dom": '<"top"l>rt<"bottom"ip><"clear">', // for hide default global search box // little confusion? don't worry I explained in the tutorial website
        "ajax": {
            "url": "/Level/LoadDataTable",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
                { "data": "LevelName", "name": "Actividad", "autoWidth": true },                        //index 0
                { "data": "UserName", "name": "Usuario", "autoWidth": true },                           //index 1
                { "data": "DiagnosticName", "name": "Diagnóstico", "autoWidth": true },                 //index 2
                { "data": "Enabled", "name": "Habilitada", "autoWidth": true },                         //index 3
                { "data": "Order", "name": "Orden", "autoWidth": true },                                //index 4
                { "data": "Actions", "name": "Acciones", "className": "actions", "orderable": false },  //index 5
        ]
    });

    //Apply Custom search on jQuery DataTables here
    oTable = $('#levelTable').DataTable();
    $('#btnSearch').click(function () {
        oTable.columns(0).search($('#FilterLevelName').val().trim());
        oTable.columns(1).search($('#FilterDiagnosticName').val().trim());
        oTable.columns(2).search($('#FilterUserId').val().trim());
        oTable.columns(3).search($('#FilterEnabled').val().trim());
        oTable.draw();
    });

    $(".select2").select2({
        selectionTitleAttribute: false
    });

    $('.select2-selection__rendered').hover(function () {
        $(this).removeAttr('title');
    });
});


function GetDetailLevel(levelId) {
    $('#detailModal .modal-body').html('<span>Cargando...</span>')
    $.ajax({
        url: '/Level/GetDetail',
        type: 'GET',
        dataType: "html",
        data: { levelId: levelId },
        success: function (data) {
            $('#detailModal .modal-body').html(data);
        },
        error: function (e) {
            alert("Ha ocurrido un errror, intente nuevamente");
        }
    });
};

function DeleteLevel(id) {
    var name = $("a[data-id='" + id + "']").parent().parent().children('td:first').text();
    var messaje = '¿Está seguro que desea eliminar la Actividad <strong>' + name + '</strong>?';
    $("#modalConfirmationDelete").data('id', id)
    $('p.bodyModalConfirmationDelete').html(messaje);
};

function confirmationDelete() {
    var id = $("#modalConfirmationDelete").data('id');
    $.ajax({
        url: '/Level/Delete',
        type: 'POST',
        data: { id: id },
        success: function (data) {
            $('#modalConfirmationDelete').modal('hide')
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            if (data.resultado) {
                $("a[data-id='" + data.id + "']").closest('tr').remove();
            }
            else {
                alert("Se produjo un error al eliminar: " + data.clave + " Valor: " + data.valor);
            };
        },
        error: function (e) {
            alert("Se produjo un error al eliminar, intente nuevamente mas tarde");
        }
    });
};