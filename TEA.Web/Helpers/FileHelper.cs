﻿namespace TEA.Web.Helpers
{
    using System;
    using System.IO;
    using System.Text.RegularExpressions;

    public static class FileHelper
    {
        /// <summary>
        /// Renombra el nombre de un archivo para que no sea único
        /// </summary>
        /// <param name="name">Nombre para renombrar</param>
        /// <returns></returns>
        public static string RenameFileName(string name)
        {
            var fileName = Path.GetFileNameWithoutExtension(name);
            var rgx = new Regex(@"((?![A-Za-zÁÉÍÓÚáéíóúñÑ0-9\-_]).)");
            fileName = rgx.Replace(fileName, string.Empty);
            if (fileName.Length > 50)
            {
                fileName = fileName.Substring(0, 50);
            };

            // Renombro el documento para que no se generen nombres duplicados.
            fileName = string.Format("{0}-{1}{2}", fileName, DateTime.Now.ToString("ddMMyyyyhhmmss"), Path.GetExtension(name));
            return fileName;
        }
    }
}