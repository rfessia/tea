﻿namespace TEA.Web.Helpers
{
    using Newtonsoft.Json;
    using System.IO;

    /// <summary>
    /// Extensiones sobre object.
    /// </summary>
    public static class ObjectExtensions
    {
        #region Public Methods

        /// <summary>
        /// Retorna la representación JSON del objecto.
        /// </summary>
        /// <param name="obj">Objeto.</param>
        /// <returns>Representación JSON del objecto</returns>
        public static string ToJson(this object obj)
        {
            JsonSerializer js = JsonSerializer.Create(new JsonSerializerSettings() { DateTimeZoneHandling = DateTimeZoneHandling.Utc });
            js.Converters.Add(new DateTimeConverter());
            var jw = new StringWriter();
            js.Serialize(jw, obj);
            return jw.ToString();
        }

        #endregion
    }
}