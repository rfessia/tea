﻿namespace TEA.Web.Helpers
{
    using System.Web.Mvc;

    /// <summary>
    /// Helper que registra una variable en Javascript.
    /// </summary>
    public static class RegisterJavascriptVariableHelper
    {
        /// <summary>
        /// Retorna código Javascript para la inicialización de una variable.
        /// </summary>
        /// <param name="helper">Support rendering HTML.</param>
        /// <param name="name">.</param>
        /// <param name="value">.</param>
        /// <returns>Código Javascript.</returns>
        public static MvcHtmlString RegisterJavascriptVariable(this HtmlHelper helper, string name, object value)
        {
            return GetHtmlContent(name, value);
        }

        //TODO: Ver descripción del método.
        /// <summary>
        ///  Genera el código HTML para la visualización del control de avance.
        /// </summary>
        /// <param name="name">Porcentage de avance.</param>
        /// <param name="value">Especifica si deben presentar documentación.</param>
        /// <returns>Código HTML.</returns>
        private static MvcHtmlString GetHtmlContent(string name, object value)
        {
            var js = string.Format("{0} = {1};", name, value.ToJson());
            return new MvcHtmlString(js);
        }
    }
}