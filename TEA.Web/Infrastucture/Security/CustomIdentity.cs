﻿namespace TEA.Web.Infrastucture.Security
{
    using System;
    using System.Security.Principal;

    public class CustomIdentity : IIdentity
    {
        public CustomIdentity(string name, string extraData)
        {
            IsAuthenticated = true;
            Name = name;
            Id = Convert.ToInt32(extraData.Split('|')[0]);
            UserName = extraData.Split('|')[1];
            AuthenticationType = "Forms";
        }

        public string AuthenticationType { get; private set; }
        public bool IsAuthenticated { get; private set; }
        public string Name { get; private set; }
        public string UserName { get; private set; }
        public int Id { get; private set; }
    }
}