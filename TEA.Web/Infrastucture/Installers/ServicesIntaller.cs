﻿namespace TEA.Web.Infrastucture.Installers
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Contratos;
    using Servicios;

    public class ServicesIntaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            // Services.
            container.Register(Component.For(typeof(IServiceAnswer)).ImplementedBy(typeof(ServiceAnswer)).LifestylePerWebRequest());
            container.Register(Component.For(typeof(IServiceDiagnostic)).ImplementedBy(typeof(ServiceDiagnostic)).LifestylePerWebRequest());
            container.Register(Component.For(typeof(IServiceDiagnosticType)).ImplementedBy(typeof(ServiceDiagnosticType)).LifestylePerWebRequest());
            container.Register(Component.For(typeof(IServiceLevel)).ImplementedBy(typeof(ServiceLevel)).LifestylePerWebRequest());
            container.Register(Component.For(typeof(IServiceStage)).ImplementedBy(typeof(ServiceStage)).LifestylePerWebRequest());
            container.Register(Component.For(typeof(IServiceUser)).ImplementedBy(typeof(ServiceUser)).LifestylePerWebRequest());
            container.Register(Component.For(typeof(IServiceReport)).ImplementedBy(typeof(ServiceReport)).LifestylePerWebRequest());
            container.Register(Component.For(typeof(IServiceAuthentication)).ImplementedBy(typeof(ServiceAuthentication)).LifestylePerWebRequest());
            container.Register(Component.For(typeof(IServiceRole)).ImplementedBy(typeof(ServiceRole)).LifestylePerWebRequest());
            container.Register(Component.For(typeof(IServiceGender)).ImplementedBy(typeof(ServiceGender)).LifestylePerWebRequest());
        }
    }
}