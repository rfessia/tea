﻿
namespace TEA.Web.Infrastucture.Installers
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Controllers;
    using System.Web.Mvc;

    /// <summary>
    /// Registra todos los controllers en el container
    /// </summary>
    public class ControllersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            //// Registrar todos los controllers.
            container.Register(
                Classes
                    .FromAssemblyContaining<BaseController>()
                    .BasedOn(typeof(IController))
                    .Configure(
                        component =>
                            component.LifestyleTransient()
                    )
            );
        }
    }
}