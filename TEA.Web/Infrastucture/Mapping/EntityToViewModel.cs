﻿namespace TEA.Web.Infrastucture.Mapping
{
    using AutoMapper;
    using entity = TEA.Entidades.Negocio;
    using viewsModel = TEA.Web.ViewsModel;
    using System.Linq;
    using System.Collections.Generic;

    public class EntityToViewModel : Profile
    {
        public override string ProfileName
        {
            get
            {
                return "EntityToViewModel";
            }
        }

        protected override void Configure()
        {
            //User to CreateVM
            base.CreateMap<entity.User.User, viewsModel.User.IndexUserVM>()
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.FilterUserName, opt => opt.Ignore())
                .ForMember(dest => dest.Actions, opt => opt.Ignore());

            //User to CreateVM
            base.CreateMap<entity.User.User, viewsModel.User.CreateUserVM>();

            //User to DetailUserVM
            base.CreateMap<entity.User.User, viewsModel.User.DetailUserVM>()
                .ForMember(dest => dest.Birthdate, opt => opt.MapFrom(src => src.Birthdate.ToShortDateString()))
                .ForMember(dest => dest.DocumentNumber, opt => opt.MapFrom(src => src.DocumentNumber.ToString()))
                .ForMember(dest => dest.Gender, opt => opt.MapFrom(src => src.Gender != null ? src.Gender.Name : null))
                .ForMember(dest => dest.Roles, opt => opt.MapFrom(src => (src.Roles != null && src.Roles.Count() > 0) ? string.Join(", ", src.Roles.Select(x => x.Name).ToList()) : null));

            //User to EditUserVM
            base.CreateMap<entity.User.User, viewsModel.User.EditUserVM>()
                .ForMember(dest => dest.CancellationDate, opt => opt.Ignore())
                .ForMember(dest => dest.InputDate, opt => opt.Ignore())
                .ForMember(dest => dest.ModificationDate, opt => opt.Ignore())
                .ForMember(dest => dest.Roles, opt => opt.Ignore())
                .ForMember(dest => dest.SelectedRoles, opt => opt.MapFrom(src => (src.Roles != null && src.Roles.Count() > 0) ? src.Roles.Select(x => x.RoleId.ToString()).ToList() : new List<string>()));

            //User to ResetPasswordVM
            base.CreateMap<entity.User.User, viewsModel.User.ResetPasswordVM>()
                .ForMember(dest => dest.Password, opt => opt.Ignore())
                .ForMember(dest => dest.RePassword, opt => opt.Ignore());

            //Diagnostic to IndexDiagnosticVM
            base.CreateMap<entity.Diagnostic.Diagnostic, viewsModel.Diagnostic.IndexDiagnosticVM>()
                .ForMember(dest => dest.Actions, opt => opt.Ignore())
                .ForMember(dest => dest.DiagnosticTypes, opt => opt.Ignore());

            //Diagnostic to DetailDiagnosticVM
            base.CreateMap<entity.Diagnostic.Diagnostic, viewsModel.Diagnostic.DetailDiagnosticVM>()
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.User.Name));

            //Diagnostic to UpdateDiagnosticVM
            base.CreateMap<entity.Diagnostic.Diagnostic, viewsModel.Diagnostic.UpdateDiagnosticVM>()
                .ForMember(dest => dest.DiagnosticId, opt => opt.MapFrom(src => src.DiagnosticId))
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.User.UserId))
                .ForMember(dest => dest.DiagnosticTypes, opt => opt.Ignore());

            //Level to IndexLevelVM
            base.CreateMap<entity.Level.Level, viewsModel.Level.IndexLevelVM>()
                .ForMember(dest => dest.LevelId, opt => opt.MapFrom(src => src.LevelId))
                .ForMember(dest => dest.LevelName, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Order, opt => opt.MapFrom(src => src.Order.ToString()))
                .ForMember(dest => dest.Enabled, opt => opt.MapFrom(src => src.Enabled ? "SI" : "NO"))
                .ForMember(dest => dest.DiagnosticName, opt => opt.MapFrom(src => src.DiagnosticName))
                .ForMember(dest => dest.Actions, opt => opt.Ignore())
                .ForMember(dest => dest.FilterLevelName, opt => opt.Ignore())
                .ForMember(dest => dest.FilterDiagnosticName, opt => opt.Ignore());

            //Level to DetailLevelVM
            base.CreateMap<entity.Level.Level, viewsModel.Level.DetailLevelVM>()
                .ForMember(dest => dest.LevelName, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Order, opt => opt.MapFrom(src => src.Order.ToString()))
                .ForMember(dest => dest.Enabled, opt => opt.MapFrom(src => src.Enabled ? "SI" : "NO"))
                .ForMember(dest => dest.UserName, opt => opt.Ignore());

            //Level to EditLevelVM
            base.CreateMap<entity.Level.Level, viewsModel.Level.EditLevelVM>()
                .ForMember(dest => dest.LevelId, opt => opt.MapFrom(src => src.LevelId))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => string.Join(", ", src.UserName)))
                .ForMember(dest => dest.Diagnostics, opt => opt.Ignore());

            //Stage to IndexStageVM
            base.CreateMap<entity.Stage.Stage, viewsModel.Stage.IndexStageVM>()
                .ForMember(dest => dest.StageName, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.StageShared, opt => opt.MapFrom(src => src.Shared ? "SI" : "NO"))
                .ForMember(dest => dest.Actions, opt => opt.Ignore())
                .ForMember(dest => dest.FilterStageName, opt => opt.Ignore())
                .ForMember(dest => dest.FilterLevelName, opt => opt.Ignore())
                .ForMember(dest => dest.FilterDiagnosticName, opt => opt.Ignore())
                .ForMember(dest => dest.FilterUserId, opt => opt.Ignore());

            //Stage to DetailStageVM
            base.CreateMap<entity.Stage.Stage, viewsModel.Stage.DetailStageVM>()
                .ForMember(dest => dest.StageName, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.StageShared, opt => opt.MapFrom(src => src.Shared ? "SI" : "NO"));

            //Stage to EditStageVM
            base.CreateMap<entity.Stage.Stage, viewsModel.Stage.EditStageForUserVM>()
                .ForMember(dest => dest.FullNameUser, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.TreeStages, opt => opt.Ignore());

            //UserStageHistory to IndexUserStageHistoryVM
            base.CreateMap<entity.Report.UserStageHistory, viewsModel.Report.IndexUserStageHistoryVM>()
                .ForMember(dest => dest.NumberOfAttempts, opt => opt.MapFrom(src => src.NumberOfAttempts.ToString()))
                .ForMember(dest => dest.StageShared, opt => opt.MapFrom(src => src.StageShared ? "SI" : "NO"))
                .ForMember(dest => dest.FinishDate, opt => opt.MapFrom(src => src.FinishDate.ToString("dd/MM/yyyy HH:mm:ss")))
                .ForMember(dest => dest.FilterDateEnd, opt => opt.Ignore())
                .ForMember(dest => dest.FilterDateStart, opt => opt.Ignore())
                .ForMember(dest => dest.FilterStageName, opt => opt.Ignore())
                .ForMember(dest => dest.FilterLevelName, opt => opt.Ignore())
                .ForMember(dest => dest.FilterDiagnosticName, opt => opt.Ignore())
                .ForMember(dest => dest.FilterUserId, opt => opt.Ignore());
        }
    }
}