﻿namespace TEA.Web.Infrastucture.Mapping
{
    using AutoMapper;
    using System.Collections.Generic;
    using System.Linq;
    using entity = TEA.Entidades.Negocio;
    using viewsModel = TEA.Web.ViewsModel;

    public class ViewModelToEntity : Profile
    {
        public override string ProfileName
        {
            get
            {
                return "ViewModelToEntity";
            }
        }

        protected override void Configure()
        {
            //EditUserVM to UpdateDataUserRequest
            base.CreateMap<viewsModel.Account.RegisterVM, entity.User.RegisterUserRequest>()
                .ForMember(dest => dest.InputDate, opt => opt.Ignore())
                .ForMember(dest => dest.CancellationDate, opt => opt.Ignore())
                .ForMember(dest => dest.Token, opt => opt.Ignore());

            //EditUserVM to UpdateDataUserRequest
            base.CreateMap<viewsModel.User.CreateUserVM, entity.User.CreateUserRequest>();

            //EditUserVM to UpdateDataUserRequest
            base.CreateMap<viewsModel.User.EditUserVM, entity.User.UpdateDataUserRequest>();

            //CreateDiagnosticVM to CreateDiagnosticRequest
            base.CreateMap<viewsModel.Diagnostic.CreateDiagnosticVM, entity.Diagnostic.CreateDiagnosticRequest>()
                .ForMember(dest => dest.UserProfessionalId, opt => opt.MapFrom(src => src.UserProfessionalId))
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId));

            //UpdateDiagnosticVM to UpdateDiagnosticRequest
            base.CreateMap<viewsModel.Diagnostic.UpdateDiagnosticVM, entity.Diagnostic.UpdateDiagnosticRequest>()
                .ForMember(dest => dest.UserProfessionalId, opt => opt.Ignore())
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId));

            //CreateLevelVM to CreateLevelRequest
            base.CreateMap<viewsModel.Level.CreateLevelVM, entity.Level.CreateLevelRequest>();

            //EditLevelVM to UpdateLevelRequest
            base.CreateMap<viewsModel.Level.EditLevelVM, entity.Level.UpdateLevelRequest>()
                .ForMember(dest => dest.FinishDate, opt => opt.Ignore())
                .ForMember(dest => dest.CancellationDate, opt => opt.Ignore());

            //CreateStageVM to CreateStageRequest
            base.CreateMap<viewsModel.Stage.CreateSharedStageVM, entity.Stage.CreateSharedStageRequest>();

            //CreateStageForUserVM to CreateStageForUserRequest
            base.CreateMap<viewsModel.Stage.CreateStageForUserVM, entity.Stage.CreateStageForUserRequest>();

            //EditStageVM to UpdateStageRequest
            base.CreateMap<viewsModel.Stage.EditStageForUserVM, entity.Stage.UpdateStageRequest>()
                .ForMember(dest => dest.Order, opt => opt.MapFrom(src => (src.Order != null ? src.Order : 0)));

            //EditSharedStageVM to UpdateStageRequest
            base.CreateMap<viewsModel.Stage.EditSharedStageVM, entity.Stage.UpdateSharedStageRequest>();

            //EditStageForUserVM to UpdateStageRequest
            base.CreateMap<viewsModel.Stage.EditStageForUserVM, entity.Stage.UpdateStageForUserRequest>();

            //StagesToAssignVM to StagesToAssign
            base.CreateMap<viewsModel.Stage.StagesToAssignVM, entity.Stage.StagesToAssign>()
                .ForMember(dest => dest.StagesId, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.StagesSelected) ? new List<int>() : src.StagesSelected.Split(',').Select(int.Parse).ToList()));
        }
    }
}