﻿namespace TEA.Web
{
    using Infrastucture.IoC;
    using System;
    using System.Globalization;
    using System.Security.Principal;
    using System.Threading;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using TEA.Web.Infrastucture.Security;

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Configuración del contenedor de IOC.
            IoC.InitializeInstance();

            // Delegamos el manejo de instancias de los controllers al container de IoC.
            var controllerFactory = new WindsorControllerFactory(IoC.Instance);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);

            //var container = new WindsorContainer();
            //AutoMapperInstaller.RegisterProfiles(container);

            CultureInfo culture = new CultureInfo("es-AR");
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            ClientDataTypeModelValidatorProvider.ResourceClassKey = "Messages";
            DefaultModelBinder.ResourceClassKey = "Messages";
        }

        /// <summary>
        /// Application PostAuthenticateRequest
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_PostAuthenticateRequest(object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                CustomIdentity identity = new CustomIdentity(authTicket.Name, authTicket.UserData);

                string[] roles = authTicket.UserData.Split('|')[2].Split(',');

                GenericPrincipal newUser = new GenericPrincipal(identity, roles);
                Context.User = newUser;
            }
        }
    }
}
