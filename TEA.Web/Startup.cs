﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TEA.Web.Startup))]
namespace TEA.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
