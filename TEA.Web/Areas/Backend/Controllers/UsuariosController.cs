﻿namespace TEA.Web.Areas.Backend.Controllers
{
    using System.Web.Mvc;
    using System.Linq;
    using TEA.Entidades.Negocio.Usuario;
    using System;
    using TEA.Contratos;
    using TEA.Web.ViewsModel.Usuario;
    using PagedList;
    using Microsoft.AspNet.Identity;
    using System.Security.Claims;
    using TEA.Web.Infrastucture.Security;
    using TEA.Web.Filters;

    //[Authorize(Roles = UserRoles.Administrador)]
    //[CustomAuthenticationAttribute(Roles = UserRoles.Administrador)]
    [RouteArea("Backend")]
    public class UsuariosController : Controller
    {
        public IServicioUsuario ServicioUsuario;

        public UsuariosController(IServicioUsuario servicioUsuario)
        {
            this.ServicioUsuario = servicioUsuario;
        }

        //
        // GET: /Usuarios/
        public ActionResult Index(IndexVM model)
        {
            model.CurrentSort = model.SortOrder;
            model.NombreSortParm = model.SortOrder == "nombre" ? "nombre_desc" : "nombre";

            var usuarioActual = (CustomIdentity)User.Identity;
            if (usuarioActual != null)
            {
                model.IdUsuarioPadre = usuarioActual.Id;
            }

            var usuarios = this.ServicioUsuario.ObtenerUsuarios(new ObtenerUsuariosRequest() { Orden = model.SortOrder, IdUsuarioPadre = model.IdUsuarioPadre }).Usuarios;

            int pageSize = 10;
            int pageNumber = (model.Page ?? 1);

            model.Usuarios = usuarios.ToPagedList(pageNumber, pageSize);
            return View(model);
        }

        public ActionResult Create()
        {
            GuardarUsuarioRequest request = new GuardarUsuarioRequest();
            Usuario usuario = new Usuario()
            {
                Eliminado = false,
                FechaNacimiento = DateTime.Now,
            };
            request.Usuario = usuario;

            return View(request);
        }

        [HttpPost]
        public ActionResult Create(GuardarUsuarioRequest model)
        {
            var usuarioActual = (CustomIdentity)User.Identity;
            if (usuarioActual != null)
            {
                model.Usuario.IdUsuarioPadre = usuarioActual.Id;
            }

            if (ModelState.IsValid)
            {
                var user = this.ServicioUsuario.ObtenerUsuarioPorUserName(new ObtenerUsuarioRequest() { UserName = model.Usuario.UserName });

                if (user != null && user.Usuario != null)
                {
                    ModelState.AddModelError("Username", "El nombre de usuario ya existe");
                    return View(model);
                }

                this.ServicioUsuario.GuardarUsuario(model);

                return RedirectToAction("Index", "Home");
            }

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var usuario = this.ServicioUsuario.ObtenerUsuarioPorId(new ObtenerUsuarioRequest() { IdUsuario = id }).Usuario;
            
            GuardarUsuarioRequest request = new GuardarUsuarioRequest();
            request.Usuario = usuario;

            return View(request);
        }

        [HttpPost]
        public ActionResult Edit(GuardarUsuarioRequest model)
        {

            if (ModelState.IsValid)
            {
                var user = this.ServicioUsuario.ObtenerUsuarioPorUserName(new ObtenerUsuarioRequest() { UserName = model.Usuario.UserName });

                if (user != null && user.Usuario != null)
                {
                    ModelState.AddModelError("Username", "El nombre de usuario ya existe");
                    return View(model);
                }

                this.ServicioUsuario.GuardarUsuario(model);

                return RedirectToAction("Index", "Home");
            }

            return View(model);
        }
	}
}