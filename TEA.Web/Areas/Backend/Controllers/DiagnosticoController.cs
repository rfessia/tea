﻿namespace TEA.Web.Areas.Backend.Controllers
{
    using System.Web.Mvc;
    using TEA.Contratos;
    using TEA.Entidades.Negocio.Diagnostico;
    using TEA.Web.ViewsModel.Diagnostico;
    using PagedList;
    using System.Linq;
    using TEA.Entidades.Negocio.TipoDiagnostico;
    using TEA.Entidades.Negocio.Usuario;
    using System;
    using System.Collections.Generic;
    using TEA.Web.Infrastucture.Security;

    //[Authorize(Roles = UserRoles.Administrador)]
    public class DiagnosticoController : Controller
    {
        public IServicioDiagnostico ServicioDiagnostico;
        public IServicioTipoDiagnostico ServicioTipoDiagnostico;
        public IServicioUsuario ServicioUsuario;

        public DiagnosticoController(IServicioDiagnostico ServicioDiagnostico, IServicioTipoDiagnostico ServicioTipoDiagnostico, IServicioUsuario ServicioUsuario)
        {
            this.ServicioDiagnostico = ServicioDiagnostico;
            this.ServicioTipoDiagnostico = ServicioTipoDiagnostico;
            this.ServicioUsuario = ServicioUsuario;
        }

        public ActionResult Index(IndexVM model)
        {
            model.CurrentSort = model.SortOrder;
            model.NombreSortParm = model.SortOrder == "nombre" ? "nombre_desc" : "nombre";
            model.FilteredText = model.SearchThisText;

            var niveles = this.ServicioDiagnostico.ObtenerDiagnosticos(new ObtenerDiagnosticosRequest() { Orden = model.SortOrder, FilteredText = model.FilteredText  });

            int pageSize = 10;
            int pageNumber = (model.Page ?? 1);

            model.Diagnosticos = niveles.Diagnosticos.ToPagedList(pageNumber, pageSize);
            return View(model);
        }

        public ActionResult Create()
        {
            var model = new GuardarDiagnosticoRequest();
            var tipoDiagnosticos = ServicioTipoDiagnostico.ObtenerTipoDiagnosticos(new ObtenerTipoDiagnosticosRequest()).TipoDiagnostico;
            var usuarioActual = (CustomIdentity)User.Identity;

            Diagnostico diagnostico = new Diagnostico()
            {
                IdUsuarioProfesional = usuarioActual != null ? usuarioActual.Id : (int?)null,
            };
            model.Diagnostico = diagnostico;
            //model.IdUsuarios = usuarioActual != null ? this.ServicioUsuario.ObtenerIdUsuarios(new ObtenerUsuariosRequest() { IdUsuarioPadre = usuarioActual.Id }).IdUsuarios : null;
            ViewBag.ListaTipoDiagnosticos = new SelectList(tipoDiagnosticos, "Id", "Nombre");

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(GuardarDiagnosticoRequest request)
        {
            
            if (ModelState.IsValid)
            {
                var model = this.ServicioDiagnostico.GuardarDiagnostico(request);
                //Obtener de vuelta el id del diagnostico que fue guardado
                //Llamar al servicio GuardarUsuarioDiagnostico, pasandole el IdDiagnostico y los Ids de los Usuarios.
                //request.IdDiagnostico = request.Diagnostico.Id; //Algo así, ver como obtener el id de diagnostico guardado
                request.IdDiagnostico = model.IdDiagnostico;
                var usuariosDiagnosticos = this.ServicioDiagnostico.GuardarUsuarioDiagnostico(request);
                
                return RedirectToAction("Index");
            }
            var tipoDiagnosticos = ServicioTipoDiagnostico.ObtenerTipoDiagnosticos(new ObtenerTipoDiagnosticosRequest()).TipoDiagnostico;
            ViewBag.ListaTipoDiagnosticos = new SelectList(tipoDiagnosticos, "Id", "Nombre", request.Diagnostico.IdTipoDiagnostico);
            return View(request);
        }

        public ActionResult Edit(int id)
        {
            var model = new GuardarDiagnosticoRequest();
            var diagnostico = this.ServicioDiagnostico.ObtenerDiagnosticoPorId(new ObtenerDiagnosticoRequest() { IdDiagnostico = id }).Diagnostico;
            var tipoDiagnosticos = ServicioTipoDiagnostico.ObtenerTipoDiagnosticos(new ObtenerTipoDiagnosticosRequest()).TipoDiagnostico;

            model.Diagnostico = diagnostico;
            model.IdUsuarios = this.ServicioUsuario.ObtenerIdUsuariosPorDiagnostico(new ObtenerUsuariosRequest() { IdDiagnostico = id }).IdUsuarios;
            ViewBag.ListaTipoDiagnosticos = new SelectList(tipoDiagnosticos, "Id", "Nombre", model.Diagnostico.IdTipoDiagnostico);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(GuardarDiagnosticoRequest request)
        {
            if (ModelState.IsValid)
            {
                request.Diagnostico.Eliminado = false;
                var model = this.ServicioDiagnostico.GuardarDiagnostico(request);
                request.IdDiagnostico = model.IdDiagnostico;
                var usuariosDiagnosticos = this.ServicioDiagnostico.GuardarUsuarioDiagnostico(request);
                return RedirectToAction("Index");
            }

            var tipoDiagnosticos = ServicioTipoDiagnostico.ObtenerTipoDiagnosticos(new ObtenerTipoDiagnosticosRequest()).TipoDiagnostico;
            ViewBag.ListaTipoDiagnosticos = new SelectList(tipoDiagnosticos, "Id", "Nombre", request.Diagnostico.IdTipoDiagnostico);
            return View(request);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var eliminar = this.ServicioDiagnostico.EliminarDiagnostico(new EliminarDiagnosticoRequest() { IdDiagnostico = id });
            
            if (!eliminar.ValidarResponse.Valido)
            {
                var error = eliminar.ValidarResponse.Errores.First();
                return Json(new { resultado = false, clave = error.Key, valor = error.Value });
            }
            return Json(new { resultado = true, clave = "Clave exitosa", valor = "Valor exitoso" });
        }

        
        [HttpGet]
        public virtual JsonResult ObtenerUsuariosPorProfesional()
        {
            var usuarioActual = (CustomIdentity)User.Identity;
            var usuarios = usuarioActual != null ? this.ServicioUsuario.ObtenerUsuarios(new ObtenerUsuariosRequest() { IdUsuarioPadre = usuarioActual.Id }).Usuarios : null;
            var usuariosJSON = new List<Object>();

            foreach (var u in usuarios)
            {
                usuariosJSON.Add(new
                {
                    IdUsuario = u.Id,
                    NombreUsuario = u.Nombre,
                });
            }

            return Json(usuariosJSON.ToArray(), JsonRequestBehavior.AllowGet);
        }
	}
}