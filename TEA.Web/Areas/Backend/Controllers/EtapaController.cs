﻿namespace TEA.Web.Areas.Backend.Controllers
{
    using System.Web.Mvc;
    using System.Linq;
    using TEA.Contratos;
    using TEA.Web.ViewsModel.Etapa;
    using TEA.Entidades.Negocio.Etapa;
    using PagedList;
    using TEA.Entidades.Negocio.Nivel;
    using System.Web;
    using System;
    using System.IO;
    using TEA.Entidades.Negocio.Diagnostico;
    using TEA.Entidades.Negocio.Respuestas;
    using System.Collections.Generic;
    using TEA.Entidades.Negocio.TipoDiagnostico;

    public class EtapaController : Controller
    {
        public IServicioEtapa ServicioEtapa;
        public IServicioNivel ServicioNivel;
        public IServicioDiagnostico ServicioDiagnostico;
        public IServicioRespuesta ServicioRespuesta;
        public IServicioTipoDiagnostico ServicioTipoDiagnostico;

        public EtapaController(IServicioEtapa ServicioEtapa, IServicioNivel ServicioNivel, IServicioDiagnostico ServicioDiagnostico, IServicioRespuesta ServicioRespuesta, IServicioTipoDiagnostico ServicioTipoDiagnostico)
        {
            this.ServicioEtapa = ServicioEtapa;
            this.ServicioNivel = ServicioNivel;
            this.ServicioDiagnostico = ServicioDiagnostico;
            this.ServicioRespuesta = ServicioRespuesta;
            this.ServicioTipoDiagnostico = ServicioTipoDiagnostico;
        }
        //
        // GET: /Etapa/
        public ActionResult Index(IndexVM model)
        {
            model.CurrentSort = model.SortOrder;
            model.NombreSortParm = model.SortOrder == "nombre" ? "nombre_desc" : "nombre";
            model.OrdenSortParm = model.SortOrder == "orden" ? "orden_desc" : "orden";
            model.FiltroTexto = model.BuscarTexto;

            var etapas = this.ServicioEtapa.ObtenerEtapas(new ObtenerEtapasRequest() { Orden = model.SortOrder, FiltroTexto = model.FiltroTexto, FiltroIdTipoDiagnostico = model.FiltroIdTipoDiagnostico, FiltroIdDiagnostico = model.FiltroIdDiagnostico, FiltroIdNivel = model.FiltroIdNivel });

            int pageSize = 10;
            int pageNumber = (model.Page ?? 1);

            var tipoDiagnosticos = this.ServicioTipoDiagnostico.ObtenerTipoDiagnosticos(new ObtenerTipoDiagnosticosRequest()).TipoDiagnostico;
            ViewBag.ListaTipoDiagnosticos = new SelectList(tipoDiagnosticos, "Id", "Nombre", model.FiltroIdTipoDiagnostico);

            var diagnosticos = this.ServicioDiagnostico.ObtenerDiagnosticos(new ObtenerDiagnosticosRequest() { FiltroIdTipoDiagnostico = model.FiltroIdTipoDiagnostico }).Diagnosticos;
            ViewBag.ListaDiagnosticos = new SelectList(diagnosticos, "Id", "Nombre", model.FiltroIdDiagnostico);

            var niveles = this.ServicioNivel.ObtenerNiveles(new ObtenerNivelesRequest() { FiltroIdTipoDiagnostico = model.FiltroIdTipoDiagnostico, FiltroIdDiagnostico = model.FiltroIdDiagnostico }).Niveles;
            ViewBag.ListaNiveles = new SelectList(niveles, "Id", "Nombre", model.FiltroIdNivel);

            model.Etapas = etapas.Etapas.ToPagedList(pageNumber, pageSize);
            return View(model);
        }

        public ActionResult Create()
        {
            var diagnosticos = this.ServicioDiagnostico.ObtenerDiagnosticos(new ObtenerDiagnosticosRequest()).Diagnosticos;
            ViewBag.ListaDiagnosticos = new SelectList(diagnosticos, "Id", "Nombre");
            ViewBag.ListaNiveles = new SelectList(Enumerable.Empty<SelectListItem>());

            var model = new GuardarEtapaRequest();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Create(GuardarEtapaRequest request, HttpPostedFileBase[] files, bool Correcto, bool Fondo)
        public ActionResult Create(GuardarEtapaRequest request)
        {
            //HttpPostedFileBase myFile = Request.Files["files"];
            if (ModelState.IsValid)
            {
                var model = this.ServicioEtapa.GuardarEtapa(request);
                return RedirectToAction("Index");
            }

            var diagnosticos = this.ServicioDiagnostico.ObtenerDiagnosticos(new ObtenerDiagnosticosRequest()).Diagnosticos;
            var niveles = this.ServicioNivel.ObtenerNivelesPorDiagnostico(new ObtenerNivelesRequest() { IdDiagnostico = request.Etapa.IdDiagnostico }).Niveles;

            ViewBag.ListaDiagnosticos = new SelectList(diagnosticos, "Id", "Nombre", request.Etapa.IdDiagnostico);
            ViewBag.ListaNiveles = new SelectList(niveles, "Id", "Nombre", request.Etapa.IdNivel);
            return View(request);
        }

        public ActionResult Edit(int id)
        {
            var etapa = this.ServicioEtapa.ObtenerEtapaPorId(new ObtenerEtapaRequest() { IdEtapa = id });
            var model = new GuardarEtapaRequest() { Etapa = etapa.Etapa };
            var diagnosticos = this.ServicioDiagnostico.ObtenerDiagnosticos(new ObtenerDiagnosticosRequest()).Diagnosticos;
            var niveles = this.ServicioNivel.ObtenerNivelesPorDiagnostico(new ObtenerNivelesRequest() { IdDiagnostico = model.Etapa.IdDiagnostico }).Niveles;

            ViewBag.ListaDiagnosticos = new SelectList(diagnosticos, "Id", "Nombre", model.Etapa.IdDiagnostico);
            ViewBag.ListaNiveles = new SelectList(niveles, "Id", "Nombre", model.Etapa.IdNivel);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(GuardarEtapaRequest request)
        {
            if (ModelState.IsValid)
            {
                var model = this.ServicioEtapa.GuardarEtapa(request);
                return RedirectToAction("Index");
            }

            var diagnosticos = this.ServicioDiagnostico.ObtenerDiagnosticos(new ObtenerDiagnosticosRequest()).Diagnosticos;
            var niveles = this.ServicioNivel.ObtenerNivelesPorDiagnostico(new ObtenerNivelesRequest() { IdDiagnostico = request.Etapa.IdDiagnostico }).Niveles;

            ViewBag.ListaDiagnosticos = new SelectList(diagnosticos, "Id", "Nombre", request.Etapa.IdDiagnostico);
            ViewBag.ListaNiveles = new SelectList(niveles, "Id", "Nombre", request.Etapa.IdNivel);
            return View(request);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var eliminar = this.ServicioEtapa.EliminarEtapa(new EliminarEtapaRequest() { IdEtapa = id });

            if (!eliminar.ValidarResponse.Valido)
            {
                var error = eliminar.ValidarResponse.Errores.First();
                return Json(new { resultado = false, clave = error.Key, valor = error.Value });
            }
            return Json(new { resultado = true, clave = "Clave exitosa", valor = "Valor exitoso" });
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ObtenerNivelesPorDiagnostico(int idDiagnostico)
        {
            var niveles = this.ServicioNivel.ObtenerNivelesPorDiagnostico(new ObtenerNivelesRequest() { IdDiagnostico = idDiagnostico }).Niveles;
            return Json(niveles, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SubirArchivo(int id)
        {
            Respuesta respuesta = new Respuesta();
            var etapa = this.ServicioEtapa.ObtenerEtapaPorId(new ObtenerEtapaRequest() { IdEtapa = id }).Etapa;
            respuesta.IdEtapa = etapa.Id;

            var model = new GuardarRespuestaRequest()
            {
                Respuesta = respuesta,
                IdDiagnostico = etapa.IdDiagnostico,
                IdNivel = etapa.IdNivel,
                NombreDiagnostico = etapa.NombreDiagnostico,
                NombreEtapa = etapa.Nombre,
                NombreNivel = etapa.NombreNivel,
                TituloEtapa = etapa.Titulo,
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult SubirArchivo(GuardarRespuestaRequest request, HttpPostedFileBase MyFile)
        {
            HttpPostedFileBase myFile = Request.Files["file"];
            bool isUploaded = false;
            string message = "File upload failed";

            if (myFile != null && myFile.ContentLength != 0)
            {
                string pathForSaving = Server.MapPath("~/Etapas/Imagenes");
                //string pathForSaving = Server.MapPath("~/Etapa/SubirArchivo");

                var virtualPath = Url.Content(string.Format("{0}\\{1}", pathForSaving, myFile.FileName));

                if (this.CreateFolderIfNeeded(pathForSaving))
                {
                    try
                    {
                        myFile.SaveAs(Path.Combine(pathForSaving, myFile.FileName));
                        request.Respuesta.NombreImagen = myFile.FileName;

                        int idRespuesta = this.ServicioRespuesta.GuardarRespuesta(request).IdRespuesta;

                        isUploaded = true;
                        return Json(new
                        {
                            files = new[] { 
                                new {
                                    idRespuesta = idRespuesta,
                                    imgUrl = virtualPath,
                                    name = myFile.FileName,
                                    size = myFile.ContentLength, 
                                    url = "Delete", 
                                    thumbnailUrl = myFile.FileName,
                                    deleteUrl = @"\\Etapas\\Imagenes" + "\\" + myFile.FileName,
                                    deleteType = "DELETE" 
                                }
                            }
                        }, "text/html");
                    }
                    catch (Exception ex)
                    {
                        return Json(new
                        {
                            files = new[] { 
                                new {
                                    error = "Erorr al subir el archivo",
                                    name = myFile.FileName,
                                    size = myFile.ContentLength, 
                                    url = "", 
                                    thumbnailUrl = "", 
                                    deleteUrl = "", 
                                    deleteType = "DELETE" 
                                }
                            }
                        }, "text/html");
                    }
                }
            }
            else
            {
                return Json(new
                {
                    files = new[] { 
                                new {
                                    error = "Erorr al subir el archivo",
                                    name = myFile.FileName,
                                    size = myFile.ContentLength, 
                                    url = "", 
                                    thumbnailUrl = "", 
                                    deleteUrl = "", 
                                    deleteType = "DELETE" 
                                }
                            }
                }, "text/html");
            }
            return null;
        }

        [HttpPost]
        public JsonResult GuardarRespuesta(GuardarRespuestaRequest model)
        {
            //Guardar en la tabla de respuestas según el id del archivo, cual es correcta y cual es fondo.
            var respuestaCorrecta = this.ServicioRespuesta.GuardarFondoYRespuestaCorrecta(new GuardarRespuestaRequest() { IdRespCorrecta = model.IdRespCorrecta, IdFondo = model.IdFondo, IdEtapa = model.IdEtapa });

            //var respuestaCorrecta = this.ServicioRespuesta.GuardarRespuestaCorrecta(new GuardarRespuestaRequest() { IdRespCorrecta = model.IdRespCorrecta });
            if (!respuestaCorrecta.ValidarResponse.Valido)
            {
                var error = respuestaCorrecta.ValidarResponse.Errores.First();
                return Json(new { resultado = false, clave = error.Key, valor = error.Value });
            }
            //var fondo = this.ServicioRespuesta.GuardarFondo(new GuardarRespuestaRequest() { IdFondo = model.IdFondo });
            //if (!respuestaCorrecta.ValidarResponse.Valido)
            //{
            //    var error = respuestaCorrecta.ValidarResponse.Errores.First();
            //    return Json(new { resultado = false, clave = error.Key, valor = error.Value });
            //}
            return Json(new { resultado = true, clave = "Clave exitosa", valor = "Valor exitoso" });
        }

        [HttpGet]
        public JsonResult ObtenerArchivos(int idEtapa)
        {
            //Buscar todas las imágenes guardadas para inicializar al lista de respuestas.
            var etapas = this.ServicioRespuesta.ObtenerRespuestasPorEtapa(new ObtenerRespuestasRequest() { IdEtapa = idEtapa }).Respuestas;

            var listaImagenes = new List<ImagenCorrecta_js>();
            var files = new string[] { };
            
            foreach (var etapa in etapas)
            {
                var path = Server.MapPath("~/Etapas/Imagenes/" + etapa.NombreImagen);
                var sourceFileInfo = new System.IO.FileInfo(path);
                if (sourceFileInfo.Exists)
                {
                    var imagen = new ImagenCorrecta_js()
                        {
                            idRespuesta = etapa.Id.ToString(),
                            idRespuestaCorrecta = etapa.Correcto ? etapa.Id.ToString() : null,
                            idFondo = etapa.Fondo ? etapa.Id.ToString() : null,
                            imgUrl = path,
                            name = sourceFileInfo.Name,
                            size = sourceFileInfo.Length.ToString(),
                            url = "Delete",
                            thumbnailUrl = sourceFileInfo.Name,
                            deleteUrl = @"\\Etapas\\Imagenes" + "\\" + sourceFileInfo.Name,
                            deleteType = "DELETE"
                        };
                    listaImagenes.Add(imagen);
                }
                
            }

            return Json(new { files = listaImagenes }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ObtenerUltimoOrden(int idNivel)
        {
            var orden = this.ServicioEtapa.ObtenerUltimoOrden(new ObtenerEtapaRequest() { IdNivel = idNivel }).OrdenEtapa;
            return Json(new { orden = orden }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Creates the folder if needed.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        private bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    /*TODO: You must process this exception.*/
                    result = false;
                }
            }
            return result;
        }
    }
}