﻿namespace TEA.Web.Areas.Backend.Controllers
{
    using System;
    using System.Web.Mvc;
    using TEA.Contratos;
    using TEA.Entidades.Negocio.Nivel;
    using TEA.Web.ViewsModel.Nivel;
    using System.Linq;
    using PagedList;
    using TEA.Entidades.Negocio.Diagnostico;

    public class NivelController : Controller
    {
        public IServicioNivel ServicioNivel;
        public IServicioDiagnostico ServicioDiagnostico;

        public NivelController(IServicioNivel ServicioNivel, IServicioDiagnostico ServicioDiagnostico)
        {
            this.ServicioNivel = ServicioNivel;
            this.ServicioDiagnostico = ServicioDiagnostico;
        }

        public ActionResult Index(IndexVM model)
        {
            model.CurrentSort = model.SortOrder;
            model.NombreSortParm = model.SortOrder == "nombre" ? "nombre_desc" : "nombre";

            var niveles = this.ServicioNivel.ObtenerNiveles(new ObtenerNivelesRequest() { Orden = model.SortOrder });

            int pageSize = 10;
            int pageNumber = (model.Page ?? 1);

            model.Niveles = niveles.Niveles.ToPagedList(pageNumber, pageSize);
            return View(model);
        }

        public ActionResult Create()
        {
            var diagnosticos = ServicioDiagnostico.ObtenerDiagnosticos(new ObtenerDiagnosticosRequest()).Diagnosticos;
            ViewBag.ListaDiagnosticos = new SelectList(diagnosticos, "Id", "Nombre");
            var model = new GuardarNivelRequest();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(GuardarNivelRequest request)
        {
            if (ModelState.IsValid)
            {
                var model = this.ServicioNivel.GuardarNivel(request);
                return RedirectToAction("Index");
            }
            var diagnosticos = ServicioDiagnostico.ObtenerDiagnosticos(new ObtenerDiagnosticosRequest()).Diagnosticos;
            ViewBag.ListaDiagnosticos = new SelectList(diagnosticos, "Id", "Nombre", request.Nivel.IdDiagnostico);
            return View(request);
        }

        public ActionResult Edit(int id)
        {
            var nivel = this.ServicioNivel.ObtenerNivelPorId(new ObtenerNivelRequest() { IdNivel = id });
            var model = new GuardarNivelRequest()
            {
                Nivel = nivel.Nivel
            };

            var diagnosticos = ServicioDiagnostico.ObtenerDiagnosticos(new ObtenerDiagnosticosRequest()).Diagnosticos;
            ViewBag.ListaDiagnosticos = new SelectList(diagnosticos, "Id", "Nombre", model.Nivel.IdDiagnostico);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(GuardarNivelRequest request)
        {
            if (ModelState.IsValid)
            {
                request.Nivel.Eliminado = false;
                var model = this.ServicioNivel.GuardarNivel(request);
                return RedirectToAction("Index");
            }
            var diagnosticos = ServicioDiagnostico.ObtenerDiagnosticos(new ObtenerDiagnosticosRequest()).Diagnosticos;
            ViewBag.ListaDiagnosticos = new SelectList(diagnosticos, "Id", "Nombre", request.Nivel.IdDiagnostico);
            return View(request);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var eliminar = this.ServicioNivel.EliminarNivel(new EliminarNivelRequest() { IdNivel = id });

            if (!eliminar.ValidarResponse.Valido)
            {
                var error = eliminar.ValidarResponse.Errores.First();
                return Json(new { resultado = false, clave = error.Key, valor = error.Value });
            }
            return Json(new { resultado = true, clave = "Clave exitosa", valor = "Valor exitoso" });
        }
	}
}