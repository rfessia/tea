﻿namespace TEA.Web.ViewsModel.Etapa
{
    using PagedList;
    using System.ComponentModel.DataAnnotations;
    using TEA.Entidades.Negocio.Etapa;
    public class IndexVM
    {
        public IPagedList<Etapa> Etapas { get; set; }
        public int? Page { get; set; }

        // Campo por el que queres filtrar
        public string FiltroTexto { get; set; }

        [Display(Name = "Típo de Diagnóstico")]
        public int? FiltroIdTipoDiagnostico { get; set; }
        [Display(Name = "Diagnóstico")]
        public int? FiltroIdDiagnostico { get; set; }
        [Display(Name = "Nivel")]
        public int? FiltroIdNivel { get; set; }
        

        [Display(Name = "Etapa")]
        public string BuscarTexto { get; set; }

        //Ordenamiento
        public string SortOrder { get; set; }
        public string CurrentSort { get; set; }
        public string NombreSortParm { get; set; }
        public string OrdenSortParm { get; set; }
        public string RzTranspSortParm { get; set; }
        public string RzEmplSortParm { get; set; }

        public string OrdenNombre { get; set; }
        public string Orden { get; set; }
    }
}