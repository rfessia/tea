﻿namespace TEA.Web.ViewsModel.Diagnostico
{
    using PagedList;
    using System.ComponentModel.DataAnnotations;
    using TEA.Entidades.Negocio.Diagnostico;
    public class IndexVM
    {
        public IPagedList<Diagnostico> Diagnosticos { get; set; }
        public int? Page { get; set; }

        // Campo por el que queres filtrar
        public string FilteredText { get; set; }
        //Ordenamiento
        public string SortOrder { get; set; }
        public string CurrentSort { get; set; }
        public string NombreSortParm { get; set; }
        public string RzTranspSortParm { get; set; }
        public string RzEmplSortParm { get; set; }
        public string OrdenNombre { get; set; }
        public string Orden { get; set; }

        [Display(Name = "Diagnóstico")]
        public string SearchThisText { get; set; }
    }
}