﻿namespace TEA.Web.ViewsModel.Nivel
{
    using PagedList;
    using System.Collections.Generic;
    using TEA.Entidades.Negocio.Nivel;
    public class IndexVM
    {
        public IPagedList<Nivel> Niveles { get; set; }
        public int? Page { get; set; }

        //Ordenamiento
        public string SortOrder { get; set; }
        public string CurrentSort { get; set; }
        public string NombreSortParm { get; set; }
        public string RzTranspSortParm { get; set; }
        public string RzEmplSortParm { get; set; }

        //public List<Nivel> Niveles { get; set; }
        public string OrdenNombre { get; set; }
        public string Orden { get; set; }
    }
}