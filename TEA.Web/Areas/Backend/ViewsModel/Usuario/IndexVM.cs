﻿namespace TEA.Web.ViewsModel.Usuario
{
    using PagedList;
    using TEA.Entidades.Negocio.Usuario;
    public class IndexVM
    {
        public IPagedList<Usuario> Usuarios { get; set; }
        public int? Page { get; set; }
        public int? IdUsuarioPadre { get; set; }

        //Ordenamiento
        public string SortOrder { get; set; }
        public string CurrentSort { get; set; }
        public string NombreSortParm { get; set; }
        public string RzTranspSortParm { get; set; }
        public string RzEmplSortParm { get; set; }

        //public List<Nivel> Niveles { get; set; }
        public string OrdenNombre { get; set; }
        public string Orden { get; set; }
    }
}