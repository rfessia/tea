﻿using System.Web.Mvc;

namespace TEA.Web.Areas.Backend
{
    public class BackendAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Backend";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Backend_default",
                "Backend/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new[] { "TEA.Web.Areas.Backend.Controllers" }
            );

            //context.MapRoute("", "Account/Login", defaults: new { action = "Login", controller = "Account", area = "Backend", type = UrlParameter.Optional });
        }
    }
}