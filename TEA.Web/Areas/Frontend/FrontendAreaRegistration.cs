﻿using System.Web.Mvc;
using System.Web.Optimization;

namespace TEA.Web.Areas.Frontend
{
    public class FrontendAreaRegistration : AreaRegistration 
    {
        public override string AreaName { get { return "Frontend"; } }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Frontend_default",
                "Frontend/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}