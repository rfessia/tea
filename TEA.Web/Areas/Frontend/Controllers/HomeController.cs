﻿namespace TEA.Web.Areas.Frontend.Controllers
{
    using Newtonsoft.Json;
    using System.Web.Mvc;
    using TEA.Contratos;
    using TEA.Entidades.Negocio.Diagnostico;
    using TEA.Entidades.Negocio.Etapa;
    using TEA.Entidades.Negocio.Nivel;
    using TEA.Entidades.Negocio.Respuestas;
    using TEA.Entidades.Negocio.TipoDiagnostico;
    using TEA.Web.Areas.Frontend.ViewsModel.Home;
    using TEA.Web.Infrastucture.Security;

    public class HomeController : Controller
    {

        public IServicioDiagnostico ServicioDiagnostico;
        public IServicioNivel ServicioNivel;
        public IServicioEtapa ServicioEtapa;
        public IServicioRespuesta ServicioRespuesta;

        public HomeController(IServicioDiagnostico ServicioDiagnostico, IServicioNivel ServicioNivel, IServicioEtapa ServicioEtapa, IServicioRespuesta ServicioRespuesta)
        {
            this.ServicioDiagnostico = ServicioDiagnostico;
            this.ServicioNivel = ServicioNivel;
            this.ServicioEtapa = ServicioEtapa;
            this.ServicioRespuesta = ServicioRespuesta;
        }

        //
        // GET: /Frontend/Home/
        public ActionResult Diagnostico()
        {
            var usuarioActual = (CustomIdentity)User.Identity;
            if (usuarioActual != null)
            {
                ViewBag.Diagnosticos = this.ServicioDiagnostico.ObtenerDiagnosticosPorUsuario(new ObtenerDiagnosticosRequest() { IdUsuario = usuarioActual.Id }).Diagnosticos;
            }
            
            return View();
        }

        public ActionResult Nivel(int id)
        {
            var usuarioActual = (CustomIdentity)User.Identity;
            if (usuarioActual != null)
            {
                ViewBag.Niveles = this.ServicioNivel.ObtenerNivelesPorDiagnosticoYUsuario(new ObtenerNivelesRequest { IdUsuario = usuarioActual.Id, IdDiagnostico = id }).Niveles;
            }

            return View();
        }

        public ActionResult Etapa(int id)
        {
            EtapaVM model = new EtapaVM();
            var usuarioActual = (CustomIdentity)User.Identity;
            if (usuarioActual != null)
            {
                model.Etapa = this.ServicioEtapa.ObtenerEtapaActual(new ObtenerEtapaRequest() { IdUsuario = usuarioActual.Id, IdNivel = id }).Etapa;
                model.Respuestas = this.ServicioRespuesta.ObtenerRespuestasPorEtapa(new ObtenerRespuestasRequest() { IdEtapa = model.Etapa.Id }).Respuestas;
            }

            return View(model);
        }

        public JsonResult RespuestaCorrecta(int id)
        {
            var etapa = this.ServicioRespuesta.ObtenerRespuestaPorId(new ObtenerRespuestaRequest { IdRespuesta = id }).Respuesta;

            string result = JsonConvert.SerializeObject(etapa);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
	}
}