﻿namespace TEA.Web.Areas.Frontend.ViewsModel.Home
{
    using System.Collections.Generic;
    using TEA.Entidades.Negocio.Respuestas;
    using TEA.Entidades.Negocio.Etapa;

    public class EtapaVM
    {
        public List<Respuesta> Respuestas { get; set; }
        public Etapa Etapa { get; set; }
    }
}