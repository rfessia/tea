﻿$(function () {
    var nombreFondo = $("#Fondo").val();
    $(".fondo").css('background-image', 'url("/Etapas/Imagenes/' + nombreFondo + '")');
    //$("#launchPad").height($(window).height() - 20);
    //var dropSpace = $(window).width() - $("#launchPad").width();
    //$("#dropZone").width(dropSpace - 70);
    //$("#dropZone").height($("#launchPad").height());

    $(".card").draggable({
        containment: '.body-content',
        appendTo: "body",
        cursor: "move",
        helper: 'clone',
        revert: "invalid",
    });

    $("#launchPad").droppable({
        tolerance: "intersect",
        accept: ".card",
        activeClass: "ui-state-default",
        hoverClass: "ui-state-hover",
        drop: function (event, ui) {
            $("#launchPad").append($(ui.draggable));
        }
    });

    $(".stackDrop1").droppable({
        tolerance: "intersect",
        //accept: ".card",
        activeClass: "ui-state-default",
        hoverClass: "ui-state-hover",
        drop: function (event, ui) {

            var id = ui.draggable.attr("id")


            $.ajax({
                cache: false,
                type: "GET",
                url: "/FrontEnd/Home/RespuestaCorrecta",
                data: { "id": id },
                success: function (data) {
                    debugger;
                    var obj = jQuery.parseJSON(data);
                    if (obj.Correcto) {
                        $(".stackDrop1").append($(ui.draggable));
                        //alert('FELICITACIONES!!!!');
                        $("#loadingDiv").show();
                    }
                    else {
                        $(ui.draggable).draggable({ revert: "valid" });
                        //alert('Vuelva a intentarlo!');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert('Error.');
                }
            });


        }
    });
});