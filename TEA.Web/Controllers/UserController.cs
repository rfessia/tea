﻿namespace TEA.Web.Controllers
{
    using AutoMapper;
    using Entidades.Negocio.Gender;
    using Filters;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using TEA.Contratos;
    using TEA.Entidades.Negocio.User;
    using TEA.Web.ViewsModel.User;

    [CustomAuthenticationAttribute(Roles = UserRoles.Administrador)]
    public class UserController : BaseController
    {
        public IServiceUser ServiceUser;
        public IServiceGender ServiceGender;

        public UserController(IServiceUser serviceUser, IServiceGender serviceGender)
        {
            this.ServiceUser = serviceUser;
            this.ServiceGender = serviceGender;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var model = new IndexUserVM();
            return View(model);
        }

        /// <summary>
        /// Retorna la información para la tabla de usuarios
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoadDataTable()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            var sortColumn = Request.Form.GetValues($"columns[{ Request.Form.GetValues("order[0][column]").FirstOrDefault()}][data]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
            var sort = string.Join("_", sortColumn, sortColumnDir);
            int skip = start != null ? Convert.ToInt16(start) : 0;
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            var topUserId = UserCurrent.Id;
            var filterUserName = Request.Form.GetValues("columns[0][search][value]").FirstOrDefault();
            var filterFullName = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var filterDocumentNumber = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var getUsersResponse = this.ServiceUser.GetUsers(new GetUsersRequest() { Order = sort, Skip = skip, PageSize = pageSize, TopUserId = topUserId, FilterUserName = filterUserName, FilterFullName = filterFullName, FilterDocumentNumber = filterDocumentNumber });
            var users = Mapper.Map<List<IndexUserVM>>(getUsersResponse.Users.ToList());
            foreach (var user in users)
            {
                var id = user.UserId;
                var htmlActions = $"<a href='/User/Edit/{id}'><i class='btn btn-primary btn-md  glyphicon glyphicon-pencil'></i></a>";
                htmlActions += $"<a onclick='GetDetailUser({id})' data-toggle='modal' data-target='#detailModal'><i class='btn btn-info btn-md glyphicon glyphicon-search'></i></a>";
                htmlActions += $"<a href='/Stage/AssignStage?userId={id}'><i class='btn btn-success btn-md glyphicon glyphicon-indent-left'></i></a>";
                htmlActions += $"<a href='/User/ResetPassword/{id}'><i class='btn btn-warning btn-md  glyphicon glyphicon-lock'></i></a>";
                htmlActions += $"<a onclick='DeleteUser({id})' data-toggle='modal' data-target='#modalConfirmationDelete' data-id='{id}'><i class='btn btn-danger btn-md glyphicon glyphicon-trash delete'></i></a>";
                user.Actions = htmlActions;
            };

            return Json(new { draw = draw, recordsFiltered = getUsersResponse.RecordsTotal, recordsTotal = getUsersResponse.RecordsTotal, data = users }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = new CreateUserVM();
            model.Genders = GetGenderSelectListItem();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateUserVM model)
        {
            model.TopUserId = UserCurrent.Id;
            if (ModelState.IsValid)
            {
                var userExist = this.ServiceUser.GetUserByUserName(new GetUserRequest() { UserName = model.UserName, UserProfessionalId = UserCurrent.Id });
                if (userExist != null && userExist.User != null)
                {
                    ModelState.AddModelError("UserName", "El nombre de usuario ya existe");
                }
                else
                {
                    var createUserRequest = Mapper.Map<CreateUserRequest>(model);
                    this.ServiceUser.CreateUser(createUserRequest);
                    return RedirectToAction("Index", "User");
                };
            };

            model.Genders = GetGenderSelectListItem();
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var getUserByIdResponse = this.ServiceUser.GetUserById(new GetUserByIdRequest { UserId = id, UserProfessionalId = UserCurrent.Id }).User;
            var model = Mapper.Map<EditUserVM>(getUserByIdResponse);
            model.Genders = GetGenderSelectListItem();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditUserVM model)
        {
            if (ModelState.IsValid)
            {
                var userExist = this.ServiceUser.GetUserByUserNameAndDifferentId(new GetUserRequest() { UserId = model.Id, UserName = model.UserName, UserProfessionalId = UserCurrent.Id });
                if (userExist != null && userExist.User != null)
                {
                    ModelState.AddModelError("Username", "El nombre de usuario ya existe");
                }
                else
                {
                    var user = Mapper.Map<UpdateDataUserRequest>(model);
                    this.ServiceUser.UpdateDataUser(user);
                    return RedirectToAction("Index", "User");
                };
            };

            model.Genders = GetGenderSelectListItem();
            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteUser(int id)
        {
            var delete = this.ServiceUser.DeleteUser(new DeleteUserRequest() { UserId = id, UserProfessionalId = UserCurrent.Id });
            if (!delete.ValidarResponse.IsValid)
            {
                var error = delete.ValidarResponse.Errors.First();
                return Json(new { resultado = false, clave = error.Key, valor = error.Value });
            };

            return Json(new { resultado = true, id = id });
        }

        [HttpGet]
        public ActionResult GetDetailUser(int userId)
        {
            var getUserByIdResponse = this.ServiceUser.GetUserById(new GetUserByIdRequest() { UserId = userId, UserProfessionalId = UserCurrent.Id });
            var userDetailVm = Mapper.Map<DetailUserVM>(getUserByIdResponse.User);
            return PartialView("_Detail", userDetailVm);
        }

        [HttpGet]
        public ActionResult ResetPassword(int id)
        {
            var user = this.ServiceUser.GetUserById(new GetUserByIdRequest { UserId = id, UserProfessionalId = UserCurrent.Id }).User;
            var model = Mapper.Map<ResetPasswordVM>(user);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordVM model)
        {
            if (ModelState.IsValid)
            {
                this.ServiceUser.ResetPasswordUser(new ResetPasswordUserRequest() { UserId = model.UserId, UserProfessionalId = UserCurrent.Id, Password = model.Password });
                return RedirectToAction("Index", "User");
            };

            return View(model);
        }

        /// <summary>
        /// Recupera una lista de items de todos los géneros
        /// </summary>
        /// <returns></returns>
        private IList<SelectListItem> GetGenderSelectListItem()
        {
            var genders = this.ServiceGender.GetAllGender(new GetGenderRequest()).Genders;
            return genders.Select(x => new SelectListItem() { Text = x.Name, Value = x.GenderId.ToString() }).ToList();
        }

        /// <summary>
        /// Recupera los nombres de los usuarios de un diagnostico
        /// </summary>
        /// <param name="diagnosticId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetUsersByDiagnostic(int diagnosticId)
        {
            var user = this.ServiceUser.GetUsersByDiagnostic(new GetUsersByDiagnosticRequest() { DiagnosticId = diagnosticId }).User;
            var usersName = user.Name;
            return Json(new { users = usersName }, JsonRequestBehavior.AllowGet);
        }
        
        #region Metodos Privados

        private int? TryParseNullable(string val)
        {
            int outValue;
            return int.TryParse(val, out outValue) ? (int?)outValue : null;
        }

        #endregion
    }
}