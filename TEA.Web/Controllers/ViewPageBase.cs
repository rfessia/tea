﻿namespace TEA.Web.Controllers
{
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;

    /// <summary>
    /// Representa las propiedades y métodos que se necesitan para 
    /// presentar una vista que usa la sintaxis de ASP.NET Razor.
    /// </summary>
    public abstract class ViewPageBase : ViewPageBase<dynamic>
    {
    }

    /// <summary>
    /// Esta clase permite inyectar los scripts JS en las vistas sin necesidad de referenciarlos.
    /// Como convención los archivos de scripts deben esta ubicados en una misma estructura de
    /// carpetas que las vistas pero dentro del directorio ViewScripts.
    /// </summary>
    /// <typeparam name="T">Type del modelo.</typeparam>
    public abstract class ViewPageBase<T> : WebViewPage<T>
    {
        private const string ViewScriptsKey = "__ViewScripts";
        private bool _isPrincipalView;

        /// <summary>
        ///  Initializes the current page.
        /// </summary>
        protected override void InitializePage()
        {
            base.InitializePage();
            AddScriptForView(VirtualPath);
        }

        /// <summary>
        /// Runs the page hierarchy for the ASP.NET Razor execution pipeline.
        /// </summary>
        public override void ExecutePageHierarchy()
        {
            base.ExecutePageHierarchy();

            //This code will execute only for the principal view,
            //not for the layout view, start view, or partial views.
            if (!_isPrincipalView) return;

            Dictionary<string, string> dctScripts = GetScriptsDictionary();

            var sw = Output as StringWriter;
            if (sw == null) return;

            StringBuilder sb = sw.GetStringBuilder();

            // De momento no sería necesario esta inclusión (shenzenn).
            // add here the javascript files for layout and viewstart
            //AddScriptForView(Layout);
            //AddScriptForView("~/Views/_ViewStart.cshtml");            

            //Insert the scripts
            foreach (var pair in dctScripts)
            {
                sb.Insert(0, System.Web.Optimization.Scripts.Render(pair.Value));
            }
        }

        /// <summary>
        /// Agrega las referencias de los scripts JS a incluir en la vista.
        /// La referencia que guarda corresponde con la utilizada en el bundle.
        /// </summary>
        /// <param name="path">Path de la vista.</param>
        private void AddScriptForView(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return;
            }

            Dictionary<string, string> dctScripts = GetScriptsDictionary();

            if (!dctScripts.ContainsKey(path))
            {
                //Change the path (for ViewScripts) and extension file (for .js).
                path = Path.ChangeExtension(path.Replace("/Views/", "/ViewScripts/"), "js");

                //Create a Bundle path to the javascripts file.
                var bundlePath = path.Replace(Path.GetExtension(path), "");

                //Check if the file exists and add to dictionary.
                if (File.Exists(Server.MapPath(path)))
                {
                    if (bundlePath.EndsWith("/Index"))
                    {
                        bundlePath = bundlePath.Replace("/Index", "/_Index");
                    }

                    dctScripts[path] = bundlePath;
                }
            }
        }

        /// <summary>
        /// Obtiene el diccionario de scripts a referenciar en la vista.
        /// </summary>
        /// <returns>Diccionario con información de los scripts.</returns>
        private Dictionary<string, string> GetScriptsDictionary()
        {
            Dictionary<string, string> dctScripts = default(Dictionary<string, string>);

            // Obtienemos el objeto HttpContext asociado a la página. 
            HttpContextBase ctx = Context;

            // De la colección de pares de clave y valor que se utiliza para organizar y compartir 
            // los datos entre un módulo y un controlador durante una solicitud HTTP validamos si
            // existe la colección de scripts JS.
            if (!ctx.Items.Contains(ViewScriptsKey))
            {
                //If the dictionary is not created yet, then this view is the principal
                _isPrincipalView = true;
                dctScripts = new Dictionary<string, string>();
                ctx.Items[ViewScriptsKey] = dctScripts;
            }
            else
            {
                dctScripts = ctx.Items[ViewScriptsKey] as Dictionary<string, string>;
            }

            return dctScripts;
        }
    }
}