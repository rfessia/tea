﻿namespace TEA.Web.Controllers
{
    using Entidades.Negocio.User;
    using Filters;
    using System.Web.Mvc;
    using TEA.Contratos;

    [CustomAuthenticationAttribute(Roles = UserRoles.Administrador)]
    public class HomeController : BaseController
    {
        public IServiceDiagnosticType ServiceDiagnosticType;

        public HomeController(IServiceDiagnosticType ServiceDiagnosticType)
        {
            this.ServiceDiagnosticType = ServiceDiagnosticType;
        }

        public ActionResult Index()
        {
            
            return View();
        }
    }
}