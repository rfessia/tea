﻿namespace TEA.Web.Controllers
{
    using AutoMapper;
    using Entidades.Negocio.User;
    using Filters;
    using Helpers;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using TEA.Contratos;
    using TEA.Entidades.Negocio.Diagnostic;
    using TEA.Entidades.Negocio.Level;
    using TEA.Entidades.Negocio.Respuestas;
    using TEA.Entidades.Negocio.Stage;
    using TEA.Web.ViewsModel.Stage;

    [CustomAuthenticationAttribute(Roles = UserRoles.Administrador)]
    public class StageController : BaseController
    {
        public IServiceStage ServiceStage;
        public IServiceLevel ServiceLevel;
        public IServiceDiagnostic ServiceDiagnostic;
        public IServiceAnswer ServiceAnswer;
        public IServiceDiagnosticType ServiceDiagnosticType;
        public IServiceUser ServiceUser;

        public StageController(IServiceStage ServiceStage, IServiceLevel ServiceLevel, IServiceDiagnostic ServiceDiagnostic, IServiceAnswer ServiceAnswer, IServiceDiagnosticType ServiceDiagnosticType, IServiceUser ServiceUser)
        {
            this.ServiceStage = ServiceStage;
            this.ServiceLevel = ServiceLevel;
            this.ServiceDiagnostic = ServiceDiagnostic;
            this.ServiceAnswer = ServiceAnswer;
            this.ServiceDiagnosticType = ServiceDiagnosticType;
            this.ServiceUser = ServiceUser;
        }

        public ActionResult Index()
        {
            var model = new IndexStageVM();
            var usersItems = this.ServiceUser.GetUsers(new GetUsersRequest() { TopUserId = UserCurrent.Id }).Users;
            model.UsersItems = usersItems.Select(x => new SelectListItem() { Text = x.Name, Value = x.UserId.ToString() }).ToList();
            return View(model);
        }

        /// <summary>
        /// Retorna la información para la tabla de estapas
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoadDataTable()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            var sortColumn = Request.Form.GetValues($"columns[{ Request.Form.GetValues("order[0][column]").FirstOrDefault()}][data]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
            var sort = string.Join("_", sortColumn, sortColumnDir);
            int skip = start != null ? Convert.ToInt16(start) : 0;
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            var filterStageName = Request.Form.GetValues("columns[0][search][value]").FirstOrDefault();
            var filterLevelName = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var filterDiagnosticName = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var filterUserId = !string.IsNullOrWhiteSpace(Request.Form.GetValues("columns[3][search][value]").FirstOrDefault()) ? Convert.ToInt16(Request.Form.GetValues("columns[3][search][value]").FirstOrDefault()) : (int?)null;
            var filterSharedStage = !string.IsNullOrWhiteSpace(Request.Form.GetValues("columns[4][search][value]").FirstOrDefault()) ? Convert.ToBoolean(Request.Form.GetValues("columns[4][search][value]").FirstOrDefault()) : (bool?)null;
            var getStagesResponse = this.ServiceStage.GetStagesFiltered(new GetStagesRequest() { Order = sort, Skip = skip, PageSize = pageSize, FilterStageName = filterStageName, FilterLevelName = filterLevelName, FilterDiagnosticName = filterDiagnosticName, UserProfessionalId = UserCurrent.Id, FilterUserId = filterUserId, FilterShared = filterSharedStage });
            var stages = Mapper.Map<List<IndexStageVM>>(getStagesResponse.Stages.ToList());
            foreach (var stage in stages)
            {
                var id = stage.StageId;
                var htmlActions = $"<a href='/Stage/Edit/{id}'><i class='btn btn-primary btn-md  glyphicon glyphicon-pencil'></i></a>";
                htmlActions += $"<a onclick='GetDetailStage({id})' data-toggle='modal' data-target='#detailModal'><i class='btn btn-info btn-md glyphicon glyphicon-search'></i></a>";
                htmlActions += $"<a onclick='DeleteStage({id})' data-toggle='modal' data-target='#modalConfirmationDelete' data-id='{id}'><i class='btn btn-danger btn-md glyphicon glyphicon-trash delete'></i></a>";
                stage.Actions = htmlActions;
            };

            return Json(new { draw = draw, recordsFiltered = getStagesResponse.RecordsTotal, recordsTotal = getStagesResponse.RecordsTotal, data = stages }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SelectTypeStageToCreate()
        {
            var model = new SelectTypeStageToCreateVM();
            var usersItems = this.ServiceUser.GetUsers(new GetUsersRequest() { TopUserId = UserCurrent.Id }).Users;
            model.UsersItems = usersItems.Select(x => new SelectListItem() { Text = x.Name, Value = x.UserId.ToString() }).ToList();
            return View(model);
        }

        [HttpPost]
        public ActionResult SelectTypeStageToCreate(SelectTypeStageToCreateVM model)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("Create", new { newStageWhitUser = model.NewStageWhitUser.Value });
            }
            else
            {
                return View(model);
            };
        }

        public ActionResult Create(int? userId)
        {
            var model = new CreateStageForUserVM()
            {
                UserId = userId,
            };

            if (userId.HasValue)
            {
                var user = ServiceUser.GetUserById(new GetUserByIdRequest() { UserId = userId.Value }).User;
                model.FullNameUser = $"{user.Name}";
                model.TreeStages = ServiceStage.GetTreeStagesJson(new GetTreeStagesRequest() { UserId = userId.Value }).TreeStageJson;
            };

            return View(model);
        }

        /// <summary>
        /// Paso 1 para CREAR una ETAPA COMPARTIDA
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CreateSharedStage(CreateSharedStageVM model)
        {
            if (ModelState.IsValid)
            {
                var request = Mapper.Map<CreateSharedStageRequest>(model);
                request.UserProfessionalId = UserCurrent.Id;
                var createStageResponse = this.ServiceStage.CreateSharedStage(request);
                if (createStageResponse.ValidateResponse.IsValid)
                {
                    return Json(new
                    {
                        isValid = true,
                        stageId = createStageResponse.StageId.Value,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    foreach (var error in createStageResponse.ValidateResponse.Errors)
                    {
                        ModelState.AddModelError(error.Key, error.Value);
                    };
                };
            };

            var errorList = ModelState.ToDictionary(
                kvp => kvp.Key,
                kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
            );

            return Json(new
            {
                isValid = false,
                errorList = errorList
            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Paso 1 para CREAR una ETAPA DE UN USUARIO EN PARTICULAR
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CreateStageForUser(CreateStageForUserVM model)
        {
            if (ModelState.IsValid)
            {
                var request = Mapper.Map<CreateStageForUserRequest>(model);
                request.UserProfessionalId = UserCurrent.Id;
                var createStageResponse = this.ServiceStage.CreateStageForUser(request);
                if (createStageResponse.ValidateResponse.IsValid)
                {
                    return Json(new
                    {
                        isValid = true,
                        stageId = createStageResponse.StageId.Value,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    foreach (var error in createStageResponse.ValidateResponse.Errors)
                    {
                        ModelState.AddModelError(error.Key, error.Value);
                    };
                };
            };

            var errorList = ModelState.ToDictionary(
                kvp => kvp.Key,
                kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
            );

            return Json(new
            {
                isValid = false,
                errorList = errorList
            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Paso 2 para CREAR una ETAPA COMPARTIDA
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CreateSharedAnswer(CreateSharedAnswerVM model)
        {
            if (ModelState.IsValid)
            {
                var updateAnswerWithTempAnswerRequest = new UpdateAnswerWithTempAnswerRequest()
                {
                    StageId = model.StageId.Value
                };

                //true-> Respuesta Temporal
                //false -> Respuesta Original
                if (model.AnswerCorrectName.Contains("true"))
                {
                    updateAnswerWithTempAnswerRequest.AnswerCorrectIdTmpAnswer = Convert.ToInt16(model.AnswerCorrectName.Split('-')[0]);
                }
                else if (model.AnswerCorrectName.Contains("false"))
                {
                    updateAnswerWithTempAnswerRequest.AnswerCorrectIdAnswer = Convert.ToInt16(model.AnswerCorrectName.Split('-')[0]);
                };

                if (model.BackgroundName.Contains("true"))
                {
                    updateAnswerWithTempAnswerRequest.BackgroundIdTmpAnswer = Convert.ToInt16(model.BackgroundName.Split('-')[0]);
                }
                else if (model.BackgroundName.Contains("false"))
                {
                    updateAnswerWithTempAnswerRequest.BackgroundIdAnswer = Convert.ToInt16(model.BackgroundName.Split('-')[0]);
                };

                var validateResponse = this.ServiceStage.UpdateSharedStage(new UpdateSharedStageRequest() { Name = model.Name, StageId = model.StageId.Value, UserProfessionalId = UserCurrent.Id }).ValidateResponse;
                if (validateResponse.IsValid)
                {
                    var getAnswersResponse = this.ServiceAnswer.GetNewsTmpAnswersDeletedByStage(new GetTmpAnswersRequest() { StageId = model.StageId.Value });
                    this.ServiceAnswer.UpdateAnswerWithTempAnswer(updateAnswerWithTempAnswerRequest);
                    foreach (var imageName in getAnswersResponse.TmpAnswers.Select(x => x.ImageName).ToList())
                    {
                        var filePath = Server.MapPath($"~/Stages/{model.StageId}/Images/{imageName}");
                        if (System.IO.File.Exists(filePath))
                        {
                            System.IO.File.Delete(filePath);
                        };
                    };

                    return Json(new
                    {
                        isValid = true,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    foreach (var error in validateResponse.Errors)
                    {
                        ModelState.AddModelError(error.Key, error.Value);
                    };
                };
            };

            var errorList = ModelState.ToDictionary(
                kvp => kvp.Key,
                kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
            );

            return Json(new
            {
                isValid = false,
                errorList = errorList
            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Paso 2 para CREAR una ETAPA DE UN USUARIO EN PARTICULAR
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CreateAnswerForUser(CreateAnswerForUserVM model)
        {
            if (ModelState.IsValid)
            {
                var updateAnswerWithTempAnswerRequest = new UpdateAnswerWithTempAnswerRequest()
                {
                    StageId = model.StageId.Value
                };

                //true-> Respuesta Temporal
                //false -> Respuesta Original
                if (model.AnswerCorrectName.Contains("true"))
                {
                    updateAnswerWithTempAnswerRequest.AnswerCorrectIdTmpAnswer = Convert.ToInt16(model.AnswerCorrectName.Split('-')[0]);
                }
                else if (model.AnswerCorrectName.Contains("false"))
                {
                    updateAnswerWithTempAnswerRequest.AnswerCorrectIdAnswer = Convert.ToInt16(model.AnswerCorrectName.Split('-')[0]);
                };

                if (model.BackgroundName.Contains("true"))
                {
                    updateAnswerWithTempAnswerRequest.BackgroundIdTmpAnswer = Convert.ToInt16(model.BackgroundName.Split('-')[0]);
                }
                else if (model.BackgroundName.Contains("false"))
                {
                    updateAnswerWithTempAnswerRequest.BackgroundIdAnswer = Convert.ToInt16(model.BackgroundName.Split('-')[0]);
                };

                var validateResponse = this.ServiceStage.UpdateStageForUser(new UpdateStageForUserRequest() { Name = model.Name, StageId = model.StageId.Value, LevelId = model.LevelId.Value, Order = model.Order.Value, UserId = model.UserId.Value, UserProfessionalId = UserCurrent.Id }).ValidateResponse;
                if (validateResponse.IsValid)
                {
                    var getAnswersResponse = this.ServiceAnswer.GetNewsTmpAnswersDeletedByStage(new GetTmpAnswersRequest() { StageId = model.StageId.Value });
                    this.ServiceAnswer.UpdateAnswerWithTempAnswer(updateAnswerWithTempAnswerRequest);
                    foreach (var imageName in getAnswersResponse.TmpAnswers.Select(x => x.ImageName).ToList())
                    {
                        var filePath = Server.MapPath($"~/Stages/{model.StageId}/Images/{imageName}");
                        if (System.IO.File.Exists(filePath))
                        {
                            System.IO.File.Delete(filePath);
                        };
                    };

                    return Json(new
                    {
                        isValid = true,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    foreach (var error in validateResponse.Errors)
                    {
                        ModelState.AddModelError(error.Key, error.Value);
                    };
                };
            };

            var errorList = ModelState.ToDictionary(
                kvp => kvp.Key,
                kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
            );

            return Json(new
            {
                isValid = false,
                errorList = errorList
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var stage = this.ServiceStage.GetStageById(new GetStageRequest() { StageId = id, UserProfessionalId = UserCurrent.Id }).Stage;
            var model = Mapper.Map<EditStageForUserVM>(stage);
            if (!model.Shared)
            {
                model.TreeStages = ServiceStage.GetTreeStagesJson(new GetTreeStagesRequest() { UserId = model.UserId.Value }).TreeStageJson;
            };

            return View(model);
        }

        /// <summary>
        /// Paso 1 para EDITAR una ETAPA COMPARTIDA
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult EditSharedStage(EditSharedStageVM model)
        {
            if (ModelState.IsValid)
            {
                var request = Mapper.Map<UpdateSharedStageRequest>(model);
                request.ModificationDate = DateTime.Now;
                request.UserProfessionalId = UserCurrent.Id;
                var updateSharedStageResponse = this.ServiceStage.UpdateSharedStage(request);
                if (updateSharedStageResponse.ValidateResponse.IsValid)
                {
                    return Json(new
                    {
                        isValid = true,
                        stageId = updateSharedStageResponse.StageId,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    foreach (var error in updateSharedStageResponse.ValidateResponse.Errors)
                    {
                        ModelState.AddModelError(error.Key, error.Value);
                    };
                };
            };

            var errorList = ModelState.ToDictionary(
                kvp => kvp.Key,
                kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
            );

            return Json(new
            {
                isValid = false,
                errorList = errorList
            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Paso 1 para EDITAR una ETAPA DE UN USUARIO EN PARTICULAR
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult EditStageForUser(EditStageForUserVM model)
        {
            if (ModelState.IsValid)
            {
                var request = Mapper.Map<UpdateStageForUserRequest>(model);
                request.ModificationDate = DateTime.Now;
                request.UserProfessionalId = UserCurrent.Id;
                var updateStageResponse = this.ServiceStage.UpdateStageForUser(request);
                if (updateStageResponse.ValidateResponse.IsValid)
                {
                    return Json(new
                    {
                        isValid = true,
                        stageId = updateStageResponse.StageId,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    foreach (var error in updateStageResponse.ValidateResponse.Errors)
                    {
                        ModelState.AddModelError(error.Key, error.Value);
                    };
                };
            };

            var errorList = ModelState.ToDictionary(
                kvp => kvp.Key,
                kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
            );

            return Json(new
            {
                isValid = false,
                errorList = errorList
            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Paso 2 para EDITAR una ETAPA COMPARTIDA
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult EditSharedAnswer(EditSharedAnswerVM model)
        {
            if (ModelState.IsValid)
            {
                var updateAnswerWithTempAnswerRequest = new UpdateAnswerWithTempAnswerRequest()
                {
                    StageId = model.StageId.Value
                };

                //true-> Respuesta Temporal
                //false -> Respuesta Original
                if (model.AnswerCorrectName.Contains("true"))
                {
                    updateAnswerWithTempAnswerRequest.AnswerCorrectIdTmpAnswer = Convert.ToInt16(model.AnswerCorrectName.Split('-')[0]);
                }
                else if (model.AnswerCorrectName.Contains("false"))
                {
                    updateAnswerWithTempAnswerRequest.AnswerCorrectIdAnswer = Convert.ToInt16(model.AnswerCorrectName.Split('-')[0]);
                };

                if (model.BackgroundName.Contains("true"))
                {
                    updateAnswerWithTempAnswerRequest.BackgroundIdTmpAnswer = Convert.ToInt16(model.BackgroundName.Split('-')[0]);
                }
                else if (model.BackgroundName.Contains("false"))
                {
                    updateAnswerWithTempAnswerRequest.BackgroundIdAnswer = Convert.ToInt16(model.BackgroundName.Split('-')[0]);
                };

                var validateResponse = this.ServiceStage.UpdateSharedStage(new UpdateSharedStageRequest() { Name = model.Name, StageId = model.StageId.Value, ModificationDate = DateTime.Now, UserProfessionalId = UserCurrent.Id }).ValidateResponse;
                if (validateResponse.IsValid)
                {
                    var getAnswersResponse = this.ServiceAnswer.GetNewsTmpAnswersDeletedByStage(new GetTmpAnswersRequest() { StageId = model.StageId.Value });
                    this.ServiceAnswer.UpdateAnswerWithTempAnswer(updateAnswerWithTempAnswerRequest);
                    foreach (var imageName in getAnswersResponse.TmpAnswers.Select(x => x.ImageName).ToList())
                    {
                        var filePath = Server.MapPath($"~/Stages/{model.StageId}/Images/{imageName}");
                        if (System.IO.File.Exists(filePath))
                        {
                            System.IO.File.Delete(filePath);
                        };
                    };

                    return Json(new
                    {
                        isValid = true,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    foreach (var error in validateResponse.Errors)
                    {
                        ModelState.AddModelError(error.Key, error.Value);
                    };
                };
            };

            var errorList = ModelState.ToDictionary(
                kvp => kvp.Key,
                kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
            );

            return Json(new
            {
                isValid = false,
                errorList = errorList
            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Paso 2 para EDITAR una ETAPA DE UN USUARIO EN PARTICULAR
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult EditAnswerForUser(EditAnswerForUserVM model)
        {
            if (ModelState.IsValid)
            {
                var updateAnswerWithTempAnswerRequest = new UpdateAnswerWithTempAnswerRequest()
                {
                    StageId = model.StageId.Value
                };

                //true-> Respuesta Temporal
                //false -> Respuesta Original
                if (model.AnswerCorrectName.Contains("true"))
                {
                    updateAnswerWithTempAnswerRequest.AnswerCorrectIdTmpAnswer = Convert.ToInt16(model.AnswerCorrectName.Split('-')[0]);
                }
                else if (model.AnswerCorrectName.Contains("false"))
                {
                    updateAnswerWithTempAnswerRequest.AnswerCorrectIdAnswer = Convert.ToInt16(model.AnswerCorrectName.Split('-')[0]);
                };

                if (model.BackgroundName.Contains("true"))
                {
                    updateAnswerWithTempAnswerRequest.BackgroundIdTmpAnswer = Convert.ToInt16(model.BackgroundName.Split('-')[0]);
                }
                else if (model.BackgroundName.Contains("false"))
                {
                    updateAnswerWithTempAnswerRequest.BackgroundIdAnswer = Convert.ToInt16(model.BackgroundName.Split('-')[0]);
                };

                var validateResponse = this.ServiceStage.UpdateStageForUser(new UpdateStageForUserRequest() { Name = model.Name, StageId = model.StageId.Value, LevelId = model.LevelId.Value, Order = model.Order.Value, UserId = model.UserId.Value, ModificationDate = DateTime.Now, UserProfessionalId = UserCurrent.Id }).ValidateResponse;
                if (validateResponse.IsValid)
                {
                    var getAnswersResponse = this.ServiceAnswer.GetNewsTmpAnswersDeletedByStage(new GetTmpAnswersRequest() { StageId = model.StageId.Value });
                    this.ServiceAnswer.UpdateAnswerWithTempAnswer(updateAnswerWithTempAnswerRequest);
                    foreach (var imageName in getAnswersResponse.TmpAnswers.Select(x => x.ImageName).ToList())
                    {
                        var filePath = Server.MapPath($"~/Stages/{model.StageId}/Images/{imageName}");
                        if (System.IO.File.Exists(filePath))
                        {
                            System.IO.File.Delete(filePath);
                        };
                    };

                    return Json(new
                    {
                        isValid = true,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    foreach (var error in validateResponse.Errors)
                    {
                        ModelState.AddModelError(error.Key, error.Value);
                    };
                };
            };

            var errorList = ModelState.ToDictionary(
                kvp => kvp.Key,
                kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
            );

            return Json(new
            {
                isValid = false,
                errorList = errorList
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AssignStage(int userId)
        {
            var user = ServiceUser.GetUserById(new GetUserByIdRequest() { UserId = userId }).User;
            if (user == null)
            {
                throw new Exception();
            };
            var levels = ServiceLevel.GetLevelsByUser(new GetLevelsByUserRequest() { UserId = userId, FromApi = false }).Levels;
            var sharedStagesResponse = ServiceStage.GetSharedStagesForAssigned(new GetSharedStagesForAssignedRequest() { UserProfessionalId = UserCurrent.Id }).Stages;
            var stagesByUserResponse = ServiceStage.GetStagesFiltered(new GetStagesRequest() { FilterUserId = userId, UserProfessionalId = UserCurrent.Id }).Stages;
            var stageIdByUser = stagesByUserResponse.Select(x => x.StageId).ToList();
            sharedStagesResponse = sharedStagesResponse.Where(x => !stageIdByUser.Contains(x.StageId)).ToList();

            var sharedStages = string.Empty;
            var stagesByUserList = new List<string>();
            var stagesToAssign = new List<StagesToAssignVM>();
            foreach (var sharedStageResponse in sharedStagesResponse.OrderBy(x => x.StageId))
            {
                sharedStages += $"<li id='{sharedStageResponse.StageId}'>{sharedStageResponse.Name}</li>";
            };

            var count = 0;
            foreach (var level in levels.OrderBy(x => x.LevelId))
            {
                var stagesByUser = string.Empty;
                var stagesByLevel = stagesByUserResponse.Where(x => x.LevelId == level.LevelId).OrderBy(x => x.Order).ToList();
                foreach (var stageByLevel in stagesByLevel)
                {
                    if (stageByLevel.Shared)
                    {
                        stagesByUser += $"<li id='{stageByLevel.StageId}' >{stageByLevel.Name}<i class='js-remove fa fa-times'></i></li>";
                    }
                    else
                    {
                        stagesByUser += $"<li id='{stageByLevel.StageId}'>{stageByLevel.Name}</li>";
                    };
                };

                var stageToAssign = new StagesToAssignVM()
                {
                    LevelId = level.LevelId,
                    LevelName = level.LevelName,
                    StagesSelected = stagesByUser,
                };

                stagesToAssign.Add(stageToAssign);
                count++;
            };

            var model = new AssignStageVM()
            {
                FullName = user.Name,
                UserId = userId,
                StagesToAssign = stagesToAssign,
                SharedStages = sharedStages,
            };

            return View(model);
        }


        [HttpPost]
        public ActionResult AssignStage(AssignStageVM model)
        {
            if (ModelState.IsValid)
            {
                var stagesToAssign = Mapper.Map<List<StagesToAssign>>(model.StagesToAssign);
                var validateResponse = ServiceStage.UpdateAssignStageForUser(new UpdateAssignStageForUserRequest() { UserId = model.UserId, StagesToAssign = stagesToAssign }).ValidateResponse;
                if (validateResponse.IsValid)
                {
                    return Json(new
                    {
                        isValid = true
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    foreach (var error in validateResponse.Errors)
                    {
                        ModelState.AddModelError(error.Key, error.Value);
                    };
                };
            };

            var errorList = ModelState.ToDictionary(
                kvp => kvp.Key,
                kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
            );

            return Json(new
            {
                isValid = false,
                errorList = errorList
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BackToStage(int? stageId)
        {
            if (stageId.HasValue && stageId > 0)
            {
                var getAnswersResponse = this.ServiceAnswer.GetNewsTmpAnswersByStage(new GetTmpAnswersRequest() { StageId = stageId.Value, IsCancel = true });
                this.ServiceAnswer.DeleteTmpAnswersByStage(new DeleteTmpAnswersRequest() { StageId = stageId.Value });
                foreach (var imageName in getAnswersResponse.TmpAnswers.Select(x => x.ImageName).ToList())
                {
                    var filePath = Server.MapPath($"~/Stages/{stageId}/Images/{imageName}");
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    };
                };
            };

            return RedirectToAction("Index", "Stage");
        }

        public ActionResult DeleteStageAndBackToStage(int? stageId, bool create)
        {
            if (stageId.HasValue && stageId > 0)
            {
                if (create)
                {
                    this.ServiceStage.DeleteStage(new DeleteStageRequest() { StageId = stageId.Value, UserProfessionalId = UserCurrent.Id });
                };

                var getAnswersResponse = this.ServiceAnswer.GetNewsTmpAnswersByStage(new GetTmpAnswersRequest() { StageId = stageId.Value, IsCancel = true });
                this.ServiceAnswer.DeleteTmpAnswersByStage(new DeleteTmpAnswersRequest() { StageId = stageId.Value });
                foreach (var imageName in getAnswersResponse.TmpAnswers.Select(x => x.ImageName).ToList())
                {
                    var filePath = Server.MapPath($"~/Stages/{stageId}/Images/{imageName}");
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    };
                };
            };

            return RedirectToAction("Index", "Stage");
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var delete = this.ServiceStage.DeleteStage(new DeleteStageRequest() { StageId = id, UserProfessionalId = UserCurrent.Id });
            if (!delete.ValidarResponse.IsValid)
            {
                var error = delete.ValidarResponse.Errors.First();
                return Json(new { resultado = false, clave = error.Key, valor = error.Value });
            }

            return Json(new { resultado = true, id = id });
        }

        [HttpPost]
        public ActionResult UploadTmpFile(SaveAnswerRequest request, HttpPostedFileBase MyFile)
        {
            HttpFileCollectionBase files = Request.Files;
            HttpPostedFileBase myFile = Request.Files["file"];
            bool isUploaded = false;
            if (myFile != null && myFile.ContentLength != 0)
            {
                var filename = FileHelper.RenameFileName(myFile.FileName);
                string pathForSaving = Server.MapPath($"~/Stages/{request.StageId}/Images");
                var virtualPath = Url.Content(string.Format("{0}\\{1}", pathForSaving, filename));
                if (this.CreateFolderIfNeeded(pathForSaving))
                {
                    try
                    {
                        var tmpAnswer = new TmpAnswer()
                        {
                            ImageOriginalName = myFile.FileName,
                            ImageName = filename,
                            StageId = request.StageId,
                        };

                        var response = this.ServiceAnswer.CreateTmpAnswer(new CreateTmpAnswerRequest() { TmpAnswer = tmpAnswer });
                        if (response.IsValid)
                        {
                            myFile.SaveAs(Path.Combine(pathForSaving, filename));
                            var tmpAnswerId = response.TmpAnswerId;
                            isUploaded = true;
                            return Json(new
                            {
                                files = new[] {
                                    new {
                                        isValid = true,
                                        answerId = tmpAnswerId,
                                        stageId = request.StageId,
                                        isTmpAnswer = true,
                                        imgUrl = filename,
                                        name = myFile.FileName,
                                        size = myFile.ContentLength,
                                        url = "Delete",
                                        thumbnailUrl = filename,
                                        deleteUrl = $"/Stage/DeleteTmpFile?tmpAnswerId={tmpAnswerId}",
                                        deleteType = "GET"
                                    }
                                }
                            }, "text/html");
                        }
                        else
                        {
                            return Json(new
                            {
                                files = new[] {
                                    new {
                                        isValid = false,
                                        error = response.ErrorMessage,
                                        name = myFile.FileName,
                                        size = myFile.ContentLength,
                                        url = "",
                                        thumbnailUrl = "",
                                        deleteUrl = "",
                                        deleteType = "DELETE"
                                    }
                                }
                            }, "text/html");
                        };
                    }
                    catch (Exception ex)
                    {
                        return Json(new
                        {
                            files = new[] {
                                new {
                                    error = "Erorr al subir el archivo",
                                    name = myFile.FileName,
                                    size = myFile.ContentLength,
                                    url = "",
                                    thumbnailUrl = "",
                                    deleteUrl = "",
                                    deleteType = "DELETE"
                                }
                            }
                        }, "text/html");
                    };
                };
            }
            else
            {
                return Json(new
                {
                    files = new[] {
                                new {
                                    error = "Erorr al subir el archivo",
                                    name = myFile.FileName,
                                    size = myFile.ContentLength,
                                    url = "",
                                    thumbnailUrl = "",
                                    deleteUrl = "",
                                    deleteType = "DELETE"
                                }
                            }
                }, "text/html");
            };

            return null;
        }

        [HttpPost]
        public JsonResult SaveAnswer(SaveAnswerRequest model)
        {
            //Guardar en la tabla de respuestas según el id del archivo, cual es correcta y cual es fondo.
            var respuestaCorrecta = this.ServiceAnswer.SaveBackgroundAndCorrectAnswer(new SaveAnswerRequest() { AnswerCorrectId = model.AnswerCorrectId, BackgroundId = model.BackgroundId, StageId = model.StageId });
            //var respuestaCorrecta = this.ServicioRespuesta.GuardarRespuestaCorrecta(new GuardarRespuestaRequest() { IdRespCorrecta = model.IdRespCorrecta });
            if (!respuestaCorrecta.ValidarResponse.IsValid)
            {
                //Actualiza las respuestas con las respuestas temporales
                //this.ServiceAnswer.UpdateAnswerWithTempAnswer(new UpdateAnswerWithTempAnswerRequest() { StageId = model.StageId });
            }
            else
            {
                var error = respuestaCorrecta.ValidarResponse.Errors.First();
                return Json(new { resultado = false, clave = error.Key, valor = error.Value });
            };

            //var fondo = this.ServicioRespuesta.GuardarFondo(new GuardarRespuestaRequest() { IdFondo = model.IdFondo });
            //if (!respuestaCorrecta.ValidarResponse.Valido)
            //{
            //    var error = respuestaCorrecta.ValidarResponse.Errores.First();
            //    return Json(new { resultado = false, clave = error.Key, valor = error.Value });
            //}

            return Json(new { resultado = true, clave = "Clave exitosa", valor = "Valor exitoso" });
        }

        [HttpGet]
        public JsonResult GetFiles(int stageId)
        {
            //Buscar todas las imágenes guardadas para inicializar al lista de respuestas.
            var answersGeneric = this.ServiceAnswer.GetAnswersAndTmpAnswersByStage(new GetAnswersAndTmpAnswersRequest() { StageId = stageId }).AnswersGeneric;
            var imagesList = new List<ImagenCorrecta_js>();
            var files = new string[] { };
            foreach (var answerGeneric in answersGeneric)
            {
                var path = Server.MapPath($"~/Stages/{stageId}/Images/{answerGeneric.ImageName}");
                var sourceFileInfo = new FileInfo(path);
                var imagen = new ImagenCorrecta_js()
                {
                    isTmpAnswer = answerGeneric.IsTmpAnswer,
                    stageId = answerGeneric.StageId.ToString(),
                    answerId = answerGeneric.AnswerId.HasValue ? answerGeneric.AnswerId.ToString() : null,
                    answerIdCorrect = answerGeneric.Correct ? answerGeneric.AnswerId.ToString() : null,
                    idFondo = answerGeneric.Background ? answerGeneric.AnswerId.ToString() : null,
                    url = "Delete",
                    deleteType = "GET"
                };

                if (sourceFileInfo.Exists)
                {
                    imagen.imgUrl = answerGeneric.ImageName;
                    imagen.name = answerGeneric.ImageOriginalName;
                    imagen.size = sourceFileInfo.Length;
                    imagen.thumbnailUrl = answerGeneric.ImageName;
                    if (answerGeneric.IsTmpAnswer)
                    {
                        imagen.deleteUrl = $"/Stage/DeleteTmpFile?tmpAnswerId={answerGeneric.AnswerId}";
                    }
                    else
                    {
                        imagen.deleteUrl = $"/Stage/CreateTmpFileToDelete?answerId={answerGeneric.AnswerId}";
                    };
                }
                else
                {
                    var unavailablePath = Server.MapPath($"~/Stages/Images/no_disponible.png");
                    var imageUnavailable = new FileInfo(unavailablePath);
                    imagen.imgUrl = answerGeneric.ImageName;
                    imagen.name = answerGeneric.ImageOriginalName;
                    imagen.size = imageUnavailable.Length;
                    imagen.thumbnailUrl = answerGeneric.ImageName;
                    imagen.deleteUrl = $"/Stage/DeleteFile?answerId={answerGeneric.AnswerId}";
                };

                imagesList.Add(imagen);
            };

            return Json(new { files = imagesList }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DeleteFile(int answerId)
        {
            var response = this.ServiceAnswer.DeleteAnswer(new DeleteAnswerRequest() { AnswerId = answerId });
            if (response.ValidateResponse.IsValid)
            {
                ///Realizar eliminación de respuesta en db
                ///ver si se tiene que eliminar físicamente las imagenes o solo realizar un cancellation date en db
                var filePath = Server.MapPath($"~/Stages/{response.StageId}/Images/{response.ImageName}");
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                };

                return Json(new { isValid = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { isValid = false, error = response.ValidateResponse.Errors.First().Value }, JsonRequestBehavior.AllowGet);
            };
        }

        [HttpGet]
        public JsonResult DeleteTmpFile(int tmpAnswerId)
        {
            var response = this.ServiceAnswer.DeleteTmpAnswer(new DeleteTmpAnswerRequest() { TmpAnswerId = tmpAnswerId });
            if (response.ValidateResponse.IsValid)
            {
                return Json(new { isValid = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { isValid = false, error = response.ValidateResponse.Errors.First().Value }, JsonRequestBehavior.AllowGet);
            };
        }

        [HttpGet]
        public JsonResult CreateTmpFileToDelete(int answerId)
        {
            var response = this.ServiceAnswer.CreateTmpAnswerToDelete(new CreateTmpAnswerToDeleteRequest() { AnswerId = answerId });
            if (response.ValidateResponse.IsValid)
            {
                return Json(new { isValid = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { isValid = false, error = response.ValidateResponse.Errors.First().Value }, JsonRequestBehavior.AllowGet);
            };
        }

        [HttpGet]
        public JsonResult GetLastOrder(int levelId)
        {
            var order = this.ServiceStage.GetLastOrderOfStages(new GetStageRequest() { LevelId = levelId }).OrderStage;
            return Json(new { order = order }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Creates the folder if needed.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        private bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    /*TODO: You must process this exception.*/
                    result = false;
                }
            }

            return result;
        }

        [HttpGet]
        public ActionResult GetDetail(int stageId)
        {
            var stage = this.ServiceStage.GetStageById(new GetStageRequest() { StageId = stageId, UserProfessionalId = UserCurrent.Id }).Stage;
            var model = Mapper.Map<DetailStageVM>(stage);
            return PartialView("_Detail", model);
        }

        #region Metodos privados

        /// <summary>
        /// Recupera una lista de items de todos los diagnosticos de un usuario
        /// </summary>
        /// <returns></returns>
        private IList<SelectListItem> GetDiagnosticSelectListItem(int userProfessionalId)
        {
            var diagnostics = this.ServiceDiagnostic.GetDiagnostics(new GetDiagnosticsRequest() { UserProfessionalId = userProfessionalId }).Diagnostics;
            return diagnostics.Select(x => new SelectListItem() { Text = x.Name, Value = x.DiagnosticId.ToString() }).ToList();
        }

        /// <summary>
        /// Recupera una lista de items de todos las etapas de un nivel
        /// </summary>
        /// <returns></returns>
        private IList<SelectListItem> GetLevelsSelectListItem(int diagnosticId)
        {
            var levels = this.ServiceLevel.GetLevelsByDiagnostic(new GetLevelsRequest() { DiagnosticId = diagnosticId, UserProfessionalId = UserCurrent.Id }).Levels;
            return levels.Select(x => new SelectListItem() { Text = x.Name, Value = x.LevelId.ToString() }).ToList();
        }

        #endregion

        #region Etapas por usuarios

        #endregion
    }
}