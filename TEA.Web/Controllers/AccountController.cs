﻿namespace TEA.Web.Controllers
{
    using AutoMapper;
    using Entidades.Negocio.Gender;
    using Microsoft.AspNet.Identity;
    using Microsoft.Owin.Security;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Security;
    using TEA.Contratos;
    using TEA.Entidades.Negocio.User;
    using ViewsModel.Account;
    using static Entidades.Base.Enumerations;

    public class AccountController : BaseController
    {
        public IServiceUser ServiceUser { get; set; }
        public IServiceAuthentication ServiceAuthentication { get; set; }
        public IServiceGender ServiceGender { get; set; }

        public AccountController(IServiceUser serviceUser, IServiceAuthentication serviceAuthentication, IServiceGender ServiceGender)
        {
            this.ServiceUser = serviceUser;
            this.ServiceAuthentication = serviceAuthentication;
            this.ServiceGender = ServiceGender;
        }

        //
        // GET: /Account/Login
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginVM model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var user = ServiceUser.GetLoginUser(new GetLoginUserRequest() { UserName = model.UserName, Password = model.Password }).User;
                if (user != null)
                {
                    var isAdmin = user.Roles.Any(x => x.RoleId == (int)RoleEnum.ADMINISTRADOR);
                    if (isAdmin)
                    {
                        //Let us now set the authentication cookie so that we can use that later.
                        ServiceAuthentication.Login(model.UserName, model.RememberMe);
                        var encryptedTicket = ServiceAuthentication.GetEncrypteTicket(new AutenticacionUsuarioRequest() { Usuario = user, RememberMe = model.RememberMe }).Encriptado;
                        // Adiciono la cookie al request para guardarla.
                        var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket)
                        {
                            HttpOnly = true
                        };

                        Response.Cookies.Add(cookie);
                        return RedirectToLocal(returnUrl);
                    };
                }
                else
                {
                    ModelState.AddModelError("MessageError", "El Nombre de Usuario o Contraseña son incorrectos.");
                };
            };

            return View("Login", model);
        }

        /// <summary>
        /// Vista alternativa de login.
        /// </summary>
        /// <param name="returnUrl">returnUrl.</param>
        /// <returns>Login view.</returns>
        [AllowAnonymous]
        public ActionResult Logon(string returnUrl)
        {
            return RedirectToAction("Login", new { returnUrl = returnUrl });
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            var model = new RegisterVM();
            model.Genders = GetGenderSelectListItem();
            return View(model);
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterVM model)
        {
            if (ModelState.IsValid)
            {
                var userExist = this.ServiceUser.GetUserByUserName(new GetUserRequest() { UserName = model.UserName });
                if (userExist != null && userExist.User != null)
                {
                    ModelState.AddModelError("UserName", "El nombre de usuario ya existe");
                }
                else
                {
                    var token = Guid.NewGuid();
                    var today = DateTime.Now;
                    var registerUserRequest = Mapper.Map<RegisterUserRequest>(model);
                    registerUserRequest.InputDate = today;
                    registerUserRequest.CancellationDate = today;
                    registerUserRequest.Token = token.ToString();
                    var response = this.ServiceUser.RegisterUser(registerUserRequest);
                    if (response.ValidarResponse.IsValid)
                    {
                        SendActivationEmail(model, response.UserId, token.ToString());
                        var title = "Activación de Usuario";
                        var alert = "info";
                        var alertTitle = "Revise su Correo Electrónico";
                        var message = $"Hemos enviado un correo electrónico para activar su cuenta a: {model.Email}";
                        return RedirectToAction("MessageActivationUser", new { title = title, alert = alert, alertTitle = alertTitle, message = message });
                    }
                    else
                    {
                        var error = response.ValidarResponse.Errors.First();
                        ModelState.AddModelError(error.Key, error.Value);
                    };
                };
            };

            model.Genders = GetGenderSelectListItem();
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ActivationUser(int id, string token)
        {
            ViewBag.title = "Activación de Usuario";
            ViewBag.alert = "danger";
            ViewBag.alertTitle = "Código no válido";
            ViewBag.message = "No se ha podido activar el usuario. El código no es válido";
            var response = this.ServiceUser.ActivationUser(new ActivationUserRequest() { UserId = id, Token = token });
            if (response.ValidarResponse.IsValid)
            {
                ViewBag.alert = "success";
                ViewBag.alertTitle = "¡Bienvenido!";
                ViewBag.message = "El usuario ha sido activado, para ingresar inicie sesión <a href='/Account/Login'>AQUI</a>";
            }
            else
            {
                ///Mostrar error
                var error = response.ValidarResponse.Errors.First();
                ViewBag.message = error.Value;
            };

            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult MessageActivationUser(string title, string alert, string alertTitle, string message)
        {
            ViewBag.title = title;
            ViewBag.alert = alert;
            ViewBag.alertTitle = alertTitle;
            ViewBag.message = message;
            return View("~/Views/Account/ActivationUser.cshtml");
        }

        [AllowAnonymous]
        public ActionResult LogOff()
        {
            ServiceAuthentication.FinalizarSesión();
            this.HttpContext.Session.Clear();
            this.HttpContext.Session.Abandon();
            return RedirectToAction("Login", "Account");
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        /// <summary>
        /// Recupera una lista de items de todos los géneros
        /// </summary>
        /// <returns></returns>
        private IList<SelectListItem> GetGenderSelectListItem()
        {
            var genders = this.ServiceGender.GetAllGender(new GetGenderRequest()).Genders;
            return genders.Select(x => new SelectListItem() { Text = x.Name, Value = x.GenderId.ToString() }).ToList();
        }

        private void SendActivationEmail(RegisterVM user, int userId, string token)
        {
            using (MailMessage mm = new MailMessage("teappweb@gmail.com", user.Email))
            {
                mm.Subject = "Activación de cuenta";
                string body = $"Bienvenido {user.UserName}, ";
                body += $"por favor, hacer <a href = '{Request.Url.Scheme}://{Request.Url.Authority}/Account/ActivationUser?id={userId}&token={token}'>CLICK AQUI</a> para activar tu cuenta.";
                mm.Body = body;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential("teappweb@gmail.com", "Teapp123456");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);
            }
        }

        #endregion
    }
}