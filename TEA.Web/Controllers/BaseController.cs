﻿namespace TEA.Web.Controllers
{
    using System.Web.Mvc;
    using TEA.Web.Infrastucture.Security;

    public class BaseController : Controller
    {
        private CustomIdentity userCurrent;
        public CustomIdentity UserCurrent { get { return userCurrent = (CustomIdentity)User.Identity; } }
    }
}