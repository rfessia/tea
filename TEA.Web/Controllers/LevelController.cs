﻿namespace TEA.Web.Controllers
{
    using AutoMapper;
    using Entidades.Negocio.User;
    using Filters;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using TEA.Contratos;
    using TEA.Entidades.Negocio.Diagnostic;
    using TEA.Entidades.Negocio.Level;
    using TEA.Web.ViewsModel.Level;

    [CustomAuthenticationAttribute(Roles = UserRoles.Administrador)]
    public class LevelController : BaseController
    {
        public IServiceUser ServiceUser;
        public IServiceLevel ServiceLevel;
        public IServiceDiagnostic ServiceDiagnostic;

        public LevelController(IServiceLevel ServiceLevel, IServiceDiagnostic ServiceDiagnostic, IServiceUser ServiceUser)
        {
            this.ServiceUser = ServiceUser;
            this.ServiceLevel = ServiceLevel;
            this.ServiceDiagnostic = ServiceDiagnostic;
        }

        public ActionResult Index()
        {
            var model = new IndexLevelVM();
            var usersItems = this.ServiceUser.GetUsers(new GetUsersRequest() { TopUserId = UserCurrent.Id }).Users;
            model.UsersItems = usersItems.Select(x => new SelectListItem() { Text = x.Name, Value = x.UserId.ToString() }).ToList();
            return View(model);
        }

        /// <summary>
        /// Retorna la información para la tabla de niveles
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoadDataTable()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            var sortColumn = Request.Form.GetValues($"columns[{ Request.Form.GetValues("order[0][column]").FirstOrDefault()}][data]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
            var sort = string.Join("_", sortColumn, sortColumnDir);
            int skip = start != null ? Convert.ToInt16(start) : 0;
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            var levelName = Request.Form.GetValues("columns[0][search][value]").FirstOrDefault();
            var diagnosticName = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var filterUserId = !string.IsNullOrWhiteSpace(Request.Form.GetValues("columns[2][search][value]").FirstOrDefault()) ? Convert.ToInt16(Request.Form.GetValues("columns[2][search][value]").FirstOrDefault()) : (int?)null;
            var filterEnabledLevel = !string.IsNullOrWhiteSpace(Request.Form.GetValues("columns[3][search][value]").FirstOrDefault()) ? Convert.ToBoolean(Request.Form.GetValues("columns[3][search][value]").FirstOrDefault()) : (bool?)null;
            var getLevelsResponse = this.ServiceLevel.GetLevels(new GetLevelsRequest() { Order = sort, Skip = skip, PageSize = pageSize, UserProfessionalId = UserCurrent.Id, FilterLevelName = levelName, FilterDiagnosticName = diagnosticName, UserId = filterUserId, FilterEnabledLevel = filterEnabledLevel });
            var levels = Mapper.Map<List<IndexLevelVM>>(getLevelsResponse.Levels.ToList());
            foreach (var level in levels)
            {
                var id = level.LevelId;
                var htmlActions = $"<a href='/Level/Edit/{id}'><i class='btn btn-primary btn-md  glyphicon glyphicon-pencil'></i></a>";
                htmlActions += $"<a onclick='GetDetailLevel({id})' data-toggle='modal' data-target='#detailModal'><i class='btn btn-info btn-md glyphicon glyphicon-search'></i></a>";
                htmlActions += $"<a onclick='DeleteLevel({id})' data-toggle='modal' data-target='#modalConfirmationDelete' data-id='{id}'><i class='btn btn-danger btn-md glyphicon glyphicon-trash delete'></i></a>";
                level.Actions = htmlActions;
            };

            return Json(new { draw = draw, recordsFiltered = getLevelsResponse.RecordsTotal, recordsTotal = getLevelsResponse.RecordsTotal, data = levels }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new CreateLevelVM()
            {
                Diagnostics = GetDiagnosticTypeSelectListItem(UserCurrent.Id),
                Enabled = true,
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateLevelVM model)
        {
            if (ModelState.IsValid)
            {
                var request = Mapper.Map<CreateLevelRequest>(model);
                var createLevelReseponse = this.ServiceLevel.CreateLevel(request);
                if (createLevelReseponse.ValidateResponse.IsValid)
                {
                    return RedirectToAction("Index", "Level");
                }
                else
                {
                    foreach (var error in createLevelReseponse.ValidateResponse.Errors)
                    {
                        ModelState.AddModelError(error.Key, error.Value);
                    };
                };
            };

            model.Diagnostics = GetDiagnosticTypeSelectListItem(UserCurrent.Id);
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var level = this.ServiceLevel.GetLevelById(new GetLevelRequest() { LevelId = id, UserProfessionalId = UserCurrent.Id }).Level;
            var model = Mapper.Map<EditLevelVM>(level);
            model.Diagnostics = GetDiagnosticTypeSelectListItem(UserCurrent.Id);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditLevelVM model)
        {
            if (ModelState.IsValid)
            {
                var request = Mapper.Map<UpdateLevelRequest>(model);
                var updateLevelReponse = this.ServiceLevel.UpdateLevel(request);
                if (updateLevelReponse.ValidateResponse.IsValid)
                {
                    return RedirectToAction("Index", "Level");
                }
                else
                {
                    foreach (var error in updateLevelReponse.ValidateResponse.Errors)
                    {
                        ModelState.AddModelError(error.Key, error.Value);
                    };
                };
            };

            model.Diagnostics = GetDiagnosticTypeSelectListItem(UserCurrent.Id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var delete = this.ServiceLevel.DeleteLevel(new DeleteLevelRequest() { LevelId = id, UserProfessionalId = UserCurrent.Id });
            if (!delete.ValidarResponse.IsValid)
            {
                var error = delete.ValidarResponse.Errors.First();
                return Json(new { resultado = false, clave = error.Key, valor = error.Value });
            }

            return Json(new { resultado = true, id = id });
        }

        [HttpGet]
        public ActionResult GetDetail(int levelId)
        {
            var level = this.ServiceLevel.GetLevelById(new GetLevelRequest() { LevelId = levelId, UserProfessionalId = UserCurrent.Id }).Level;
            var model = Mapper.Map<DetailLevelVM>(level);
            var users = this.ServiceUser.GetUsersByDiagnostic(new GetUsersByDiagnosticRequest() { DiagnosticId = level.DiagnosticId }).User;
            model.UserName = users.Name;
            return PartialView("_Detail", model);
        }

        /// <summary>
        /// Recupera una lista de items de todos los diagnosticos de un usuario
        /// </summary>
        /// <returns></returns>
        private IList<SelectListItem> GetDiagnosticTypeSelectListItem(int userProfessionalId)
        {
            var diagnostics = this.ServiceDiagnostic.GetDiagnostics(new GetDiagnosticsRequest() { UserProfessionalId = userProfessionalId }).Diagnostics;
            return diagnostics.Select(x => new SelectListItem() { Text = x.Name, Value = x.DiagnosticId.ToString() }).ToList();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetlevelsByDiagnostic(int diagnosticId)
        {
            var levels = this.ServiceLevel.GetLevelsByDiagnostic(new GetLevelsRequest() { DiagnosticId = diagnosticId, UserProfessionalId = UserCurrent.Id }).Levels;
            return Json(levels, JsonRequestBehavior.AllowGet);
        }
    }
}