﻿namespace TEA.Web.Controllers
{
    using AutoMapper;
    using Comun.Exportacion.ExportacionPdf;
    using Contratos;
    using Entidades.Negocio.Report;
    using Entidades.Negocio.User;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;
    using TEA.Web.Filters;
    using ViewsModel.Report;

    [CustomAuthenticationAttribute(Roles = UserRoles.Administrador)]
    public class ReportController : BaseController
    {
        public IServiceReport ServiceUserStage;
        public IServiceUser ServiceUser;

        public ReportController(IServiceReport ServiceUserStage, IServiceUser ServiceUser)
        {
            this.ServiceUserStage = ServiceUserStage;
            this.ServiceUser = ServiceUser;
        }

        public ActionResult Index()
        {
            var model = new IndexUserStageHistoryVM();
            var usersItems = this.ServiceUser.GetUsers(new GetUsersRequest() { TopUserId = UserCurrent.Id }).Users;
            model.UsersItems = usersItems.Select(x => new SelectListItem() { Text = x.Name, Value = x.UserId.ToString() }).ToList();
            return View(model);
        }

        /// <summary>
        /// Retorna la información para la tabla de estapas
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoadDataTable()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            var sortColumn = Request.Form.GetValues($"columns[{ Request.Form.GetValues("order[0][column]").FirstOrDefault()}][data]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
            var sort = string.Join("_", sortColumn, sortColumnDir);
            int skip = start != null ? Convert.ToInt16(start) : 0;
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            var filterUserId = !string.IsNullOrWhiteSpace(Request.Form.GetValues("columns[0][search][value]").FirstOrDefault()) ? Convert.ToInt16(Request.Form.GetValues("columns[0][search][value]").FirstOrDefault()) : (int?)null;
            var filterStageName = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var filterLevelName = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var filterDiagnosticName = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();
            var filterSharedStage = !string.IsNullOrWhiteSpace(Request.Form.GetValues("columns[4][search][value]").FirstOrDefault()) ? Convert.ToBoolean(Request.Form.GetValues("columns[4][search][value]").FirstOrDefault()) : (bool?)null;
            var filterDateStart = !string.IsNullOrWhiteSpace(Request.Form.GetValues("columns[5][search][value]").FirstOrDefault()) ? DateTime.ParseExact(Request.Form.GetValues("columns[5][search][value]").FirstOrDefault(), "dd/MM/yyyy", CultureInfo.InvariantCulture) : (DateTime?)null;
            var filterDateEnd = !string.IsNullOrWhiteSpace(Request.Form.GetValues("columns[6][search][value]").FirstOrDefault()) ? DateTime.ParseExact(Request.Form.GetValues("columns[6][search][value]").FirstOrDefault(), "dd/MM/yyyy", CultureInfo.InvariantCulture) : (DateTime?)null;
            var getStagesResponse = this.ServiceUserStage.GetUserStageHistoryFiltered(new GetUserStageHistoryRequest() { Order = sort, Skip = skip, PageSize = pageSize, FilterStageName = filterStageName, FilterLevelName = filterLevelName, FilterDiagnosticName = filterDiagnosticName, FilterUserId = filterUserId, FilterShared = filterSharedStage, UserProfessionalId = UserCurrent.Id, FilterDateEnd = filterDateEnd, FilterDateStart = filterDateStart });
            var usersStagesHistories = Mapper.Map<List<IndexUserStageHistoryVM>>(getStagesResponse.UsersStagesHistories.ToList());

            foreach (var usersStagesHistory in usersStagesHistories)
            {
                var id = usersStagesHistory.Id;
                var htmlActions = $"<a href='/Report/PrintReportById/{id}'><i class='btn btn-primary btn-md fa fa-file-pdf-o'></i></a>";
                usersStagesHistory.Actions = htmlActions;
            };

            return Json(new { draw = draw, recordsFiltered = getStagesResponse.RecordsTotal, recordsTotal = getStagesResponse.RecordsTotal, data = usersStagesHistories }, JsonRequestBehavior.AllowGet);
        }

        #region Imprimir

        [HttpGet]
        public FileContentResult PrintReport(IndexUserStageHistoryVM model)
        {
            var userProfessional = this.ServiceUser.GetUserById(new GetUserByIdRequest() { UserId = UserCurrent.Id }).User;
            var today = DateTime.Now.Date;
            var filterDateStart = !string.IsNullOrEmpty(model.FilterDateStart) ? DateTime.ParseExact(model.FilterDateStart, "dd/MM/yyyy", CultureInfo.InvariantCulture) : (DateTime?)null;
            var filterDateEnd = !string.IsNullOrEmpty(model.FilterDateEnd) ? DateTime.ParseExact(model.FilterDateEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture) : (DateTime?)null;
            var getStagesResponse = this.ServiceUserStage.GetUserStageHistoryFiltered(new GetUserStageHistoryRequest() { Order = model.FilterSortReport, FilterStageName = model.FilterStageName, FilterLevelName = model.FilterLevelName, FilterDiagnosticName = model.FilterDiagnosticName, FilterUserId = model.FilterUserId, FilterShared = model.FilterSharedStage, UserProfessionalId = UserCurrent.Id, FilterDateEnd = filterDateEnd, FilterDateStart = filterDateStart });
            PdfDocumento pdfDocumento = new PdfDocumento(PdfTamanoPagina.A4, "Detalle historico Usuario", "Creador", "Autor");
            pdfDocumento.MargenInferior = 20;
            pdfDocumento.MargenSuperior = 20;
            pdfDocumento.MargenIzquierdo = 20;
            pdfDocumento.MargenDerecho = 20;

            PdfParrafo pdfParrafo = new PdfParrafo(PdfAlineacionHorizontal.Centrado);
            PdfTexto pdfTexto = new PdfTexto(PdfTipoTexto.Titulo, $"Informe: {today.ToString("dd/MM/yyyy")}");
            pdfTexto.Fuente.Tamano = 12;
            pdfParrafo.Elementos.Add(pdfTexto);
            pdfDocumento.Parrafos.Add(pdfParrafo);
            pdfParrafo = new PdfParrafo(PdfAlineacionHorizontal.Centrado);

            ////PRIMERA PARTE////
            PdfTabla pdfTabla = new PdfTabla(new float[] { 50, 50 });
            pdfTabla.AnchoRelativo = 100;

            PdfFilaTabla pdfFilaTabla = new PdfFilaTabla(PdfTipoFilaTabla.EncabezadoTabla);
            PdfCeldaTabla pdfCeldaTabla = new PdfCeldaTabla("");
            PdfConfiguracionFuente tamanioFuenteCeldas = new PdfConfiguracionFuente();
            tamanioFuenteCeldas.Tamano = 9;
            tamanioFuenteCeldas.Familia = PdfFamiliaFuente.Helvetica;

            PdfConfiguracionFuente tamanioFuenteEncabezadoCeldas = new PdfConfiguracionFuente();
            tamanioFuenteEncabezadoCeldas.Tamano = 9;
            tamanioFuenteEncabezadoCeldas.Familia = PdfFamiliaFuente.Helvetica;
            tamanioFuenteEncabezadoCeldas.Estilo = PdfEstiloFuente.Bold;
            pdfCeldaTabla.Fuente = tamanioFuenteCeldas;

            //Encabezado
            pdfFilaTabla = new PdfFilaTabla(PdfTipoFilaTabla.EncabezadoTabla);
            pdfCeldaTabla = new PdfCeldaTabla($"Usuario Profesional: {userProfessional.Name}", PdfAlineacionHorizontal.Izquierda);
            pdfCeldaTabla.ColorBorde = System.Drawing.Color.White;
            pdfCeldaTabla.Fuente = tamanioFuenteEncabezadoCeldas;
            pdfFilaTabla.Celdas.Add(pdfCeldaTabla);

            pdfCeldaTabla = new PdfCeldaTabla("", PdfAlineacionHorizontal.Izquierda);
            pdfCeldaTabla.ColorBorde = System.Drawing.Color.White;
            pdfFilaTabla.Celdas.Add(pdfCeldaTabla);
            pdfTabla.Filas.Add(pdfFilaTabla);
            //

            pdfParrafo.Elementos.Add(pdfTabla);
            pdfParrafo.EspacioAbajo = 10F;
            /////////////////
            pdfDocumento.Parrafos.Add(pdfParrafo);

            //////Seccion items//
            PdfParrafo pdfParrafoDatosItems = new PdfParrafo(PdfAlineacionHorizontal.Izquierda);
            pdfParrafoDatosItems = new PdfParrafo(PdfAlineacionHorizontal.Centrado);
            PdfTabla pdfTablaDatosItems = new PdfTabla(new float[] { 15, 20, 15, 15, 15, 10, 10 });
            pdfTablaDatosItems.AnchoRelativo = 100;

            PdfFilaTabla pdfFilaTablaDatosItems = new PdfFilaTabla(PdfTipoFilaTabla.EncabezadoTabla);
            PdfCeldaTabla pdfCeldaTablaDatosItems = new PdfCeldaTabla("");

            //Encabezado
            pdfFilaTablaDatosItems = new PdfFilaTabla(PdfTipoFilaTabla.EncabezadoTabla);
            pdfCeldaTablaDatosItems = new PdfCeldaTabla("USUARIO", PdfAlineacionHorizontal.Centrado);
            pdfCeldaTablaDatosItems.Fuente = tamanioFuenteEncabezadoCeldas;
            pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
            pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

            pdfCeldaTablaDatosItems = new PdfCeldaTabla("ETAPA", PdfAlineacionHorizontal.Centrado);
            pdfCeldaTablaDatosItems.Fuente = tamanioFuenteEncabezadoCeldas;
            pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
            pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

            pdfCeldaTablaDatosItems = new PdfCeldaTabla("ACTIVIDAD", PdfAlineacionHorizontal.Centrado);
            pdfCeldaTablaDatosItems.Fuente = tamanioFuenteEncabezadoCeldas;
            pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
            pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

            pdfCeldaTablaDatosItems = new PdfCeldaTabla("DIAGNOSTICO", PdfAlineacionHorizontal.Centrado);
            pdfCeldaTablaDatosItems.Fuente = tamanioFuenteEncabezadoCeldas;
            pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
            pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

            pdfCeldaTablaDatosItems = new PdfCeldaTabla("COMPARTIDA", PdfAlineacionHorizontal.Centrado);
            pdfCeldaTablaDatosItems.Fuente = tamanioFuenteEncabezadoCeldas;
            pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
            pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

            pdfCeldaTablaDatosItems = new PdfCeldaTabla("INTENTOS", PdfAlineacionHorizontal.Centrado);
            pdfCeldaTablaDatosItems.Fuente = tamanioFuenteEncabezadoCeldas;
            pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
            pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

            pdfCeldaTablaDatosItems = new PdfCeldaTabla("FECHA", PdfAlineacionHorizontal.Centrado);
            pdfCeldaTablaDatosItems.Fuente = tamanioFuenteEncabezadoCeldas;
            pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
            pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

            pdfTablaDatosItems.Filas.Add(pdfFilaTablaDatosItems);
            //

            foreach (var item in getStagesResponse.UsersStagesHistories)
            {
                //Detalle
                pdfFilaTablaDatosItems = new PdfFilaTabla(PdfTipoFilaTabla.DetalleTabla);
                pdfCeldaTablaDatosItems = new PdfCeldaTabla(item.UserName, PdfAlineacionHorizontal.Izquierda);
                pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
                pdfCeldaTablaDatosItems.Fuente = tamanioFuenteCeldas;
                pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

                pdfCeldaTablaDatosItems = new PdfCeldaTabla(item.StageName.ToString(), PdfAlineacionHorizontal.Derecha);
                pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
                pdfCeldaTablaDatosItems.Fuente = tamanioFuenteCeldas;
                pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

                pdfCeldaTablaDatosItems = new PdfCeldaTabla(item.LevelName.ToString(), PdfAlineacionHorizontal.Derecha);
                pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
                pdfCeldaTablaDatosItems.Fuente = tamanioFuenteCeldas;
                pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

                pdfCeldaTablaDatosItems = new PdfCeldaTabla(item.DiagnosticName.ToString(), PdfAlineacionHorizontal.Derecha);
                pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
                pdfCeldaTablaDatosItems.Fuente = tamanioFuenteCeldas;
                pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

                pdfCeldaTablaDatosItems = new PdfCeldaTabla(item.StageShared == true ? "SI" : "NO", PdfAlineacionHorizontal.Derecha);
                pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
                pdfCeldaTablaDatosItems.Fuente = tamanioFuenteCeldas;
                pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

                pdfCeldaTablaDatosItems = new PdfCeldaTabla(item.NumberOfAttempts.ToString(), PdfAlineacionHorizontal.Derecha);
                pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
                pdfCeldaTablaDatosItems.Fuente = tamanioFuenteCeldas;
                pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

                pdfCeldaTablaDatosItems = new PdfCeldaTabla(item.FinishDate.ToString("dd/MM/yyyy HH:mm:ss"), PdfAlineacionHorizontal.Derecha);
                pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
                pdfCeldaTablaDatosItems.Fuente = tamanioFuenteCeldas;
                pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

                pdfTablaDatosItems.Filas.Add(pdfFilaTablaDatosItems);
                //

            };

            pdfParrafoDatosItems.Elementos.Add(pdfTablaDatosItems);
            pdfDocumento.Parrafos.Add(pdfParrafoDatosItems);
            var generadorPdf = new GeneradorPdf(pdfDocumento);
            byte[] archivoPdf = generadorPdf.ObtenerPdf();
            var fileContentResult = new FileContentResult(archivoPdf, GeneradorPdf.ContentType);
            fileContentResult.FileDownloadName = $"Informe-{today.ToString("dd_MM_yyyy_HH_mm_ss")}.pdf";
            return fileContentResult;
        }

        [HttpGet]
        public FileContentResult PrintReportById(int id)
        {
            var userProfessional = this.ServiceUser.GetUserById(new GetUserByIdRequest() { UserId = UserCurrent.Id }).User;
            var today = DateTime.Now.Date;
            var getStagesResponse = this.ServiceUserStage.GetUserStageHistoryById(new GetUserStageHistoryByIdRequest() { UserStageHistoryId = id });
            PdfDocumento pdfDocumento = new PdfDocumento(PdfTamanoPagina.A4, "Detalle historico Usuario", "Creador", "Autor");
            pdfDocumento.MargenInferior = 20;
            pdfDocumento.MargenSuperior = 20;
            pdfDocumento.MargenIzquierdo = 20;
            pdfDocumento.MargenDerecho = 20;

            PdfParrafo pdfParrafo = new PdfParrafo(PdfAlineacionHorizontal.Centrado);
            PdfTexto pdfTexto = new PdfTexto(PdfTipoTexto.Titulo, $"Informe: {today.ToString("dd/MM/yyyy")}");
            pdfTexto.Fuente.Tamano = 12;
            pdfParrafo.Elementos.Add(pdfTexto);
            pdfDocumento.Parrafos.Add(pdfParrafo);
            pdfParrafo = new PdfParrafo(PdfAlineacionHorizontal.Centrado);

            ////PRIMERA PARTE////
            PdfTabla pdfTabla = new PdfTabla(new float[] { 50, 50 });
            pdfTabla.AnchoRelativo = 100;

            PdfFilaTabla pdfFilaTabla = new PdfFilaTabla(PdfTipoFilaTabla.EncabezadoTabla);
            PdfCeldaTabla pdfCeldaTabla = new PdfCeldaTabla("");
            PdfConfiguracionFuente tamanioFuenteCeldas = new PdfConfiguracionFuente();
            tamanioFuenteCeldas.Tamano = 9;
            tamanioFuenteCeldas.Familia = PdfFamiliaFuente.Helvetica;

            PdfConfiguracionFuente tamanioFuenteEncabezadoCeldas = new PdfConfiguracionFuente();
            tamanioFuenteEncabezadoCeldas.Tamano = 9;
            tamanioFuenteEncabezadoCeldas.Familia = PdfFamiliaFuente.Helvetica;
            tamanioFuenteEncabezadoCeldas.Estilo = PdfEstiloFuente.Bold;
            pdfCeldaTabla.Fuente = tamanioFuenteCeldas;

            //Encabezado
            pdfFilaTabla = new PdfFilaTabla(PdfTipoFilaTabla.EncabezadoTabla);
            pdfCeldaTabla = new PdfCeldaTabla($"Usuario Profesional: {userProfessional.Name}", PdfAlineacionHorizontal.Izquierda);
            pdfCeldaTabla.ColorBorde = System.Drawing.Color.White;
            pdfCeldaTabla.Fuente = tamanioFuenteEncabezadoCeldas;
            pdfFilaTabla.Celdas.Add(pdfCeldaTabla);

            pdfCeldaTabla = new PdfCeldaTabla("", PdfAlineacionHorizontal.Izquierda);
            pdfCeldaTabla.ColorBorde = System.Drawing.Color.White;
            pdfFilaTabla.Celdas.Add(pdfCeldaTabla);
            pdfTabla.Filas.Add(pdfFilaTabla);
            //

            pdfParrafo.Elementos.Add(pdfTabla);
            pdfParrafo.EspacioAbajo = 10F;
            /////////////////
            pdfDocumento.Parrafos.Add(pdfParrafo);

            //////Seccion items//
            PdfParrafo pdfParrafoDatosItems = new PdfParrafo(PdfAlineacionHorizontal.Izquierda);
            pdfParrafoDatosItems = new PdfParrafo(PdfAlineacionHorizontal.Centrado);
            PdfTabla pdfTablaDatosItems = new PdfTabla(new float[] { 15, 20, 15, 15, 15, 10, 10 });
            pdfTablaDatosItems.AnchoRelativo = 100;

            PdfFilaTabla pdfFilaTablaDatosItems = new PdfFilaTabla(PdfTipoFilaTabla.EncabezadoTabla);
            PdfCeldaTabla pdfCeldaTablaDatosItems = new PdfCeldaTabla("");

            //Encabezado
            pdfFilaTablaDatosItems = new PdfFilaTabla(PdfTipoFilaTabla.EncabezadoTabla);
            pdfCeldaTablaDatosItems = new PdfCeldaTabla("USUARIO", PdfAlineacionHorizontal.Centrado);
            pdfCeldaTablaDatosItems.Fuente = tamanioFuenteEncabezadoCeldas;
            pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
            pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

            pdfCeldaTablaDatosItems = new PdfCeldaTabla("ETAPA", PdfAlineacionHorizontal.Centrado);
            pdfCeldaTablaDatosItems.Fuente = tamanioFuenteEncabezadoCeldas;
            pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
            pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

            pdfCeldaTablaDatosItems = new PdfCeldaTabla("ACTIVIDAD", PdfAlineacionHorizontal.Centrado);
            pdfCeldaTablaDatosItems.Fuente = tamanioFuenteEncabezadoCeldas;
            pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
            pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

            pdfCeldaTablaDatosItems = new PdfCeldaTabla("DIAGNOSTICO", PdfAlineacionHorizontal.Centrado);
            pdfCeldaTablaDatosItems.Fuente = tamanioFuenteEncabezadoCeldas;
            pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
            pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

            pdfCeldaTablaDatosItems = new PdfCeldaTabla("COMPARTIDA", PdfAlineacionHorizontal.Centrado);
            pdfCeldaTablaDatosItems.Fuente = tamanioFuenteEncabezadoCeldas;
            pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
            pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

            pdfCeldaTablaDatosItems = new PdfCeldaTabla("INTENTOS", PdfAlineacionHorizontal.Centrado);
            pdfCeldaTablaDatosItems.Fuente = tamanioFuenteEncabezadoCeldas;
            pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
            pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

            pdfCeldaTablaDatosItems = new PdfCeldaTabla("FECHA", PdfAlineacionHorizontal.Centrado);
            pdfCeldaTablaDatosItems.Fuente = tamanioFuenteEncabezadoCeldas;
            pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
            pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

            pdfTablaDatosItems.Filas.Add(pdfFilaTablaDatosItems);
            //

            var usersStagesHistory = getStagesResponse.UsersStagesHistory;
            //Detalle
            pdfFilaTablaDatosItems = new PdfFilaTabla(PdfTipoFilaTabla.DetalleTabla);
            pdfCeldaTablaDatosItems = new PdfCeldaTabla(usersStagesHistory.UserName, PdfAlineacionHorizontal.Izquierda);
            pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
            pdfCeldaTablaDatosItems.Fuente = tamanioFuenteCeldas;
            pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

            pdfCeldaTablaDatosItems = new PdfCeldaTabla(usersStagesHistory.StageName.ToString(), PdfAlineacionHorizontal.Derecha);
            pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
            pdfCeldaTablaDatosItems.Fuente = tamanioFuenteCeldas;
            pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

            pdfCeldaTablaDatosItems = new PdfCeldaTabla(usersStagesHistory.LevelName.ToString(), PdfAlineacionHorizontal.Derecha);
            pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
            pdfCeldaTablaDatosItems.Fuente = tamanioFuenteCeldas;
            pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

            pdfCeldaTablaDatosItems = new PdfCeldaTabla(usersStagesHistory.DiagnosticName.ToString(), PdfAlineacionHorizontal.Derecha);
            pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
            pdfCeldaTablaDatosItems.Fuente = tamanioFuenteCeldas;
            pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

            pdfCeldaTablaDatosItems = new PdfCeldaTabla(usersStagesHistory.StageShared == true ? "SI" : "NO", PdfAlineacionHorizontal.Derecha);
            pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
            pdfCeldaTablaDatosItems.Fuente = tamanioFuenteCeldas;
            pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

            pdfCeldaTablaDatosItems = new PdfCeldaTabla(usersStagesHistory.NumberOfAttempts.ToString(), PdfAlineacionHorizontal.Derecha);
            pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
            pdfCeldaTablaDatosItems.Fuente = tamanioFuenteCeldas;
            pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

            pdfCeldaTablaDatosItems = new PdfCeldaTabla(usersStagesHistory.FinishDate.ToString("dd/MM/yyyy HH:mm:ss"), PdfAlineacionHorizontal.Derecha);
            pdfCeldaTablaDatosItems.ColorBorde = System.Drawing.Color.Black;
            pdfCeldaTablaDatosItems.Fuente = tamanioFuenteCeldas;
            pdfFilaTablaDatosItems.Celdas.Add(pdfCeldaTablaDatosItems);

            pdfTablaDatosItems.Filas.Add(pdfFilaTablaDatosItems);
            //

            
            pdfParrafoDatosItems.Elementos.Add(pdfTablaDatosItems);
            pdfDocumento.Parrafos.Add(pdfParrafoDatosItems);
            var generadorPdf = new GeneradorPdf(pdfDocumento);
            byte[] archivoPdf = generadorPdf.ObtenerPdf();
            var fileContentResult = new FileContentResult(archivoPdf, GeneradorPdf.ContentType);
            fileContentResult.FileDownloadName = $"Informe-{today.ToString("dd_MM_yyyy_HH_mm_ss")}.pdf";
            return fileContentResult;
        }

        #endregion
    }
}