﻿namespace TEA.Web.Controllers
{
    using AutoMapper;
    using Filters;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using TEA.Contratos;
    using TEA.Entidades.Negocio.Diagnostic;
    using TEA.Entidades.Negocio.DiagnosticType;
    using TEA.Entidades.Negocio.User;
    using TEA.Web.Infrastucture.Security;
    using TEA.Web.ViewsModel.Diagnostic;

    [CustomAuthenticationAttribute(Roles = UserRoles.Administrador)]
    public class DiagnosticController : BaseController
    {
        public IServiceDiagnostic ServiceDiagnostic;
        public IServiceDiagnosticType ServiceDiagnosticType;
        public IServiceUser ServiceUser;

        public DiagnosticController(IServiceDiagnostic ServiceDiagnostic, IServiceDiagnosticType ServiceDiagnosticType, IServiceUser ServiceUser)
        {
            this.ServiceDiagnostic = ServiceDiagnostic;
            this.ServiceDiagnosticType = ServiceDiagnosticType;
            this.ServiceUser = ServiceUser;
        }

        public ActionResult Index()
        {
            var model = new IndexDiagnosticVM()
            {
                DiagnosticTypes = GetDiagnosticTypeSelectListItem(),
            };

            return View(model);
        }

        /// <summary>
        /// Retorna la información para la tabla de diagnósticos
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoadDataTable()
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            var sortColumn = Request.Form.GetValues($"columns[{ Request.Form.GetValues("order[0][column]").FirstOrDefault()}][data]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
            var sort = string.Join("_", sortColumn, sortColumnDir);
            int skip = start != null ? Convert.ToInt16(start) : 0;
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            var diagnosticTypeId = !string.IsNullOrWhiteSpace(Request.Form.GetValues("columns[1][search][value]").FirstOrDefault()) ? Convert.ToInt16(Request.Form.GetValues("columns[1][search][value]").FirstOrDefault()) : (int?)null;
            var diagnosticName = Request.Form.GetValues("columns[0][search][value]").FirstOrDefault();
            var getDiagnosticsResponse = this.ServiceDiagnostic.GetDiagnostics(new GetDiagnosticsRequest() { Order = sort, Skip = skip, PageSize = pageSize, FilterDiagnosticTypeId = diagnosticTypeId, FilterDiagnostic = diagnosticName, UserProfessionalId = UserCurrent.Id });
            var diagnostics = Mapper.Map<List<IndexDiagnosticVM>>(getDiagnosticsResponse.Diagnostics.ToList());
            foreach (var diagnostic in diagnostics)
            {
                var id = diagnostic.DiagnosticId;
                var htmlActions = $"<a href='/Diagnostic/Edit/{id}'><i class='btn btn-primary btn-md  glyphicon glyphicon-pencil'></i></a>";
                htmlActions += $"<a onclick='GetDetailDiagnostic({id})' data-toggle='modal' data-target='#detailModal'><i class='btn btn-info btn-md glyphicon glyphicon-search'></i></a>";
                htmlActions += $"<a onclick='DeleteDiagnostic({id})' data-toggle='modal' data-target='#modalConfirmationDelete' data-id='{id}'><i class='btn btn-danger btn-md glyphicon glyphicon-trash delete'></i></a>";
                diagnostic.Actions = htmlActions;
            };

            return Json(new { draw = draw, recordsFiltered = getDiagnosticsResponse.RecordsTotal, recordsTotal = getDiagnosticsResponse.RecordsTotal, data = diagnostics }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var usersItems = this.ServiceUser.GetUsers(new GetUsersRequest() { TopUserId = UserCurrent.Id }).Users;
            var model = new CreateDiagnosticVM()
            {
                DiagnosticTypes = GetDiagnosticTypeSelectListItem(),
                UsersItems = usersItems.Select(x => new SelectListItem() { Text = x.Name, Value = x.UserId.ToString() }).ToList()
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateDiagnosticVM model)
        {
            if (ModelState.IsValid)
            {
                var createDiagnosticRequest = Mapper.Map<CreateDiagnosticRequest>(model);
                createDiagnosticRequest.UserProfessionalId = UserCurrent.Id;
                var createDiagnosticReseponse = this.ServiceDiagnostic.CreateDiagnostic(createDiagnosticRequest);
                if (createDiagnosticReseponse.ValidateResponse.IsValid)
                {
                    return RedirectToAction("Index", "Diagnostic");
                }
                else
                {
                    foreach (var error in createDiagnosticReseponse.ValidateResponse.Errors)
                    {
                        ModelState.AddModelError(error.Key, error.Value);
                    };
                };
            };

            model.DiagnosticTypes = GetDiagnosticTypeSelectListItem();
            var usersItems = this.ServiceUser.GetUsers(new GetUsersRequest() { TopUserId = UserCurrent.Id }).Users;
            model.UsersItems = usersItems.Select(x => new SelectListItem() { Text = x.Name, Value = x.UserId.ToString() }).ToList();
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var diagnostic = this.ServiceDiagnostic.GetDiagnosticById(new GetDiagnosticRequest() { DiagnosticId = id, UserProfessionalId = UserCurrent.Id }).Diagnostic;
            var model = Mapper.Map<UpdateDiagnosticVM>(diagnostic);
            model.DiagnosticTypes = GetDiagnosticTypeSelectListItem();
            var usersItems = this.ServiceUser.GetUsers(new GetUsersRequest() { TopUserId = UserCurrent.Id }).Users;
            model.UsersItems = usersItems.Select(x => new SelectListItem() { Text = x.Name, Value = x.UserId.ToString() }).ToList();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UpdateDiagnosticVM model)
        {
            ///TODO: Hacer servicio para actualizar diagnóstico, verificar el get y post de la edición de diagnóstico
            if (ModelState.IsValid)
            {
                var updateDiagnosticRequest = Mapper.Map<UpdateDiagnosticRequest>(model);
                updateDiagnosticRequest.UserProfessionalId = UserCurrent.Id;
                var updateDiagnosticReponse = this.ServiceDiagnostic.UpdateDiagnostic(updateDiagnosticRequest);
                if (updateDiagnosticReponse.ValidateResponse.IsValid)
                {
                    return RedirectToAction("Index", "Diagnostic");
                }
                else
                {
                    foreach (var error in updateDiagnosticReponse.ValidateResponse.Errors)
                    {
                        ModelState.AddModelError(error.Key, error.Value);
                    };
                };
            };

            model.DiagnosticTypes = GetDiagnosticTypeSelectListItem();
            var usersItems = this.ServiceUser.GetUsers(new GetUsersRequest() { TopUserId = UserCurrent.Id }).Users;
            model.UsersItems = usersItems.Select(x => new SelectListItem() { Text = x.Name, Value = x.UserId.ToString() }).ToList();
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var eliminar = this.ServiceDiagnostic.DeleteDiagnostic(new DeleteDiagnosticRequest() { DiagnosticId = id, UserProfessionalId = UserCurrent.Id });
            if (!eliminar.ValidarResponse.IsValid)
            {
                var error = eliminar.ValidarResponse.Errors.First();
                return Json(new { resultado = false, clave = error.Key, valor = error.Value });
            };

            return Json(new { resultado = true, id = id });
        }

        [HttpGet]
        public ActionResult GetDetail(int diagnosticId)
        {
            var getDiagnosticResponse = this.ServiceDiagnostic.GetDiagnosticById(new GetDiagnosticRequest() { DiagnosticId = diagnosticId, UserProfessionalId = UserCurrent.Id });
            var detailDiagnosticVM = Mapper.Map<DetailDiagnosticVM>(getDiagnosticResponse.Diagnostic);
            return PartialView("_Detail", detailDiagnosticVM);
        }

        [HttpGet]
        public JsonResult GetUsersByProfessional()
        {
            var Users = this.ServiceUser.GetUsers(new GetUsersRequest() { TopUserId = UserCurrent.Id }).Users;
            var usuariosJSON = new List<Object>();
            foreach (var u in Users)
            {
                usuariosJSON.Add(new
                {
                    IdUsuario = u.UserId,
                    NombreUsuario = u.Name,
                });
            }

            return Json(usuariosJSON.ToArray(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Recupera una lista de items de todos los tipos de diagnosticos
        /// </summary>
        /// <returns></returns>
        private IList<SelectListItem> GetDiagnosticTypeSelectListItem()
        {
            var diagnosticTypes = this.ServiceDiagnosticType.GetDiagnosticTypes(new GetDiagnosticTypesRequest()).DiagnosticTypes;
            return diagnosticTypes.Select(x => new SelectListItem() { Text = x.Name, Value = x.DiagnosticTypeId.ToString() }).ToList();
        }
    }
}