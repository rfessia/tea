﻿$(function () {
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });

    $(".select2").select2({
        selectionTitleAttribute: false
    });

    $('.select2-selection__rendered').hover(function () {
        $(this).removeAttr('title');
    });

    if ($("#DiagnosticId").val() != null && $("#DiagnosticId").val() != undefined && $("#DiagnosticId").val() != "") {
        GetUsers($("#DiagnosticId").val());
    };

    $("#DiagnosticId").change(function () {
        CleanUser();
        var diagnosticId = this.value;
        if (diagnosticId != "" && diagnosticId != undefined) {
            GetUsers(diagnosticId);
        };
    });
});

function GetUsers(diagnosticId) {
    var usuarios = [];
    $.ajax({
        type: "GET",
        async: false,
        url: "/User/GetUsersByDiagnostic",
        data: { diagnosticId: diagnosticId },
        success: function (data) {
            $("#User").html(data.users);
        }
    });

    return usuarios;
};

function CleanUser() {
    $("#User").html("");
};