﻿$(function () {
    $("[data-mask]").inputmask();
    $('#reportTable').DataTable({
        language: {
            url: '/Scripts/plugins/datatables/Spanish.json'
        },
        "processing": true, // for show processing bar
        "serverSide": true, // for process on server side
        "orderMulti": false, // for disable multi column order
        "dom": '<"top"l>rt<"bottom"ip><"clear">', // for hide default global search box // little confusion? don't worry I explained in the tutorial website
        "ajax": {
            "url": "/Report/LoadDataTable",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
                { "data": "UserName", "name": "Usuario", "autoWidth": true },                       //index 0
                { "data": "StageName", "name": "Etapa", "autoWidth": true },                        //index 1
                { "data": "LevelName", "name": "Actividad", "autoWidth": true },                        //index 2
                { "data": "DiagnosticName", "name": "Diagnóstico", "autoWidth": true },             //index 3
                { "data": "StageShared", "name": "Compartida", "autoWidth": true },                 //index 4
                { "data": "NumberOfAttempts", "name": "Intentos", "autoWidth": true },              //index 5
                { "data": "FinishDate", "name": "Fecha", "autoWidth": true },                       //index 6
                { "data": "Actions", "name": "Acciones", "autoWidth": true },                       //index 7
        ]
    });

    //Apply Custom search on jQuery DataTables here
    oTable = $('#reportTable').DataTable();
    $('#btnSearch').click(function () {
        oTable.columns(0).search($('#FilterUserId').val().trim());
        oTable.columns(1).search($('#FilterStageName').val().trim());
        oTable.columns(2).search($('#FilterLevelName').val().trim());
        oTable.columns(3).search($('#FilterDiagnosticName').val().trim());
        oTable.columns(4).search($('#FilterSharedStage').val().trim());
        oTable.columns(5).search($('#FilterDateStart').val().trim());
        oTable.columns(6).search($('#FilterDateEnd').val().trim());

        oTable.draw();
    });

    $(".select2").select2({
        selectionTitleAttribute: false
    });

    $('.select2-selection__rendered').hover(function () {
        $(this).removeAttr('title');
    });
});

function printReport() {
    var table = $('#reportTable').DataTable();
    var order = table.order();
    var typeOrder = order[0][1];
    var index = order[0][0];
    var filterSortReport = table.columns(index).dataSrc(0)[0] + "_" + typeOrder;
    var url = window.location.origin + "/Report/PrintReport?";
    url = url + "FilterUserId=" + encodeURIComponent($('#FilterUserId').val().trim());
    url = url + "&FilterStageName=" + encodeURIComponent($('#FilterStageName').val().trim());
    url = url + "&FilterLevelName=" + encodeURIComponent($('#FilterLevelName').val().trim());
    url = url + "&FilterDiagnosticName=" + encodeURIComponent($('#FilterDiagnosticName').val().trim());
    url = url + "&FilterSharedStage=" + encodeURIComponent($('#FilterSharedStage').val().trim());
    url = url + "&FilterDateStart=" + encodeURIComponent($('#FilterDateStart').val().trim());
    url = url + "&FilterDateEnd=" + encodeURIComponent($('#FilterDateEnd').val().trim());
    url = url + "&FilterSortReport=" + encodeURIComponent(filterSortReport);
    window.open(url, '_blank');
};