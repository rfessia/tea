﻿$(function () {
    $('#example1').DataTable({
        language: {
            url: '/Scripts/plugins/datatables/Spanish.json'
        }
    });

    $('#userTable').DataTable({
        language: {
            url: '/Scripts/plugins/datatables/Spanish.json'
        },
        "processing": true, // for show processing bar
        "serverSide": true, // for process on server side
        "orderMulti": false, // for disable multi column order
        "dom": '<"top"l>rt<"bottom"ip><"clear">', // for hide default global search box // little confusion? don't worry I explained in the tutorial website
        "ajax": {
            "url": "/User/LoadDataTable",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
                { "data": "FullName", "name": "Nombre Completo", "autoWidth": true },                   //index 0
                { "data": "Username", "name": "Nombre de Usuario", "autoWidth": true },                 //index 1
                { "data": "DocumentNumber", "name": "Número de Documento", "autoWidth": true },         //index 2
                { "data": "Actions", "name": "Acciones", "className": "actions", "orderable": false },  //index 3
        ]
    });

    //Apply Custom search on jQuery DataTables here
    oTable = $('#userTable').DataTable();
    $('#btnSearch').click(function () {
        oTable.columns(0).search($('#FilterUserName').val().trim());
        oTable.columns(1).search($('#FilterFullName').val().trim());
        oTable.columns(2).search($('#FilterDocumentNumber').val().trim());
        oTable.draw();
    });
});

function GetDetailUser(userId) {
    $('#detailModal .modal-body').html('<span>Cargando...</span>')
    $.ajax({
        url: '/User/GetDetailUser',
        type: 'GET',
        dataType: "html",
        data: { userId: userId },
        success: function (data) {
            $('#detailModal .modal-body').html(data);
        },
        error: function (e) {
            alert("Ha ocurrido un errror, intente nuevamente");
        }
    });
};

function DeleteUser(id) {
    var name = $("a[data-id='" + id + "']").parent().parent().children('td:first').text();
    var messaje = '¿Está seguro que desea eliminar el Usuario <strong>' + name + '</strong>?';
    $("#modalConfirmationDelete").data('id', id)
    $('p.bodyModalConfirmationDelete').html(messaje);
};

function confirmationDelete() {
    var id = $("#modalConfirmationDelete").data('id');
    $.ajax({
        url: '/User/DeleteUser',
        type: 'POST',
        data: { id: id },
        success: function (data) {
            $('#modalConfirmationDelete').modal('hide')
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            if (data.resultado) {
                $("a[data-id='" + data.id + "']").closest('tr').remove();
            }
            else {
                alert("Se produjo un error al eliminar: " + data.clave + " Valor: " + data.valor);
            };
        },
        error: function (e) {
            alert("Se produjo un error al eliminar, intente nuevamente mas tarde");
        }
    });
};