﻿$(function () {
    $('#diagnosticTable').DataTable({
        language: {
            url: '/Scripts/plugins/datatables/Spanish.json'
        },
        "processing": true, // for show processing bar
        "serverSide": true, // for process on server side
        "orderMulti": false, // for disable multi column order
        "dom": '<"top"l>rt<"bottom"ip><"clear">', // for hide default global search box // little confusion? don't worry I explained in the tutorial website
        "ajax": {
            "url": "/Diagnostic/LoadDataTable",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
                { "data": "Name", "name": "Diagóstico", "autoWidth": true },                            //index 0
                { "data": "DiagnosticTypeName", "name": "Tipo de Diagóstico", "autoWidth": true },      //index 1
                { "data": "Actions", "name": "Acciones", "className": "actions", "orderable": false },  //index 2
        ]
    });

    //Apply Custom search on jQuery DataTables here
    oTable = $('#diagnosticTable').DataTable();
    $('#btnSearch').click(function () {
        //Apply search for Nombre de diagnostico // DataTable column index 0
        oTable.columns(0).search($('#FilterDiagnosticName').val().trim());
        //Apply search for tipo de diagnostico // DataTable column index 2
        oTable.columns(1).search($('#DiagnosticTypeId').val().trim());
        //hit search on server
        oTable.draw();
    });
});

function GetDetailDiagnostic(diagnosticId) {
    $('#detailModal .modal-body').html('<span>Cargando...</span>')
    $.ajax({
        url: '/Diagnostic/GetDetail',
        type: 'GET',
        dataType: "html",
        data: { diagnosticId: diagnosticId },
        success: function (data) {
            $('#detailModal .modal-body').html(data);
        },
        error: function (e) {
            alert("Ha ocurrido un errror, intente nuevamente");
        }
    });
};

function DeleteDiagnostic(id) {
    var name = $("a[data-id='" + id + "']").parent().parent().children('td:first').text();
    var messaje = '¿Está seguro que desea eliminar el diagnóstico <strong>' + name + '</strong>?';
    $("#modalConfirmationDelete").data('id', id)
    $('p.bodyModalConfirmationDelete').html(messaje);
};

function confirmationDelete() {
    var id = $("#modalConfirmationDelete").data('id');
    $.ajax({
        url: '/Diagnostic/Delete',
        type: 'POST',
        data: { id: id },
        success: function (data) {
            $('#modalConfirmationDelete').modal('hide')
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            if (data.resultado) {
                $("a[data-id='" + data.id + "']").closest('tr').remove();
            }
            else {
                alert("Se produjo un error al eliminar: " + data.clave + " Valor: " + data.valor);
            };
        },
        error: function (e) {
            alert("Se produjo un error al eliminar, intente nuevamente mas tarde");
        }
    });
};