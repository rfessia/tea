﻿$(function () {
    $(".select2").select2({
        selectionTitleAttribute: false
    });

    $('.select2-selection__rendered').hover(function () {
        $(this).removeAttr('title');
    });
});