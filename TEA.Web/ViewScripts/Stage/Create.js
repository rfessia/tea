﻿$(function () {
    if (initialJsonModel != null) {
        if (initialJsonModel.UserId != null) {
            if ($('#SelectedUserId').length) {
                Users = GetUsers();
                $('#SelectedUserId').selectivity({
                    items: Users,
                    allowClear: true,
                    placeholder: 'Buscar Usuario'
                });

                $("#SelectedUserId").change(function (obj) {
                    if (obj.value > 0) {
                        $("#UserId").val(obj.value);
                    }
                    else {
                        $("#UserId").val(null);
                    }
                });
            };
        };

        var treeStages = initialJsonModel.TreeStages
        $('#TreeStage').treeview({
            //selectedBackColor: onhoverColor,
            //onhoverColor: onhoverColor,
            highlightSelected: false,
            showBorder: false,
            expandIcon: 'fa fa-caret-right',
            collapseIcon: 'fa fa-caret-down',
            showCheckbox: true,
            data: treeStages,
            multiSelect: false,
            multiCheck: false,
        });
    };

    $("#saveFiles").prop('disabled', true);
    $('#fileupload').fileupload({
        add: function (e, data) {
            data.url = '/Stage/UploadTmpFile'
            if (e.isDefaultPrevented()) {
                return false;
            };

            var $this = $(this),
                that = $this.data('blueimp-fileupload') ||
                    $this.data('fileupload'),
                options = that.options;
            data.context = that._renderUpload(data.files)
                .data('data', data)
                .addClass('processing');
            options.filesContainer[
                options.prependFiles ? 'prepend' : 'append'
            ](data.context);
            that._forceReflow(data.context);
            that._transition(data.context);
            data.process(function () {
                return $this.fileupload('process', data);
            }).always(function () {
                data.context.each(function (index) {
                    $(this).find('.size').text(
                        that._formatFileSize(data.files[index].size)
                    );
                }).removeClass('processing');
                that._renderPreviews(data);
            }).done(function () {
                data.context.find('.start').prop('disabled', false);
                if ((that._trigger('added', e, data) !== false) &&
                    (options.autoUpload || data.autoUpload) &&
                    data.autoUpload !== false) {
                    data.submit();
                }
            }).fail(function () {
                if (data.files.error) {
                    data.context.each(function (index) {
                        var error = data.files[index].error;
                        if (error) {
                            $(this).find('.error').text(error);
                        }
                    });
                }
            });
        }
    });

    $("#StageId").change(function () {
        var stageId = $(this).val();
        $("#btnCancel").attr("href", "/Stage/DeleteStageAndBackToStage?stageId=" + stageId + "&create=true");
    });

    // Smart Wizard
    $('#smartwizard').smartWizard({
        selected: 0,
        theme: 'arrows',
        transitionEffect: 'fade',
        toolbarSettings: {
            toolbarPosition: 'bottom'
        },
        anchorSettings: {
            markDoneStep: true, // add done css
            markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
            removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
            enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
        }
    });

    $("#smartwizard").on("leaveStep", function (e, anchorObject, stepNumber, stepDirection) {
        var elmForm = $("#form-step-" + stepNumber);
        // stepDirection === 'forward' :- this condition allows to do the form validation
        // only on forward navigation, that makes easy navigation on backwards still do the validation when going next
        var result = true;
        if (stepDirection === 'forward' && elmForm) {
            if (stepNumber == 0) {
                result = CreateStage();
            };
        };

        return result;
    });

    $("#smartwizard").on("showStep", function (e, anchorObject, stepNumber, stepDirection) {
        if (stepNumber == 1) {
            $("#saveFiles").prop('disabled', false);
        } else {
            $("#saveFiles").prop('disabled', true);
        }
    });

    $("#saveFiles").click(function (e) {
        e.preventDefault();
        var formdata = new FormData($('#fileupload')[0]);
        formdata.append('AnswerCorrectName', $('input:checked.inRespCorrecta').val());
        formdata.append('BackgroundName', $('input:checked.inFondo').val());

        var url = "";
        var userId = $("#UserId").val();
        if (userId != "" && userId != undefined && userId != null) {
            url = "/Stage/CreateAnswerForUser";
        }
        else {
            url = "/Stage/CreateSharedAnswer";
        };

        /*POST*/
        $.ajax({
            url: url,
            type: 'POST',
            contentType: false, // Not to set any content header
            processData: false, // Not to process data
            data: formdata,
            success: function (result) {
                if (result.isValid == false) {
                    writeErrors(result.errorList);
                }
                else {
                    var url = (window.location.origin + '/Stage')
                    window.location.href = url;
                };
            },
            error: function (e) {
                alert('error');
            }
        });
    });
});

function GetUsers(clasName) {
    var usuarios = [];
    $.ajax({
        type: "GET",
        async: false,
        url: "/Diagnostic/GetUsersByProfessional",
        success: function (data) {
            $.each(data, function (i, item) {
                usuarios.push({
                    id: item.IdUsuario,
                    text: item.NombreUsuario,
                    idClassInput: clasName,
                    nameInput: clasName
                })
            });
        }
    });

    return usuarios;
}

function CreateStage() {
    var stageId = $("#StageId").val();
    if (stageId != "" && stageId != undefined && stageId != null) {
        return true;
    };

    var url = "";
    var userId = $("#UserId").val();
    if (userId != "" && userId != undefined && userId != null) {
        url = "/Stage/CreateStageForUser";
        var levelIdSelected = $("#TreeStage>ul>li.node-checked").attr('id');
        if (levelIdSelected != null && levelIdSelected != undefined && levelIdSelected != "") {
            $("#LevelId").val(levelIdSelected);
        }
        else {
            $("#LevelId").val(null);
        };
    }
    else {
        url = "/Stage/CreateSharedStage";
    };

    var response = false;
    var formdata = new FormData($('#fileupload')[0]);
    if ($('.template-upload').length > 0) {
        $('.template-upload').each(function (index) {
            formdata.append($(this).data().data.files[0].name, $(this).data().data.files[0]);
        });
    };

    $.ajax({
        url: url,
        type: "POST",
        async: false,
        contentType: false, // Not to set any content header
        processData: false, // Not to process data
        data: formdata,
        success: function (result) {
            response = result.isValid;
            if (result.isValid == false) {
                writeErrors(result.errorList);
            }
            else {
                if (result.stageId != null) {
                    $("#StageId").val(result.stageId).trigger('change');
                }
                else {
                    alert("error");
                };
            };
        },
        error: function (err) {
            alert(err.statusText);
        }
    });

    return response;
};

function GetLastOrder(levelId) {
    $.ajax({
        cache: false,
        type: "GET",
        url: "/Stage/GetLastOrder",
        data: { "levelId": levelId },
        success: function (data) {
            $("#Order").val(data.order);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert('Error');
        }
    });
};

function CleanLevel() {
    var levelId = $("#LevelId");
    levelId.html('<option value="">Seleccione una Actividad...</option>');
    levelId.attr("disabled", "disabled");
};

function CleanOrder() {
    $("#Order").val("");
};

function CleanUser() {
    $("#User").html("");
};

function clickFondo(e) {
    var backgroundName = $('#BackgroundName');
    backgroundName.val('');
    var elemento = $(e);
    //Oculto img respuesta correcta
    elemento.parent().siblings().find('a.aRespCorrecta').css("display", "none");
    //Oculto img Fondo
    elemento.css("display", "none");
    //Cargo img nuevo fondo
    elemento.siblings('span').css("display", "block");
    //Marco hidden
    elemento.siblings('input').prop('checked', true);
    var fila = $(e).parent('td').parent('tr');
    //Oculto inputs
    fila.find(':input').prop("disabled", true);
    //Marco Fila
    fila.css("background-color", "rgb(160, 197, 232);");
    //Oculto img Hermanas nuevo fondo
    fila.siblings().children('td').children('span.sFondo').css("display", "none")
    //Por cada fila hermana que NO tenga checkeado respCorrecta
    fila.siblings().children('td').children('input.inRespCorrecta').each(function () {
        if (!this.checked) {
            //Fondo transparente
            $(this).parent('td').parent('tr').css("background-color", "transparent");
            //Muestro img Hermanas de fondo
            $(this).parent('td').siblings().children('a.aFondo').css("display", "block")
            //Muestro img RespCorrecta hermanas
            $(this).siblings('a.aRespCorrecta').css("display", "block")
            //Muestro inputs de hermanas
            $(this).prop("disabled", false);
            $(this).parent('td').siblings().find(':input').prop("disabled", false);
        };
    });

    //Desmarco hidden de hermanas
    fila.siblings().children('td').children('input.inFondo').prop('checked', false);
    backgroundName.val($('input:checked.inFondo').val());
}

function clickRespCorrecta(e) {
    var answerCorrectName = $('#AnswerCorrectName');
    answerCorrectName.val('');
    var elemento = $(e);
    //Oculto img Fondo
    elemento.parent().siblings().find('a.aFondo').css("display", "none");
    //Oculto img RespCorrecta
    elemento.css("display", "none");
    //Cargo img nueva RespCorrecta
    elemento.siblings('span').css("display", "block");
    //Marco hidden
    elemento.siblings('input').prop('checked', true);
    var fila = $(e).parent('td').parent('tr');
    //Oculto inputs
    fila.find(':input').prop("disabled", true);
    //Marco Fila
    fila.css("background-color", "rgb(160, 225, 160)");
    //Oculto img Hermanas nueva RespCorrecta
    fila.siblings().children('td').children('span.sRespCorrecta').css("display", "none")
    //Por cada fila hermana que NO tenga checkeado Fondo
    fila.siblings().children('td').children('input.inFondo').each(function () {
        if (!this.checked) {
            //Fondo transparente
            $(this).parent('td').parent('tr').css("background-color", "transparent");
            $(this).siblings('a.aFondo').css("display", "block")
            //Muestro img RespCorrecta hermanas
            $(this).parent('td').siblings().children('a.aRespCorrecta').css("display", "block")
            //Muestro inputs de hermanas
            $(this).prop("disabled", false);
            $(this).parent('td').siblings().find(':input').prop("disabled", false);
        }
    });

    //Desmarco hidden de hermanas
    fila.siblings().children('td').children('input.inRespCorrecta').prop('checked', false);
    answerCorrectName.val($('input:checked.inRespCorrecta').val());
}

function errorDuplicateName(name) {
    $.notify({
        icon: 'icon fa fa-ban',
        title: '<strong>¡Error!</strong>',
        message: 'No se puede cargar el archivo, ya que existe uno con el mismo nombre: "' + name + '"'
    }, {
        type: 'danger',
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<h4><span data-notify="icon"></span> {1}</h4>' +
            '{2}' +
            '</div>'
    });
};

function writeErrors(errorList) {
    for (var key in errorList) {
        // skip loop if the property is from prototype
        if (!errorList.hasOwnProperty(key)) continue;
        var obj = errorList[key];
        for (var prop in obj) {
            // skip loop if the property is from prototype
            if (!obj.hasOwnProperty(prop)) continue;
            var input = $("input[name='" + key + "']");
            if (input.length > 0) {
                input.removeClass("valid");
                input.addClass("input-validation-error");
            };

            var span = $("[data-valmsg-for='" + key + "']");
            if (span.length > 0) {
                span.html("<span for='" + key + "' class>" + obj[prop] + "</span>");
                span.removeClass("field-validation-valid");
                span.addClass("field-validation-error");
            };

            if (key == "AnswerCorrectName" || key == "BackgroundName") {
                var p = $("p[name='" + key + "']");
                p.html(obj[prop]);
            };
        };
    };
};