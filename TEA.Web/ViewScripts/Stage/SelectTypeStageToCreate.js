﻿$(function () {
    $("#newStage").click(function () {
        $("span.errorSelectUser").hide();
        $(this).addClass("active");
        $("#newSharedStage").removeClass("active");
        $("#NewStageWhitUser").val(true);
        var userId = $('#FilterUserId').val();
        if (userId != "" && userId != undefined && userId != null) {
            blockUIGeneric();
            var url = (window.location.origin + '/Stage/Create?userId=' + userId);
            window.location.href = url;
        }
        else {
            $("span.errorSelectUser").show();
        };
    });

    $("#newSharedStage").click(function () {
        blockUIGeneric();
        $(this).addClass("active");
        $("#newStage").removeClass("active");
        $("#NewStageWhitUser").val(false);
    });

    $(".select2").select2({
        selectionTitleAttribute: false
    });

    $('.select2-selection__rendered').hover(function () {
        $(this).removeAttr('title');
    });

    $('.select2-selection__rendered').click(function () {
        $("span.errorSelectUser").hide();
    });
});