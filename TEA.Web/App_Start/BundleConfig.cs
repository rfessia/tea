﻿namespace TEA.Web
{
    using System.Collections.Generic;
    using System.IO;
    using System.Web;
    using System.Web.Optimization;

    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Clear();
            bundles.ResetAll();
            BundleTable.EnableOptimizations = true;
            bundles.Add(new ScriptBundle("~/Bundles/JQuery").Include(
                "~/Scripts/plugins/jQuery/jquery-2.2.3.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/globalize.js",
                        "~/Scripts/globalize.culture.es-AR.js",
                        "~/Scripts/messages_es_AR.js"));

            bundles.Add(new ScriptBundle("~/bundles/blockui").Include(
                        "~/Scripts/jquery.blockUI.js"));

            bundles.Add(new ScriptBundle("~/Bundles/Bootstrap").Include(
                "~/Scripts/plugins/bootstrap/js/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/Bundles/Select2").Include(
                "~/Scripts/plugins/select2/select2.full.min.js"));

            bundles.Add(new ScriptBundle("~/Bundles/Input-mask").Include(
                "~/Scripts/plugins/input-mask/jquery.inputmask.js",
                "~/Scripts/plugins/input-mask/jquery.inputmask.date.extensions.js",
                "~/Scripts/plugins/input-mask/jquery.inputmask.extensions.js"));

            bundles.Add(new ScriptBundle("~/Bundles/Daterangepicker").Include(
                "~/Scripts/plugins/daterangepicker/daterangepicker.js"));

            bundles.Add(new ScriptBundle("~/Bundles/Datepicker").Include(
                "~/Scripts/plugins/datepicker/bootstrap-datepicker.js"));

            bundles.Add(new ScriptBundle("~/Bundles/Colorpicker").Include(
                "~/Scripts/plugins/colorpicker/bootstrap-colorpicker.min.js"));

            bundles.Add(new ScriptBundle("~/Bundles/Timepicker").Include(
                "~/Scripts/plugins/timepicker/bootstrap-timepicker.min.js"));

            bundles.Add(new ScriptBundle("~/Bundles/SlimScroll").Include(
                "~/Scripts/plugins/slimScroll/jquery.slimscroll.min.js"));

            bundles.Add(new ScriptBundle("~/Bundles/Icheck").Include(
                "~/Scripts/plugins/iCheck/icheck.min.js"));

            bundles.Add(new ScriptBundle("~/Bundles/Fastclick").Include(
                "~/Scripts/plugins/fastclick/fastclick.js"));

            bundles.Add(new ScriptBundle("~/Bundles/App").Include(
                "~/Scripts/dist/js/app.min.js"));

            bundles.Add(new ScriptBundle("~/Bundles/Demo").Include(
                "~/Scripts/dist/js/demo.js"));

            bundles.Add(new ScriptBundle("~/Bundles/Datatables").Include(
                "~/Scripts/plugins/datatables/jquery.dataTables.min.js",
                "~/Scripts/plugins/datatables/dataTables.bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/selectivity").Include(
                "~/Scripts/Selectivity/selectivity-full.js"));

            //ESTILO
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css"));
            //"~/Content/style.css",
            //"~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Daterangepicker/css").Include(
                "~/Content/plugins/daterangepicker/daterangepicker.css"));

            bundles.Add(new StyleBundle("~/Datepicker/css").Include(
                "~/Content/plugins/datepicker/datepicker3.css"));

            bundles.Add(new StyleBundle("~/ICheck/css").Include(
                "~/Content/plugins/iCheck/all.css"));

            bundles.Add(new StyleBundle("~/Colorpicker/css").Include(
                "~/Content/plugins/colorpicker/bootstrap-colorpicker.min.css"));

            bundles.Add(new StyleBundle("~/Timepicker/css").Include(
                "~/Content/plugins/timepicker/bootstrap-timepicker.min.css"));

            bundles.Add(new StyleBundle("~/Select2/css").Include(
                "~/Content/plugins/select2/select2.min.css"));

            bundles.Add(new StyleBundle("~/AdminLTE/css").Include(
                "~/Content/dist/css/AdminLTE.css"));

            bundles.Add(new StyleBundle("~/Skins/css").Include(
                "~/Content/dist/css/skins/_all-skins.min.css"));

            bundles.Add(new StyleBundle("~/DataTables/css").Include(
                "~/Content/plugins/datatables/dataTables.bootstrap.css"));

            bundles.Add(new StyleBundle("~/Content/selectivity").Include(
                "~/Content/selectivity/selectivity-full.css"));

            // Registro los scripts correspondientes a cada vista.
            foreach (var scriptBundle in GetScriptViewsBundleCollection())
            {
                bundles.Add(scriptBundle);
            }
        }

        /// <summary>
        /// Obtengo los bundles correspondientes a las scripts de las vistas.
        /// Formato nombre: '/ViewScripts/ControllerName/ViewName'.
        /// Ejemplo: '/ViewScripts/Home/Index'.
        /// </summary>
        /// <returns>Colección de bundles.</returns>
        private static List<Bundle> GetScriptViewsBundleCollection()
        {
            var scriptBundles = new List<Bundle>();

            // Recorro los directorios que hay dentro de ViewScripts
            // y genero los bundles de los archivos de scripts.
            foreach (var directory in Directory.GetDirectories(HttpContext.Current.Server.MapPath("/ViewScripts"), "*", SearchOption.AllDirectories))
            {
                foreach (var jsFile in Directory.GetFiles(directory, "*.js", SearchOption.AllDirectories))
                {
                    var fileInfo = new FileInfo(jsFile);
                    var path = string.Empty;
                    var parent = fileInfo.Directory;

                    while (parent.Name != "ViewScripts")
                    {
                        path = "/" + parent.Name + path;
                        parent = parent.Parent;
                    }

                    path = "~/ViewScripts" + path;

                    var file = string.Format("{0}/{1}", path, fileInfo.Name);
                    var bundleName = file.Remove(file.Length - 3, 3);

                    if (bundleName.EndsWith("/Index"))
                    {
                        bundleName = bundleName.Replace("/Index", "/_Index");
                    }

                    scriptBundles.Add(new ScriptBundle(bundleName).Include(file));
                }
            }

            return scriptBundles;
        }
    }
}
