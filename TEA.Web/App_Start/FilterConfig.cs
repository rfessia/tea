﻿namespace TEA.Web
{
    using System.Web.Mvc;
    using TEA.Web.Filters;

    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new CustomAuthenticationAttribute());
        }
    }
}
