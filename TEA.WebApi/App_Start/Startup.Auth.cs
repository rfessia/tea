﻿namespace TEA.WebApi
{
    using Contratos;
    using Microsoft.AspNet.Identity;
    using Microsoft.Owin;
    using Microsoft.Owin.Security.Cookies;
    using Microsoft.Owin.Security.OAuth;
    using Owin;
    using Providers;
    using System;
    using System.Web.Http;

    public partial class Startup
    {
        public IServiceUser ServiceUser;

        public Startup()
        {
        }

        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            app.Use<InvalidAuthenticationMiddleware>();
            //var container = new WindsorContainer();
            //var aServiceUser = new DependencyResolver(container).GetService(typeof(IServiceUser));

            ServiceUser = (IServiceUser)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(IServiceUser));

            // Configure the db context and user manager to use a single instance per request
            //app.CreatePerOwinContext(ApplicationDbContext.Create);
            //app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);


            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new ApplicationOAuthProvider(this.ServiceUser),
                AccessTokenExpireTimeSpan = TimeSpan.FromHours(6),
                AllowInsecureHttp = true,
                RefreshTokenProvider = new MyRefreshTokenProvider()
                //AuthenticationMode = AuthenticationMode.Active
            };

            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);
        }
    }
}