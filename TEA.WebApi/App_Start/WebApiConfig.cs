﻿namespace TEA.WebApi
{
    using Microsoft.Owin.Security.OAuth;
    using System.Web.Http;

    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "Api/{controller}/{id}",
                defaults: new { controller = "Home", id = RouteParameter.Optional }
            );

            // Enforce HTTPS
            //config.Filters.Add(new TEA.WebApi.Filters.RequireHttpsAttribute());
        }
    }
}
