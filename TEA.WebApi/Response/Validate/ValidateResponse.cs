﻿using System.Collections.Generic;

namespace TEA.WebApi.Response.Validate
{
    public class ValidateResponse
    {
        /// <summary>
        /// Indica si es valido
        /// </summary>
        public bool IsValid { get; set; }

        /// <summary>
        /// Errores
        /// </summary>
        public Dictionary<string, string> Errors { get; set; }
    }
}