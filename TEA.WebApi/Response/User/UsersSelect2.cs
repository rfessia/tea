﻿namespace TEA.WebApi.Response.User
{
    /// <summary>
    /// Usuarios en Formato Select2
    /// </summary>
    public class UsersSelect2
    {
        /// <summary>
        /// Id del Usuario para Select2
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Nombre del Usuario para Select2
        /// </summary>
        public string text { get; set; }
    }
}