﻿namespace TEA.WebApi.Response.User
{
    using System.Collections.Generic;
    using Validate;

    /// <summary>
    /// Response para todos los usuarios de un usuario profesional
    /// </summary>
    public class GetMyUsersResponse
    {
        /// <summary>
        /// Validación del response
        /// </summary>
        public ValidateResponse Validate { get; set; }
     
        /// <summary>
        /// Lista de Usuarios en Formato Select2
        /// </summary>
        public List<UsersSelect2> Users { get; set; }
    }
}