﻿namespace TEA.WebApi.Response.Home
{
    public class HomeResponse
    {
        public string Welcome { get; set; }
    }
}