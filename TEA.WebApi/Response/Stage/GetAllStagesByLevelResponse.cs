﻿namespace TEA.WebApi.Response.Stage
{
    public class GetAllStagesByLevelResponse
    {
        /// <summary>
        /// Id del nivel
        /// </summary>
        public int LevelId { get; set; }

        /// <summary>
        /// Id de la etapa
        /// </summary>
        public int StageId { get; set; }

        /// <summary>
        /// Estado de la etapa
        /// 0: Bloqueado
        /// 1: Activo
        /// </summary>
        public int State { get; set; }

        /// <summary>
        /// Etapa actual
        /// </summary>
        public bool CurrentStage { get; set; }
    }
}