﻿namespace TEA.WebApi.Response.Stage
{
    using System.Collections.Generic;
    using TEA.Entidades.Negocio.Respuestas;

    public class GetStagesWithAnswersResponse
    {
        /// <summary>
        /// Id de etapa
        /// </summary>
        public int StageId { get; set; }

        /// <summary>
        /// Id de nivel
        /// </summary>
        public int LevelId { get; set; }

        /// <summary>
        /// Indica si es último nivel
        /// </summary>
        public bool LevelLast { get; set; }

        /// <summary>
        /// Lista de respuestas
        /// </summary>
        public List<Answer> Answers { get; set; }

        /// <summary>
        /// Representa la última etapa de un nivel
        /// </summary>
        public bool Last { get; set; }
    }
}