﻿namespace TEA.WebApi.Response.Level
{
    public class GetAllLevelsByUserResponse
    {
        /// <summary>
        /// Id del Nivel
        /// </summary>
        public int LevelId { get; set; }

        /// <summary>
        /// Estado del nivel
        /// 0: Bloqueado
        /// 1: Activo
        /// </summary>
        public int State { get; set; }

        /// <summary>
        /// Nivel Actual
        /// </summary>
        public bool CurrentLevel { get; set; }

        /// <summary>
        /// Cantidad de etapas que tiene el nivel
        /// </summary>
        public int CountStages { get; set; }

        /// <summary>
        /// Cantidad de etapas finalizadas
        /// </summary>
        public int FinishedStages { get; set; }
    }
}