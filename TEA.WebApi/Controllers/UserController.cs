﻿namespace TEA.WebApi.Controllers
{
    using AutoMapper;
    using Contratos;
    using Entidades.Negocio.User;
    using Response.User;
    using Response.Validate;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http;

    public class UserController : ApiBaseController
    {
        private readonly IServiceUser ServiceUser;

        public UserController(IServiceUser ServiceUser)
        {
            this.ServiceUser = ServiceUser;
        }

        [Route("Api/User/GetMyUsers")]
        [HttpGet]
        public GetMyUsersResponse GetMyUsers(string userName, string password)
        {
            var response = new GetMyUsersResponse();
            var validate = new ValidateResponse() { IsValid = false };
            var user = ServiceUser.GetLoginUser(new GetLoginUserRequest() { UserName = userName, Password = password }).User;
            if (user != null)
            {
                var roleAdmin = user.Roles.Where(x => x.Name.Equals(UserRoles.Administrador)).FirstOrDefault();
                if (roleAdmin != null)
                {
                    var getUsersResponse = this.ServiceUser.GetUsers(new GetUsersRequest() { TopUserId = user.UserId });
                    response.Users = Mapper.Map<List<UsersSelect2>>(getUsersResponse.Users);
                    validate.IsValid = true;
                };
            };
            
            response.Validate = validate;
            return response;
        }

        [Route("Api/User/IsLoggedIn")]
        [HttpGet]
        [Authorize]
        public ValidateResponse IsLoggedIn()
        {
            var userId = UserCurrent.Id;
            var user = ServiceUser.GetUserById(new GetUserByIdRequest() { UserId = userId }).User;
            if (user != null)
            {
                var response = new ValidateResponse() { IsValid = true };
                return response;
            }
            else
            {
                var response = new ValidateResponse() { IsValid = false };
                return response;
            };
        }
    }
}