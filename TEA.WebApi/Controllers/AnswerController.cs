﻿namespace TEA.WebApi.Controllers
{
    using Contratos;
    using System.Web.Mvc;

    [Authorize]
    public class AnswerController :  ApiBaseController
    {
        public IServiceAnswer ServiceAnswer;

        public AnswerController(IServiceAnswer ServiceAnswer)
        {
            this.ServiceAnswer = ServiceAnswer;
        }

        //// GET: Level
        //[Route("Api/Answer/GetByLevel")]
        //public List<GetAllStagesByLevelResponse> GetByLevel(int levelId)
        //{
        //    var userIdentity = new CustomIdentity();
        //    var getStagesByUserAndLevelResponse = this.ServiceStage.GetStagesByUserAndLevel(new GetStagesByUserAndLevelRequest() { UserId = userIdentity.Id, LevelId = levelId });
        //    var model = Mapper.Map<List<GetAllStagesByLevelResponse>>(getStagesByUserAndLevelResponse.Stages);
        //    return model;
        //}
    }
}