﻿namespace TEA.WebApi.Controllers
{
    using Contratos;
    using Entidades.Negocio.Diagnostic;
    using System.Collections.Generic;
    using System.Web.Http;

    [Authorize]
    public class DiagnosticController : ApiBaseController
    {
        public IServiceDiagnostic ServiceDiagnostic;

        public DiagnosticController(IServiceDiagnostic ServiceDiagnostic)
        {
            this.ServiceDiagnostic = ServiceDiagnostic;
        }

        public IEnumerable<Diagnostic> Get(int id)
        {
            var getDiagnosticsResponse = this.ServiceDiagnostic.GetDiagnosticsByUser(new GetDiagnosticsRequest() { UserId = UserCurrent.Id });
            var diagnostic = getDiagnosticsResponse.Diagnostics;

            return diagnostic;
        }
    }
}
