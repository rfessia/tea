﻿namespace TEA.WebApi.Controllers
{
    using AutoMapper;
    using Contratos;
    using Entidades.Negocio.Level;
    using Entidades.Negocio.User;
    using Response.Level;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http;

    //[Authorize]
    public class LevelController : ApiBaseController
    {
        private readonly IServiceLevel ServiceLevel;

        public LevelController(IServiceLevel ServiceLevel)
        {
            this.ServiceLevel = ServiceLevel;
        }

        // GET: Level
        [Route("Api/Level/GetAll")]
        [Authorize(Roles = UserRoles.Administrador + "," + UserRoles.Usuario)]
        public List<GetAllLevelsByUserResponse> GetAll()
        {
            var userId = UserCurrent.Id;
            var isAdmin = UserCurrent.Roles.Where(x => x.Equals(UserRoles.Administrador)).FirstOrDefault();
            if (isAdmin != null && UserCurrent.UserId != null)
            {
                userId = UserCurrent.UserId.Value;
            };

            var getLevelsByUserResponse = this.ServiceLevel.GetLevelsByUser(new GetLevelsByUserRequest() { UserId = userId, FromApi = true });
            var model = Mapper.Map<List<GetAllLevelsByUserResponse>>(getLevelsByUserResponse.Levels);
            return model;
        }
    }
}