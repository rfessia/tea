﻿namespace TEA.WebApi.Controllers
{
    using AutoMapper;
    using Contratos;
    using Entidades.Negocio.Stage;
    using Entidades.Negocio.User;
    using Response.Stage;
    using Response.Validate;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web.Http;

    [Authorize]
    public class StageController : ApiBaseController
    {
        public IServiceStage ServiceStage;
        public IServiceAnswer ServiceAnswer;

        public StageController(IServiceStage ServiceStage, IServiceAnswer ServiceAnswer)
        {
            this.ServiceStage = ServiceStage;
            this.ServiceAnswer = ServiceAnswer;
        }

        [Route("Api/Stage/GetStagesWithAnswers")]
        [HttpGet]
        public List<GetStagesWithAnswersResponse> Get(int stageId, int levelId)
        {
            var userId = UserCurrent.Id;
            var isAdmin = UserCurrent.Roles.Where(x => x.Equals(UserRoles.Administrador)).FirstOrDefault();
            if (isAdmin != null && UserCurrent.UserId != null)
            {
                userId = UserCurrent.UserId.Value;
            };

            var getStagesWithAnswersResponse = this.ServiceStage.GetNextStagesWithAnswers(new GetNextStagesWithAnswersRequest() { UserId = userId, StageId = stageId, LevelId = levelId });
            var model = Mapper.Map<List<GetStagesWithAnswersResponse>>(getStagesWithAnswersResponse.Stages);
            return model;
        }

        [Route("Api/Stage/GetAllByLevel")]
        [HttpGet]
        public List<GetAllStagesByLevelResponse> GetAll(int levelId)
        {
            var userId = UserCurrent.Id;
            var isAdmin = UserCurrent.Roles.Where(x => x.Equals(UserRoles.Administrador)).FirstOrDefault();
            if (isAdmin != null && UserCurrent.UserId != null)
            {
                userId = UserCurrent.UserId.Value;
            };

            var getStagesByUserAndLevelResponse = this.ServiceStage.GetStagesByUserAndLevel(new GetStagesByUserAndLevelRequest() { UserId = userId, LevelId = levelId });
            var model = Mapper.Map<List<GetAllStagesByLevelResponse>>(getStagesByUserAndLevelResponse.Stages);
            return model;
        }

        [Route("Api/Stage/SaveCorrectStage")]
        [HttpGet]
        public ValidateResponse SaveCorrectStage(int stageId, int levelId, int numberOfAttempts, string myDate)
        {
            var userId = UserCurrent.Id;
            var isAdmin = UserCurrent.Roles.Where(x => x.Equals(UserRoles.Administrador)).FirstOrDefault();
            if (isAdmin != null && UserCurrent.UserId != null)
            {
                var response = new ValidateResponse() { IsValid = true };
                return response;
            }
            else
            {
                var finishDate = DateStringToDateTime(myDate);
                var saveCorrectStageResponse = this.ServiceStage.SaveCorrectStage(new SaveCorrectStageRequest() { StageId = stageId, LevelId = levelId, UserId = UserCurrent.Id, NumberOfAttempts = numberOfAttempts, FinishDate = finishDate });
                var model = Mapper.Map<ValidateResponse>(saveCorrectStageResponse.Validate);
                return model;
            }
        }

        private DateTime DateStringToDateTime(string myDate)
        {
            var result = DateTime.ParseExact(myDate, "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            return result;
        } 
    }
}