﻿namespace TEA.WebApi.Controllers
{
    using Infrastucture.Security;
    using System.Web.Http;

    public partial class ApiBaseController : ApiController
    {
        #region Properties

        private CustomIdentity userCurrent;
        public CustomIdentity UserCurrent { get { return userCurrent = new CustomIdentity(); } }

        #endregion

    }
}