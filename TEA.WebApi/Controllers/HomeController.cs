﻿namespace TEA.WebApi.Controllers
{
    using Response.Home;
    using System.Web.Http;
    //using System.Web.Mvc;

    //[Authorize]
    public class HomeController : ApiBaseController
    {
        [Route("Api")]
        [HttpGet]
        // GET: Home
        public HomeResponse Index()
        {
            return new HomeResponse() { Welcome = "Welcome to TEA Api" };
        }
    }
}