﻿namespace TEA.WebApi
{
    using Infrastucture.IoC;
    using System;
    using System.Linq;
    using System.Web;
    using System.Web.Http;
    using System.Web.Mvc;
    using TEA.WebApi.App_Start;

    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            // Configuración del contenedor de IOC.
            IoC.InitializeInstance();
            // Delegamos el manejo de instancias de los controllers al container de IoC.
            var controllerFactory = new WindsorControllerFactory(IoC.Instance);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);
        }

        protected void Application_BeginRequest()
        {
            if (Request.Headers.AllKeys.Contains("Origin") && Request.HttpMethod == "OPTIONS")
            {
                Response.Flush();
            }
        }
    }
}