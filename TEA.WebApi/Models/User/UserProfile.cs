﻿namespace TEA.WebApi.Models.User
{
    /// <summary>
    /// Datos del Usuario
    /// </summary>
    public class UserProfile
    {
        /// <summary>
        /// Si está autenticado
        /// </summary>
        public bool IsAuthenticated { get; set; }
        
        /// <summary>
        /// Username
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Roles del usuario 
        /// </summary>
        public string Roles { get; set; }

        /// <summary>
        /// Id del Usuario de Prueba
        /// </summary>
        public string UserId { get; set; }
    }
}