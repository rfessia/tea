﻿namespace TEA.WebApi.Models.Stage
{
    using System.Collections.Generic;
    using TEA.Entidades.Negocio.Respuestas;

    public class StagesModel
    {
        /// <summary>
        /// Respuestas
        /// </summary>
        public List<Answer> Answers { get; set; }

        /// <summary>
        /// Id de la etapa
        /// </summary>
        public int StageId { get; set; }

        /// <summary>
        /// Indica si es última etapa
        /// </summary>
        public bool Last { get; set; }
    }
}