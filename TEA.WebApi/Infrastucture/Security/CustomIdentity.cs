﻿namespace TEA.WebApi.Infrastucture.Security
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    public class CustomIdentity
    {
        public CustomIdentity() { }

        public string AuthenticationType = System.Security.Claims.ClaimsPrincipal.Current.Identity.AuthenticationType;
        public bool IsAuthenticated = System.Security.Claims.ClaimsPrincipal.Current.Identity.IsAuthenticated;
        public string Name = System.Security.Claims.ClaimsPrincipal.Current.Claims.Where(x => x.Type == ClaimTypes.Name).Select(x => x.Value).FirstOrDefault();
        public string UserName = System.Security.Claims.ClaimsPrincipal.Current.Claims.Where(x => x.Type == "UserName").Select(x => x.Value).FirstOrDefault();
        public int Id = System.Security.Claims.ClaimsPrincipal.Current.Claims.Where(x => x.Type == "Id").Select(x => int.Parse(x.Value)).FirstOrDefault();
        public int? UserId = System.Security.Claims.ClaimsPrincipal.Current.Claims.Where(x => x.Type == "UserId").Select(x => string.IsNullOrEmpty(x.Value) ? (int?)null : int.Parse(x.Value)).FirstOrDefault();
        public List<string> Roles = System.Security.Claims.ClaimsPrincipal.Current.Claims.Where(x => x.Type == ClaimTypes.Role).Select(x => x.Value).ToList();
    }
}