﻿namespace TEA.WebApi.Infrastucture.Mapping
{
    using AutoMapper;
    using entity = Entidades.Negocio;
    using response = Response;

    public class EntityToResponse : Profile
    {
        public override string ProfileName
        {
            get
            {
                return "EntityToResponse";
            }
        }

        protected override void Configure()
        {
            //User to GetMyUsersResponse
            base.CreateMap<entity.User.User, response.User.UsersSelect2>()
                .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.text, opt => opt.MapFrom(src => src.Name));

            //GetLevelByUser to GetAllLevelsByUserResponse
            base.CreateMap<entity.Level.GetLevelByUser, response.Level.GetAllLevelsByUserResponse>();

            //GetStageByUserAndLevel to GetAllStagesByLevelResponse
            base.CreateMap<entity.Stage.GetStageByUserAndLevel, response.Stage.GetAllStagesByLevelResponse>();

            //Stage to GetStagesWithAnswersResponse
            base.CreateMap<entity.Stage.Stage, response.Stage.GetStagesWithAnswersResponse>();

            //ValidateResponse to ValidateResponse
            base.CreateMap<entity.ValidateResponse, response.Validate.ValidateResponse>();
        }
    }
}
