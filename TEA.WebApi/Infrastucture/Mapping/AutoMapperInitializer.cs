﻿namespace TEA.WebApi.Infrastucture.Mapping
{
    using AutoMapper;
    using System.Collections.Generic;
    using System.Linq;

    public static class AutoMapperInitializer
    {
        public static void Initialize(IEnumerable<Profile> profiles)
        {
            Mapper.Initialize(config => AddProfiles(config, profiles));
        }

        private static void AddProfiles(IConfiguration configuration, IEnumerable<Profile> profiles)
        {
            profiles.ToList().ForEach(configuration.AddProfile);
        }
    }
}