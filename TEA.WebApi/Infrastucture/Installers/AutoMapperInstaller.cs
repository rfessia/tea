﻿namespace TEA.WebApi.Infrastucture.Installers
{
    using AutoMapper;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using Mapping;
    using TEA.Servicios.Infrastucture.Mapping;

    public static class AutoMapperInstaller
    {
        public static void RegisterProfiles(IWindsorContainer windsorContainer)
        {
            windsorContainer.Register(
                Types.FromAssemblyInThisApplication()
                    .BasedOn<Profile>()
                    .WithService.Base()
                    .Configure(c => c.Named(c.Implementation.FullName))
                    .LifestyleTransient());

            windsorContainer.Register(
                Types.FromAssemblyContaining<ModelToEntity>()
                    .BasedOn<Profile>()
                    .WithService.Base()
                    .Configure(c => c.Named(c.Implementation.FullName))
                    .LifestyleTransient());

            var profiles = windsorContainer.ResolveAll<Profile>();

            if (profiles.Length > 0)
            {
                AutoMapperInitializer.Initialize(profiles);
            }
        }
    }
}