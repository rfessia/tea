﻿namespace TEA.WebApi.Infrastucture.Installers
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Controllers;
    using Microsoft.Owin.Security.OAuth;
    using Providers;
    using System.Web.Http;
    using System.Web.Http.Controllers;
    using System.Web.Mvc;

    /// <summary>
    /// Registra todos los controllers en el container
    /// </summary>
    public class ControllersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            //// Registrar todos los controllers.
            container.Register(
                Classes
                    .FromAssemblyContaining<ApiBaseController>()
                    .BasedOn(typeof(IHttpController))
                    .Configure(
                        component =>
                            //component.LifestyleTransient()
                            component.LifestyleScoped()
                    )
            );

            //container.Register(
            //    Classes
            //        .FromAssemblyContaining<OAuthAuthorizationServerProvider>()
            //        .BasedOn(typeof(IOAuthAuthorizationServerProvider))
            //        .Configure(
            //            component =>
            //                component.LifestyleTransient()
            //                //component.LifestyleScoped()
            //        )
            //);
        }
    }
}