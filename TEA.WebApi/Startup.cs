﻿using Microsoft.Owin;
using Owin;
using System.Web.Http;

[assembly: OwinStartup("StartupApi", typeof(TEA.WebApi.Startup))]

namespace TEA.WebApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}