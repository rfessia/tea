﻿namespace TEA.WebApi.Providers
{
    using Contratos;
    using Entidades.Negocio.User;
    using Microsoft.Owin.Security;
    using Microsoft.Owin.Security.Cookies;
    using Microsoft.Owin.Security.OAuth;
    using Models.User;
    using System.Collections.Generic;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using System.Web.Script.Serialization;
    using System.Linq;

    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        public IServiceUser ServiceUser;

        public ApplicationOAuthProvider(IServiceUser serviceUser)
        {
            this.ServiceUser = serviceUser;
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // Should really do some validation here :)
            context.Validated();
            return base.ValidateClientAuthentication(context);
        }

        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var user = ServiceUser.GetLoginUser(new GetLoginUserRequest() { UserName = context.UserName, Password = context.Password }).User;
            if (user == null)
            {
                context.SetError("Autorization Error", "Usuario o contraseña incorrecta");
                context.Response.Headers.Add("AuthorizationResponse", new[] { "Failed" });
                return base.GrantResourceOwnerCredentials(context);
            };

            var data = context.Request.ReadFormAsync();
            var userIdKey = data.Result.Where(x => x.Key == "userId").Select(x => x.Value).FirstOrDefault();
            string userId = null;
            foreach (var role in user.Roles)
            {
                if (role.Name == UserRoles.Administrador)
                {
                    if (userIdKey != null && userIdKey.Count() > 0)
                    {
                        userId = userIdKey[0];
                        break;
                    }
                    else
                    {
                        context.SetError("IsAdmin", "");
                        context.Response.Headers.Add("IsAdmin", new[] { "Done" });
                        return base.GrantResourceOwnerCredentials(context);
                    };
                };
            };

            //Realizar Mapeo con Automapper
            var userProfile = new UserProfile()
            {
                IsAuthenticated = true,
                Name = user.Name,
                UserName = user.UserName,
                Roles = string.Join(",", user.Roles.Select(x => x.Name)),
                UserId = userId,
            };

            var claims = new List<Claim>();
            claims.Add(new Claim("Id", user.UserId.ToString()));
            claims.Add(new Claim(ClaimTypes.Name, user.Name));
            claims.Add(new Claim("UserName", user.UserName));
            foreach (var role in user.Roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role.Name));
            };

            if (!string.IsNullOrEmpty(userId))
            {
                claims.Add(new Claim("UserId", userId));
            };

            ClaimsIdentity oAuthIdentity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
            ClaimsIdentity cookiesIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationType);

            AuthenticationProperties properties = CreateProperties(userProfile);
            AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
            context.Validated(ticket);
            context.Request.Context.Authentication.SignIn(cookiesIdentity);

            return base.GrantResourceOwnerCredentials(context);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(UserProfile userProfile)
        {
            var userProfileSerializer = new JavaScriptSerializer().Serialize(userProfile);
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "user", userProfileSerializer },
            };
            return new AuthenticationProperties(data);
        }
    }
}