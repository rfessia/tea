﻿namespace TEA.WebApi.Providers
{
    using Microsoft.Owin;
    using System.Threading.Tasks;

    public class InvalidAuthenticationMiddleware : OwinMiddleware
    {
        public InvalidAuthenticationMiddleware(OwinMiddleware next)
            : base(next)
        {
        }

        public override async Task Invoke(IOwinContext context)
        {
            await Next.Invoke(context);

            if (context.Response.StatusCode == 400 && context.Response.Headers.ContainsKey("AuthorizationResponse"))
            {
                context.Response.Headers.Remove("AuthorizationResponse");
                context.Response.StatusCode = 401;
            }
            else if (context.Response.StatusCode == 400 && context.Response.Headers.ContainsKey("IsAdmin")) {
                context.Response.StatusCode = 200;
            };
        }
    }
}